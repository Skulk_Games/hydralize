#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime;
using System.Text;
using System.Reflection;
using System.Xml;

namespace Skulk.Hydralize
{
  /// <summary>
  /// The interface implemented to allow an object to be Hydralized.
  /// </summary>
  public interface IHydralize
  {
    /// <summary>
    /// Gets the data from the object and stores it in the <see cref="HydralizeInfo"/> object.
    /// </summary>
    /// <param name="info">The object that stores the information describing this object.</param>
    void GetObjectData(HydralizeInfo info);

    /// <summary>
    /// Loads the data into this object from the <see cref="HydralizeInfo"/> object.
    /// </summary>
    /// <param name="info">The object to load the data from.</param>
    void LoadObjectData(HydralizeInfo info);
  }

  /// <summary>
  /// The different ways the object data can be hydralized
  /// </summary>
  public enum HydralizeModes : byte
  {
    /// <summary>
    /// The object data will not be indexed and use member names as keys
    /// </summary>
    NORMAL = 0,

    /// <summary>
    /// The object data will not be indexed but keys will be indices
    /// </summary>
    LITE = 1,

    /// <summary>
    /// The object data will be indexed and use member names as keys
    /// </summary>
    INDEXED = 2,

    /// <summary>
    /// The object data actually represents a primitive value
    /// </summary>
    PRIMITIVE = 4,

    /// <summary>
    /// The object is actually a directly hydralized value and not an object
    /// </summary>
    DIRECT = 8,

    /// <summary>
    /// The object data describes a generic type
    /// </summary>
    GENERIC = 16,

    /// <summary>
    /// The object data will be indexed and use indices as keys
    /// </summary>
    LITE_INDEXED = 3,
  }

  /// <summary>
  /// An object that stores information describing the fields of another class.
  /// </summary>
  public class HydralizeInfo
  {
    /// <summary>
    /// The type index value that indicates a primitive type in an indexed object.
    /// </summary>
    public const int PRIMITIVE_TYPE_INDEX = -1;

    /// <summary>
    /// The mode bitmask indicating how the data is Hydralized
    /// </summary>
    private byte m_mode;

    /// <summary>
    /// The ID of the registered assembly for the object this data describes
    /// </summary>
    private byte m_assemblyId;

    /// <summary>
    /// The ID of the class within the assembly that this data describes
    /// </summary>
    private int m_typeIndex;

    /// <summary>
    /// The dictionary of entry values for this hydralized object
    /// </summary>
    private Dictionary<string, HydralizeData> m_objData = new Dictionary<string, HydralizeData>();

    /// <summary>
    /// The custom context in use on this instance for indexing objects
    /// </summary>
    /// <remarks>
    /// If null, the <see cref="Hydralizer.ms_assemblyContext"/> should be used
    /// </remarks>
    private IndexContext m_context;

    /// <summary>
    /// The array of objects describing the indexing information for the generic arguments of this type
    /// </summary>
    private IndexedGenericType[] m_genericArgs;

    /// <summary>
    /// Default constructor, mostly just used to instantiate by reflection
    /// </summary>
    public HydralizeInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this <see cref="HydralizeInfo"/> with default values and the specified index context
    /// </summary>
    /// <param name="context">The index context to use for object lookups</param>
    public HydralizeInfo(IndexContext context)
    {
      m_context = context;
    }

    /// <summary>
    /// Constructor for <see cref="HydralizeInfo"/>
    /// </summary>
    /// <param name="lite">Indicates if size should be minimized by referencing data by index instead of names</param>
    /// <param name="primitive">Indicates if this describes a primitive value</param>
    /// <param name="direct">Indicates if this info describes a single value instead of an object</param>
    /// <param name="context">The context to use if dehyrdalizing an indexed object</param>
    public HydralizeInfo(bool lite = false, bool primitive = false, bool direct = false, IndexContext context = null)
    {
      Lite = lite;
      Primitive = primitive;
      Direct = direct;
      m_context = context;
    }

    /// <summary>
    /// Constructs a <see cref="HydralizeInfo"/> class with the specified settings
    /// </summary>
    /// <param name="lite">Indicates if info should be Hydralized in lite mode</param>
    /// <param name="assemblyId">The index of the registered assembly the object is contained within</param>
    /// <param name="typeIndex">The index of the object's type within the registered assembly</param>
    /// <param name="direct">Indicates if this directly describes a value instead of an object</param>
    /// <param name="context">The index context to use in this object</param>
    public HydralizeInfo(bool lite, byte assemblyId, int typeIndex, bool direct = false, IndexContext context = null)
    {
      Lite = lite;
      m_assemblyId = assemblyId;
      m_typeIndex = typeIndex;
      Indexed = true;
      Direct = direct;
      m_context = context;
    }

    /// <summary>
    /// Initializes this <see cref="HydralizeInfo"/> to describe the specified type
    /// </summary>
    /// <param name="lite">Indicates if info should be Hydralized in lite mode</param>
    /// <param name="type">The type of the object this info will describe</param>
    /// <param name="context">The context to use when indexing</param>
    public HydralizeInfo(bool lite, Type type, IndexContext context)
    {
      Lite = lite;
      HydraTypeCodes typeCode = HydraType.DetermineType(type);
      if (HydraType.IsPrimitiveType(typeCode))
      {
        Primitive = true;
        Direct = true;
      }
      else if (typeCode != HydraTypeCodes.OBJECT || typeCode != HydraTypeCodes.HYDRA_OBJECT)
      {
        Direct = true;
      }

      if (context.Index(type, out m_assemblyId, out m_typeIndex))
      {
        Indexed = true;
      }

      if (type.IsGenericType)
      {
        Generic = true;
        SetGenericArgs(type);
      }
    }

    /// <summary>
    /// Indicates if the data will be hydralized in a compressed format or more verbose.
    /// </summary>
    public bool Lite
    {
      get => (m_mode & 0x01) == 1;
      set
      {
        if (value)
        {
          m_mode |= 0x01;
        }
        else
        {
          m_mode = (byte)(m_mode & ~(0x01));
        }
      }
    }

    /// <summary>
    /// Indicates if the data contains an assembly and type index identifying the class type of the object.
    /// </summary>
    public bool Indexed
    {
      get
      {
        return (m_mode & 0x02) != 0;
      }
      set
      {
        if (value)
        {
          m_mode |= 0x02;
        }
        else
        {
          m_mode = (byte)(m_mode & ~(0x02));
        }
      }
    }

    /// <summary>
    /// Indicates if the data describes a primitive value instead of an object.
    /// </summary>
    public bool Primitive
    {
      get
      {
        return (m_mode & 0x04) != 0;
      }
      set
      {
        if (value)
        {
          m_mode |= 0x04;
        }
        else
        {
          m_mode = (byte)(m_mode & ~(0x04));
        }
      }
    }

    /// <summary>
    /// Indicates this info describes a single direct value and not an object.
    /// </summary>
    public bool Direct
    {
      get
      {
        return (m_mode & 0x08) != 0;
      }
      set
      {
        if (value)
        {
          m_mode |= 0x08;
        }
        else
        {
          m_mode = (byte)(m_mode & ~(0x08));
        }
      }
    }

    /// <summary>
    /// Indicates this info describes a generic object.
    /// </summary>
    public bool Generic
    {
      get
      {
        return (m_mode & 0x10) != 0;
      }
      set
      {
        if (value)
        {
          m_mode |= 0x10;
        }
        else
        {
          m_mode = (byte)(m_mode & ~(0x10));
        }
      }
    }

    /// <summary>
    /// If indexed this indicates the Id of the assembly that contains the object this info describes
    /// </summary>
    public byte AssemblyId { get => m_assemblyId; set => m_assemblyId = value; }

    /// <summary>
    /// If indexed this indicates the Id of the class within the assembly that this object info describes
    /// </summary>
    public int TypeIndex { get => m_typeIndex; set => m_typeIndex = value; }

    /// <summary>
    /// If indexing this indicates the custom context to use, if null the default Hydralizer context will be used
    /// </summary>
    public IndexContext Context { get => m_context; }

    /// <summary>
    /// Contains an array of objects specifying the indexing information for the generic arguments of this object
    /// </summary>
    public IndexedGenericType[] GenericArgs { get => m_genericArgs; set => m_genericArgs = value; }

    /// <summary>
    /// Sets the generic args indexing on this object from the specified type
    /// </summary>
    /// <param name="type">The generic type this info describes</param>
    public void SetGenericArgs(Type type)
    {
      Type[] args = type.GetGenericArguments();
      if (args.Length > 0)
      {
        Generic = true;
        m_genericArgs = new IndexedGenericType[args.Length];
        for (int i = 0; i < args.Length; ++i)
        {
          m_genericArgs[i] = new IndexedGenericType(args[i], m_context);
        }
      }
    }

    /// <summary>
    /// Gets the generic type of the described object
    /// </summary>
    /// <remarks>
    /// The info must be indexed and the described object must be generic otherwise null is returned
    /// </remarks>
    /// <returns>The generic type of the described object if applicable, otherwise null</returns>
    public Type GetGenericType()
    {
      if (Indexed && Generic)
      {
        Type baseType = null;
        if (m_context != null)
        {
          baseType = m_context.Lookup(m_assemblyId, m_typeIndex);
        }
        else
        {
          baseType = Hydralizer.DefaultContext.Lookup(m_assemblyId, m_typeIndex);
        }

        if (baseType != null && m_genericArgs != null)
        {
          Type[] genericArgs = new Type[m_genericArgs.Length];
          for (int i = 0; i < genericArgs.Length; ++i)
          {
            genericArgs[i] = m_genericArgs[i].GetGenericType(m_context);
          }

          return baseType.MakeGenericType(genericArgs);
        }
      }

      return null;
    }

    /// <summary>
    /// Adds the value of the specified type to this hydralize information
    /// </summary>
    /// <param name="name">The key name associated with the data</param>
    /// <param name="val">The actual object value</param>
    /// <param name="type">The type of the value</param>
    public void AddInfo(string name, object val, Type type)
    {
      if (val == null)
      {
        if (!m_objData.ContainsKey(name))
        {
          HydraTypeCodes typeCode = HydraType.DetermineType(type);
          if (typeCode != HydraTypeCodes.INVALID)
          {
            typeCode |= HydraTypeCodes.NULL_FLAG;
            HydralizeData data = new HydralizeData(val, typeCode);
            m_objData.Add(name, data);
          }
          else
          {
            throw new Exception($"Invalid object type, couldn't add object of type: {type} to HydralizeInfo.");
          }
        }
      }
      else
      {
        AddInfo(name, val);
      }
    }

    /// <summary>
    /// Adds the object data with the associated name.
    /// </summary>
    /// <param name="name">The key name associated with the data, in lite mode this name is not used but is required to be unique here when adding info.</param>
    /// <param name="value">The object value to add.</param>
    public void AddInfo(string name, object value)
    {
      if (value == null)
      {
        throw new Exception($"{name} is null, can't Hydralize a null value.");
      }

      if (!m_objData.ContainsKey(name))
      {
        HydraTypeCodes typeCode = HydraType.DetermineType(value.GetType());
        switch (typeCode)
        {
          case HydraTypeCodes.HYDRA_OBJECT:
          {
            HydralizeInfo info = value as HydralizeInfo;
            if (info == null)
            {
              AddInfo(name, (IHydralize)value, Lite);
            }
            else
            {
              AddInfo(name, info);
            }
          }
          break;

          case HydraTypeCodes.OBJECT:
          {
            HydralizeInfo info = value as HydralizeInfo;
            if (info == null)
            {
              AddInfo(name, Hydralizer.SaveObjectInfo(value, Lite, Indexed, m_context));
            }
            else
            {
              AddInfo(name, info);
            }
          }
          break;

          case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
          {
            HydralizeInfo[] infos = value as HydralizeInfo[];
            if (infos == null)
            {
              AddInfo(name, (IHydralize[])value, Lite);
            }
            else
            {
              AddInfo(name, infos);
            }
          }
          break;

          case HydraTypeCodes.ARRAY_OBJECT:
          {
            HydralizeInfo[] infos = value as HydralizeInfo[];
            if (infos == null)
            {
              AddInfo(name, (object[])value);
            }
            else
            {
              AddInfo(name, infos);
            }
          }
          break;

          case HydraTypeCodes.INVALID:
          {
            throw new Exception("Invalid object type, couldn't add object to HydralizeInfo.");
          }

          default:
          {
            HydralizeData data = new HydralizeData(value, typeCode);
            m_objData.Add(name, data);
          }
          break;
        }
      }
    }

    /// <summary>
    /// Adds the byte value to this instance
    /// </summary>
    /// <param name="name">The key name of this value</param>
    /// <param name="value">The byte value</param>
    public void AddInfo(string name, byte value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a signed byte value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, sbyte value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a UInt16 value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, UInt16 value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a char value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, char value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a UInt32 value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, UInt32 value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a UInt64 value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, UInt64 value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a Int16 value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, Int16 value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a Int32 value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, Int32 value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a Int64 value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, Int64 value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a string value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, string value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a byte array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, byte[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a UInt16 array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, UInt16[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a char array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, char[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a UInt32 array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, UInt32[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a UInt64 array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, UInt64[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a Int16 array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, Int16[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a Int32 array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, Int32[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a Int64 value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, Int64[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a float value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, float value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a double value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, double value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds an enum value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="enumVal">The object value to store</param>
    public void AddInfo(string name, Enum enumVal)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(enumVal);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a bool value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, bool value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a string array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, string[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a float array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, float[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a double array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, double[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a bool array value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="value">The object value to store</param>
    public void AddInfo(string name, bool[] value)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(value);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a Hydralizeable object value to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="obj">The object value to store</param>
    /// <param name="lite">Indicates if the object should be stored in lite mode</param>
    public void AddInfo(string name, IHydralize obj, bool lite = false)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeInfo info;
        if (Indexed)
        {
          info = Hydralizer.CreateIndexedInfo(obj, lite, false, m_context);
        }
        else
        {
          info = new HydralizeInfo(lite, false, false, m_context);
        }

        obj.GetObjectData(info);
        HydralizeData data = new HydralizeData(info);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a <see cref="HydralizeInfo"/> object to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="objInfo">The object info class to store</param>
    public void AddInfo(string name, HydralizeInfo objInfo)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(objInfo);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds a Hydralizeable object array to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="objs">The object values to store</param>
    /// <param name="lite">Indicates whether the objects should be stored in lite mode</param>
    public void AddInfo(string name, IHydralize[] objs, bool lite = false)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeInfo[] infos = new HydralizeInfo[objs.Length];
        for (int i = 0; i < objs.Length; i++)
        {
          if (Indexed)
          {
            infos[i] = Hydralizer.CreateIndexedInfo(objs[i], lite, false, m_context);
          }
          else
          {
            infos[i] = new HydralizeInfo(lite);
          }

          objs[i].GetObjectData(infos[i]);
        }

        HydralizeData data = new HydralizeData(infos);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds the array of regular objects to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="objs">The array of objects to add</param>
    public void AddInfo(string name, object[] objs)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeInfo[] infos = new HydralizeInfo[objs.Length];
        for (int i = 0; i < objs.Length; ++i)
        {
          infos[i] = Hydralizer.SaveObjectInfo(objs[i], Lite, Indexed, m_context);
        }

        HydralizeData data = new HydralizeData(infos);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Adds an array of <see cref="HydralizeInfo"/> objects describing Hydralizeable objects to this object info
    /// </summary>
    /// <param name="name">The key name of the value</param>
    /// <param name="objInfos">The object value information to store</param>
    public void AddInfo(string name, HydralizeInfo[] objInfos)
    {
      if (!m_objData.ContainsKey(name))
      {
        HydralizeData data = new HydralizeData(objInfos);
        m_objData.Add(name, data);
      }
    }

    /// <summary>
    /// Uses information in this map to add the value to the <see cref="HydralizeInfo"/> object
    /// </summary>
    /// <param name="fullName">The full path name for the value to set</param>
    /// <param name="value">The value to set for the item</param>
    /// <param name="typeInfo">The object specifying type information for the value to add</param>
    /// <returns>True if the object was added to the info object, false otherwise</returns>
    public bool AddInfo(string fullName, object value, HydraTypeInfo typeInfo)
    {
      // Parse the path
      int pos = fullName.IndexOf('.');
      string member = null;
      string subPath = null;
      if (pos > 0)
      {
        member = fullName.Substring(0, pos);
        subPath = fullName.Substring(pos + 1);
      }
      else
      {
        member = fullName;
      }

      if (subPath == null)
      {
        // Create the data to add
        if (value != null && HydraType.DetermineType(value.GetType()) != typeInfo.TypeCode)
        {
          // Convert the value
          if (typeInfo.IsArray)
          {
            value = HydralizeData.ChangeArray(value as Array, HydraType.DetermineType(typeInfo.GetElementType().TypeCode));
          }
          else
          {
            value = Convert.ChangeType(value, HydraType.DetermineType(typeInfo.TypeCode));
          }
        }

        HydralizeData data = new HydralizeData(value, typeInfo.TypeCode);
        m_objData.Add(member, data);
        return true;
      }
      else
      {
        // Pass off to nested object
        if (m_objData.TryGetValue(member, out HydralizeData data))
        {
          if (data.HydraTypeCode == HydraTypeCodes.OBJECT || data.HydraTypeCode == HydraTypeCodes.HYDRA_OBJECT)
          {
            HydralizeInfo info = data.Data as HydralizeInfo;
            if (info != null)
            {
              return info.AddInfo(subPath, value, typeInfo);
            }
          }
        }
        else
        {
          // Can't add data for types we don't know about
          return false;
        }
      }

      return false;
    }

    /// <summary>
    /// Clears the added info keys and values.
    /// </summary>
    public void ClearInfo()
    {
      m_objData.Clear();
    }

    /// <summary>
    /// Gets the raw object associated with the specified key or index if in lite mode.
    /// </summary>
    /// <param name="name">The string key to use if not in lite mode.</param>
    /// <param name="index">The index of the object if in lite mode.</param>
    /// <returns>The object associated with the key or index if found, otherwise null.</returns>
    public object GetRaw(string name, int index)
    {
      if (Lite)
      {
        return GetRaw(index);
      }
      else
      {
        return GetRaw(name);
      }
    }

    /// <summary>
    /// Gets the raw object associated with the specified key.
    /// </summary>
    /// <param name="name">The object key string.</param>
    /// <returns>The object found, null otherwise.</returns>
    public object GetRaw(string name)
    {
      HydralizeData data;
      if (m_objData.TryGetValue(name, out data))
      {
        return data.Data;
      }

      return null;
    }

    /// <summary>
    /// Gets the raw object at the specified index.
    /// </summary>
    /// <param name="index">The index of the object to get.</param>
    /// <returns>The raw object found at the index, null otherwise.</returns>
    public object GetRaw(int index)
    {
      return GetRaw(index.ToString());
    }

    /// <summary>
    /// Gets the <see cref="HydraTypeCodes"/> of the value with the specified key name or index
    /// </summary>
    /// <param name="name">The name key to use if not in lite mode</param>
    /// <param name="index">The index key to use if in lite mode</param>
    /// <returns></returns>
    public HydraTypeCodes GetHydraType(string name, int index)
    {
      if (Lite)
      {
        return GetHydraType(index);
      }
      else
      {
        return GetHydraType(name);
      }
    }

    /// <summary>
    /// Gets the <see cref="HydraTypeCodes"/> of the value with the specified key name
    /// </summary>
    /// <param name="name">The key name of the value type to get</param>
    /// <returns>The <see cref="HydraTypeCodes"/> of the value</returns>
    public HydraTypeCodes GetHydraType(string name)
    {
      HydralizeData data;
      if (m_objData.TryGetValue(name, out data))
      {
        return data.HydraTypeCode;
      }

      return HydraTypeCodes.NULL_FLAG;
    }

    /// <summary>
    /// Gets the <see cref="HydraTypeCodes"/> of the value with the specified index
    /// </summary>
    /// <param name="index">The index of the value type to get</param>
    /// <returns>The <see cref="HydraTypeCodes"/> of the value</returns>
    public HydraTypeCodes GetHydraType(int index)
    {
      return GetHydraType(index.ToString());
    }

    /// <summary>
    /// Determines if the field with the specified key name or index is a null type
    /// </summary>
    /// <param name="name">The key name of the field data if the data is not in lite mode</param>
    /// <param name="index">The index of the field data if the data is in lite mode</param>
    /// <returns>True if the field data was found and is a null type, false otherwise</returns>
    public bool IsNullType(string name, int index)
    {
      if (Lite)
      {
        return IsNullType(index);
      }
      else
      {
        return IsNullType(name);
      }
    }

    /// <summary>
    /// Determines if the field with the specified key name is a null type
    /// </summary>
    /// <param name="name">The key name of the field data to check</param>
    /// <returns>True if the field data was found and is a null type, false otherwise</returns>
    public bool IsNullType(string name)
    {
      if (m_objData.TryGetValue(name, out HydralizeData data))
      {
        return data.IsNullType;
      }

      return false;
    }

    /// <summary>
    /// Determines if the field with the specified key index is a null type
    /// </summary>
    /// <param name="index">The key index of the field data to check</param>
    /// <returns>True if the field data was found and is a null type, false otherwise</returns>
    public bool IsNullType(int index)
    {
      return IsNullType(index.ToString());
    }

    /// <summary>
    /// Attempts to get the object data with the associated key name or index if in lite mode.
    /// </summary>
    /// <typeparam name="T">The type of the data to get.</typeparam>
    /// <param name="name">The string key for the object data, used if not in lite mode.</param>
    /// <param name="index">The index of the object data to get, only used if in lite mode.</param>
    /// <returns>The object data value or the default value of the type if not found.</returns>
    public T Get<T>(string name, int index)
    {
      if (Lite)
      {
        return Get<T>(index);
      }
      else
      {
        return Get<T>(name);
      }
    }

    /// <summary>
    /// Attempts to get the object data with the associated key name.
    /// </summary>
    /// <typeparam name="T">The type of the data to get.</typeparam>
    /// <param name="name">The string key for the object data.</param>
    /// <returns>The object data value or the default value of the type if not found.</returns>
    public T Get<T>(string name)
    {
      HydralizeData data;
      if (m_objData.TryGetValue(name, out data))
      {
        if (data.IsArrayType)
        {
          MethodInfo method = typeof(HydralizeInfo).GetMethod("GetArray", new Type[] { typeof(string) });
          MethodInfo genMethod = method.MakeGenericMethod(typeof(T).GetElementType());
          return (T)genMethod.Invoke(this, new object[] { name });
        }

        return (T)data.Data;
      }

      return default(T);
    }

    /// <summary>
    /// Attempts to get the object data for a data element in the lite format.
    /// </summary>
    /// <typeparam name="T">The type of the data to get.</typeparam>
    /// <param name="index">The index of the item to get, 0 based.</param>
    /// <returns>The object data value or the default value of the type if not found.</returns>
    public T Get<T>(int index)
    {
      return Get<T>(index.ToString());
    }

    /// <summary>
    /// Attempts to get the array data with the associated key name or index if in lite mode.
    /// </summary>
    /// <typeparam name="T">The element type of the array to get.</typeparam>
    /// <param name="name">The string key for the array data, used if not in lite mode.</param>
    /// <param name="index">The index of the array data to get, only used if in lite mode.</param>
    /// <returns>The array data value or null if not found.</returns>
    public T[] GetArray<T>(string name, int index)
    {
      if (Lite)
      {
        return GetArray<T>(index);
      }
      else
      {
        return GetArray<T>(name);
      }
    }

    /// <summary>
    /// Gets an array value from the Hydralize info
    /// </summary>
    /// <typeparam name="T">The element type of the array to get</typeparam>
    /// <param name="name">The string key of the item to get</param>
    /// <returns>The array data in this info or null if no data was found</returns>
    public T[] GetArray<T>(string name)
    {
      if (m_objData.TryGetValue(name, out HydralizeData data))
      {
        // Mono handles enums a little differently, which means a direct cast from the object to enum array fails.
        // This is redundant on Windows but needed to support mono on linux.
        if (typeof(T).IsEnum && !data.IsNullType)
        {
          Array array = (Array)data.Data;
          Array enums = Array.CreateInstance(typeof(T), array.Length);
          for (int i = 0; i < array.Length; ++i)
          {
            enums.SetValue((T)array.GetValue(i), i);
          }
          return (T[])enums;
        }
        else
        {
          if (data.Data != null && typeof(T[]) == data.Data.GetType())
          {
            return (T[])data.Data;
          }
          else if (data.IsArrayType && (data.HydraTypeCode == HydraTypeCodes.ARRAY_OBJECT || data.HydraTypeCode == HydraTypeCodes.ARRAY_HYDRA_OBJECT))
          {
            return GetObjArray<T>(name);
          }
          else
          {
            return (T[])data.Data;
          }
        }
      }
      return null;
    }

    /// <summary>
    /// Gets an array value from the Hydralize info
    /// </summary>
    /// <typeparam name="T">The element type of the array to get</typeparam>
    /// <param name="index">The index of the item to get, 0 based</param>
    /// <returns>The array data in this info or null if no data was found</returns>
    public T[] GetArray<T>(int index)
    {
      return GetArray<T>(index.ToString());
    }

    /// <summary>
    /// Attempts to get the object data for a Hydra object
    /// </summary>
    /// <typeparam name="T">The type of hydra object to get.</typeparam>
    /// <param name="name">The string key for the object data, used if not in lite mode.</param>
    /// <param name="index">The index for the object data to get, used if in lite mode.</param>
    /// <returns>An instance of the hydra object with the loaded values, or the default instance of the class if not found.</returns>
    public T GetObj<T>(string name, int index) where T : new()
    {
      if (Lite)
      {
        return GetObj<T>(index);
      }
      else
      {
        return GetObj<T>(name);
      }
    }

    /// <summary>
    /// Attempts to get the object data for a Hydra object
    /// </summary>
    /// <typeparam name="T">The type of hydra object to get.</typeparam>
    /// <param name="name">The string key for the object data.</param>
    /// <returns>An instance of the hydra object with the loaded values, or the default instance of the class if not found.</returns>
    public T GetObj<T>(string name) where T : new()
    {
      HydralizeData data;
      if (m_objData.TryGetValue(name, out data))
      {
        T result = new T();
        HydralizeInfo info = (HydralizeInfo)data.Data;
        object obj = result;
        Hydralizer.LoadObjectInfo(ref obj, info);
        return (T)obj;
      }

      return default(T);
    }

    /// <summary>
    /// Attempts to get the object data for a Hydra object in lite format.
    /// </summary>
    /// <typeparam name="T">The type of hydra object to get.</typeparam>
    /// <param name="index">The index for the object data.</param>
    /// <returns>An instance of the hydra object with the loaded values, or the default instance of the class if not found.</returns>
    public T GetObj<T>(int index) where T : new()
    {
      return GetObj<T>(index.ToString());
    }

    /// <summary>
    /// Attempts to get the object data for an array of Hydra objects.
    /// </summary>
    /// <typeparam name="T">The type of hydra object array to get.</typeparam>
    /// <param name="name">The string key for the object data array, used if not in lite mode.</param>
    /// <param name="index">The index for the object data array, used in lite mode.</param>
    /// <returns>An instance of the hydra object array with the loaded values, or null if not found.</returns>
    public T[] GetObjArray<T>(string name, int index)
    {
      if (Lite)
      {
        return GetObjArray<T>(index);
      }
      else
      {
        return GetObjArray<T>(name);
      }
    }

    /// <summary>
    /// Attempts to get the object data for an array of Hydra objects.
    /// </summary>
    /// <typeparam name="T">The type of hydra object array to get.</typeparam>
    /// <param name="name">The string key for the object data array.</param>
    /// <returns>An instance of the hydra object array with the loaded values, or null if not found.</returns>
    public T[] GetObjArray<T>(string name)
    {
      HydralizeData data;
      if (m_objData.TryGetValue(name, out data))
      {
        HydralizeInfo[] infos = (HydralizeInfo[])data.Data;
        if (infos != null)
        {
          T[] objs = new T[infos.Length];
          for (int i = 0; i < infos.Length; i++)
          {
            if (infos[i].Indexed)
            {
              objs[i] = (T)Hydralizer.Dehydralize(infos[i]);
            }
            else if (infos[i].Primitive || infos[i].Direct)
            {
              object raw = infos[i].GetRaw(0);

              if (raw != null && typeof(T).IsAssignableFrom(raw.GetType()))
              {
                objs[i] = (T)raw;
              }
            }
            else
            {
              ConstructorInfo constructorInfo = typeof(T).GetConstructor(Type.EmptyTypes);
              if (constructorInfo != null)
              {
                object tmpObj = constructorInfo.Invoke(null);
                objs[i] = (T)tmpObj;
                Hydralizer.LoadObjectInfo(ref tmpObj, infos[i]);
              }
              else
              {
                Console.WriteLine("Unable to construct object: {0}, no default constructor found.", typeof(T).FullName);
              }
            }
          }

          return objs;
        }
      }

      return null;
    }

    /// <summary>
    /// Attempts to get the object data for an array of Hydra objects.
    /// </summary>
    /// <typeparam name="T">The type of hydra object array to get.</typeparam>
    /// <param name="index">The index for the object data array.</param>
    /// <returns>An instance of the hydra object array with the loaded values, or null if not found.</returns>
    public T[] GetObjArray<T>(int index)
    {
      return GetObjArray<T>(index.ToString());
    }

    /// <summary>
    /// Converts the object data into a serialized stream of bytes in a format understood by Hydra
    /// </summary>
    /// <returns>The array of bytes that represents an objects data.</returns>
    public byte[] Hydralize()
    {
      int length = CalculateByteLength();
      byte[] buffer = new byte[length];
      int index = 0;
      Hydralizer.Put(buffer, m_mode, ref index);
      if (Indexed)
      {
        Hydralizer.Put(buffer, m_assemblyId, ref index, false);
        Hydralizer.Put(buffer, m_typeIndex, ref index, false);

        if (Generic)
        {
          Hydralizer.Put(buffer, m_genericArgs, ref index);
        }
      }

      Hydralizer.Put(buffer, (UInt32)length, ref index, !Lite);

      foreach (KeyValuePair<string, HydralizeData> pair in m_objData)
      {
        if (!Lite)
        {
          Hydralizer.Put(buffer, pair.Key, ref index);
        }

        Hydralizer.Put(buffer, pair.Value.Data, pair.Value.HydraTypeCode, ref index);
      }

      return buffer;
    }

    /// <summary>
    /// Parses an array of bytes back into object data if possible.
    /// </summary>
    /// <param name="buffer">The array of hydralized bytes containing an object's data.</param>
    /// <param name="offset">The offset in the buffer to begin parses object data.</param>
    public void Dehydralize(byte[] buffer, ref int offset)
    {
      m_objData.Clear();
      HydralizeData data = null;
      bool valid = true;
      int start = offset;

      if (buffer == null)
      {
        return;
      }

      data = Hydralizer.Get(buffer, ref offset, false, m_context);
      if (data != null)
      {
        if (data.HydraTypeCode == HydraTypeCodes.UINT8)
        {
          m_mode = (byte)data.Data;
        }
        else
        {
          valid = false;
          return;
        }

        if (Indexed)
        {
          m_assemblyId = Hydralizer.GetByte(buffer, ref offset);
          m_typeIndex = Hydralizer.GetInt32(buffer, ref offset);

          if (Generic)
          {
            m_genericArgs = Hydralizer.GetIndexedGenericArgs(buffer, ref offset);
          }
        }

        if (!Lite)
        {
          data = Hydralizer.Get(buffer, ref offset, Lite, m_context);
          if (data.HydraTypeCode != HydraTypeCodes.UINT32)
          {
            valid = false;
          }
        }

        if (valid)
        {
          UInt32 len = 0;
          if (!Lite && data != null)
          {
            len = (UInt32)data.Data;
          }
          else
          {
            len = Hydralizer.GetUInt32(buffer, ref offset);
          }

          if (buffer.Length >= len)
          {
            bool finished = false;
            string key;
            while (offset < start + len && !finished)
            {
              if (!Lite)
              {
                data = Hydralizer.Get(buffer, ref offset, Lite, m_context);
                if (data != null)
                {
                  if (data.HydraTypeCode == HydraTypeCodes.STRING)
                  {
                    key = (string)data.Data;
                    data = Hydralizer.Get(buffer, ref offset, Lite, m_context);
                    if (data != null)
                    {
                      m_objData.Add(key, data);
                    }
                    else
                    {
                      finished = true;
                    }
                  }
                  else
                  {
                    finished = true;
                  }
                }
                else
                {
                  finished = true;
                }
              }
              else
              {
                data = Hydralizer.Get(buffer, ref offset, Lite, m_context);
                if (data != null)
                {
                  m_objData.Add(m_objData.Count.ToString(), data);
                }
                else
                {
                  finished = true;
                }
              }
            }
          }
        }
      }
    }

    /// <summary>
    /// Converts the data in this info object to a JSON string
    /// </summary>
    /// <param name="level">The indent level of this info object</param>
    /// <returns>The JSON string representing this data</returns>
    public string ToJson(int level)
    {
      return ToJson().Write(level);
    }

    /// <summary>
    /// Stores the object data into a JSON object
    /// </summary>
    /// <returns>The JSON object created that stores this data</returns>
    public JSON.JsonObject ToJson()
    {
      JSON.JsonObject jsonObject = new JSON.JsonObject();
      jsonObject.Elements.Add("mode", new JSON.JsonElement() { Key = "mode", TokenValue = new JSON.JsonValue() { Value = m_mode } });
      
      if (Indexed)
      {
        jsonObject.Elements.Add("assembly", new JSON.JsonElement() { Key = "assembly", TokenValue = new JSON.JsonValue() { Value = m_assemblyId } });
        jsonObject.Elements.Add("type", new JSON.JsonElement() { Key = "type", TokenValue = new JSON.JsonValue() { Value = m_typeIndex } });
        if (Generic && (GenericArgs?.Length ?? 0) > 0)
        {
          JSON.JsonArray args = new JSON.JsonArray();
          for (int i = 0; i < GenericArgs.Length; ++i)
          {
            args.Values.Add(GenericArgs[i].ToJson());
          }
          jsonObject.Elements.Add("args", new JSON.JsonElement() { Key = "args", TokenValue = args });
        }
      }

      int index = 0;
      if (m_objData.Count > 0)
      {
        JSON.JsonArray dataArr = new JSON.JsonArray();
        foreach (KeyValuePair<string, HydralizeData> pair in m_objData)
        {
          string key = pair.Key;
          if (Lite)
          {
            key = index++.ToString();
          }

          dataArr.Values.Add(pair.Value.ToJson(key));
        }

        jsonObject.Elements.Add("data", new JSON.JsonElement() { Key = "data", TokenValue = dataArr });
      }

      return jsonObject;
    }

    /// <summary>
    /// Sets the values on this object to the values specified on the indicated JSON object
    /// </summary>
    /// <param name="jsonObject">The JSON object containing the values to set on this instance</param>
    public void FromJson(JSON.JsonObject jsonObject)
    {
      if (jsonObject.Elements.TryGetValue("mode", out JSON.JsonElement modeElement))
      {
        m_mode = (byte)(ulong)(modeElement.TokenValue as JSON.JsonValue).Value;
      }
      if (Indexed)
      {
        if (jsonObject.Elements.TryGetValue("assembly", out JSON.JsonElement assemblyElement))
        {
          m_assemblyId = (byte)(ulong)(assemblyElement.TokenValue as JSON.JsonValue).Value;
        }
        if (jsonObject.Elements.TryGetValue("type", out JSON.JsonElement typeElement))
        {
          m_typeIndex = (int)(ulong)(typeElement.TokenValue as JSON.JsonValue).Value;
        }
        if (Generic)
        {
          if (jsonObject.Elements.TryGetValue("args", out JSON.JsonElement argsElement))
          {
            JSON.JsonArray jsonArray = argsElement.TokenValue as JSON.JsonArray;
            GenericArgs = new IndexedGenericType[jsonArray.Values.Count];
            for (int i = 0; i < jsonArray.Values.Count; ++i)
            {
              GenericArgs[i] = new IndexedGenericType();
              GenericArgs[i].FromJson(jsonArray.Values[i] as JSON.JsonObject);
            }
          }
        }
      }

      if (jsonObject.Elements.TryGetValue("data", out JSON.JsonElement dataElement))
      {
        JSON.JsonArray dataArray = dataElement.TokenValue as JSON.JsonArray;
        for (int i = 0; i < dataArray.Values.Count; ++i)
        {
          HydralizeData data = new HydralizeData();
          string name = data.FromJson(dataArray.Values[i] as JSON.JsonObject);
          m_objData.Add(name, data);
        }
      }
    }

    /// <summary>
    /// Creates an XML string describing the information in this <see cref="HydralizeInfo"/> object
    /// </summary>
    /// <param name="level">The level of this tag, used to indicate indentation</param>
    /// <returns>An XML string containing the information in this object</returns>
    public string ToXml(int level = 0)
    {
      StringBuilder indent = new StringBuilder();
      StringBuilder tag = new StringBuilder();

      for (int i = 0; i < level; ++i)
      {
        indent.Append("  ");
      }

      if (Indexed)
      {
        tag.AppendFormat("{0}<HydralizeInfo mode='{1}' assembly='{2}' type='{3}'>\n", indent.ToString(), m_mode, m_assemblyId, m_typeIndex);
        if (Generic && GenericArgs != null)
        {
          for (int i = 0; i < GenericArgs.Length; ++i)
          {
            tag.Append(GenericArgs[i].ToXml(level + 1));
          }
        }
      }
      else
      {
        tag.AppendFormat("{0}<HydralizeInfo mode='{1}'>\n", indent.ToString(), m_mode);
      }

      int index = 0;
      foreach (KeyValuePair<string, HydralizeData> pair in m_objData)
      {
        string key = pair.Key;
        if (Lite)
        {
          key = index++.ToString();
        }

        tag.Append(pair.Value.ToXml(key, level + 1));
      }

      tag.AppendFormat("{0}</HydralizeInfo>\n", indent.ToString());
      return tag.ToString();
    }

    /// <summary>
    /// Sets the values in this object from the specified XML string
    /// </summary>
    /// <param name="xml">The XML string to parse data from</param>
    public void FromXml(string xml)
    {
      m_objData.Clear();
      XmlDocument doc = new XmlDocument();
      doc.LoadXml(xml);
      XmlNode node = doc.SelectSingleNode("/HydralizeInfo");
      FromXml(node);
    }

    /// <summary>
    /// Sets the values in this object from the specified XML node
    /// </summary>
    /// <param name="node">The XML document node to parse data from</param>
    public void FromXml(XmlNode node)
    {
      if (node == null)
      {
        return;
      }

      if (byte.TryParse(node.Attributes["mode"].Value, out m_mode))
      {
        if (Indexed)
        {
          m_assemblyId = byte.Parse(node.Attributes["assembly"].Value);
          m_typeIndex = int.Parse(node.Attributes["type"].Value);
          if (Generic)
          {
            var collection = node.SelectNodes("./IndexedArg");
            if (collection.Count > 0)
            {
              GenericArgs = new IndexedGenericType[collection.Count];
              for (int i = 0; i < GenericArgs.Length; ++i)
              {
                GenericArgs[i] = new IndexedGenericType();
                GenericArgs[i].FromXml(collection[i]);
              }
            }
          }
        }
      }

      foreach (XmlNode dataNode in node.ChildNodes)
      {
        if (dataNode.Name == "Data")
        {
          HydralizeData data = new HydralizeData(dataNode, m_context);
          XmlAttribute nameAttribute = dataNode.Attributes["name"];
          m_objData.Add(nameAttribute.Value, data);
        }
      }
    }

    /// <summary>
    /// Gets the fields of this object as an array of key and HydralizeData pairs
    /// </summary>
    /// <returns>The key and HydralizeData object for each field in this object</returns>
    public KeyValuePair<string, HydralizeData>[] GetFields()
    {
      return m_objData.ToArray();
    }

    private int CalculateByteLength()
    {
      // The lite flag and its type take up 2 bytes, object length takes 4 and may or may not have a type depending on mode.
      int length = sizeof(UInt32) + sizeof(byte) * 2;
      if (Indexed)
      {
        // assembly ID (byte) and typeIndex (int32)
        length += 5;

        if (Generic)
        {
          // Add generic type indexing data
          length += CalculateGenericArgsByteLength();
        }
      }

      if (!Lite)
      {
        length++;
      }

      foreach (KeyValuePair<string, HydralizeData> pair in m_objData)
      {
        if (!Lite)
        {
          // Typecode(byte) length(uint32) string for key
          length += sizeof(byte) + sizeof(UInt32) + pair.Key.Length;
        }

        // Typecode(byte) for data
        length += sizeof(byte);
        length += CalculateTypeLength(pair.Value);
      }

      return length;
    }

    private int CalculateTypeLength(HydralizeData hydraData)
    {
      int length = 0;

      switch (hydraData.HydraTypeCode)
      {
        case HydraTypeCodes.INT8:
        case HydraTypeCodes.UINT8:
          length += sizeof(byte);
          break;

        case HydraTypeCodes.UINT16:
        case HydraTypeCodes.INT16:
        case HydraTypeCodes.CHAR:
          length += 2;
          break;

        case HydraTypeCodes.UINT32:
        case HydraTypeCodes.INT32:
          length += 4;
          break;

        case HydraTypeCodes.UINT64:
        case HydraTypeCodes.INT64:
          length += 8;
          break;

        case HydraTypeCodes.STRING:
          length += sizeof(UInt32) + ((string)(hydraData.Data)).Length;
          break;

        case HydraTypeCodes.BOOL:
          length++;
          break;

        case HydraTypeCodes.FLOAT:
          length += sizeof(float);
          break;

        case HydraTypeCodes.DOUBLE:
          length += sizeof(double);
          break;

        case HydraTypeCodes.ENUM:
        {
          // ENUM isn't ever returned by System.TypeCode they always return underlying types and so
          // HydraTypeCodes works the same way and this typecode value isn't used for storage
          break;
        }

        case HydraTypeCodes.HYDRA_OBJECT:
        case HydraTypeCodes.OBJECT:
        {
          length += ((HydralizeInfo)hydraData.Data).CalculateByteLength();
          break;
        }

        case HydraTypeCodes.DATETIME:
        {
          length += sizeof(Int64);
          break;
        }

        case HydraTypeCodes.ARRAY_INT8:
        case HydraTypeCodes.ARRAY_UINT8:
          length += sizeof(UInt32) + ((byte[])(hydraData.Data)).Length;
          break;

        case HydraTypeCodes.ARRAY_INT16:
        case HydraTypeCodes.ARRAY_UINT16:
          length += sizeof(UInt32) + ((UInt16[])(hydraData.Data)).Length * 2;
          break;

        case HydraTypeCodes.ARRAY_CHAR:
          length += sizeof(UInt32) + ((char[])(hydraData.Data)).Length * 2;
          break;

        case HydraTypeCodes.ARRAY_INT32:
        case HydraTypeCodes.ARRAY_UINT32:
          length += sizeof(UInt32) + ((UInt32[])(hydraData.Data)).Length * 4;
          break;

        case HydraTypeCodes.ARRAY_UINT64:
        case HydraTypeCodes.ARRAY_INT64:
          length += sizeof(UInt32) + ((UInt64[])(hydraData.Data)).Length * 8;
          break;

        case HydraTypeCodes.ARRAY_STRING:
        {
          length += sizeof(UInt32);
          string[] data = (string[])hydraData.Data;
          foreach (string str in data)
          {
            length += sizeof(UInt32) + (str?.Length ?? 0);
          }
        }
        break;

        case HydraTypeCodes.ARRAY_BOOL:
        {
          length += sizeof(UInt32) + ((bool[])hydraData.Data).Length;
          break;
        }

        case HydraTypeCodes.ARRAY_FLOAT:
        {
          length += sizeof(UInt32) + ((float[])hydraData.Data).Length * sizeof(float);
          break;
        }

        case HydraTypeCodes.ARRAY_DOUBLE:
        {
          length += sizeof(UInt32) + ((double[])hydraData.Data).Length * sizeof(double);
          break;
        }

        case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
        case HydraTypeCodes.ARRAY_OBJECT:
        {
          length += sizeof(UInt32);
          HydralizeInfo[] infos = (HydralizeInfo[])hydraData.Data;
          for (int i = 0; i < infos.Length; i++)
          {
            length += infos[i].CalculateByteLength();
          }
          break;
        }

        case HydraTypeCodes.ARRAY_ENUM:
        {
          // Currently only used as a convenient flag type, not actually possible to get this code as a System typecode for an enum
          // returns based on the underlying type. HydraTypeCodes thus follow the same pattern and always work with the underlying type
          break;
        }

        case HydraTypeCodes.ARRAY_DATETIME:
        {
          length += sizeof(UInt32);
          DateTime[] dtArr = (DateTime[])hydraData.Data;
          length += dtArr.Length * sizeof(Int64);
          break;
        }
      }

      return length;
    }

    /// <summary>
    /// Calculates the number of bytes the generic args will take when serialized
    /// </summary>
    /// <returns>The number of bytes the generic args will use when serialized</returns>
    private int CalculateGenericArgsByteLength()
    {
      if (m_genericArgs == null)
      {
        return 0;
      }
      else
      {
        int sum = 1; // length byte
        for (int i = 0; i < m_genericArgs.Length; ++i)
        {
          sum += m_genericArgs[i].CalculateByteSize();
        }

        return sum;
      }
    }
  }
}
