#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Describes generic Hydralize data used in the <see cref="HydralizeInfo"/> class.
  /// </summary>
  public class HydralizeData
  {
    private HydraTypeCodes m_type;
    private object m_data;

    /// <summary>
    /// The data value
    /// </summary>
    public object Data { get => m_data; set => m_data = value; }

    /// <summary>
    /// The type of the data
    /// </summary>
    public HydraTypeCodes HydraTypeCode { get => m_type; set => m_type = value; }

    /// <summary>
    /// Indicates if the <see cref="HydraTypeCodes"/> of this data indicates the value is null
    /// </summary>
    public bool IsNullType { get => (HydraTypeCode & HydraTypeCodes.NULL_FLAG) == HydraTypeCodes.NULL_FLAG; }

    /// <summary>
    /// Indicates if the data type is an array
    /// </summary>
    public bool IsArrayType { get => HydraType.FromArrayType(m_type) != HydraTypeCodes.INVALID; }

    /// <summary>
    /// Default constructor.
    /// </summary>
    public HydralizeData()
    {
      // Empty
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with the specified value and type
    /// </summary>
    /// <param name="data">The value object</param>
    /// <param name="type">The type of the value</param>
    public HydralizeData(object data, HydraTypeCodes type)
    {
      m_type = type;
      m_data = data;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with the data specified in an XML node
    /// </summary>
    /// <param name="xml">The xml string for the data node to parse</param>
    /// <param name="context">The context to use when indexing objects</param>
    public HydralizeData(XmlNode xml, IndexContext context = null)
    {
      XmlAttribute typeAttribute = xml.Attributes["type"];
      if (Enum.TryParse(typeAttribute.Value, out m_type))
      {
        FromXml(xml, context);
      }
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with byte data
    /// </summary>
    /// <param name="data">The byte value to store</param>
    public HydralizeData(byte data)
    {
      m_data = data;
      m_type = HydraTypeCodes.UINT8;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with signed byte data
    /// </summary>
    /// <param name="data">The signed byte value to store</param>
    public HydralizeData(sbyte data)
    {
      m_data = data;
      m_type = HydraTypeCodes.INT8;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with UInt16 data
    /// </summary>
    /// <param name="data">The UInt16 value to store</param>
    public HydralizeData(UInt16 data)
    {
      m_data = data;
      m_type = HydraTypeCodes.UINT16;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with char data
    /// </summary>
    /// <param name="data">The char value to store</param>
    public HydralizeData(char data)
    {
      m_data = data;
      m_type = HydraTypeCodes.CHAR;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with UInt32 data
    /// </summary>
    /// <param name="data">The UInt32 value to store</param>
    public HydralizeData(UInt32 data)
    {
      m_data = data;
      m_type = HydraTypeCodes.UINT32;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with UInt64 data
    /// </summary>
    /// <param name="data">The UInt64 value to store</param>
    public HydralizeData(UInt64 data)
    {
      m_data = data;
      m_type = HydraTypeCodes.UINT64;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with Int16 data
    /// </summary>
    /// <param name="data">The Int16 value to store</param>
    public HydralizeData(Int16 data)
    {
      m_data = data;
      m_type = HydraTypeCodes.INT16;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with Int32 data
    /// </summary>
    /// <param name="data">The Int32 value to store</param>
    public HydralizeData(Int32 data)
    {
      m_data = data;
      m_type = HydraTypeCodes.INT32;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with Int64 data
    /// </summary>
    /// <param name="data">The Int64 value to store</param>
    public HydralizeData(Int64 data)
    {
      m_data = data;
      m_type = HydraTypeCodes.INT64;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with string data
    /// </summary>
    /// <param name="data">The string value to store</param>
    public HydralizeData(string data)
    {
      m_data = data;
      m_type = HydraTypeCodes.STRING;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with Enum data
    /// </summary>
    /// <param name="data">The Enum value to store</param>
    public HydralizeData(Enum data)
    {
      Type enumType = data.GetType().GetEnumUnderlyingType();
      HydraTypeCodes enumTypeCode = HydraType.DetermineType(enumType);
      m_data = data;
      m_type = enumTypeCode;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with byte array data
    /// </summary>
    /// <param name="data">The byte array value to store</param>
    public HydralizeData(byte[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_UINT8;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with UInt16 array data
    /// </summary>
    /// <param name="data">The UInt16 array value to store</param>
    public HydralizeData(UInt16[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_UINT16;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with char array data
    /// </summary>
    /// <param name="data">The char array value to store</param>
    public HydralizeData(char[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_CHAR;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with UInt32 array data
    /// </summary>
    /// <param name="data">The UInt32 array value to store</param>
    public HydralizeData(UInt32[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_UINT32;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with UInt64 array data
    /// </summary>
    /// <param name="data">The UInt64 array value to store</param>
    public HydralizeData(UInt64[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_UINT64;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with signed byte array data
    /// </summary>
    /// <param name="data">The signed byte array value to store</param>
    public HydralizeData(sbyte[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_INT8;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with Int16 array data
    /// </summary>
    /// <param name="data">The Int16 array value to store</param>
    public HydralizeData(Int16[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_INT16;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with Int32 array data
    /// </summary>
    /// <param name="data">The Int32 array value to store</param>
    public HydralizeData(Int32[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_INT32;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with Int64 array data
    /// </summary>
    /// <param name="data">The Int64 array value to store</param>
    public HydralizeData(Int64[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_INT64;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with float data
    /// </summary>
    /// <param name="data">The float value to store</param>
    public HydralizeData(float data)
    {
      m_data = data;
      m_type = HydraTypeCodes.FLOAT;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with double data
    /// </summary>
    /// <param name="data">The double value to store</param>
    public HydralizeData(double data)
    {
      m_data = data;
      m_type = HydraTypeCodes.DOUBLE;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with Hydralizeable object data
    /// </summary>
    /// <param name="data">The info describing a Hydralizeable object to store</param>
    public HydralizeData(HydralizeInfo data)
    {
      m_data = data;
      m_type = HydraTypeCodes.HYDRA_OBJECT;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with a sub HydralizeData object
    /// </summary>
    /// <param name="data">The HydralizeData to store</param>
    /// <param name="typeCode">The type code to set on this data</param>
    public HydralizeData(HydralizeData data, HydraTypeCodes typeCode)
    {
      m_data = data;
      m_type = typeCode;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with bool data
    /// </summary>
    /// <param name="data">The bool value to store</param>
    public HydralizeData(bool data)
    {
      m_data = data;
      m_type = HydraTypeCodes.BOOL;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with string array data
    /// </summary>
    /// <param name="data">The string array value to store</param>
    public HydralizeData(string[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_STRING;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with bool array data
    /// </summary>
    /// <param name="data">The bool array value to store</param>
    public HydralizeData(bool[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_BOOL;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with float array data
    /// </summary>
    /// <param name="data">The float array value to store</param>
    public HydralizeData(float[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_FLOAT;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with double array data
    /// </summary>
    /// <param name="data">The double array value to store</param>
    public HydralizeData(double[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_DOUBLE;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with Hydralizeable object array data
    /// </summary>
    /// <param name="data">The info objects describing the array of Hydralizeable objects to store</param>
    public HydralizeData(HydralizeInfo[] data)
    {
      m_data = data;
      m_type = HydraTypeCodes.ARRAY_HYDRA_OBJECT;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with a datetime value
    /// </summary>
    /// <param name="dt">The datetime value to store</param>
    public HydralizeData(DateTime dt)
    {
      m_data = dt;
      m_type = HydraTypeCodes.DATETIME;
    }

    /// <summary>
    /// Initializes an instance of the <see cref="HydralizeData"/> class with an array of datetime values
    /// </summary>
    /// <param name="dts">The array of datetime values to store</param>
    public HydralizeData(DateTime[] dts)
    {
      m_data = dts;
      m_type = HydraTypeCodes.ARRAY_DATETIME;
    }

    /// <summary>
    /// Stores the data in the object as data in a JSON object
    /// </summary>
    /// <param name="name">The name of this data item</param>
    /// <returns>The JSON object created that holds this data</returns>
    public JSON.JsonObject ToJson(string name)
    {
      JSON.JsonObject jsonObj = new JSON.JsonObject();
      jsonObj.Elements.Add("name", new JSON.JsonElement() { Key = "name", TokenValue = new JSON.JsonValue() { Value = name } });
      jsonObj.Elements.Add("type", new JSON.JsonElement() { Key = "type", TokenValue = new JSON.JsonValue() { Value = m_type } });

      if (m_data != null && (m_data.GetType().IsEnum || (m_data.GetType().IsArray && m_data.GetType().GetElementType().IsEnum)))
      {
        Type enumType = null;
        if (m_data.GetType().IsArray)
        {
          enumType = m_data.GetType().GetElementType();
        }
        else
        {
          enumType = m_data.GetType();
        }
        string enumName = String.Format("{0}, {1}", enumType.FullName, enumType.Assembly.GetName().Name);


        jsonObj.Elements.Add("enum", new JSON.JsonElement() { Key = "enum", TokenValue = new JSON.JsonValue() { Value = enumName } });
      }

      if (IsArrayType)
      {
        JSON.JsonArray jsonArr = new JSON.JsonArray();
        switch (m_type)
        {
          case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
          case HydraTypeCodes.ARRAY_OBJECT:
          {
            HydralizeInfo[] infos = m_data as HydralizeInfo[];
            if (infos != null)
            {
              for (int i = 0; i < infos.Length; ++i)
              {
                jsonArr.Values.Add(infos[i].ToJson());
              }
            }
            break;
          }

          default:
          {
            Array arr = m_data as Array;
            for (int i = 0; i < arr.Length; ++i)
            {
              jsonArr.Values.Add(new JSON.JsonValue() { Value = arr.GetValue(i) });
            }
            break;
          }
        }

        jsonObj.Elements.Add("data", new JSON.JsonElement() { Key = "data", TokenValue = jsonArr });
      }
      else
      {
        switch (m_type)
        {
          case HydraTypeCodes.OBJECT:
          case HydraTypeCodes.HYDRA_OBJECT:
          {
            HydralizeInfo info = m_data as HydralizeInfo;
            if (info != null)
            {
              jsonObj.Elements.Add("data", new JSON.JsonElement() { Key = "data", TokenValue = info.ToJson() });
            }
            break;
          }

          default:
          {
            jsonObj.Elements.Add("data", new JSON.JsonElement() { Key = "data", TokenValue = new JSON.JsonValue() { Value = m_data } });
            break;
          }
        }
      }

      return jsonObj;
    }

    /// <summary>
    /// Sets the values on this instance to the values specified in the indicated JSON object
    /// </summary>
    /// <param name="jsonObject">The JSON object specifying the values for this data instance</param>
    /// <returns>The name of this data item taken from the specified JSON object</returns>
    public string FromJson(JSON.JsonObject jsonObject)
    {
      string name = null;
      if (jsonObject.Elements.TryGetValue("name", out JSON.JsonElement nameElement))
      {
        name = (string)(nameElement.TokenValue as JSON.JsonValue).Value;
      }
      if (jsonObject.Elements.TryGetValue("type", out JSON.JsonElement typeElement))
      {
        object typeObj = (typeElement.TokenValue as JSON.JsonValue).Value;
        switch (Type.GetTypeCode(typeObj.GetType()))
        {
          case TypeCode.String:
          {
            Enum.TryParse<HydraTypeCodes>(typeObj as string, false, out m_type);
            break;
          }

          case TypeCode.UInt64:
          {
            m_type = (HydraTypeCodes)(ulong)typeObj;
            break;
          }
        }
      }
      string enumTypeName = null;
      if (jsonObject.Elements.TryGetValue("enum", out JSON.JsonElement enumElement))
      {
        enumTypeName = (string)(enumElement.TokenValue as JSON.JsonValue).Value;
      }
      if (jsonObject.Elements.TryGetValue("data", out JSON.JsonElement dataElement))
      {
        switch (dataElement.TokenValue.TokenType)
        {
          case JSON.JsonTokenType.Value:
          {
            // Handle primitives
            JSON.JsonValue jsonValue = dataElement.TokenValue as JSON.JsonValue;
            switch (m_type)
            {
              case HydraTypeCodes.BOOL:
              {
                m_data = (bool)jsonValue.Value;
                break;
              }

              case HydraTypeCodes.CHAR:
              {
                m_data = ((string)jsonValue.Value)[0];
                break;
              }

              case HydraTypeCodes.STRING:
              {
                m_data = (string)jsonValue.Value;
                break;
              }

              case HydraTypeCodes.DATETIME:
              {
                m_data = DateTime.Parse((string)jsonValue.Value);
                break;
              }

              case HydraTypeCodes.DOUBLE:
              case HydraTypeCodes.FLOAT:
              case HydraTypeCodes.INT8:
              case HydraTypeCodes.INT16:
              case HydraTypeCodes.INT32:
              case HydraTypeCodes.INT64:
              case HydraTypeCodes.UINT8:
              case HydraTypeCodes.UINT16:
              case HydraTypeCodes.UINT32:
              case HydraTypeCodes.UINT64:
              {
                if (enumTypeName != null && jsonValue.Value is string)
                {
                  Type enumType = Type.GetType(enumTypeName, false);
                  if (enumType != null)
                  {
                    m_data = Enum.Parse(enumType, (string)jsonValue.Value, false);
                  }
                }
                else
                {
                  m_data = Convert.ChangeType(jsonValue.Value, HydraType.DetermineType(m_type));
                }
                break;
              }
            }

            break;
          }

          case JSON.JsonTokenType.Array:
          {
            JSON.JsonArray jsonArray = dataElement.TokenValue as JSON.JsonArray;
            switch (m_type)
            {
              case HydraTypeCodes.ARRAY_OBJECT:
              case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
              {
                HydralizeInfo[] infos = new HydralizeInfo[jsonArray.Values.Count];
                for (int i = 0; i < jsonArray.Values.Count; ++i)
                {
                  HydralizeInfo info = new HydralizeInfo();
                  info.FromJson(jsonArray.Values[i] as JSON.JsonObject);
                  infos[i] = info;
                }

                m_data = infos;
                break;
              }

              default:
              {
                // Array of primitive type
                Type elementType = null;
                Array arr = null;
                if (enumTypeName != null)
                {
                  elementType = Type.GetType(enumTypeName, false);
                  arr = Array.CreateInstance(elementType, jsonArray.Values.Count);
                  for (int i = 0; i < jsonArray.Values.Count; ++i)
                  {
                    // Parse enum
                    string enumVal = (string)(jsonArray.Values[i] as JSON.JsonValue).Value;
                    arr.SetValue(Enum.Parse(elementType, enumVal), i);
                  }
                }
                else
                {
                  elementType = HydraType.DetermineType(HydraType.FromArrayType(m_type));
                  arr = Array.CreateInstance(elementType, jsonArray.Values.Count);
                  for (int i = 0; i < jsonArray.Values.Count; ++i)
                  {
                    arr.SetValue(Convert.ChangeType((jsonArray.Values[i] as JSON.JsonValue).Value, elementType), i);
                  }
                }

                m_data = arr;
                break;
              }
            }
            break;
          }

          case JSON.JsonTokenType.Object:
          {
            JSON.JsonObject jsonObj = dataElement.TokenValue as JSON.JsonObject;
            HydralizeInfo info = new HydralizeInfo();
            info.FromJson(jsonObj);
            m_data = info;
            break;
          }
        }
      }

      return name;
    }

    /// <summary>
    /// Converts this instance data to an XML string
    /// </summary>
    /// <param name="name">The name of this data</param>
    /// <param name="level">The level of indenting to use in the XML string</param>
    /// <returns>The generated XML string</returns>
    public string ToXml(string name, int level = 0)
    {
      StringBuilder tag = new StringBuilder();
      StringBuilder indent = new StringBuilder();
      for (int i = 0; i < level; ++i)
      {
        indent.Append("  ");
      }

      if (m_data != null && (m_data.GetType().IsEnum || (m_data.GetType().IsArray && m_data.GetType().GetElementType().IsEnum)))
      {
        string enumName;
        if (m_data.GetType().IsArray)
        {
          enumName = m_data.GetType().GetElementType().AssemblyQualifiedName;
        }
        else
        {
          enumName = m_data.GetType().AssemblyQualifiedName;
        }
        tag.AppendFormat("{0}<Data name='{1}' type='{2}' enum='{3}'>", indent.ToString(), name, m_type, enumName);
      }
      else
      {
        tag.AppendFormat("{0}<Data name='{1}' type='{2}'>", indent.ToString(), name, m_type);
      }
      tag.Append(DataToXml(level + 1));
      if (!IsNullType && (IsArrayType || m_type == HydraTypeCodes.OBJECT || m_type == HydraTypeCodes.HYDRA_OBJECT))
      {
        tag.Append(indent.ToString());
      }
      tag.AppendLine("</Data>");
      return tag.ToString();
    }

    private string DataToXml(int level = 0)
    {
      StringBuilder indent = new StringBuilder();
      StringBuilder tag = new StringBuilder();

      for (int i = 0; i < level; ++i)
      {
        indent.Append("  ");
      }

      if (IsNullType)
      {
        tag.AppendFormat("{0}", "NULL");
      }
      else if (IsArrayType)
      {
        tag.Append("\n");
        Array array = m_data as Array;
        if (array != null)
        {
          HydraTypeCodes elementType = HydraType.FromArrayType(m_type);
          for (int i = 0; i < array.Length; ++i)
          {
            if (elementType == HydraTypeCodes.OBJECT || elementType == HydraTypeCodes.HYDRA_OBJECT)
            {
              tag.AppendFormat("{0}<{1}>\n", indent.ToString(), elementType);
              tag.Append(ToXml(elementType, array.GetValue(i), level + 1));
              tag.AppendFormat("{0}</{1}>\n", indent.ToString(), elementType);
            }
            else
            {
              tag.AppendFormat("{0}<{1}>{2}</{1}>\n", indent.ToString(), elementType, ToXml(elementType, array.GetValue(i)));
            }
          }
        }
      }
      else
      {
        if (m_type == HydraTypeCodes.OBJECT || m_type == HydraTypeCodes.HYDRA_OBJECT)
        {
          tag.Append("\n");
        }
        tag.Append(ToXml(m_type, m_data, level));
      }

      return tag.ToString();
    }

    private static string ToXml(HydraTypeCodes type, object data, int level = 0)
    {
      if (data == null)
      {
        return string.Empty;
      }
      else
      {
        type = HydraType.DetermineType(data);
      }

      StringBuilder indent = new StringBuilder();
      StringBuilder tag = new StringBuilder();

      for (int i = 0; i < level; ++i)
      {
        indent.Append("  ");
      }

      switch (type)
      {
        case HydraTypeCodes.STRING:
        {
          tag.Append(data.ToString().Replace("&", "&amp;").Replace("<", "&lt;"));
        }
        break;

        case HydraTypeCodes.ENUM:
        case HydraTypeCodes.BOOL:
        case HydraTypeCodes.DOUBLE:
        case HydraTypeCodes.FLOAT:
        case HydraTypeCodes.INT16:
        case HydraTypeCodes.INT32:
        case HydraTypeCodes.INT64:
        case HydraTypeCodes.INT8:
        case HydraTypeCodes.UINT16:
        case HydraTypeCodes.CHAR:
        case HydraTypeCodes.UINT32:
        case HydraTypeCodes.UINT64:
        case HydraTypeCodes.UINT8:
        {
          tag.Append(data.ToString());
        }
        break;

        case HydraTypeCodes.DATETIME:
        {
          tag.Append(((DateTime)data).ToString("o"));
        }
        break;

        case HydraTypeCodes.OBJECT:
        case HydraTypeCodes.HYDRA_OBJECT:
        {
          HydralizeInfo info = data as HydralizeInfo;
          if (info != null)
          {
            tag.Append(info.ToXml(level));
          }
        }
        break;
      }

      return tag.ToString();
    }

    /// <summary>
    /// Parses data from an XML node
    /// </summary>
    /// <param name="tag">The tag containing the data for this instance</param>
    /// <param name="context">The context to use when indexing objects</param>
    public void FromXml(XmlNode tag, IndexContext context)
    {
      m_data = ObjectFromXml(tag, context);
    }

    /// <summary>
    /// Converts a list to an array
    /// </summary>
    /// <param name="list">The list of values</param>
    /// <param name="overrideType">The element type to override, if not specified the list element type will be used</param>
    /// <returns>The converted array of values</returns>
    public static Array ListToArray(IList list, HydraTypeCodes overrideType = HydraTypeCodes.INVALID)
    {
      HydraTypeCodes elementHydraType = overrideType == HydraTypeCodes.INVALID ? HydraType.DetermineType(list.GetType().GetGenericArguments()[0]) : overrideType;
      Array array = null;
      switch (elementHydraType)
      {
        case HydraTypeCodes.UINT8:
          array = new byte[list.Count];
          break;

        case HydraTypeCodes.UINT16:
          array = new UInt16[list.Count];
          break;

        case HydraTypeCodes.CHAR:
          array = new char[list.Count];
          break;

        case HydraTypeCodes.UINT32:
          array = new UInt32[list.Count];
          break;

        case HydraTypeCodes.UINT64:
          array = new UInt64[list.Count];
          break;

        case HydraTypeCodes.INT8:
          array = new sbyte[list.Count];
          break;

        case HydraTypeCodes.INT16:
          array = new Int16[list.Count];
          break;

        case HydraTypeCodes.INT32:
          array = new Int32[list.Count];
          break;

        case HydraTypeCodes.INT64:
          array = new Int64[list.Count];
          break;

        case HydraTypeCodes.BOOL:
          array = new bool[list.Count];
          break;

        case HydraTypeCodes.FLOAT:
          array = new float[list.Count];
          break;

        case HydraTypeCodes.DOUBLE:
          array = new double[list.Count];
          break;

        case HydraTypeCodes.STRING:
        {
          array = new string[list.Count];
        }
        break;

        case HydraTypeCodes.HYDRA_OBJECT:
          array = new IHydralize[list.Count];
          break;

        case HydraTypeCodes.OBJECT:
          array = new object[list.Count];
          break;

        case HydraTypeCodes.DATETIME:
          array = new DateTime[list.Count];
          break;
      }

      if (array != null)
      {
        for (int i = 0; i < array.Length; i++)
        {
          array.SetValue(list[i], i);
        }
      }

      return array;
    }

    /// <summary>
    /// Creates a new array by converting the source array elements into the specified type
    /// </summary>
    /// <param name="source">The array of values to convert</param>
    /// <param name="newElementType">The new type for each element</param>
    /// <returns>The array of converted elements</returns>
    public static Array ChangeArray(Array source, Type newElementType)
    {
      Array dest = Array.CreateInstance(newElementType, source.Length);
      for (int i = 0; i < source.Length; ++i)
      {
        dest.SetValue(Convert.ChangeType(source.GetValue(i), newElementType), i);
      }
      return dest;
    }

    private object ObjectFromXml(XmlNode node, IndexContext context)
    {
      return ObjectFromXml(node, m_type, context);
    }

    /// <summary>
    /// Parses an XML tag value to create the object data for this instance
    /// </summary>
    /// <param name="tag">The Data XML tag to create an object from</param>
    /// <param name="typeCode">The type of data to get</param>
    /// <param name="context">The context to use when resolving indexed objects</param>
    /// <returns>The object data parsed from the XML tag</returns>
    private object ObjectFromXml(XmlNode tag, HydraTypeCodes typeCode, IndexContext context)
    {
      object data = null;
      HydraTypeCodes elementType = HydraType.FromArrayType(typeCode);
      bool isEnum = false;
      if (tag.Attributes["enum"] != null)
      {
        isEnum = true;
      }

      if (elementType != HydraTypeCodes.INVALID)
      {
        Array array = null;
        if (isEnum)
        {
          Type enumType = Type.GetType(tag.Attributes["enum"].Value);
          if (enumType != null)
          {
            array = Array.CreateInstance(enumType, tag.ChildNodes.Count);
            for (int i = 0; i < array.Length; ++i)
            {
              string valueString = tag.ChildNodes[i].InnerText;
              array.SetValue(Enum.Parse(enumType, tag.ChildNodes[i].InnerText), i);
            }
          }
        }
        else
        {
          Type arrayType = HydraType.DetermineType(elementType);
          array = Array.CreateInstance(arrayType, tag.ChildNodes.Count);

          for (int i = 0; i < tag.ChildNodes.Count; ++i)
          {
            array.SetValue(ObjectFromXml(tag.ChildNodes[i], elementType, context), i);
          }
        }

        return array;
      }

      if (isEnum)
      {
        typeCode = HydraTypeCodes.ENUM;
      }

      switch (typeCode)
      {
        case HydraTypeCodes.BOOL:
        {
          if (bool.TryParse(tag.InnerText, out bool value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.DATETIME:
        {
          if (DateTime.TryParse(tag.InnerText, out DateTime time))
          {
            data = time;
          }
        }
        break;

        case HydraTypeCodes.DOUBLE:
        {
          if (double.TryParse(tag.InnerText, out double value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.ENUM:
        {
          try
          {
            Type enumType = Type.GetType(tag.Attributes["enum"].Value);
            data = Enum.Parse(enumType, tag.InnerText);
          }
          catch
          {
            data = 0;
          }
        }
        break;

        case HydraTypeCodes.FLOAT:
        {
          if (float.TryParse(tag.InnerText, out float value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.INT16:
        {
          if (Int16.TryParse(tag.InnerText, out Int16 value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.INT32:
        {
          if (Int32.TryParse(tag.InnerText, out Int32 value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.INT64:
        {
          if (Int64.TryParse(tag.InnerText, out Int64 value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.INT8:
        {
          if (sbyte.TryParse(tag.InnerText, out sbyte value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.STRING:
        {
          data = tag.InnerText;
        }
        break;

        case HydraTypeCodes.UINT16:
        {
          if (UInt16.TryParse(tag.InnerText, out UInt16 value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.CHAR:
        {
          if (char.TryParse(tag.InnerText, out char value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.UINT32:
        {
          if (UInt32.TryParse(tag.InnerText, out UInt32 value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.UINT64:
        {
          if (UInt64.TryParse(tag.InnerText, out UInt64 value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.UINT8:
        {
          if (byte.TryParse(tag.InnerText, out byte value))
          {
            data = value;
          }
        }
        break;

        case HydraTypeCodes.HYDRA_OBJECT:
        case HydraTypeCodes.OBJECT:
        {
          HydralizeInfo info = new HydralizeInfo(context);
          info.FromXml(tag.FirstChild);
          data = info;
        }
        break;
      }

      return data;
    }
  }
}
