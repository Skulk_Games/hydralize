#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Skulk.Hydralize
{
  /// <summary>
  /// A converter implementation for generic dictionary collections
  /// </summary>
  public class DictionaryConverter : IHydralizeConverter
  {
    /// <inheritdoc/>
    public Type ConvertType => typeof(Dictionary<,>);

    /// <inheritdoc/>
    public void Dehydralize(object instance, ref HydralizeInfo info)
    {
      // pull out a key and value array
      MethodInfo[] methods = GetType().GetMethods();
      MethodInfo method = null;
      for (int i = 0; i < methods.Length; ++i)
      {
        if (methods[i].IsGenericMethod && methods[i].Name == nameof(Dehydralize))
        {
          method = methods[i].MakeGenericMethod(instance.GetType().GetGenericArguments());
          break;
        }
      }

      method.Invoke(this, new object[] { instance, info });
    }

    /// <inheritdoc/>
    public void Dehydralize<TKey, TValue>(Dictionary<TKey, TValue> dict, ref HydralizeInfo info)
    {
      TKey[] keys = info.GetArray<TKey>("keys", 0);
      TValue[] values = info.GetArray<TValue>("values", 1);

      if (keys != null && values != null)
      {
        for (int i = 0; i < keys.Length && i < values.Length; ++i)
        {
          dict.Add(keys[i], values[i]);
        }
      }
    }

    /// <inheritdoc/>
    public void Hydralize(object value, ref HydralizeInfo info)
    {
      MethodInfo[] methods = GetType().GetMethods();
      MethodInfo method = null;
      for (int i = 0; i < methods.Length; ++i)
      {
        if (methods[i].IsGenericMethod && methods[i].Name == nameof(Hydralize))
        {
          method = methods[i].MakeGenericMethod(value.GetType().GetGenericArguments());
          break;
        }
      }

      method?.Invoke(this, new object[] { value, info });
    }

    /// <inheritdoc/>
    public void Hydralize<TKey, TValue>(Dictionary<TKey, TValue> dict, ref HydralizeInfo info)
    {
      TKey[] keys = new TKey[dict.Count];
      TValue[] values = new TValue[dict.Count];
      dict.Keys.CopyTo(keys, 0);
      dict.Values.CopyTo(values, 0);

      info.AddInfo("keys", keys);
      info.AddInfo("values", values);
    }

    /// <inheritdoc/>
    public HydraFieldInfo[] GetFields(Type type)
    {
      HydraFieldInfo[] fields = new HydraFieldInfo[2];
      Type[] typeArgs = type.GetGenericArguments();
      fields[0] = new HydraFieldInfo("keys", typeArgs[0].MakeArrayType());
      fields[1] = new HydraFieldInfo("values", typeArgs[1].MakeArrayType());
      return fields;
    }
  }
}
