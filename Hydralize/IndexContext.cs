#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Specifies the group ID and index of a type.
  /// </summary>
  public struct IndexAddress
  {
    /// <summary>
    /// The ID of the group the indexed type resides in.
    /// </summary>
    public byte m_groupId;

    /// <summary>
    /// The index of the type within the index group.
    /// </summary>
    public int m_typeIndex;

    /// <summary>
    /// Initializes this index addres with the specified group ID and type index.
    /// </summary>
    /// <param name="group">The ID of the group for the indexed type.</param>
    /// <param name="type">The index of the type in the group.</param>
    public IndexAddress(byte group, int type)
    {
      m_groupId = group;
      m_typeIndex = type;
    }
  }

  /// <summary>
  /// Provides context information when using indexed HydralizeInfo objects.
  /// </summary>
  public class IndexContext
  {
    private Dictionary<byte, Type[]> m_groupLookup = new Dictionary<byte, Type[]>();
    private Dictionary<Type, IndexAddress> m_typeLookup = new Dictionary<Type, IndexAddress>();

    /// <summary>
    /// The number of type groups that are indexed
    /// </summary>
    public int Count => m_groupLookup.Count;

    /// <summary>
    /// Default constructor
    /// </summary>
    public IndexContext()
    {
      AddAssembly(0, typeof(Hydralizer).Assembly);
    }

    /// <summary>
    /// Gets a copied list of the <see cref="KeyValuePair{TKey, TValue}"/> entries in this context
    /// </summary>
    /// <returns>A copied list of the assembly entries in this context</returns>
    public List<KeyValuePair<byte, Type[]>> GetPairs()
    {
      lock (m_groupLookup)
      {
        List<KeyValuePair<byte, Type[]>> pairs = new List<KeyValuePair<byte, Type[]>>();
        foreach (KeyValuePair<byte, Type[]> pair in m_groupLookup)
        {
          pairs.Add(pair);
        }

        return pairs;
      }
    }

    /// <summary>
    /// Adds an assembly to the supported assemblies to allow classes defined in the assembly to be indexed
    /// </summary>
    /// <param name="id">The ID to associate with this assembly, 0 is reserved for the Hydralize assembly</param>
    /// <param name="assembly">The assembly to index</param>
    /// <returns>True if the assembly was added, false if the ID is already in use or the assembly is already indexed</returns>
    public bool AddAssembly(byte id, Assembly assembly)
    {
      if (!m_groupLookup.ContainsKey(id))
      {
        Type[] types = assembly.GetTypes();
        m_groupLookup.Add(id, types);
        UpdateTypeLookup(id, types);
        return true;
      }
      return false;
    }

    /// <summary>
    /// Adds the array of types to the index context as a group.
    /// </summary>
    /// <param name="id">The ID to assign to the group.</param>
    /// <param name="types">The array of types in the group.</param>
    /// <returns>True if the group was added, false if the ID is already in use.</returns>
    public bool AddGroup(byte id, Type[] types)
    {
      if (!m_groupLookup.ContainsKey(id))
      {
        m_groupLookup.Add(id, types);
        UpdateTypeLookup(id, types);
        return true;
      }

      return false;
    }

    /// <summary>
    /// Clears all but the Hydralize assembly
    /// </summary>
    public void Clear()
    {
      m_groupLookup.Clear();
      m_typeLookup.Clear();
      AddAssembly(0, typeof(Hydralizer).Assembly);
    }

    /// <summary>
    /// Gets the group with the specified ID
    /// </summary>
    /// <param name="id">The ID of the assembly to get</param>
    /// <param name="types">The array of types in the group with the specified ID</param>
    /// <returns>True if the assembly was found, false if no assembly was found</returns>
    public bool GetGroup(byte id, out Type[] types)
    {
      return m_groupLookup.TryGetValue(id, out types);
    }

    /// <summary>
    /// Indexes the specified object
    /// </summary>
    /// <param name="obj">The object to index, if null the indexing will fail</param>
    /// <param name="assemblyId">The ID of the assembly for the object if successful, otherwise 0</param>
    /// <param name="typeIndex">The ID of the type for the object if successful, otherwise 0</param>
    /// <returns>True if the indexing succeeded, otherwise false</returns>
    public bool Index(object obj, out byte assemblyId, out int typeIndex)
    {
      if (obj != null)
      {
        return Index(obj.GetType(), out assemblyId, out typeIndex);
      }

      assemblyId = 0;
      typeIndex = 0;
      return false;
    }

    /// <summary>
    /// Indexes the specified type
    /// </summary>
    /// <param name="type">The type of the object to index</param>
    /// <param name="assemblyId">The ID of the assembly for the object if successful, otherwise 0</param>
    /// <param name="typeIndex">The ID of the type for the object if successful, otherwise 0</param>
    /// <returns>True if the indexing succeeded, otherwise false</returns>
    public bool Index(Type type, out byte assemblyId, out int typeIndex)
    {
      if (type.IsGenericType)
      {
        type = type.GetGenericTypeDefinition();
      }

      if (m_typeLookup.TryGetValue(type, out IndexAddress address))
      {
        assemblyId = address.m_groupId;
        typeIndex = address.m_typeIndex;
        return true;
      }

      assemblyId = 0;
      typeIndex = 0;
      return false;
    }

    /// <summary>
    /// Looks up the specified type in this context
    /// </summary>
    /// <param name="groupId">The ID of the group</param>
    /// <param name="typeIndex">The index of the type</param>
    /// <returns>The type found in this context or null if not found</returns>
    public Type Lookup(byte groupId, int typeIndex)
    {
      if (m_groupLookup.TryGetValue(groupId, out Type[] types))
      {
        if (typeIndex >= 0 && typeIndex < types.Length)
        {
          return types[typeIndex];
        }
      }

      return null;
    }

    /// <summary>
    /// Looks up and instantiates the specified type
    /// </summary>
    /// <param name="assemblyId">The ID of the assembly for the type to instantiate</param>
    /// <param name="typeIndex">The index of the type to instantiate</param>
    /// <returns>The instantiated object or null if unable to instantiate</returns>
    public object Instantiate(byte assemblyId, int typeIndex)
    {
      try
      {
        Type type = Lookup(assemblyId, typeIndex);
        if (type != null)
        {
          if (type.IsGenericType)
          {
            // We can't instantiate generic types this way
            return null;
          }

          object obj = Activator.CreateInstance(type);
          return obj;
        }
      }
      catch
      {
        // Do nothing, just return null
      }

      return null;
    }

    /// <summary>
    /// Updates the <see cref="m_typeLookup"/> dictionary with addresses for the specified array of types.
    /// </summary>
    /// <param name="id">The group ID of the type array.</param>
    /// <param name="types">The array of types in the group.</param>
    private void UpdateTypeLookup(byte id, Type[] types)
    {
      for (int i = 0; i < types.Length; ++i)
      {
        if (types[i] != null)
        {
          if (m_typeLookup.ContainsKey(types[i]))
          {
            m_typeLookup[types[i]] = new IndexAddress(id, i);
          }
          else
          {
            m_typeLookup.Add(types[i], new IndexAddress(id, i));
          }
        }
      }
    }
  }
}
