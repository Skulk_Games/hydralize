#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Provides static methods for Hydralizing and Dehydralizing data.
  /// </summary>
  public static class Hydralizer
  {
    /// <summary>
    /// The default context to use when indexing objects
    /// </summary>
    private static IndexContext ms_assemblyContext = new IndexContext();

    /// <summary>
    /// The collection of type converters to use when Hydralizing objects
    /// </summary>
    private static Dictionary<Type, IHydralizeConverter> ms_converters = new Dictionary<Type, IHydralizeConverter>()
    {
      { typeof(Dictionary<,>), new DictionaryConverter() },
      { typeof(List<>), new ListConverter() }
    };

    /// <summary>
    /// Common core types to add to indexing.
    /// </summary>
    private static readonly Type[] ms_coreTypes = new Type[]
    {
      typeof(object),
      typeof(List<>),
      typeof(Dictionary<,>)
    };

    /// <summary>
    /// Gets the default index context used by the <see cref="Hydralizer"/>.
    /// </summary>
    public static IndexContext DefaultContext { get => ms_assemblyContext; }

    /// <summary>
    /// An array of core types that are commonly added to indexing when using List and Dictionary converters.
    /// </summary>
    public static Type[] CoreTypes { get => ms_coreTypes; }

    /// <summary>
    /// Adds the specified assembly and all its references in sequential order to the supported assemblies.
    /// </summary>
    /// <param name="assembly">The main assembly to add with all its references.</param>
    public static void AddAllReferencedAssemblies(Assembly assembly)
    {
      byte offset = (byte)ms_assemblyContext.Count;
      AddSupportedAssembly(offset++, assembly);
      AssemblyName[] assemblies = assembly.GetReferencedAssemblies();
      for (byte i = 0; i < assemblies.Length; i++)
      {
        try
        {
          AddSupportedAssembly((byte)(i + offset), Assembly.LoadFrom(assemblies[i].FullName));
        }
        catch
        {
          // Failed to load an assembly, skip for now.
        }
      }
    }

    /// <summary>
    /// Adds an assembly to the Hydralizer supported assemblies to allow classes defined in the assembly to be indexed
    /// </summary>
    /// <param name="id">The ID to associate with this assembly, 0 is reserved for the Hydralize assembly</param>
    /// <param name="assembly">The assembly to index</param>
    /// <returns>True if the assembly was added or the same assembly is already present, false if a different assembly is already indexed</returns>
    public static bool AddSupportedAssembly(byte id, Assembly assembly)
    {
      if (ms_assemblyContext.AddAssembly(id, assembly))
      {
        return true;
      }
      else
      {
        if (ms_assemblyContext.GetGroup(id, out Type[] storedTypes))
        {
          Type[] assemblyTypes = assembly.GetTypes();
          if (assemblyTypes.Length == storedTypes.Length)
          {
            for (int i = 0; i < assemblyTypes.Length; ++i)
            {
              if (assemblyTypes[i] != storedTypes[i])
              {
                return false;
              }
            }

            return true;
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Clears all converters in the dictionary on the <see cref="Hydralizer"/>
    /// </summary>
    public static void ClearConverters()
    {
      lock (ms_converters)
      {
        ms_converters.Clear();
      }
    }

    /// <summary>
    /// Loads converters in the specified assembly by creating new instances and adding them to the dictionary of converters in use on the <see cref="Hydralizer"/>
    /// </summary>
    /// <param name="assembly">The assembly to search for converters</param>
    public static void LoadConverters(Assembly assembly)
    {
      Type[] types = assembly.GetTypes();
      for (int i = 0; i < types.Length; ++i)
      {
        if (!types[i].IsInterface && typeof(IHydralizeConverter).IsAssignableFrom(types[i]))
        {
          try
          {
            IHydralizeConverter converter = Activator.CreateInstance(types[i]) as IHydralizeConverter;
            AddConverter(converter);
          }
          catch (Exception ex)
          {
            Console.WriteLine("Exception while instantiating converter: {0}, message: '{1}'", types[i].Name, ex.Message);
          }
        }
      }
    }

    /// <summary>
    /// Adds a converter to the Hydralizer
    /// </summary>
    /// <param name="converter">The converter instance to add</param>
    public static void AddConverter(IHydralizeConverter converter)
    {
      lock (ms_converters)
      {
        if (!ms_converters.ContainsKey(converter.ConvertType))
        {
          ms_converters.Add(converter.ConvertType, converter);
        }
      }
    }

    /// <summary>
    /// Searches for a converter for the specified type
    /// </summary>
    /// <param name="type">The type the converter is for</param>
    /// <returns>The converter for the specified type or null if no converter was found</returns>
    public static IHydralizeConverter LookupConverter(Type type)
    {
      if (type.IsGenericType)
      {
        if (ms_converters.TryGetValue(type.GetGenericTypeDefinition(), out IHydralizeConverter converter))
        {
          return converter;
        }
      }
      else
      {
        if (ms_converters.TryGetValue(type, out IHydralizeConverter converter))
        {
          return converter;
        }
      }

      return null;
    }

    /// <summary>
    /// Puts the data in the <see cref="HydralizeData"/> object into an array
    /// </summary>
    /// <param name="array">The array to put the value into</param>
    /// <param name="hydralizeData">The object containing the HydralizeData</param>
    /// <param name="offset">The byte offset to place the value at in the array</param>
    /// <param name="putType">If true the type byte will be placed in the buffer as well</param>
    public static void Put(byte[] array, HydralizeData hydralizeData, ref int offset, bool putType = true)
    {
      Put(array, hydralizeData.Data, hydralizeData.HydraTypeCode, ref offset, putType);
    }

    /// <summary>
    /// Puts a value of the specified type into an array
    /// </summary>
    /// <param name="array">The array to put the value into</param>
    /// <param name="value">The value to place in the array</param>
    /// <param name="hydraType">The type of the value</param>
    /// <param name="offset">The byte offset to place the value at in the array</param>
    /// <param name="putType">If true the type byte will be placed in the buffer as well</param>
    public static void Put(byte[] array, object value, HydraTypeCodes hydraType, ref int offset, bool putType = true)
    {
      switch (hydraType)
      {
        case HydraTypeCodes.UINT8:
        {
          Put(array, (byte)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.UINT16:
        {
          Put(array, (UInt16)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.UINT32:
        {
          Put(array, (UInt32)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.UINT64:
        {
          Put(array, (UInt64)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.INT8:
        {
          Put(array, (sbyte)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.INT16:
        {
          Put(array, (Int16)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.INT32:
        {
          Put(array, (Int32)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.INT64:
        {
          Put(array, (Int64)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.BOOL:
        {
          Put(array, (bool)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.FLOAT:
        {
          Put(array, (float)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.DOUBLE:
        {
          Put(array, (double)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.STRING:
        {
          Put(array, (string)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.DATETIME:
        {
          Put(array, (DateTime)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_UINT8:
        {
          Put(array, (byte[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_UINT16:
        {
          Put(array, (UInt16[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_UINT32:
        {
          Put(array, (UInt32[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_UINT64:
        {
          Put(array, (UInt64[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_INT8:
        {
          Put(array, (sbyte[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_INT16:
        {
          Put(array, (Int16[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_INT32:
        {
          Put(array, (Int32[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_INT64:
        {
          Put(array, (Int64[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_STRING:
        {
          Put(array, (string[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_BOOL:
        {
          Put(array, (bool[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_FLOAT:
        {
          Put(array, (float[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_DOUBLE:
        {
          Put(array, (double[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.HYDRA_OBJECT:
        case HydraTypeCodes.OBJECT:
        {
          Put(array, (HydralizeInfo)value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
        case HydraTypeCodes.ARRAY_OBJECT:
        {
          Put(array, (HydralizeInfo[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.ARRAY_DATETIME:
        {
          Put(array, (DateTime[])value, ref offset, putType);
          break;
        }

        case HydraTypeCodes.CHAR:
        {
          if (putType)
          {
            Put(array, (byte)HydraTypeCodes.CHAR, ref offset, false);
          }
          Put(array, (UInt16)(char)value, ref offset, false);
          break;
        }

        case HydraTypeCodes.ARRAY_CHAR:
        {
          if (putType)
          {
            Put(array, (byte)HydraTypeCodes.ARRAY_CHAR, ref offset, false);
          }
          Put(array, Array.ConvertAll((char[])value, x => (UInt16)x), ref offset, false);
          break;
        }

        case HydraTypeCodes.NULL_ARRAY_BOOL:
        case HydraTypeCodes.NULL_ARRAY_DOUBLE:
        case HydraTypeCodes.NULL_ARRAY_ENUM:
        case HydraTypeCodes.NULL_ARRAY_FLOAT:
        case HydraTypeCodes.NULL_ARRAY_HYDRA_OBJECT:
        case HydraTypeCodes.NULL_ARRAY_OBJECT:
        case HydraTypeCodes.NULL_ARRAY_INT16:
        case HydraTypeCodes.NULL_ARRAY_INT32:
        case HydraTypeCodes.NULL_ARRAY_INT64:
        case HydraTypeCodes.NULL_ARRAY_INT8:
        case HydraTypeCodes.NULL_ARRAY_STRING:
        case HydraTypeCodes.NULL_ARRAY_UINT16:
        case HydraTypeCodes.NULL_ARRAY_UINT32:
        case HydraTypeCodes.NULL_ARRAY_UINT64:
        case HydraTypeCodes.NULL_ARRAY_UINT8:
        case HydraTypeCodes.NULL_HYDRA_OBJECT:
        case HydraTypeCodes.NULL_OBJECT:
        case HydraTypeCodes.NULL_STRING:
        {
          Put(array, hydraType, ref offset);
        }
        break;

        case HydraTypeCodes.ENUM:
        case HydraTypeCodes.ARRAY_ENUM:
        {
          // Not a directly used type - the underlying type is used instead
          throw new ArgumentException(string.Format("HydraTypeCode: {0}, is not valid in the Put method. The underlying type should be used instead", hydraType));
        }
      }
    }

    /// <summary>
    /// Puts a byte into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, byte value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.UINT8, ref offset);
      }

      if (array.Length >= offset + sizeof(byte))
      {
        array[offset++] = value;
      }
    }

    /// <summary>
    /// Puts a signed byte into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, sbyte value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.INT8, ref offset);
      }

      if (array.Length >= offset + sizeof(byte))
      {
        array[offset++] = (byte)value;
      }
    }

    /// <summary>
    /// Puts a UInt16 into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, UInt16 value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.UINT16, ref offset);
      }

      if (array.Length >= offset + sizeof(UInt16))
      {

        byte[] aVal = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
        {
          Array.Reverse(aVal);
        }

        Array.Copy(aVal, 0, array, offset, sizeof(UInt16));
        offset += sizeof(UInt16);
      }
    }

    /// <summary>
    /// Puts a UInt32 into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, UInt32 value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.UINT32, ref offset);
      }

      if (array.Length >= offset + sizeof(UInt32))
      {
        byte[] aVal = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
        {
          Array.Reverse(aVal);
        }

        Array.Copy(aVal, 0, array, offset, sizeof(UInt32));
        offset += sizeof(UInt32);
      }
    }

    /// <summary>
    /// Puts a UInt64 into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, UInt64 value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.UINT64, ref offset);
      }

      if (array.Length >= offset + sizeof(UInt64))
      {
        byte[] aVal = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
        {
          Array.Reverse(aVal);
        }

        Array.Copy(aVal, 0, array, offset, sizeof(UInt64));
        offset += sizeof(UInt64);
      }
    }

    /// <summary>
    /// Puts an Int16 into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, Int16 value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.INT16, ref offset);
      }

      if (array.Length >= offset + sizeof(Int16))
      {
        byte[] aVal = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
        {
          Array.Reverse(aVal);
        }

        Array.Copy(aVal, 0, array, offset, aVal.Length);
        offset += sizeof(Int16);
      }
    }

    /// <summary>
    /// Puts an Int32 into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, Int32 value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.INT32, ref offset);
      }

      if (array.Length >= offset + sizeof(Int32))
      {
        byte[] aVal = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
        {
          Array.Reverse(aVal);
        }

        Array.Copy(aVal, 0, array, offset, aVal.Length);
        offset += sizeof(Int32);
      }
    }

    /// <summary>
    /// Puts an Int64 into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, Int64 value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.INT64, ref offset);
      }

      if (array.Length >= offset + sizeof(Int64))
      {
        byte[] aVal = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
        {
          Array.Reverse(aVal);
        }

        Array.Copy(aVal, 0, array, offset, aVal.Length);
        offset += sizeof(Int64);
      }
    }

    /// <summary>
    /// Puts a float into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, float value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.FLOAT, ref offset);
      }

      if (array.Length >= offset + sizeof(float))
      {
        byte[] aVal = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
        {
          Array.Reverse(aVal);
        }

        Array.Copy(aVal, 0, array, offset, aVal.Length);
        offset += sizeof(float);
      }
    }

    /// <summary>
    /// Puts a double into a buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, double value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.DOUBLE, ref offset);
      }

      if (array.Length >= offset + sizeof(double))
      {
        byte[] aVal = BitConverter.GetBytes(value);
        if (BitConverter.IsLittleEndian)
        {
          Array.Reverse(aVal);
        }

        Array.Copy(aVal, 0, array, offset, aVal.Length);
        offset += sizeof(double);
      }
    }

    /// <summary>
    /// Puts a bool into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, bool value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.BOOL, ref offset);
      }

      if (array.Length >= offset + sizeof(byte))
      {
        array[offset++] = value ? (byte)1 : (byte)0;
      }
    }

    /// <summary>
    /// Puts a string into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, string value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.STRING, ref offset);
        }

        if (array.Length >= offset + value.Length + sizeof(UInt32))
        {
          byte[] aStr = System.Text.Encoding.ASCII.GetBytes(value);
          Put(array, (UInt32)aStr.Length, ref offset, false);
          Array.Copy(aStr, 0, array, offset, aStr.Length);
          offset += aStr.Length;
        }
      }
      else
      {
        if (array.Length >= offset + sizeof(uint))
        {
          // use uint MaxValue as a flag for a null string
          Put(array, uint.MaxValue, ref offset, false);
        }
      }
    }

    /// <summary>
    /// Puts a DateTime value into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, DateTime value, ref int offset, bool putType = true)
    {
      if (putType)
      {
        Put(array, HydraTypeCodes.DATETIME, ref offset);
      }

      Put(array, value.ToBinary(), ref offset, false);
    }

    /// <summary>
    /// Puts an array of DateTime values into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="values">The DateTime values to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the values</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, DateTime[] values, ref int offset, bool putType = true)
    {
      if (values != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_DATETIME, ref offset);
        }

        Put(array, (UInt32)values.Length, ref offset, false);
        for (int i = 0; i < values.Length; i++)
        {
          Put(array, values[i], ref offset, false);
        }
      }
    }

    /// <summary>
    /// Puts a byte array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, byte[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_UINT8, ref offset);
        }

        if (array.Length >= offset + value.Length + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          Array.Copy(value, 0, array, offset, value.Length);
          offset += value.Length;
        }
      }
    }

    /// <summary>
    /// Puts a UInt16 array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, UInt16[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_UINT16, ref offset);
        }

        if (array.Length >= offset + value.Length * sizeof(UInt16) + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts a UInt32 array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, UInt32[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_UINT32, ref offset);
        }

        if (array.Length >= offset + value.Length * sizeof(UInt32) + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts a UInt64 array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, UInt64[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_UINT64, ref offset);
        }

        if (array.Length >= offset + value.Length * sizeof(UInt64) + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts an array of signed byte values into the buffer
    /// </summary>
    /// <param name="array">The buffer array to put the values in</param>
    /// <param name="value">The signed byte values to put in the buffer</param>
    /// <param name="offset">The offset in the buffer array to start placing values</param>
    /// <param name="putType">Indicates if the type code should be put in the buffer</param>
    public static void Put(byte[] array, sbyte[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_INT8, ref offset);
        }

        if (array.Length >= offset + value.Length + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          Array.Copy(value, 0, array, offset, value.Length);
          offset += value.Length;
        }
      }
    }

    /// <summary>
    /// Puts an Int16 array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, Int16[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_INT16, ref offset);
        }

        if (array.Length >= offset + value.Length * sizeof(Int16) + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts an Int32 array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, Int32[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_INT32, ref offset);
        }

        if (array.Length >= offset + value.Length * sizeof(Int32) + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts a Int64 array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, Int64[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_INT64, ref offset);
        }

        if (array.Length >= offset + value.Length * sizeof(Int64) + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts a string array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, string[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_STRING, ref offset);
        }

        int length = sizeof(UInt32);
        foreach (string str in value)
        {
          length += sizeof(UInt32) + (str?.Length ?? 0);
        }

        if (array.Length >= offset + length)
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts a bool array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, bool[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_BOOL, ref offset);
        }

        if (array.Length >= offset + value.Length + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts a float array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, float[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_FLOAT, ref offset);
        }

        if (array.Length >= offset + value.Length * sizeof(float) + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts a double array into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="value">The value to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, double[] value, ref int offset, bool putType = true)
    {
      if (value != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_DOUBLE, ref offset);
        }

        if (array.Length >= offset + value.Length * sizeof(double) + sizeof(UInt32))
        {
          Put(array, (UInt32)value.Length, ref offset, false);
          for (uint i = 0; i < value.Length; i++)
          {
            Put(array, value[i], ref offset, false);
          }
        }
      }
    }

    /// <summary>
    /// Puts Hydralized object info into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="info">The object values to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, HydralizeInfo info, ref int offset, bool putType = true)
    {
      if (info != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.HYDRA_OBJECT, ref offset);
        }

        byte[] objBytes = info.Hydralize();
        if (array.Length >= offset + objBytes.Length)
        {
          Array.Copy(objBytes, 0, array, offset, objBytes.Length);
          offset += objBytes.Length;
        }
      }
    }

    /// <summary>
    /// Puts an array of Hydralized object info into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="infos">The object values to place in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the value</param>
    /// <param name="putType">If true the type byte will be put in the buffer as well</param>
    public static void Put(byte[] array, HydralizeInfo[] infos, ref int offset, bool putType = true)
    {
      if (infos != null)
      {
        if (putType)
        {
          Put(array, HydraTypeCodes.ARRAY_HYDRA_OBJECT, ref offset);
        }

        Put(array, (UInt32)infos.Length, ref offset, false);
        for (int i = 0; i < infos.Length; i++)
        {
          Put(array, infos[i], ref offset, false);
        }
      }
    }

    /// <summary>
    /// Puts an array of indexed generic type arguments into a Hydralized buffer
    /// </summary>
    /// <param name="array">The buffer to put the value in</param>
    /// <param name="genericArgs">The array of indexed generic args to put in the buffer</param>
    /// <param name="offset">The byte offset to begin placing the values</param>
    public static void Put(byte[] array, IndexedGenericType[] genericArgs, ref int offset)
    {
      if (genericArgs != null)
      {
        Put(array, (byte)genericArgs.Length, ref offset, false);
        for (int i = 0; i < genericArgs.Length; ++i)
        {
          if (!genericArgs[i].Put(array, ref offset))
          {
            return;
          }
        }
      }
    }

    /// <summary>
    /// Gets the next Hydralized field in the array at the specified offset
    /// </summary>
    /// <param name="array">The raw buffer of hydralized data</param>
    /// <param name="offset">The byte offset to start reading at, automatically incremented as the data is read</param>
    /// <param name="lite">If the data is in lite format</param>
    /// <param name="context">The index context to use when getting objects</param>
    /// <returns>A <see cref="HydralizeData"/> object describing the field data read from the buffer</returns>
    public static HydralizeData Get(byte[] array, ref int offset, bool lite = false, IndexContext context = null)
    {
      HydraTypeCodes typeCode = (HydraTypeCodes)GetByte(array, ref offset);
      HydralizeData result = null;
      switch (typeCode)
      {
        case HydraTypeCodes.UINT8:
          result = new HydralizeData(GetByte(array, ref offset));
          break;

        case HydraTypeCodes.INT8:
          result = new HydralizeData((sbyte)GetByte(array, ref offset));
          break;

        case HydraTypeCodes.UINT16:
          result = new HydralizeData(GetUInt16(array, ref offset));
          break;

        case HydraTypeCodes.CHAR:
          result = new HydralizeData((char)GetUInt16(array, ref offset));
          break;

        case HydraTypeCodes.UINT32:
          result = new HydralizeData(GetUInt32(array, ref offset));
          break;

        case HydraTypeCodes.UINT64:
          result = new HydralizeData(GetUInt64(array, ref offset));
          break;

        case HydraTypeCodes.INT16:
          result = new HydralizeData(GetInt16(array, ref offset));
          break;

        case HydraTypeCodes.INT32:
          result = new HydralizeData(GetInt32(array, ref offset));
          break;

        case HydraTypeCodes.INT64:
          result = new HydralizeData(GetInt64(array, ref offset));
          break;

        case HydraTypeCodes.BOOL:
          result = new HydralizeData(GetBool(array, ref offset));
          break;

        case HydraTypeCodes.STRING:
          result = new HydralizeData(GetString(array, ref offset));
          break;

        case HydraTypeCodes.FLOAT:
          result = new HydralizeData(GetFloat(array, ref offset));
          break;

        case HydraTypeCodes.DOUBLE:
          result = new HydralizeData(GetDouble(array, ref offset));
          break;

        case HydraTypeCodes.DATETIME:
          result = new HydralizeData(GetDateTime(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_UINT8:
          result = new HydralizeData(GetByteArray(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_UINT16:
          result = new HydralizeData(GetUInt16Array(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_CHAR:
          result = new HydralizeData(Array.ConvertAll(GetUInt16Array(array, ref offset), x => (char)x));
          break;

        case HydraTypeCodes.ARRAY_UINT32:
          result = new HydralizeData(GetUInt32Array(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_UINT64:
          result = new HydralizeData(GetUInt64Array(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_INT8:
          result = new HydralizeData(GetSByteArray(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_INT16:
          result = new HydralizeData(GetInt16Array(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_INT32:
          result = new HydralizeData(GetInt32Array(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_INT64:
          result = new HydralizeData(GetInt64Array(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_STRING:
          result = new HydralizeData(GetStringArray(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_BOOL:
          result = new HydralizeData(GetBoolArray(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_FLOAT:
          result = new HydralizeData(GetFloatArray(array, ref offset));
          break;

        case HydraTypeCodes.ARRAY_DOUBLE:
          result = new HydralizeData(GetDoubleArray(array, ref offset));
          break;

        case HydraTypeCodes.HYDRA_OBJECT:
        case HydraTypeCodes.OBJECT:
        {
          result = new HydralizeData(GetObjectInfo(array, ref offset, context));
          break;
        }

        case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
        case HydraTypeCodes.ARRAY_OBJECT:
        {
          result = new HydralizeData(GetObjectArray(array, ref offset, context));
          break;
        }

        case HydraTypeCodes.ARRAY_DATETIME:
        {
          result = new HydralizeData(GetDateTimeArray(array, ref offset));
          break;
        }

        case HydraTypeCodes.NULL_ARRAY_BOOL:
        case HydraTypeCodes.NULL_ARRAY_DOUBLE:
        case HydraTypeCodes.NULL_ARRAY_ENUM:
        case HydraTypeCodes.NULL_ARRAY_FLOAT:
        case HydraTypeCodes.NULL_ARRAY_HYDRA_OBJECT:
        case HydraTypeCodes.NULL_ARRAY_INT16:
        case HydraTypeCodes.NULL_ARRAY_INT32:
        case HydraTypeCodes.NULL_ARRAY_INT64:
        case HydraTypeCodes.NULL_ARRAY_INT8:
        case HydraTypeCodes.NULL_ARRAY_OBJECT:
        case HydraTypeCodes.NULL_ARRAY_STRING:
        case HydraTypeCodes.NULL_ARRAY_UINT16:
        case HydraTypeCodes.NULL_ARRAY_UINT32:
        case HydraTypeCodes.NULL_ARRAY_UINT64:
        case HydraTypeCodes.NULL_ARRAY_UINT8:
        case HydraTypeCodes.NULL_HYDRA_OBJECT:
        case HydraTypeCodes.NULL_OBJECT:
        case HydraTypeCodes.NULL_STRING:
        {
          result = new HydralizeData(null, typeCode);
        }
        break;
      }

      return result;
    }

    /// <summary>
    /// Gets Hydralized object info from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized object info</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <param name="context">The index context to use for object info</param>
    /// <returns>The Hydralized object info read from the buffer</returns>
    public static HydralizeInfo GetObjectInfo(byte[] array, ref int offset, IndexContext context = null)
    {
      HydralizeInfo result = new HydralizeInfo(context);
      result.Dehydralize(array, ref offset);
      return result;
    }

    /// <summary>
    /// Gets a Hydralized byte from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The byte value read from the buffer</returns>
    public static byte GetByte(byte[] array, ref int offset)
    {
      if (array.Length >= offset + sizeof(byte))
      {
        return array[offset++];
      }

      return 0;
    }

    /// <summary>
    /// Gets a Hydralized UInt16 from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The UInt16 value read from the buffer</returns>
    public static UInt16 GetUInt16(byte[] array, ref int offset)
    {
      UInt16 result = 0;
      if (array.Length >= offset + sizeof(UInt16))
      {
        if (BitConverter.IsLittleEndian)
        {
          byte[] aVal = new byte[sizeof(UInt16)];
          Array.Copy(array, offset, aVal, 0, aVal.Length);
          Array.Reverse(aVal);
          result = BitConverter.ToUInt16(aVal, 0);
        }
        else
        {
          result = BitConverter.ToUInt16(array, (int)offset);
        }

        offset += sizeof(UInt16);
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized UInt32 from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The UInt32 value read from the buffer</returns>
    public static UInt32 GetUInt32(byte[] array, ref int offset)
    {
      UInt32 result = 0;
      if (array.Length >= offset + sizeof(UInt32))
      {
        if (BitConverter.IsLittleEndian)
        {
          byte[] aVal = new byte[sizeof(UInt32)];
          Array.Copy(array, offset, aVal, 0, aVal.Length);
          Array.Reverse(aVal);
          result = BitConverter.ToUInt32(aVal, 0);
        }
        else
        {
          result = BitConverter.ToUInt32(array, (int)offset);
        }

        offset += sizeof(UInt32);
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized UInt64 from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The UInt64 value read from the buffer</returns>
    public static UInt64 GetUInt64(byte[] array, ref int offset)
    {
      UInt64 result = 0;
      if (array.Length >= offset + sizeof(UInt64))
      {
        if (BitConverter.IsLittleEndian)
        {
          byte[] aVal = new byte[sizeof(UInt64)];
          Array.Copy(array, offset, aVal, 0, aVal.Length);
          Array.Reverse(aVal);
          result = BitConverter.ToUInt64(aVal, 0);
        }
        else
        {
          result = BitConverter.ToUInt64(array, (int)offset);
        }

        offset += sizeof(UInt64);
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized Int16 from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The Int16 value read from the buffer</returns>
    public static Int16 GetInt16(byte[] array, ref int offset)
    {
      Int16 result = 0;
      if (array.Length >= offset + sizeof(Int16))
      {
        if (BitConverter.IsLittleEndian)
        {
          byte[] aVal = new byte[sizeof(Int16)];
          Array.Copy(array, offset, aVal, 0, aVal.Length);
          Array.Reverse(aVal);
          result = BitConverter.ToInt16(aVal, 0);
        }
        else
        {
          result = BitConverter.ToInt16(array, (int)offset);
        }

        offset += sizeof(Int16);
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized Int32 from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The Int32 value read from the buffer</returns>
    public static Int32 GetInt32(byte[] array, ref int offset)
    {
      Int32 result = 0;
      if (array.Length >= offset + sizeof(Int32))
      {
        if (BitConverter.IsLittleEndian)
        {
          byte[] aVal = new byte[sizeof(Int32)];
          Array.Copy(array, offset, aVal, 0, aVal.Length);
          Array.Reverse(aVal);
          result = BitConverter.ToInt32(aVal, 0);
        }
        else
        {
          result = BitConverter.ToInt32(array, (int)offset);
        }

        offset += sizeof(Int32);
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized Int64 from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The Int64 value read from the buffer</returns>
    public static Int64 GetInt64(byte[] array, ref int offset)
    {
      Int64 result = 0;
      if (array.Length >= offset + sizeof(Int64))
      {
        if (BitConverter.IsLittleEndian)
        {
          byte[] aVal = new byte[sizeof(Int64)];
          Array.Copy(array, offset, aVal, 0, aVal.Length);
          Array.Reverse(aVal);
          result = BitConverter.ToInt64(aVal, 0);
        }
        else
        {
          result = BitConverter.ToInt64(array, (int)offset);
        }

        offset += sizeof(Int64);
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized bool from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The bool value read from the buffer</returns>
    public static bool GetBool(byte[] array, ref int offset)
    {
      return GetByte(array, ref offset) != 0;
    }

    /// <summary>
    /// Gets a Hydralized string from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The string value read from the buffer</returns>
    public static string GetString(byte[] array, ref int offset)
    {
      string result = null;
      UInt32 len = GetUInt32(array, ref offset);

      // length of MaxValue is used to flag a null string
      if (len < uint.MaxValue && array.Length >= offset + len)
      {
        result = System.Text.Encoding.ASCII.GetString(array, offset, (int)len);
        offset += (int)len;
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized float from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The float value read from the buffer</returns>
    public static float GetFloat(byte[] array, ref int offset)
    {
      float result = 0;
      if (array.Length >= offset + sizeof(float))
      {
        if (BitConverter.IsLittleEndian)
        {
          byte[] aVal = new byte[sizeof(float)];
          Array.Copy(array, offset, aVal, 0, aVal.Length);
          Array.Reverse(aVal);
          result = BitConverter.ToSingle(aVal, 0);
        }
        else
        {
          result = BitConverter.ToSingle(array, (int)offset);
        }

        offset += sizeof(float);
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized double from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The double value read from the buffer</returns>
    public static double GetDouble(byte[] array, ref int offset)
    {
      double result = 0;
      if (array.Length >= offset + sizeof(double))
      {
        if (BitConverter.IsLittleEndian)
        {
          byte[] aVal = new byte[sizeof(double)];
          Array.Copy(array, offset, aVal, 0, aVal.Length);
          Array.Reverse(aVal);
          result = BitConverter.ToDouble(aVal, 0);
        }
        else
        {
          result = BitConverter.ToDouble(array, (int)offset);
        }

        offset += sizeof(double);
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized DateTime from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The DateTime value read from the buffer</returns>
    public static DateTime GetDateTime(byte[] array, ref int offset)
    {
      return DateTime.FromBinary(GetInt64(array, ref offset));
    }

    /// <summary>
    /// Gets a Hydralized byte array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The byte array value read from the buffer</returns>
    public static byte[] GetByteArray(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + len)
      {
        byte[] buffer = new byte[len];
        Array.Copy(array, offset, buffer, 0, len);
        offset += (int)len;
        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized UInt16 array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The UInt16 array value read from the buffer</returns>
    public static UInt16[] GetUInt16Array(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + sizeof(UInt16) * len)
      {
        UInt16[] buffer = new UInt16[len];
        for (int i = 0; i < len; i++)
        {
          buffer[i] = GetUInt16(array, ref offset);
        }

        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized UInt32 array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The UInt32 array value read from the buffer</returns>
    public static UInt32[] GetUInt32Array(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + sizeof(UInt32) * len)
      {
        UInt32[] buffer = new UInt32[len];
        for (int i = 0; i < len; i++)
        {
          buffer[i] = GetUInt32(array, ref offset);
        }

        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized UInt64 array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The UInt64 array value read from the buffer</returns>
    public static UInt64[] GetUInt64Array(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + sizeof(UInt64) * len)
      {
        UInt64[] buffer = new UInt64[len];
        for (int i = 0; i < len; i++)
        {
          buffer[i] = GetUInt64(array, ref offset);
        }

        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized signed byte array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The signed byte array value read from the buffer</returns>
    public static sbyte[] GetSByteArray(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + len)
      {
        sbyte[] buffer = new sbyte[len];
        Array.Copy(array, offset, buffer, 0, len);
        offset += (int)len;
        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized Int16 array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The Int16 array value read from the buffer</returns>
    public static Int16[] GetInt16Array(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + sizeof(Int16) * len)
      {
        Int16[] buffer = new Int16[len];
        for (int i = 0; i < len; i++)
        {
          buffer[i] = GetInt16(array, ref offset);
        }

        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized Int32 array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The Int32 array value read from the buffer</returns>
    public static Int32[] GetInt32Array(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + sizeof(Int32) * len)
      {
        Int32[] buffer = new Int32[len];
        for (int i = 0; i < len; i++)
        {
          buffer[i] = GetInt32(array, ref offset);
        }

        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized Int64 array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The Int64 array value read from the buffer</returns>
    public static Int64[] GetInt64Array(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + sizeof(Int64) * len)
      {
        Int64[] buffer = new Int64[len];
        for (int i = 0; i < len; i++)
        {
          buffer[i] = GetInt64(array, ref offset);
        }

        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized string array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The string array value read from the buffer</returns>
    public static string[] GetStringArray(byte[] array, ref int offset)
    {
      UInt32 count = GetUInt32(array, ref offset);
      string[] result = new string[count];
      for (int i = 0; i < count && offset < array.Length; i++)
      {
        result[i] = GetString(array, ref offset);
      }

      return result;
    }

    /// <summary>
    /// Gets a Hydralized bool array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The bool array value read from the buffer</returns>
    public static bool[] GetBoolArray(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + len)
      {
        bool[] buffer = new bool[len];
        for (int i = 0; i < len; i++)
        {
          buffer[i] = GetBool(array, ref offset);
        }

        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized float array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The float array value read from the buffer</returns>
    public static float[] GetFloatArray(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + sizeof(float) * len)
      {
        float[] buffer = new float[len];
        for (int i = 0; i < len; i++)
        {
          buffer[i] = GetFloat(array, ref offset);
        }

        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized double array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The double array value read from the buffer</returns>
    public static double[] GetDoubleArray(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      if (array.Length >= offset + sizeof(double) * len)
      {
        double[] buffer = new double[len];
        for (int i = 0; i < len; i++)
        {
          buffer[i] = GetDouble(array, ref offset);
        }

        return buffer;
      }

      return null;
    }

    /// <summary>
    /// Gets a Hydralized object array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <param name="context">The context to use when indexing objects</param>
    /// <returns>The Hydralized object info array read from the buffer</returns>
    public static HydralizeInfo[] GetObjectArray(byte[] array, ref int offset, IndexContext context = null)
    {
      UInt32 len = GetUInt32(array, ref offset);
      HydralizeInfo[] infos = new HydralizeInfo[len];
      for (int i = 0; i < len; i++)
      {
        infos[i] = GetObjectInfo(array, ref offset, context);
      }

      return infos;
    }

    /// <summary>
    /// Gets a Hydralized DateTime array from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the Hydralized value</param>
    /// <param name="offset">The byte offset to start reading</param>
    /// <returns>The Hydralized DateTime array read from the buffer</returns>
    public static DateTime[] GetDateTimeArray(byte[] array, ref int offset)
    {
      UInt32 len = GetUInt32(array, ref offset);
      DateTime[] dts = new DateTime[len];
      for (int i = 0; i < len; i++)
      {
        dts[i] = GetDateTime(array, ref offset);
      }

      return dts;
    }

    /// <summary>
    /// Gets an array of indexed generic type information from a buffer
    /// </summary>
    /// <param name="array">The buffer containing the indexed generic type information</param>
    /// <param name="offset">The index of the first byte to start reading</param>
    /// <returns>The array of indexed generic type information read from the buffer</returns>
    public static IndexedGenericType[] GetIndexedGenericArgs(byte[] array, ref int offset)
    {
      // Read the array length
      byte len = GetByte(array, ref offset);
      IndexedGenericType[] args = new IndexedGenericType[len];
      for (int i = 0; i < len; ++i)
      {
        args[i] = new IndexedGenericType();
        args[i].Get(array, ref offset);
      }

      return args;
    }

    /// <summary>
    /// Serializes the object into a format understood by the Hydra library
    /// </summary>
    /// <param name="obj">The object to serialize</param>
    /// <param name="lite">If the serialization should be optimized and data should be referenced by indices in place of strings</param>
    /// <param name="indexed">Indicates if the object should be indexed when Hydralized</param>
    /// <param name="context">The context to use when indexing an object, if null the default static context will be used</param>
    /// <returns>The array of serialized bytes that represents the object data</returns>
    public static byte[] Hydralize(IHydralize obj, bool lite = false, bool indexed = false, IndexContext context = null)
    {
      HydralizeInfo info;
      if (indexed)
      {
        info = CreateIndexedInfo(obj, lite, false, context);
      }
      else
      {
        info = new HydralizeInfo(lite);
      }

      obj.GetObjectData(info);
      return info.Hydralize();
    }

    /// <summary>
    /// Attempts to Hydralize an object into an array of bytes
    /// </summary>
    /// <param name="obj">The object to Hydralize</param>
    /// <param name="lite">Indicates if the object should be Hydralized in lite mode reducing the size</param>
    /// <param name="indexed">Indicates if the object should be indexed when Hydralized</param>
    /// <param name="context">The context to use when indexing objects, if null the default static context will be used</param>
    /// <returns>The Hydralize array of bytes</returns>
    public static byte[] Hydralize(object obj, bool lite = false, bool indexed = false, IndexContext context = null)
    {
      HydralizeInfo info = SaveObjectInfo(obj, lite, indexed, context);
      return info.Hydralize();
    }

    /// <summary>
    /// Turns the data in the specified object into an XML string
    /// </summary>
    /// <param name="obj">The object to Hydralize into XML</param>
    /// <param name="lite">Indicates if the object should be Hydralized in lite mode reducing the size</param>
    /// <param name="indexed">Indicates if the object should be indexed when Hydralized</param>
    /// <param name="context">The context to use when indexing objects, if null the default static context will be used</param>
    /// <returns>The Hydralized XML string</returns>
    public static string ToXml(object obj, bool lite = false, bool indexed = false, IndexContext context = null)
    {
      HydralizeInfo info = SaveObjectInfo(obj, lite, indexed, context);
      return info.ToXml();
    }

    /// <summary>
    /// Turns the data in the specified object into a JSON string
    /// </summary>
    /// <param name="obj">The object to Hydralize into JSON</param>
    /// <param name="lite">Indicates if the object should be Hydralized in lite mode to reduce size</param>
    /// <param name="indexed">Indicates if the object should be indexed</param>
    /// <param name="context">The context to use when indexing objects, if null the default static context will be used</param>
    /// <returns>The Hydralized JSON string</returns>
    public static string ToJson(object obj, bool lite = false, bool indexed = false, IndexContext context = null)
    {
      HydralizeInfo info = SaveObjectInfo(obj, lite, indexed, context);
      return info.ToJson(0);
    }

    /// <summary>
    /// Deserializes the array of bytes in the Hydralize format into data for the specified object to load.
    /// </summary>
    /// <param name="obj">The object that will load the values from the object data array.</param>
    /// <param name="buffer">The object data as a serialized array of bytes</param>
    /// <param name="offset">The index at which to begin deserialization in the buffer.</param>
    /// <param name="context">The context to use for indexed objects</param>
    public static void Dehydralize(IHydralize obj, byte[] buffer, ref int offset, IndexContext context = null)
    {
      HydralizeInfo info = new HydralizeInfo(context);
      info.Dehydralize(buffer, ref offset);
      obj.LoadObjectData(info);
    }

    /// <summary>
    /// Deserializes the array of bytes in the Hydralize format into data for the specified object to load.
    /// </summary>
    /// <param name="obj">The object that will load the values from the object data array.</param>
    /// <param name="buffer">The object data as a serialized array of bytes</param>
    /// <param name="offset">The index at which to begin deserialization in the buffer.</param>
    /// <param name="context">The context to use for indexed objects</param>
    public static void Dehydralize(ref object obj, byte[] buffer, ref int offset, IndexContext context = null)
    {
      HydralizeInfo info = new HydralizeInfo(context);
      info.Dehydralize(buffer, ref offset);
      LoadObjectInfo(ref obj, info);
    }

    /// <summary>
    /// Deserializes the array of bytes in the Hydralize format into data for the specified object to load.
    /// </summary>
    /// <param name="obj">The object that will load the values from the object data array.</param>
    /// <param name="buffer">The object data as a serialized array of bytes</param>
    /// <param name="offset">The index at which to begin deserialization in the buffer.</param>
    /// <param name="context">The context to use for indexed objects</param>
    public static void Dehydralize(object obj, byte[] buffer, ref int offset, IndexContext context = null)
    {
      HydralizeInfo info = new HydralizeInfo(context);
      info.Dehydralize(buffer, ref offset);
      LoadObjectInfo(ref obj, info);
    }

    /// <summary>
    /// Attempts to automatically deserialize a Hydra object from the buffer using indexed data
    /// </summary>
    /// <param name="buffer">The raw byte buffer to load the object data from</param>
    /// <param name="offset">The index at which to begin deserialization</param>
    /// <param name="context">The context to use when indexing objects</param>
    /// <returns>The Hydra object deserialized or null if the object wasn't determined</returns>
    public static object Dehydralize(byte[] buffer, ref int offset, IndexContext context = null)
    {
      HydralizeInfo info = new HydralizeInfo(context);
      info.Dehydralize(buffer, ref offset);

      return Dehydralize(info);
    }

    /// <summary>
    /// Attempts to dehydralize the indexed object info into a new object instance
    /// </summary>
    /// <param name="info">The indexed object info</param>
    /// <returns>The dehydralized object or null if the info was not indexed</returns>
    public static object Dehydralize(HydralizeInfo info)
    {
      if (info.Primitive)
      {
        return info.GetRaw(0);
      }
      else if (info.Direct)
      {
        switch (info.GetHydraType(0))
        {
          case HydraTypeCodes.ARRAY_OBJECT:
          case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
          {
            return info.GetObjArray<object>(0);
          }
        }
      }
      else if (info.Indexed)
      {
        object obj = null;
        IndexContext context = info.Context ?? ms_assemblyContext;
        if (info.Generic)
        {
          obj = Activator.CreateInstance(info.GetGenericType());
        }
        else
        {
          obj = context.Instantiate(info.AssemblyId, info.TypeIndex);
        }

        if (obj != null)
        {
          LoadObjectInfo(ref obj, info);
          return obj;
        }
      }

      return null;
    }

    /// <summary>
    /// Dehydralizes an array of indexed <see cref="HydralizeInfo"/> objects into an array of dehydralized objects
    /// </summary>
    /// <param name="infos">The array of indexed object info to dehydralize</param>
    /// <param name="context">The index context to use when dehydralizing indexed objects</param>
    /// <returns>An array of dehydralized objects</returns>
    public static object[] Dehydralize(HydralizeInfo[] infos, IndexContext context = null)
    {
      if (infos == null)
      {
        return null;
      }

      if (context == null)
      {
        context = ms_assemblyContext;
      }

      object[] result = new object[infos.Length];
      for (int i = 0; i < infos.Length; i++)
      {
        result[i] = Dehydralize(infos[i]);
      }

      return result;
    }

    /// <summary>
    /// Deserializes the XML string in the Hydralize format into data for the specified object to load.
    /// </summary>
    /// <param name="obj">The object that will load the values from the object data array.</param>
    /// <param name="xml">The XML string describing the object data</param>
    /// <param name="context">The context to use for indexed objects</param>
    public static void FromXml(object obj, string xml, IndexContext context = null)
    {
      HydralizeInfo info = new HydralizeInfo(context);
      info.FromXml(xml);
      IHydralize hydraObj = obj as IHydralize;
      if (hydraObj != null)
      {
        hydraObj.LoadObjectData(info);
      }
      else
      {
        LoadObjectInfo(ref obj, info);
      }
    }

    /// <summary>
    /// Creates an indexed <see cref="HydralizeInfo"/> object from the Hydralizeable object specified with the specified index context
    /// </summary>
    /// <param name="obj">The object to hydralize</param>
    /// <param name="lite">If true the object is hydralized in lite mode</param>
    /// <param name="direct">Indicates if the object is a directly hydralized value instead of an object</param>
    /// <param name="context">The context to use when indexing</param>
    /// <returns>The object describing the Hydralized object</returns>
    public static HydralizeInfo CreateIndexedInfo(object obj, bool lite, bool direct, IndexContext context = null)
    {
      if (direct)
      {
        return new HydralizeInfo(lite, 0, 0, direct, context);
      }

      if (context == null)
      {
        context = ms_assemblyContext;
      }

      if (context.Index(obj, out byte assemblyId, out int typeId))
      {
        HydralizeInfo info = new HydralizeInfo(lite, assemblyId, typeId, direct, context);
        Type baseType = obj.GetType();

        if (baseType.IsGenericType)
        {
          info.SetGenericArgs(obj.GetType());
        }

        return info;
      }

      return new HydralizeInfo(lite, false, false, context);
    }

    /// <summary>
    /// Loads the Hydralized object info into the specified object
    /// </summary>
    /// <param name="obj">An instance of the object to load the values into</param>
    /// <param name="info">The object info values to load</param>
    public static void LoadObjectInfo(ref object obj, HydralizeInfo info)
    {
      if (obj != null)
      {
        Type objType = obj.GetType();
        IHydralizeConverter converter = LookupConverter(objType);
        if (converter != null)
        {
          converter.Dehydralize(obj, ref info);
          return;
        }
      }

      IHydralize hydralize = obj as IHydralize;
      if (hydralize != null)
      {
        hydralize.LoadObjectData(info);
      }
      else
      {
        LoadObjectInfo(ref obj, info, null, info.Context);
      }
    }

    /// <summary>
    /// Attempts to Hydralize an object into a <see cref="HydralizeInfo"/> instance describing the object
    /// </summary>
    /// <param name="obj">The object to Hydralize</param>
    /// <param name="lite">Indicates if the object should be Hydralized in lite mode reducing the size</param>
    /// <param name="indexed">Indicates if the object should be indexed when Hydralized</param>
    /// <param name="context">The context to use when indexing objects, if null the default static context will be used</param>
    /// <returns>The Hydralize info for the object</returns>
    public static HydralizeInfo SaveObjectInfo(object obj, bool lite = false, bool indexed = false, IndexContext context = null)
    {
      HydralizeInfo info;
      bool primitive = false;
      bool direct = false;
      HydraTypeCodes hydraTypeCode = HydraTypeCodes.INVALID;

      if (obj != null)
      {
        hydraTypeCode = HydraType.DetermineType(obj);
        direct = (hydraTypeCode != HydraTypeCodes.OBJECT && hydraTypeCode != HydraTypeCodes.HYDRA_OBJECT);
        primitive = HydraType.IsPrimitiveType(hydraTypeCode);
      }
      else
      {
        // directly null
        info = new HydralizeInfo(lite, true, true, context);
        info.AddInfo("0", (object)null, typeof(object));
        return info;
      }

      // If primitive only carry indexed flag for consistency
      if (!primitive && indexed)
      {
        info = CreateIndexedInfo(obj, lite, direct, context);
      }
      else
      {
        info = new HydralizeInfo(lite, primitive, direct, context);
      }

      if (primitive || direct)
      {
        info.AddInfo("0", obj);
      }
      else
      {
        if (obj != null)
        {
          Type objType = obj.GetType();
          IHydralizeConverter converter = LookupConverter(objType);
          if (converter != null)
          {
            converter.Hydralize(obj, ref info);
            return info;
          }
        }

        IHydralize hydralize = obj as IHydralize;
        if (hydralize != null)
        {
          hydralize.GetObjectData(info);
        }
        else if (obj != null)
        {
          SaveObjectInfo(obj, info);
        }
      }

      return info;
    }

    /// <summary>
    /// Saves this object's fields by reflection into the info object
    /// </summary>
    /// <param name="obj">The object to save the info for</param>
    /// <param name="info">The object where this object's data will be stored.</param>
    /// <param name="callerType">The type of object data to store.</param>
    public static void SaveObjectInfo(object obj, HydralizeInfo info, Type callerType = null)
    {
      Type self = callerType ?? obj.GetType();
      if (self.BaseType != null)
      {
        SaveObjectInfo(obj, info, self.BaseType);
      }

      FieldInfo[] myFields = self.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
      foreach (FieldInfo field in myFields)
      {
        // Skip constants and delegates
        if (!field.IsLiteral && !typeof(Delegate).IsAssignableFrom(field.FieldType))
        {
          if (field.DeclaringType != self)
          {
            // Once you hit base class fields then we're done
            break;
          }

          object[] attribs = field.GetCustomAttributes(false);
          bool isHydralizeable = true;
          for (int i = 0; i < attribs.Length; i++)
          {
            if ((attribs[i] as NonHydralizeableFieldAttribute) != null)
            {
              isHydralizeable = false;
              break;
            }
          }

          if (isHydralizeable)
          {
            object val = field.GetValue(obj);
            info.AddInfo(field.Name, val, field.FieldType);
          }
        }
      }
    }

    /// <summary>
    /// Loads this object data by reflection from the info object.
    /// </summary>
    /// <param name="obj">The object to load the Hydralize object info into</param>
    /// <param name="info">The object containing this instance's data</param>
    /// <param name="callerType">The type of data to get from the info</param>
    /// <param name="context">The index context to use when dehydralizing indexed objects</param>
    /// <returns>The first unretrieved data index.</returns>
    public static int LoadObjectInfo(ref object obj, HydralizeInfo info, Type callerType, IndexContext context)
    {
      int idx = 0;
      Type self = callerType ?? obj.GetType();
      if (self.BaseType != null)
      {
        idx = LoadObjectInfo(ref obj, info, self.BaseType, context);
      }

      FieldInfo[] myFields = self.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
      for (int i = 0; i < myFields.Length; i++)
      {
        // Skip constants and delegates
        if (myFields[i].IsLiteral || typeof(Delegate).IsAssignableFrom(myFields[i].FieldType))
        {
          continue;
        }

        if (myFields[i].DeclaringType != self)
        {
          // Once you hit base class fields, we're done loading
          break;
        }

        object[] attribs = myFields[i].GetCustomAttributes(false);
        bool isHydralizeable = true;
        for (int j = 0; j < attribs.Length; j++)
        {
          if ((attribs[j] as NonHydralizeableFieldAttribute) != null)
          {
            isHydralizeable = false;
            break;
          }
        }

        if (isHydralizeable)
        {
          try
          {
            object val = info.GetRaw(myFields[i].Name, idx);

            if (val != null)
            {
              HydraTypeCodes hydraType = HydraType.DetermineType(myFields[i].FieldType);
              switch (hydraType)
              {
                case HydraTypeCodes.HYDRA_OBJECT:
                {
                  if (((HydralizeInfo)val).Indexed)
                  {
                    object result = Dehydralize((HydralizeInfo)val);
                    myFields[i].SetValue(obj, result);
                  }
                  else
                  {
                    MethodInfo method = typeof(HydralizeInfo).GetMethod("GetObj", BindingFlags.Instance | BindingFlags.Public, null, new Type[] { typeof(string), typeof(int) }, null);
                    if (method != null)
                    {
                      object result = method.MakeGenericMethod(myFields[i].FieldType).Invoke(info, new object[] { myFields[i].Name, idx });
                      myFields[i].SetValue(obj, result);
                    }
                  }

                  break;
                }

                case HydraTypeCodes.OBJECT:
                {
                  HydralizeInfo objectInfo = val as HydralizeInfo;
                  if (objectInfo != null)
                  {
                    if (objectInfo.Indexed)
                    {
                      object result = Dehydralize((HydralizeInfo)val);
                      myFields[i].SetValue(obj, result);
                    }
                    else
                    {
                      object result = Activator.CreateInstance(myFields[i].FieldType);
                      LoadObjectInfo(ref result, objectInfo);
                      myFields[i].SetValue(obj, result);
                    }
                  }
                  else
                  {
                    // Most likely a primitive
                    myFields[i].SetValue(obj, val);
                  }
                }
                break;

                case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
                {
                  if (info.Indexed)
                  {
                    object[] result = Dehydralize((HydralizeInfo[])val, context);
                    Array array = Array.CreateInstance(myFields[i].FieldType.GetElementType(), result.Length);
                    for (int element = 0; element < array.Length; element++)
                    {
                      array.SetValue(result[element], element);
                    }

                    myFields[i].SetValue(obj, array);
                  }
                  else
                  {
                    MethodInfo method = typeof(HydralizeInfo).GetMethod("GetObjArray", BindingFlags.Instance | BindingFlags.Public, null, new Type[] { typeof(string), typeof(int) }, null);
                    if (method != null)
                    {
                      object result = method.MakeGenericMethod(myFields[i].FieldType.GetElementType()).Invoke(info, new object[] { myFields[i].Name, idx });
                      myFields[i].SetValue(obj, result);
                    }
                  }
                  break;
                }

                case HydraTypeCodes.ARRAY_OBJECT:
                {
                  if (info.Indexed)
                  {
                    object[] result = Dehydralize((HydralizeInfo[])val, context);
                    Array array = Array.CreateInstance(myFields[i].FieldType.GetElementType(), result.Length);
                    for (int element = 0; element < array.Length; element++)
                    {
                      array.SetValue(result[element], element);
                    }

                    myFields[i].SetValue(obj, array);
                  }
                  else
                  {
                    HydralizeInfo[] infos = (HydralizeInfo[])val;
                    Array array = Array.CreateInstance(myFields[i].FieldType.GetElementType(), infos.Length);
                    for (int element = 0; element < array.Length; element++)
                    {
                      if (infos[element].Direct || infos[element].Primitive)
                      {
                        object rawValue = Dehydralize(infos[element]);
                        if (rawValue != null)
                        {
                          if (myFields[i].FieldType.GetElementType().IsAssignableFrom(rawValue.GetType()))
                          {
                            array.SetValue(rawValue, element);
                          }
                          else
                          {
                            array.SetValue(Convert.ChangeType(rawValue, myFields[i].FieldType.GetElementType()), element);
                          }
                        }
                      }
                      else
                      {
                        object temp = Activator.CreateInstance(myFields[i].FieldType.GetElementType());
                        LoadObjectInfo(ref temp, infos[element]);
                        array.SetValue(temp, element);
                      }
                    }

                    myFields[i].SetValue(obj, array);
                  }
                }
                break;

                case HydraTypeCodes.ENUM:
                case HydraTypeCodes.ARRAY_ENUM:
                {
                  // This should never happen
                  myFields[i].SetValue(obj, val);
                }
                break;

                default:
                {
                  if (info.GetHydraType(myFields[i].Name, idx) != hydraType)
                  {
                    myFields[i].SetValue(obj, Convert.ChangeType(val, myFields[i].FieldType.GetElementType()));
                  }
                  else
                  {
                    myFields[i].SetValue(obj, val);
                  }
                }
                break;
              }
            }
            else if (info.IsNullType(myFields[i].Name, idx))
            {
              myFields[i].SetValue(obj, val);
            }
          }
          catch (Exception ex)
          {
            Console.WriteLine("Unable to load value for field {0}", myFields[i].Name);
            Console.WriteLine("Exception: {0}", ex.Message);
          }
          finally
          {
            ++idx;
          }
        }
      }

      return idx;
    }

    /// <summary>
    /// Gets the collection of Hydralizeable fields for the specified type
    /// </summary>
    /// <param name="callerType">The ancestor type to get fields for, if null the most derived type of the object is used</param>
    /// <returns>The array of Hydralizeable fields</returns>
    public static FieldInfo[] GetHydralizeableFields(Type callerType)
    {
      List<FieldInfo> fields = new List<FieldInfo>();
      if (callerType.BaseType != null)
      {
        fields.AddRange(GetHydralizeableFields(callerType.BaseType));
      }

      FieldInfo[] myFields = callerType.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
      foreach (FieldInfo field in myFields)
      {
        if (!field.IsLiteral)
        {
          object[] attribs = field.GetCustomAttributes(false);
          bool isHydralizeable = true;
          for (int i = 0; i < attribs.Length; i++)
          {
            if ((attribs[i] as NonHydralizeableFieldAttribute) != null)
            {
              isHydralizeable = false;
              break;
            }
          }

          if (isHydralizeable)
          {
            fields.Add(field);
          }
        }
      }

      return fields.ToArray();
    }

    /// <summary>
    /// Puts a typecode into an array
    /// </summary>
    /// <param name="array">The array to put the type byte into</param>
    /// <param name="type">The type to put in the array</param>
    /// <param name="offset">The offset of the byte in the array where the type will be put</param>
    private static void Put(byte[] array, HydraTypeCodes type, ref int offset)
    {
      if (array.Length >= offset + sizeof(HydraTypeCodes))
      {
        array[offset++] = (byte)type;
      }
    }
  }
}
