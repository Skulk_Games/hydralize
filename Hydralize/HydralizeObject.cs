#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Skulk.Hydralize
{
  /// <summary>
  /// A base class that uses reflection to provide a hydralizeable interface to all fields within any derived objects
  /// </summary>
  public class HydralizeObject : IHydralize
  {
    /// <summary>
    /// Creates an object map using this object
    /// </summary>
    /// <returns>An object map describing this instance type</returns>
    public ObjectMap GetObjectMap()
    {
      return new ObjectMap(GetType());
    }

    /// <inheritdoc />
    public void GetObjectData(HydralizeInfo info)
    {
      PreGetObject();
      Hydralizer.SaveObjectInfo(this, info, null);
    }

    /// <inheritdoc />
    public void LoadObjectData(HydralizeInfo info)
    {
      object obj = this;
      Hydralizer.LoadObjectInfo(ref obj, info, null, info.Context);
      PostLoadObject();
    }

    /// <summary>
    /// Method invoked just before this object's data is Hydralized
    /// </summary>
    protected virtual void PreGetObject()
    {
      // Empty
    }

    /// <summary>
    /// Method invoked after this object has been loaded frome Hydralizeable data
    /// </summary>
    protected virtual void PostLoadObject()
    {
      // Empty
    }
  }
}
