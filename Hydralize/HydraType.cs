#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Utility class for interacting with <see cref="HydraTypeCodes"/>.
  /// </summary>
  public static class HydraType
  {
    /// <summary>
    /// Determines the <see cref="Type"/> used to represent an object of the <see cref="HydraTypeCodes"/> value in a <see cref="HydralizeData"/> object
    /// </summary>
    /// <param name="hydraType">The Hydra type code</param>
    /// <returns>The system type of the object stored in a <see cref="HydralizeData"/> to represent the specified Hydra type code</returns>
    public static Type DetermineType(HydraTypeCodes hydraType)
    {
      switch (hydraType)
      {
        case HydraTypeCodes.NULL_ARRAY_BOOL:
        case HydraTypeCodes.ARRAY_BOOL:
          return typeof(bool[]);

        case HydraTypeCodes.NULL_ARRAY_DOUBLE:
        case HydraTypeCodes.ARRAY_DOUBLE:
          return typeof(double[]);

        case HydraTypeCodes.NULL_ARRAY_ENUM:
        case HydraTypeCodes.ARRAY_ENUM:
          return typeof(Enum[]);

        case HydraTypeCodes.NULL_ARRAY_FLOAT:
        case HydraTypeCodes.ARRAY_FLOAT:
          return typeof(float[]);

        case HydraTypeCodes.NULL_ARRAY_OBJECT:
        case HydraTypeCodes.ARRAY_OBJECT:
        case HydraTypeCodes.NULL_ARRAY_HYDRA_OBJECT:
        case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
          return typeof(HydralizeInfo[]);

        case HydraTypeCodes.NULL_ARRAY_INT16:
        case HydraTypeCodes.ARRAY_INT16:
          return typeof(Int16[]);

        case HydraTypeCodes.NULL_ARRAY_INT32:
        case HydraTypeCodes.ARRAY_INT32:
          return typeof(Int32[]);

        case HydraTypeCodes.NULL_ARRAY_INT64:
        case HydraTypeCodes.ARRAY_INT64:
          return typeof(Int64[]);

        case HydraTypeCodes.NULL_ARRAY_INT8:
        case HydraTypeCodes.ARRAY_INT8:
          return typeof(sbyte[]);

        case HydraTypeCodes.NULL_ARRAY_STRING:
        case HydraTypeCodes.ARRAY_STRING:
          return typeof(string[]);

        case HydraTypeCodes.NULL_ARRAY_UINT16:
        case HydraTypeCodes.ARRAY_UINT16:
          return typeof(UInt16[]);

        case HydraTypeCodes.NULL_ARRAY_UINT32:
        case HydraTypeCodes.ARRAY_UINT32:
          return typeof(UInt32[]);

        case HydraTypeCodes.NULL_ARRAY_UINT64:
        case HydraTypeCodes.ARRAY_UINT64:
          return typeof(UInt64[]);

        case HydraTypeCodes.NULL_ARRAY_UINT8:
        case HydraTypeCodes.ARRAY_UINT8:
          return typeof(byte[]);

        case HydraTypeCodes.BOOL:
          return typeof(bool);

        case HydraTypeCodes.DOUBLE:
          return typeof(double);

        case HydraTypeCodes.ENUM:
          return typeof(Enum);

        case HydraTypeCodes.FLOAT:
          return typeof(float);

        case HydraTypeCodes.NULL_OBJECT:
        case HydraTypeCodes.OBJECT:
        case HydraTypeCodes.NULL_HYDRA_OBJECT:
        case HydraTypeCodes.HYDRA_OBJECT:
          return typeof(HydralizeInfo);

        case HydraTypeCodes.INT16:
          return typeof(Int16);

        case HydraTypeCodes.INT32:
          return typeof(Int32);

        case HydraTypeCodes.INT64:
          return typeof(Int64);

        case HydraTypeCodes.INT8:
          return typeof(sbyte);

        case HydraTypeCodes.INVALID:
          return null;

        case HydraTypeCodes.NULL_STRING:
        case HydraTypeCodes.STRING:
          return typeof(string);

        case HydraTypeCodes.UINT16:
          return typeof(UInt16);

        case HydraTypeCodes.UINT32:
          return typeof(UInt32);

        case HydraTypeCodes.UINT64:
          return typeof(UInt64);

        case HydraTypeCodes.UINT8:
          return typeof(byte);

        case HydraTypeCodes.DATETIME:
          return typeof(DateTime);

        case HydraTypeCodes.ARRAY_DATETIME:
          return typeof(DateTime[]);

        case HydraTypeCodes.CHAR:
          return typeof(char);

        case HydraTypeCodes.ARRAY_CHAR:
          return typeof(char[]);

        default:
          return null;
      }
    }

    /// <summary>
    /// Determines the <see cref="HydraTypeCodes"/> of the indicated type
    /// </summary>
    /// <param name="dataType">The type to determine the Hydra type for</param>
    /// <returns>The Hydra type code of the system type</returns>
    public static HydraTypeCodes DetermineType(Type dataType)
    {
      TypeCode type = Type.GetTypeCode(dataType);
      switch (type)
      {
        case TypeCode.Boolean:
          return HydraTypeCodes.BOOL;

        case TypeCode.Byte:
          return HydraTypeCodes.UINT8;

        case TypeCode.Char:
          return HydraTypeCodes.CHAR;

        case TypeCode.DateTime:
          return HydraTypeCodes.DATETIME;

        case TypeCode.DBNull:
          return HydraTypeCodes.INVALID;

        case TypeCode.Decimal:
          return HydraTypeCodes.INVALID;

        case TypeCode.Double:
          return HydraTypeCodes.DOUBLE;

        case TypeCode.Empty:
          return HydraTypeCodes.INVALID;

        case TypeCode.Int16:
          return HydraTypeCodes.INT16;

        case TypeCode.Int32:
          return HydraTypeCodes.INT32;

        case TypeCode.Int64:
          return HydraTypeCodes.INT64;

        case TypeCode.Object:
        {
          if (typeof(IHydralize).IsAssignableFrom(dataType) || typeof(HydralizeInfo).IsAssignableFrom(dataType))
          {
            return HydraTypeCodes.HYDRA_OBJECT;
          }

          if (dataType.HasElementType)
          {
            return ToArrayType(DetermineType(dataType.GetElementType()));
          }

          if (typeof(object[]).IsAssignableFrom(dataType))
          {
            return HydraTypeCodes.ARRAY_OBJECT;
          }
          else
          {
            return HydraTypeCodes.OBJECT;
          }
        }

        case TypeCode.SByte:
          return HydraTypeCodes.INT8;

        case TypeCode.Single:
          return HydraTypeCodes.FLOAT;

        case TypeCode.String:
          return HydraTypeCodes.STRING;

        case TypeCode.UInt16:
        {
          return HydraTypeCodes.UINT16;
        }

        case TypeCode.UInt32:
          return HydraTypeCodes.UINT32;

        case TypeCode.UInt64:
          return HydraTypeCodes.UINT64;

        default:
          return HydraTypeCodes.INVALID;
      }
    }

    /// <summary>
    /// Determines the <see cref="HydraTypeCodes"/> of the specified object
    /// </summary>
    /// <param name="data">The object to determine the type of</param>
    /// <returns>The corresponding <see cref="HydraTypeCodes"/> of the object</returns>
    public static HydraTypeCodes DetermineType(object data)
    {
      if (data == null)
      {
        return HydraTypeCodes.INVALID;
      }
      else
      {
        return DetermineType(data.GetType());
      }
    }

    /// <summary>
    /// Translates the element type into the corresponding array type
    /// </summary>
    /// <param name="typeCode">The element type to translate</param>
    /// <returns>The corresponding array type</returns>
    public static HydraTypeCodes ToArrayType(HydraTypeCodes typeCode)
    {
      switch (typeCode)
      {
        case HydraTypeCodes.BOOL:
          return HydraTypeCodes.ARRAY_BOOL;

        case HydraTypeCodes.DOUBLE:
          return HydraTypeCodes.ARRAY_DOUBLE;

        case HydraTypeCodes.ENUM:
          return HydraTypeCodes.ARRAY_ENUM;

        case HydraTypeCodes.FLOAT:
          return HydraTypeCodes.ARRAY_FLOAT;

        case HydraTypeCodes.HYDRA_OBJECT:
          return HydraTypeCodes.ARRAY_HYDRA_OBJECT;

        case HydraTypeCodes.OBJECT:
          return HydraTypeCodes.ARRAY_OBJECT;

        case HydraTypeCodes.INT8:
          return HydraTypeCodes.ARRAY_INT8;

        case HydraTypeCodes.INT16:
          return HydraTypeCodes.ARRAY_INT16;

        case HydraTypeCodes.INT32:
          return HydraTypeCodes.ARRAY_INT32;

        case HydraTypeCodes.INT64:
          return HydraTypeCodes.ARRAY_INT64;

        case HydraTypeCodes.STRING:
          return HydraTypeCodes.ARRAY_STRING;

        case HydraTypeCodes.UINT16:
          return HydraTypeCodes.ARRAY_UINT16;

        case HydraTypeCodes.CHAR:
          return HydraTypeCodes.ARRAY_CHAR;

        case HydraTypeCodes.UINT32:
          return HydraTypeCodes.ARRAY_UINT32;

        case HydraTypeCodes.UINT64:
          return HydraTypeCodes.ARRAY_UINT64;

        case HydraTypeCodes.UINT8:
          return HydraTypeCodes.ARRAY_UINT8;

        case HydraTypeCodes.DATETIME:
          return HydraTypeCodes.ARRAY_DATETIME;

        default:
          return HydraTypeCodes.INVALID;
      }
    }

    /// <summary>
    /// Translates the element type into the corresponding element type
    /// </summary>
    /// <param name="arrayType">The array element type to translate</param>
    /// <returns>The corresponding element type</returns>
    public static HydraTypeCodes FromArrayType(HydraTypeCodes arrayType)
    {
      switch (arrayType)
      {
        case HydraTypeCodes.ARRAY_BOOL:
          return HydraTypeCodes.BOOL;

        case HydraTypeCodes.ARRAY_DOUBLE:
          return HydraTypeCodes.DOUBLE;

        case HydraTypeCodes.ARRAY_ENUM:
          return HydraTypeCodes.ENUM;

        case HydraTypeCodes.ARRAY_FLOAT:
          return HydraTypeCodes.FLOAT;

        case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
          return HydraTypeCodes.HYDRA_OBJECT;

        case HydraTypeCodes.ARRAY_OBJECT:
          return HydraTypeCodes.OBJECT;

        case HydraTypeCodes.ARRAY_INT8:
          return HydraTypeCodes.INT8;

        case HydraTypeCodes.ARRAY_INT16:
          return HydraTypeCodes.INT16;

        case HydraTypeCodes.ARRAY_INT32:
          return HydraTypeCodes.INT32;

        case HydraTypeCodes.ARRAY_INT64:
          return HydraTypeCodes.INT64;

        case HydraTypeCodes.ARRAY_STRING:
          return HydraTypeCodes.STRING;

        case HydraTypeCodes.ARRAY_UINT16:
          return HydraTypeCodes.UINT16;

        case HydraTypeCodes.ARRAY_CHAR:
          return HydraTypeCodes.CHAR;

        case HydraTypeCodes.ARRAY_UINT32:
          return HydraTypeCodes.UINT32;

        case HydraTypeCodes.ARRAY_UINT64:
          return HydraTypeCodes.UINT64;

        case HydraTypeCodes.ARRAY_UINT8:
          return HydraTypeCodes.UINT8;

        case HydraTypeCodes.ARRAY_DATETIME:
          return HydraTypeCodes.DATETIME;

        default:
          return HydraTypeCodes.INVALID;
      }
    }

    /// <summary>
    /// Determines if the specified <see cref="HydraTypeCodes"/> value represents an array.
    /// </summary>
    /// <param name="hydraType">The typecode to check.</param>
    /// <returns>True if the typecode is an array type, false otherwise.</returns>
    public static bool IsArrayType(HydraTypeCodes hydraType)
    {
      return FromArrayType(hydraType) != HydraTypeCodes.INVALID;
    }

    /// <summary>
    /// Determines if the specified <see cref="HydraTypeCodes"/> value indicates a primitive type from a hydralizeable perspective.
    /// </summary>
    /// <param name="hydraType">The type to determine if it is a hydralizeable primitive.</param>
    /// <returns>True if the specified type is a hydralizeable primitive, false otherwise.</returns>
    public static bool IsPrimitiveType(HydraTypeCodes hydraType)
    {
      switch (hydraType)
      {
        case HydraTypeCodes.ARRAY_BOOL:
        case HydraTypeCodes.BOOL:
        case HydraTypeCodes.ARRAY_CHAR:
        case HydraTypeCodes.CHAR:
        case HydraTypeCodes.ARRAY_DATETIME:
        case HydraTypeCodes.DATETIME:
        case HydraTypeCodes.ARRAY_DOUBLE:
        case HydraTypeCodes.DOUBLE:
        case HydraTypeCodes.ARRAY_ENUM:
        case HydraTypeCodes.ENUM:
        case HydraTypeCodes.ARRAY_FLOAT:
        case HydraTypeCodes.FLOAT:
        case HydraTypeCodes.ARRAY_INT16:
        case HydraTypeCodes.INT16:
        case HydraTypeCodes.ARRAY_INT32:
        case HydraTypeCodes.INT32:
        case HydraTypeCodes.ARRAY_INT64:
        case HydraTypeCodes.INT64:
        case HydraTypeCodes.ARRAY_INT8:
        case HydraTypeCodes.INT8:
        case HydraTypeCodes.ARRAY_STRING:
        case HydraTypeCodes.STRING:
        case HydraTypeCodes.ARRAY_UINT16:
        case HydraTypeCodes.UINT16:
        case HydraTypeCodes.ARRAY_UINT32:
        case HydraTypeCodes.UINT32:
        case HydraTypeCodes.ARRAY_UINT64:
        case HydraTypeCodes.UINT64:
        case HydraTypeCodes.ARRAY_UINT8:
        case HydraTypeCodes.UINT8:
        {
          return true;
        }
      }

      return false;
    }
  }

  /// <summary>
  /// The data types that Hydralization supports
  /// </summary>
  public enum HydraTypeCodes : byte
  {
    /// <summary>
    /// Indicates and invalid unsupported data type.
    /// </summary>
    INVALID,

    /// <summary>
    /// Unsigned byte
    /// </summary>
    UINT8,

    /// <summary>
    /// Unsigned 16 bit value
    /// </summary>
    UINT16,

    /// <summary>
    /// Unsigned 32 bit value
    /// </summary>
    UINT32,

    /// <summary>
    /// Unsigned 64 bit value
    /// </summary>
    UINT64,

    /// <summary>
    /// Signed byte
    /// </summary>
    INT8,

    /// <summary>
    /// Signed 16 bit value
    /// </summary>
    INT16,

    /// <summary>
    /// Signed 32 bit value
    /// </summary>
    INT32,

    /// <summary>
    /// Signed 64 bit value
    /// </summary>
    INT64,

    /// <summary>
    /// String
    /// </summary>
    STRING,

    /// <summary>
    /// Boolean
    /// </summary>
    BOOL,

    /// <summary>
    /// 32 bit floating point value
    /// </summary>
    FLOAT,

    /// <summary>
    /// 64 bit double precision floating point value
    /// </summary>
    DOUBLE,

    /// <summary>
    /// Enum value
    /// </summary>
    ENUM,

    /// <summary>
    /// A Hydralizeable object
    /// </summary>
    HYDRA_OBJECT,

    /// <summary>
    /// A generic object
    /// </summary>
    OBJECT,

    /// <summary>
    /// A datetime value
    /// </summary>
    DATETIME,

    /// <summary>
    /// An array of unsigned bytes
    /// </summary>
    ARRAY_UINT8,

    /// <summary>
    /// An array of unsigned 16 bit values
    /// </summary>
    ARRAY_UINT16,

    /// <summary>
    /// An array of unsigned 32 bit values
    /// </summary>
    ARRAY_UINT32,

    /// <summary>
    /// An array of unsigned 64 bit values
    /// </summary>
    ARRAY_UINT64,

    /// <summary>
    /// An array of signed bytes
    /// </summary>
    ARRAY_INT8,

    /// <summary>
    /// An array of signed 16 bit values
    /// </summary>
    ARRAY_INT16,

    /// <summary>
    /// An array of signed 32 bit values
    /// </summary>
    ARRAY_INT32,

    /// <summary>
    /// An array of signed 64 bit values
    /// </summary>
    ARRAY_INT64,

    /// <summary>
    /// An array of strings
    /// </summary>
    ARRAY_STRING,

    /// <summary>
    /// An array of boolean values
    /// </summary>
    ARRAY_BOOL,

    /// <summary>
    /// An array of floating point values
    /// </summary>
    ARRAY_FLOAT,

    /// <summary>
    /// An array of double precision floating point values
    /// </summary>
    ARRAY_DOUBLE,

    /// <summary>
    /// An array of hydralizeable objects
    /// </summary>
    ARRAY_HYDRA_OBJECT,

    /// <summary>
    /// An array of generic objects
    /// </summary>
    ARRAY_OBJECT,

    /// <summary>
    /// An array of enum values
    /// </summary>
    ARRAY_ENUM,

    /// <summary>
    /// An array of datetime values
    /// </summary>
    ARRAY_DATETIME,

    /// <summary>
    /// A char value
    /// </summary>
    CHAR,

    /// <summary>
    /// An array of char values
    /// </summary>
    ARRAY_CHAR,

    /// <summary>
    /// The null flag bit that indicates the type holds a null value
    /// </summary>
    NULL_FLAG = 0x80,

    /// <summary>
    /// A string type with a null value
    /// </summary>
    NULL_STRING = STRING | NULL_FLAG,

    /// <summary>
    /// A Hydra object with a null value
    /// </summary>
    NULL_HYDRA_OBJECT = HYDRA_OBJECT | NULL_FLAG,

    /// <summary>
    /// An object with a null value
    /// </summary>
    NULL_OBJECT = OBJECT | NULL_FLAG,

    /// <summary>
    /// A byte array with a null value
    /// </summary>
    NULL_ARRAY_UINT8 = ARRAY_UINT8 | NULL_FLAG,

    /// <summary>
    /// A UInt16 array with a null value
    /// </summary>
    NULL_ARRAY_UINT16 = ARRAY_UINT16 | NULL_FLAG,

    /// <summary>
    /// A UInt32 array with a null value
    /// </summary>
    NULL_ARRAY_UINT32 = ARRAY_UINT32 | NULL_FLAG,

    /// <summary>
    /// A UInt64 array with a null value
    /// </summary>
    NULL_ARRAY_UINT64 = ARRAY_UINT64 | NULL_FLAG,

    /// <summary>
    /// A signed byte array with a null value
    /// </summary>
    NULL_ARRAY_INT8 = ARRAY_INT8 | NULL_FLAG,

    /// <summary>
    /// An Int16 array with a null value
    /// </summary>
    NULL_ARRAY_INT16 = ARRAY_INT16 | NULL_FLAG,

    /// <summary>
    /// An Int32 array with a null value
    /// </summary>
    NULL_ARRAY_INT32 = ARRAY_INT32 | NULL_FLAG,

    /// <summary>
    /// An Int64 array with a null value
    /// </summary>
    NULL_ARRAY_INT64 = ARRAY_INT64 | NULL_FLAG,

    /// <summary>
    /// A string array with a null value
    /// </summary>
    NULL_ARRAY_STRING = ARRAY_STRING | NULL_FLAG,

    /// <summary>
    /// A boolean array with a null value
    /// </summary>
    NULL_ARRAY_BOOL = ARRAY_BOOL | NULL_FLAG,

    /// <summary>
    /// A float array with a null value
    /// </summary>
    NULL_ARRAY_FLOAT = ARRAY_FLOAT | NULL_FLAG,

    /// <summary>
    /// A double array with a null value
    /// </summary>
    NULL_ARRAY_DOUBLE = ARRAY_DOUBLE | NULL_FLAG,

    /// <summary>
    /// An array of hydra objects with a null value
    /// </summary>
    NULL_ARRAY_HYDRA_OBJECT = ARRAY_HYDRA_OBJECT | NULL_FLAG,

    /// <summary>
    /// An array of objects with a null value
    /// </summary>
    NULL_ARRAY_OBJECT = ARRAY_OBJECT | NULL_FLAG,

    /// <summary>
    /// An array of enums with a null value
    /// </summary>
    NULL_ARRAY_ENUM = ARRAY_ENUM | NULL_FLAG,

    /// <summary>
    /// An array of chars with a null value
    /// </summary>
    NULL_ARRAY_CHAR = ARRAY_CHAR | NULL_FLAG
  }
}
