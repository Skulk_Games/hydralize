#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Security.Cryptography;

namespace Skulk.Hydralize.Encoding
{
  /// <summary>
  /// Manages creating ICryptoTransform objects for encryption/decryption using the Tiny Encryption Algorithm.
  /// </summary>
  public class TinyEncryption
  {
    private UInt32[] m_key = new uint[4];

    /// <summary>
    /// Constructor for the <see cref="TinyEncryption"/> class.
    /// </summary>
    /// <param name="key">The 128 bit encryption key to use.</param>
    public TinyEncryption(UInt32[] key)
    {
      if (key.Length != 4)
      {
        throw new Exception("Invalid key size for TinyEncoder. The key must be 4 UInt32 values.");
      }

      Array.Copy(key, m_key, 4);
    }

    /// <summary>
    /// Initializes this instance with the specified array of bytes for the key.
    /// </summary>
    /// <param name="key">The 16 byte array to use for the encryption key.</param>
    public TinyEncryption(byte[] key)
    {
      if (key.Length != 16)
      {
        throw new Exception($"Invalid key size: {key.Length}, must be 16 bytes.");
      }

      int offset = 0;
      for (int i = 0; i < 4; ++i)
      {
        m_key[i] = Hydralizer.GetUInt32(key, ref offset);
      }
    }

    /// <summary>
    /// Gets a new instance of an encryptor instance using the key from this object.
    /// </summary>
    /// <returns>The new instance of an encryptor using this object's key.</returns>
    public ICryptoTransform GetEncryptor()
    {
      return new TinyEncoder(m_key);
    }

    /// <summary>
    /// Gets a new instance of a decryptor instance using the key from this object.
    /// </summary>
    /// <returns>The new instance of a decryptor using this object's key.</returns>
    public ICryptoTransform GetDecryptor()
    {
      return new TinyDecoder(m_key);
    }
  }
}
