#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Skulk.Hydralize.Encoding
{
  /// <summary>
  /// Delegate for events involving an <see cref="ICryptoTransform"/>
  /// </summary>
  /// <returns>The <see cref="ICryptoTransform"/> result for the event</returns>
  public delegate ICryptoTransform GetTransformDelegate();

  /// <summary>
  /// An object holding a pair of <see cref="ICryptoTransform"/>s used for transcoding data.
  /// </summary>
  public class TranscoderPair
  {
    private ICryptoTransform m_encoder;
    private ICryptoTransform m_decoder;

    /// <summary>
    /// The transform to use when encoding plaintext data to cryptotext.
    /// </summary>
    public ICryptoTransform Encoder { get => m_encoder; set => m_encoder = value; }

    /// <summary>
    /// The transform to use when decoding cryptotext to plaintext.
    /// </summary>
    public ICryptoTransform Decoder { get => m_decoder; set => m_decoder = value; }

    /// <summary>
    /// Event method used to create an encoder instance
    /// </summary>
    public event GetTransformDelegate CreateEncoder;

    /// <summary>
    /// Event method used to create a decoder instance
    /// </summary>
    public event GetTransformDelegate CreateDecoder;

    /// <summary>
    /// Empty constructor for reflection use
    /// </summary>
    private TranscoderPair()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this object with the specified encoder/decoder pair.
    /// </summary>
    /// <param name="encoder">The transform to use when encoding data.</param>
    /// <param name="decoder">The transform to use when decoding data.</param>
    public TranscoderPair(ICryptoTransform encoder, ICryptoTransform decoder)
    {
      m_encoder = encoder;
      m_decoder = decoder;
    }

    /// <summary>
    /// Initializes this object with the specified event delegate pair to create the encoder/decoder pair.
    /// </summary>
    /// <param name="encoder">The method to call in order to create new encoder instances</param>
    /// <param name="decoder">The method to call in order to create new decoder instances</param>
    public TranscoderPair(GetTransformDelegate encoder, GetTransformDelegate decoder)
    {
      CreateEncoder += encoder;
      m_encoder = CreateEncoder.Invoke();
      CreateDecoder += decoder;
      m_decoder = CreateDecoder.Invoke();
    }

    /// <summary>
    /// Creates a new <see cref="TranscoderPair"/> from the create encoder/decoder event methods or returns null if the event methods are not set.
    /// </summary>
    /// <returns>A new <see cref="TranscoderPair"/> instance with new encoder/decoder instances or null if the event methods aren't set</returns>
    public TranscoderPair Clone()
    {
      if (CreateEncoder != null && CreateDecoder != null)
      {
        return new TranscoderPair(CreateEncoder, CreateDecoder);
      }

      return null;
    }

    /// <summary>
    /// Encodes the specified byte array
    /// </summary>
    /// <param name="buffer">The buffer of bytes to encode</param>
    /// <returns>A new array containing encoded bytes</returns>
    public byte[] Encode(byte[] buffer)
    {
      return Encode(buffer, 0, buffer.Length);
    }

    /// <summary>
    /// Encodes a segment of bytes from a byte array
    /// </summary>
    /// <param name="buffer">The buffer containing bytes to encode</param>
    /// <param name="offset">The offset of the first byte in the buffer to encode</param>
    /// <param name="length">The number of bytes in the buffer to encode</param>
    /// <returns>A new array containing the encoded bytes</returns>
    public byte[] Encode(byte[] buffer, int offset, int length)
    {
      byte[] bufferOut = new byte[length + m_encoder.OutputBlockSize * 2];
      int count = Encode(buffer, offset, length, ref bufferOut, 0);
      if (count != bufferOut.Length)
      {
        byte[] newBuffer = new byte[count];
        Array.Copy(bufferOut, 0, newBuffer, 0, count);
        bufferOut = newBuffer;
      }

      return bufferOut;
    }

    /// <summary>
    /// Optimally encodes an array of bytes in place using the indicated output buffer
    /// </summary>
    /// <param name="bufferIn">The buffer containing bytes to encode</param>
    /// <param name="offsetIn">The offset of the first byte to encode</param>
    /// <param name="length">The number of bytes to encode</param>
    /// <param name="bufferOut">The buffer where encoded bytes will be placed</param>
    /// <param name="offsetOut">The index where the first encoded byte will be placed</param>
    /// <returns>The number of bytes that were placed in the encoded buffer</returns>
    public int Encode(byte[] bufferIn, int offsetIn, int length, ref byte[] bufferOut, int offsetOut)
    {
      if (m_encoder != null)
      {
        // Initialize stream with educated guess as to capacity
        MemoryStream encodedStream = new MemoryStream(bufferOut);
        encodedStream.Seek(offsetOut, SeekOrigin.Begin);
        using (CryptoStream cryptoStream = new CryptoStream(encodedStream, m_encoder, CryptoStreamMode.Write))
        {
          cryptoStream.Write(bufferIn, offsetIn, length);
          cryptoStream.FlushFinalBlock();
          return (int)(encodedStream.Position - offsetOut);
        }
      }

      return 0;
    }

    /// <summary>
    /// Decodes the specified byte array
    /// </summary>
    /// <param name="buffer">The buffer of bytes to decode</param>
    /// <returns>A new array containing decoded bytes</returns>
    public byte[] Decode(byte[] buffer)
    {
      return Decode(buffer, 0, buffer.Length);
    }

    /// <summary>
    /// Decodes a segment of bytes from a byte array
    /// </summary>
    /// <param name="buffer">The buffer containing bytes to Decode</param>
    /// <param name="offset">The offset of the first byte in the buffer to Decode</param>
    /// <param name="length">The number of bytes in the buffer to Decode</param>
    /// <returns>A new array containing the Decoded bytes</returns>
    public byte[] Decode(byte[] buffer, int offset, int length)
    {
      byte[] bufferOut = new byte[length];
      int count = Decode(buffer, offset, length, ref bufferOut, 0);

      if (count != length)
      {
        byte[] newBuffer = new byte[count];
        Array.Copy(bufferOut, 0, newBuffer, 0, count);
        bufferOut = newBuffer;
      }

      return bufferOut;
    }

    /// <summary>
    /// Optimally Decodes an array of bytes in place using the indicated buffers
    /// </summary>
    /// <param name="bufferIn">The buffer containing bytes to decode</param>
    /// <param name="offsetIn">The offset of the first byte to decode</param>
    /// <param name="length">The number of bytes to decode</param>
    /// <param name="bufferOut">The buffer where decoded bytes will be placed</param>
    /// <param name="offsetOut">The index where the first decoded byte will be placed</param>
    /// <returns>The number of bytes that were placed in the decoded buffer</returns>
    public int Decode(byte[] bufferIn, int offsetIn, int length, ref byte[] bufferOut, int offsetOut)
    {
      if (m_decoder != null)
      {
        // Initialize stream with educated guess as to capacity
        using (MemoryStream encodedStream = new MemoryStream(bufferIn))
        {
          encodedStream.Seek(offsetIn, SeekOrigin.Begin);
          using (CryptoStream cryptoStream = new CryptoStream(encodedStream, m_decoder, CryptoStreamMode.Read))
          {
            int count = 0;
            int step = 0;
            int remaining = length;
            do
            {
              step = cryptoStream.Read(bufferOut, offsetOut + count, remaining);
              count += step;
              remaining -= step;
            } while (step > 0 && remaining >= m_decoder.InputBlockSize);

            return count;
          }
        }
      }

      return 0;
    }
  }
}
