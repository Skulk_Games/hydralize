#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Skulk.Hydralize.Encoding
{
  /// <summary>
  /// Decryption implementation for the tiny encryption algorithm.
  /// </summary>
  public class TinyDecoder : ICryptoTransform
  {
    private const UInt32 DELTA = 0x9E3779B9;
    private UInt32[] m_key = new uint[4];

    /// <inheritdoc/>
    public bool CanReuseTransform => true;

    /// <inheritdoc/>
    public bool CanTransformMultipleBlocks => true;

    /// <inheritdoc/>
    public int InputBlockSize => 8;

    /// <inheritdoc/>
    public int OutputBlockSize => 8;

    /// <summary>
    /// Constructs this decryptor with the specified key.
    /// </summary>
    /// <param name="key">The 16 byte key value to use when decrypting.</param>
    public TinyDecoder(byte[] key)
    {
      if (key.Length != 16)
      {
        throw new Exception($"Invalid key size: {key.Length}, must be 16 bytes.");
      }

      int offset = 0;
      for (int i = 0; i < 4; ++i)
      {
        m_key[i] = Hydralizer.GetUInt32(key, ref offset);
      }
    }

    /// <summary>
    /// Constructs this decryptor with the specified key.
    /// </summary>
    /// <param name="key">The array of 4 uints used as the key when decrypting.</param>
    public TinyDecoder(uint[] key)
    {
      if (key.Length != 4)
      {
        throw new Exception($"Invalid key size: {key.Length}, must be 4 uints.");
      }

      m_key = key;
    }

    /// <inheritdoc/>
    public void Dispose()
    {
      // do nothing, everything is managed.
    }

    /// <inheritdoc/>
    public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
    {
      int blockCount = inputCount / 8;
      int count = 0;
      if (inputBuffer.Length >= inputOffset + inputCount && outputBuffer.Length >= outputOffset + inputCount)
      {
        for (int i = 0; i < blockCount; ++i, inputOffset += 8, outputOffset += 8, count += 8)
        {
          Decrypt(inputBuffer, inputOffset, outputBuffer, outputOffset);
          if (i == blockCount - 1)
          {
            // Check for padding - this is not great as it could interpret data as valid padding possibly
            count -= RemoveBlockPadding(outputBuffer, outputOffset);
          }
        }
      }

      return count;
    }

    /// <inheritdoc/>
    public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
    {
      if (inputCount == 0)
      {
        return new byte[0];
      }
      else if (inputCount != 8)
      {
        // Can't decrypt anything less than a full block
        throw new Exception("Can not decrypt data that is not a multiple of the block size.");
      }

      byte[] block = new byte[8];
      Decrypt(inputBuffer, inputOffset, block, 0);

      int pad = block[7];

      byte[] decoded = new byte[8 - pad];
      Array.Copy(block, 0, decoded, 0, decoded.Length);
      return decoded;
    }

    private void Decrypt(ref UInt32 v0, ref UInt32 v1)
    {
      UInt32 sum = 0xC6EF3720;
      for (int i = 0; i < 32; ++i)
      {
        v1 -= ((v0 << 4) + m_key[2]) ^ (v0 + sum) ^ ((v0 >> 5) + m_key[3]);
        v0 -= ((v1 << 4) + m_key[0]) ^ (v1 + sum) ^ ((v1 >> 5) + m_key[1]);
        sum -= DELTA;
      }
    }

    /// <summary>
    /// Checks a block for padding and returns indicating the amount of padding.
    /// </summary>
    /// <param name="buffer">The buffer of decrypted bytes.</param>
    /// <param name="offset">The offset in the buffer where the block begins.</param>
    /// <returns>The number of padded bytes found.</returns>
    protected int RemoveBlockPadding(byte[] buffer, int offset)
    {
      byte pad = buffer[offset + 7];
      if (pad > 0 && pad <= 8)
      {
        for (int i = 1; i < pad - 1; ++i)
        {
          if (buffer[offset + (7 - i)] != 0)
          {
            return 0;
          }
        }

        // Set pad value to 0
        buffer[offset + 7] = 0;
        return pad;
      }

      return 0;
    }

    /// <summary>
    /// Decrypts a block
    /// </summary>
    /// <param name="vIn">The input data to decrypt</param>
    /// <param name="inIdx">The index in the array to start decrypting</param>
    /// <param name="vOut">The output decrypted data</param>
    /// <param name="outIdx">The index to start putting decrypted values</param>
    /// <returns>True if the block was decrypted, false otherwise</returns>
    protected void Decrypt(byte[] vIn, int inIdx, byte[] vOut, int outIdx)
    {
      UInt32 v0 = (((UInt32)vIn[inIdx] << 24) + ((UInt32)vIn[inIdx + 1] << 16) + ((UInt32)vIn[inIdx + 2] << 8) + (UInt32)vIn[inIdx + 3]);
      UInt32 v1 = (((UInt32)vIn[inIdx + 4] << 24) + ((UInt32)vIn[inIdx + 5] << 16) + ((UInt32)vIn[inIdx + 6] << 8) + (UInt32)vIn[inIdx + 7]);
      Decrypt(ref v0, ref v1);
      vOut[outIdx] = (byte)(v0 >> 24);
      vOut[outIdx + 1] = (byte)(v0 >> 16);
      vOut[outIdx + 2] = (byte)(v0 >> 8);
      vOut[outIdx + 3] = (byte)v0;
      vOut[outIdx + 4] = (byte)(v1 >> 24);
      vOut[outIdx + 5] = (byte)(v1 >> 16);
      vOut[outIdx + 6] = (byte)(v1 >> 8);
      vOut[outIdx + 7] = (byte)v1;
    }
  }
}
