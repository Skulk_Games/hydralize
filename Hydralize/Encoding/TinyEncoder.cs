#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Skulk.Hydralize.Encoding
{
  /// <summary>
  /// Implements the encryption for the tiny encryption algorithm.
  /// </summary>
  public class TinyEncoder : ICryptoTransform
  {
    private const UInt32 DELTA = 0x9E3779B9;
    private UInt32[] m_key = new uint[4];

    /// <inheritdoc/>
    public bool CanReuseTransform => true;

    /// <inheritdoc/>
    public bool CanTransformMultipleBlocks => true;

    /// <inheritdoc/>
    public int InputBlockSize => 8;

    /// <inheritdoc/>
    public int OutputBlockSize => 8;

    /// <summary>
    /// Initializes this encryptor with the specified key.
    /// </summary>
    /// <param name="key">The 16 byte array to use as the key when encrypting.</param>
    public TinyEncoder(byte[] key)
    {
      if (key.Length != 16)
      {
        throw new Exception($"Invalid key size: {key.Length}, must be 16 bytes.");
      }

      int offset = 0;
      for (int i = 0; i < 4; ++i)
      {
        m_key[i] = Hydralizer.GetUInt32(key, ref offset);
      }
    }

    /// <summary>
    /// Initializes this encryptor with the specified key.
    /// </summary>
    /// <param name="key">The array of 4 uints to use as the key when encrypting.</param>
    public TinyEncoder(uint[] key)
    {
      if (key.Length != 4)
      {
        throw new Exception($"Invalid key size: {key.Length}, must be 4 uints.");
      }

      m_key = key;
    }

    /// <inheritdoc/>
    public void Dispose()
    {
      // do nothing, everything is managed.
    }

    /// <inheritdoc/>
    public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
    {
      int blockCount = inputCount / 8;
      int count = 0;
      if (inputBuffer.Length >= inputOffset + inputCount && outputBuffer.Length >= outputOffset + inputCount)
      {
        for (int i = 0; i < blockCount; ++i, inputOffset += 8, outputOffset += 8, count += 8)
        {
          Encrypt(inputBuffer, inputOffset, outputBuffer, outputOffset);
        }
      }

      return count;
    }

    /// <inheritdoc/>
    public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
    {
      int remainder = inputCount % 8;
      int paddedSize = inputCount + (8 - remainder);
      int pad = paddedSize - inputCount;
      byte[] block = new byte[paddedSize];
      Array.Copy(inputBuffer, inputOffset, block, 0, inputCount);
      block[block.Length - 1] = (byte)pad;
      Encrypt(block, 0, block, 0);
      return block;
    }

    /// <summary>
    /// Encrypts a block
    /// </summary>
    /// <param name="vIn">The input data to encrypt</param>
    /// <param name="inIdx">The index in the array to start encrypting</param>
    /// <param name="vOut">The output encrypted data</param>
    /// <param name="outIdx">The index to start putting encrypted values</param>
    /// <returns>True if the block was encrypted, false otherwise</returns>
    protected bool Encrypt(byte[] vIn, int inIdx, byte[] vOut, int outIdx)
    {
      if (vIn.Length >= inIdx + 8 && vOut.Length >= outIdx + 8)
      {
        UInt32 v0 = (((UInt32)vIn[inIdx] << 24) + ((UInt32)vIn[inIdx + 1] << 16) + ((UInt32)vIn[inIdx + 2] << 8) + (UInt32)vIn[inIdx + 3]);
        UInt32 v1 = (((UInt32)vIn[inIdx + 4] << 24) + ((UInt32)vIn[inIdx + 5] << 16) + ((UInt32)vIn[inIdx + 6] << 8) + (UInt32)vIn[inIdx + 7]);
        Encrypt(ref v0, ref v1);
        vOut[outIdx] = (byte)(v0 >> 24);
        vOut[outIdx + 1] = (byte)(v0 >> 16);
        vOut[outIdx + 2] = (byte)(v0 >> 8);
        vOut[outIdx + 3] = (byte)v0;
        vOut[outIdx + 4] = (byte)(v1 >> 24);
        vOut[outIdx + 5] = (byte)(v1 >> 16);
        vOut[outIdx + 6] = (byte)(v1 >> 8);
        vOut[outIdx + 7] = (byte)v1;
        return true;
      }

      return false;
    }

    /// <summary>
    /// Encrypts the specified values in place.
    /// </summary>
    /// <param name="v0">The first 4 byte value to encrypt.</param>
    /// <param name="v1">The second 4 byte value to encrypt.</param>
    private void Encrypt(ref UInt32 v0, ref UInt32 v1)
    {
      UInt32 sum = 0;
      for (int i = 0; i < 32; ++i)
      {
        sum += DELTA;
        v0 += ((v1 << 4) + m_key[0]) ^ (v1 + sum) ^ ((v1 >> 5) + m_key[1]);
        v1 += ((v0 << 4) + m_key[2]) ^ (v0 + sum) ^ ((v0 >> 5) + m_key[3]);
      }
    }
  }
}
