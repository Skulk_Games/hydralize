#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydralize
{
  /// <summary>
  /// An interface for classes that can convert objects for serialization in the <see cref="Hydralizer"/> class
  /// </summary>
  public interface IHydralizeConverter
  {
    /// <summary>
    /// Gets the type the converter supports
    /// </summary>
    /// <remarks>
    /// For generic types this should be the generic base class
    /// </remarks>
    Type ConvertType { get; }

    /// <summary>
    /// Adds the object data into the specified info object
    /// </summary>
    /// <param name="value">The object to hydralize</param>
    /// <param name="info">The info object to use for storing object data</param>
    void Hydralize(object value, ref HydralizeInfo info);

    /// <summary>
    /// Gets the data from the specified info object and assigns it to appropriate values in the instance object
    /// </summary>
    /// <param name="instance">The object to load values into</param>
    /// <param name="info">The info describing object data</param>
    void Dehydralize(object instance, ref HydralizeInfo info);

    /// <summary>
    /// Gets a description of the fields that are now associated with the type when using this converter
    /// </summary>
    /// <param name="type">The type of the object instance to get the fields for</param>
    /// <returns>An array of the fields associated with the specified type when using this converter</returns>
    HydraFieldInfo[] GetFields(Type type);
  }
}
