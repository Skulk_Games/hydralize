#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydralize.JSON
{
  /// <summary>
  /// Holds a direct JSON value
  /// </summary>
  public class JsonValue : JsonToken
  {
    private object value;

    /// <summary>
    /// The value to represent
    /// </summary>
    public object Value { get => value; set => this.value = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public JsonValue()
    {
      TokenType = JsonTokenType.Value;
    }

    /// <inheritdoc/>
    public override string Write(int indentLevel = 0)
    {
      if (value == null)
      {
        return "NULL";
      }
      else
      {
        if (value.GetType().IsEnum)
        {
          // Write the enum name as a string
          return "\"" + value.ToString() + "\"";
        }
        else
        {
          switch (Type.GetTypeCode(value.GetType()))
          {
            case TypeCode.Boolean:
            case TypeCode.Byte:
            case TypeCode.Double:
            case TypeCode.Int16:
            case TypeCode.Int32:
            case TypeCode.Int64:
            case TypeCode.SByte:
            case TypeCode.Single:
            case TypeCode.UInt16:
            case TypeCode.UInt32:
            case TypeCode.UInt64:
            {
              return value.ToString();
            }

            case TypeCode.String:
            case TypeCode.Char:
            {
              return "\"" + value.ToString().Replace("\"", "\\\"") + "\"";
            }

            case TypeCode.DateTime:
            {
              return "\"" + ((DateTime)value).ToString("O") + "\"";
            }

            case TypeCode.Object:
            {
              JsonObject jsonObj = value as JsonObject;
              if (jsonObj != null)
              {
                return jsonObj.Write(indentLevel);
              }
              else
              {
                // Shouldn't happen, but we could handle it maybe
                return "NULL";
              }
            }
          }
        }
      }

      return "NULL";
    }

    /// <inheritdoc/>
    public override bool Read(string json, ref int index)
    {
      // Read until you find nonwhitespace
      for (int c = index; c < json.Length; ++c)
      {
        switch (json[c])
        {
          case ' ':
          case '\t':
          case '\n':
          {
            // Continue searching
            break;
          }

          case '"':
          {
            // Parse a string
            int endIndex = GetStringEnd(json, c + 1);
            if (endIndex > c)
            {
              value = json.Substring(c + 1, endIndex - (c + 1)).Replace("\\\"", "\"");
              index = endIndex;
              return true;
            }
            else
            {
              return false;
            }
          }

          default:
          {
            // Parse some bool or numeric
            int endIndex = EndOfValue(json, c);
            if (endIndex > c)
            {
              string valueString = json.Substring(c, endIndex - c);
              if (valueString.Length >= 4)
              {
                if (bool.TryParse(valueString, out bool result))
                {
                  value = result;
                  index = endIndex;
                  return true;
                }
              }

              // Check sign
              if (valueString[0] == '-')
              {
                // Try to parse as signed long
                if (long.TryParse(valueString, out long result))
                {
                  value = result;
                  index = endIndex;
                  return true;
                }
              }
              else
              {
                // Try to parse as unsigned long
                if (ulong.TryParse(valueString, out ulong result))
                {
                  value = result;
                  index = endIndex;
                  return true;
                }
              }

              // Try to parse as double
              if (double.TryParse(valueString, out double dbl))
              {
                value = dbl;
                index = endIndex;
                return true;
              }
            }
            break;
          }
        }
      }

      return false;
    }

    /// <summary>
    /// Finds the end of a JSON value by searching for the index of the first character that is not whitespace and not a comma.
    /// </summary>
    /// <param name="str">The string to search for whitespace.</param>
    /// <param name="offset">The index of the first character to search.</param>
    /// <returns>The index of the first whitespace character found or -1 if not found.</returns>
    private int EndOfValue(string str, int offset)
    {
      int idx = str.IndexOfAny(new char[] { ' ', '\t', '\n', ',' }, offset);
      return idx == -1 ? str.Length : idx;
    }

    /// <summary>
    /// Gets the index of the last quote in a string value.
    /// </summary>
    /// <param name="str">The string to search for non-escaped quotes.</param>
    /// <param name="offset">The index of the first character to start searching.</param>
    /// <returns>The index of character that is the closing quote on the string or -1 if not found.</returns>
    private int GetStringEnd(string str, int offset)
    {
      // Find the end
      bool escaped = false;
      for (int i = offset; i < str.Length; ++i)
      {
        if (escaped)
        {
          escaped = false;
          continue;
        }

        switch (str[i])
        {
          case '\\':
          {
            escaped = true;
            break;
          }

          case '"':
          {
            return i;
          }
        }
      }

      return -1;
    }
  }
}
