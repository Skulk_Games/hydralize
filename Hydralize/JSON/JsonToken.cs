#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.Hydralize.JSON
{
  /// <summary>
  /// Enumeration of the different types of JSON tokens
  /// </summary>
  public enum JsonTokenType
  {
    /// <summary>
    /// The JSON describes object data
    /// </summary>
    Object,

    /// <summary>
    /// The JSON describes array data
    /// </summary>
    Array,

    /// <summary>
    /// The JSON describes a direct value
    /// </summary>
    Value,
  };

  /// <summary>
  /// Abstract class for JSON tokens: object, array, or values
  /// </summary>
  public abstract class JsonToken
  {
    /// <summary>
    /// The type of this token
    /// </summary>
    public JsonTokenType TokenType { get; set; }

    /// <summary>
    /// Writes the key in JSON.
    /// </summary>
    /// <param name="indentLevel">The level of indentation to write to.</param>
    /// <returns>A string that specifies the key name in JSON.</returns>
    public abstract string Write(int indentLevel = 0);

    /// <summary>
    /// Reads the key value from a string starting at the specified index.
    /// </summary>
    /// <param name="json">The JSON string to read.</param>
    /// <param name="index">The index of the first character to consider while reading. The value will be updated to the index of the value to parse if successful.</param>
    /// <returns>True if a key name was successfully parsed, otherwise false.</returns>
    public abstract bool Read(string json, ref int index);

    /// <summary>
    /// Parses the JSON string and instantiates the appropriate token for the data.
    /// </summary>
    /// <param name="json">The JSON string to parse</param>
    /// <param name="index">The index of the first character to consider when parsing the token, the index is updated to the first character not used in this token</param>
    /// <returns>The JSON token parsed from the string</returns>
    public static JsonToken Parse(string json, ref int index)
    {
      // Read until you find nonwhitespace
      for (int c = index; c < json.Length; ++c)
      {
        switch (json[c])
        {
          case ' ':
          case '\t':
          case '\n':
          {
            // Continue searching
            break;
          }

          case '{':
          {
            // Parse the content
            JsonObject obj = new JsonObject();
            if (obj.Read(json, ref c))
            {
              index = c;
              return obj;
            }
            else
            {
              return null;
            }
          }

          case '[':
          {
            JsonArray arr = new JsonArray();
            if (arr.Read(json, ref c))
            {
              index = c;
              return arr;
            }
            else
            {
              return null;
            }
          }

          case '"':
          {
            // Parse a string
            int endIndex = json.IndexOf('"', c + 1);
            JsonValue jsonStr = new JsonValue();
            if (jsonStr.Read(json, ref c))
            {
              index = endIndex;
              return jsonStr;
            }
            else
            {
              return null;
            }
          }

          default:
          {
            // Parse some bool or numeric
            JsonValue jsonVal = new JsonValue();
            if (jsonVal.Read(json, ref c))
            {
              return jsonVal;
            }
            else
            {
              return null;
            }
          }
        }
      }

      return null;
    }

    /// <summary>
    /// Find the character index for the end of the specified JSON token.
    /// </summary>
    /// <param name="json">The JSON string to search.</param>
    /// <param name="offset">The index of the first character to search.</param>
    /// <param name="tokenType">The type of token to find.</param>
    /// <returns>The index of the closing character for the token or -1 if not found.</returns>
    public int FindEndIndex(string json, int offset, JsonTokenType tokenType)
    {
      switch (tokenType)
      {
        case JsonTokenType.Object:
        {
          return FindContainerEnd(json, offset, '{', '}');
        }

        case JsonTokenType.Array:
        {
          return FindContainerEnd(json, offset, '[', ']');
        }

        case JsonTokenType.Value:
        {
          // Find the start of it
          for (int i = offset; i < json.Length; ++i)
          {
            if (!char.IsWhiteSpace(json[i]) && json[i] != ',')
            {
              // Now find the end
              int idx = json.IndexOfAny(new char[] { ' ', '\t', '\n', ',' }, i);
              if (idx != -1)
              {
                // Return the character directly preceeding the whitespace
                return --idx;
              }
            }
          }

          break;
        }
      }

      return -1;
    }

    /// <summary>
    /// Finds the index of the container closing character
    /// </summary>
    /// <param name="json">The JSON string to search</param>
    /// <param name="offset">The index of the first character to consider</param>
    /// <param name="open">The character that indicates the open of this container type</param>
    /// <param name="close">The character that indicates the close of this container type</param>
    /// <returns>The index of the closing character for the first container found of the specified type or -1 if not found</returns>
    private int FindContainerEnd(string json, int offset, char open, char close)
    {
      int level = 0;
      bool quoted = false;
      bool escaped = false;
      for (int i = 0; i < json.Length; ++i)
      {
        // Check for escapes and quoting
        if (json[i] == '\\')
        {
          escaped = !escaped;
          continue;
        }
        else if (escaped)
        {
          // This character is escaped, reset escaped state
          escaped = false;
          continue;
        }
        else
        {
          // Not escaped
          if (json[i] == '"')
          {
            quoted = !quoted;
            continue;
          }
        }

        // Check the level and find close
        if (!quoted)
        {
          if (json[i] == open)
          {
            ++level;
          }
          else if (json[i] == close)
          {
            if (level == 1)
            {
              return i;
            }
            else
            {
              --level;
            }
          }
        }
      }

      return -1;
    }

    /// <summary>
    /// Splits the string on the JSON keys ignoring characters within quotes, objects, and arrays.
    /// </summary>
    /// <param name="str">The string to split.</param>
    /// <returns>The array of strings separated by the delimiter.</returns>
    public string[] SplitKeys(string str)
    {
      bool quoted = false;
      bool escaped = false;
      int arrayLevel = 0;
      int objectLevel = 0;
      List<string> strings = new List<string>();
      StringBuilder entry = new StringBuilder();
      for (int i = 0; i < str.Length; ++i)
      {
        // Check the level and find close
        if (!quoted)
        {
          if (str[i] == ',')
          {
            if (arrayLevel == 0 && objectLevel == 0)
            {
              if (entry.Length > 0)
              {
                strings.Add(entry.ToString());
                entry.Clear();
              }
              continue;
            }
          }
          else if (str[i] == '[')
          {
            ++arrayLevel;
          }
          else if (str[i] == ']')
          {
            --arrayLevel;
          }
          else if (str[i] == '{')
          {
            ++objectLevel;
          }
          else if (str[i] == '}')
          {
            --objectLevel;
          }
        }

        entry.Append(str[i]);

        // Check for escapes and quoting
        if (str[i] == '\\')
        {
          escaped = !escaped;
        }
        else if (escaped)
        {
          // This character is escaped, reset escaped state
          escaped = false;
        }
        else
        {
          // Not escaped
          if (str[i] == '"')
          {
            quoted = !quoted;
          }
        }
      }

      if (entry.Length > 0)
      {
        strings.Add(entry.ToString());
      }

      return strings.ToArray();
    }
  }
}
