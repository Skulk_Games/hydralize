#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.Hydralize.JSON
{
  /// <summary>
  /// Holds data related to a JSON array
  /// </summary>
  public class JsonArray : JsonToken
  {
    private List<JsonToken> values = new List<JsonToken>();

    /// <summary>
    /// The values of the JSON array
    /// </summary>
    public List<JsonToken> Values { get => values; set => values = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public JsonArray()
    {
      TokenType = JsonTokenType.Array;
    }

    /// <summary>
    /// Reads the JSON string and sets the array values
    /// </summary>
    /// <param name="json">The JSON string to read into this array</param>
    /// <param name="index">The index of the first character to consider when reading this array</param>
    /// <returns></returns>
    public override bool Read(string json, ref int index)
    {
      int startIndex = json.IndexOf('[', index);
      if (startIndex >= index)
      {
        int endIndex = FindEndIndex(json, index, JsonTokenType.Array);
        if (endIndex > startIndex)
        {
          string arrayString = json.Substring(startIndex + 1, endIndex - (startIndex + 1));
          string[] elementStrings = SplitKeys(arrayString);
          for (int i = 0; i < elementStrings.Length; ++i)
          {
            string trimmed = elementStrings[i].Trim();
            if (trimmed.Length > 0)
            {
              int offset = 0;
              JsonToken token = JsonToken.Parse(elementStrings[i], ref offset);
              if (token != null)
              {
                values.Add(token);
                index += elementStrings[i].Length + 1;
                if (elementStrings.Length > i + 1)
                {
                  ++index;
                }
              }
              else
              {
                return false;
              }
            }
            else
            {
              // Ignore whitespace entries
            }
          }

          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Writes the array data to a JSON string
    /// </summary>
    /// <param name="indentLevel">The level of indentation to write with</param>
    /// <returns>The array data as a JSON string</returns>
    public override string Write(int indentLevel = 0)
    {
      StringBuilder arr = new StringBuilder();
      arr.Append("[ ");
      for (int i = 0; i < values.Count; ++i)
      {
        if (i > 0)
        {
          arr.Append(", ");
        }

        switch (values[i].TokenType)
        {
          case JsonTokenType.Value:
          {
            arr.Append(values[i].Write(0));
            break;
          }

          default:
          {
            arr.Append("\n");
            arr.Append(values[i].Write(indentLevel + 1));
            break;
          }
        }
        
      }

      if (values.Count > 0 && values[values.Count - 1].TokenType == JsonTokenType.Value)
      {
        arr.Append(" ");
      }

      arr.Append("]");
      return arr.ToString();
    }
  }
}
