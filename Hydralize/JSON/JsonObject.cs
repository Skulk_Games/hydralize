#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.Hydralize.JSON
{
  /// <summary>
  /// Holds JSON data that describes an object
  /// </summary>
  public class JsonObject : JsonToken
  {
    private Dictionary<string, JsonElement> elements = new Dictionary<string, JsonElement>();

    /// <summary>
    /// The elements that make up the object
    /// </summary>
    public Dictionary<string, JsonElement> Elements { get => elements; set => elements = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public JsonObject()
    {
      TokenType = JsonTokenType.Object;
    }

    /// <summary>
    /// Reads the specified JSON string into this object data
    /// </summary>
    /// <param name="json">The JSON string to read</param>
    /// <param name="index">The index of the first character to consider in this object</param>
    /// <returns>True if the JSON was successfully read into this object, otherwise false</returns>
    public override bool Read(string json, ref int index)
    {
      int startIndex = json.IndexOf('{', index);
      if (startIndex >= index)
      {
        int endIndex = FindEndIndex(json, index, JsonTokenType.Object);
        if (endIndex > startIndex)
        {
          string objString = json.Substring(startIndex + 1, endIndex - (startIndex + 1));
          string[] elementStrings = SplitKeys(objString);
          for (int i = 0; i < elementStrings.Length; ++i)
          {
            string trimmed = elementStrings[i].Trim();
            if (trimmed.Length > 0)
            {
              JsonElement element = new JsonElement();
              int offset = 0;
              if (element.Read(elementStrings[i], ref offset))
              {
                elements.Add(element.Key, element);
                index += elementStrings[i].Length + 1;
                if (elementStrings.Length > i + 1)
                {
                  ++index;
                }
              }
              else
              {
                return false;
              }
            }
            else
            {
              // Ignore whitespace entries
            }
          }

          return true;
        }
      }

      return false;
    }

    /// <summary>
    /// Writes this data to a JSON string
    /// </summary>
    /// <param name="indentLevel">The indent level to use when writing the string</param>
    /// <returns>The JSON string representing the object data</returns>
    public override string Write(int indentLevel = 0)
    {
      StringBuilder indent = new StringBuilder();
      for (int i = 0; i < indentLevel; ++i)
      {
        indent.Append("  ");
      }

      StringBuilder obj = new StringBuilder();
      obj.AppendFormat("{0}{{\n", indent.ToString());
      int index = 0;
      foreach (KeyValuePair<string, JsonElement> pair in elements)
      {
        obj.Append(pair.Value.Write(indentLevel + 1));
        if (index + 1 < elements.Count)
        {
          obj.Append(",\n");
        }
        else
        {
          obj.Append("\n");
        }
        ++index;
      }

      obj.AppendFormat("{0}}}", indent.ToString());
      return obj.ToString();
    }
  }
}
