#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.Hydralize.JSON
{
  /// <summary>
  /// Specifies a key and token pair for JSON data
  /// </summary>
  public class JsonElement
  {
    private string key;
    private JsonToken tokenValue;

    /// <summary>
    /// The name of the key for this element
    /// </summary>
    public string Key { get => key; set => key = value; }

    /// <summary>
    /// The JSON token value for this element
    /// </summary>
    public JsonToken TokenValue { get => tokenValue; set => tokenValue = value; }

    /// <summary>
    /// Writes the key in JSON.
    /// </summary>
    /// <param name="indentLevel">The level of indentation to write to.</param>
    /// <returns>A string that specifies the key name in JSON.</returns>
    public string Write(int indentLevel = 0)
    {
      StringBuilder indent = new StringBuilder();
      for (int i = 0; i < indentLevel; ++i)
      {
        indent.Append("  ");
      }

      switch (tokenValue?.TokenType ?? JsonTokenType.Value)
      {
        case JsonTokenType.Object:
        {
          return String.Format("{0}\"{1}\":\n{2}", indent.ToString(), key, tokenValue.Write(indentLevel));
        }

        default:
        {
          return String.Format("{0}\"{1}\": {2}", indent.ToString(), key, TokenValue.Write(indentLevel));
        }
      }
    }

    /// <summary>
    /// Reads the key value from a string starting at the specified index.
    /// </summary>
    /// <param name="json">The JSON string to read.</param>
    /// <param name="index">The index of the first character to consider while reading. The value will be updated to the index of the value to parse if successful.</param>
    /// <returns>True if a key name was successfully parsed, otherwise false.</returns>
    public bool Read(string json, ref int index)
    {
      int keyEnd = json.IndexOf(':', index);
      if (keyEnd > index)
      {
        string subString = json.Substring(index, keyEnd - index);
        // Find the key quotes
        int quoteStart = subString.IndexOf('"');
        int quoteEnd = subString.IndexOf('"', quoteStart + 1);

        if (quoteStart >= 0 && quoteEnd > quoteStart)
        {
          key = subString.Substring(quoteStart + 1, quoteEnd - (quoteStart + 1));
          index = keyEnd + 1;
          tokenValue = JsonToken.Parse(json, ref index);
          return tokenValue != null;
        }
      }

      return false;
    }
  }
}
