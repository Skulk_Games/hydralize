#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize.Encoding;
using System;
using System.IO;
using System.Security.Cryptography;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Enumeration of the formats <see cref="LoadableSettings"/> can use
  /// </summary>
  public enum SettingsFormats
  {
    /// <summary>
    /// Save and load settings in raw binary format
    /// </summary>
    Binary,

    /// <summary>
    /// Save and load settings in XML format
    /// </summary>
    Xml
  }

  /// <summary>
  /// A base class used to implement easy saving and loading of an object to/from disk
  /// </summary>
  public class LoadableSettings : HydralizeObject
  {
    /// <summary>
    /// The encoder to use for saving data
    /// </summary>
    [NonHydralizeableField]
    private ICryptoTransform m_encoder = null;

    /// <summary>
    /// The decoder to use when loading data
    /// </summary>
    [NonHydralizeableField]
    private ICryptoTransform m_decoder = null;

    /// <summary>
    /// The current version of this code object, not overwritten when a file is loaded
    /// </summary>
    [NonHydralizeableField]
    private int m_saveVersion = 0;

    /// <summary>
    /// The version of the file loaded/saved
    /// </summary>
    private int m_fileVersion = 0;

    /// <summary>
    /// The save version of this class, this is not overwritten when file data is loaded and this value overwrites <see cref="FileVersion"/>
    /// when this object is saved.
    /// </summary>
    public int SaveVersion { get => m_saveVersion; }

    /// <summary>
    /// The loaded file version from the last loaded file, this value is set to <see cref="SaveVersion"/> when being saved.
    /// </summary>
    public int FileVersion { get => m_fileVersion; set => m_fileVersion = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public LoadableSettings()
    {
      // Empty
    }

    /// <summary>
    /// Initializes a <see cref="LoadableSettings"/> class with the specified settings
    /// </summary>
    /// <param name="saveVersion">The current save version of this class</param>
    /// <param name="encoder">The encoder to use when saving data</param>
    /// <param name="decoder">The decoder to use when loading data</param>
    public LoadableSettings(int saveVersion = 0, ICryptoTransform encoder = null, ICryptoTransform decoder = null)
    {
      m_saveVersion = saveVersion;
      m_fileVersion = saveVersion;
      m_encoder = encoder;
      m_decoder = decoder;
    }

    /// <summary>
    /// Saves this object data to the file specified
    /// </summary>
    /// <param name="filename">The filename to save this object data to</param>
    /// <param name="format">The data format to use when saving</param>
    /// <param name="lite">Indicates if the data should be saved in lite mode, saving space by not using field names.</param>
    /// <returns>True if the data was successfully written to file, false otherwise</returns>
    public virtual bool Save(string filename, SettingsFormats format = SettingsFormats.Binary, bool lite = false)
    {
      FileStream fout = null;
      bool success = false;

      try
      {
        fout = File.Open(filename, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
        m_fileVersion = m_saveVersion;
        byte[] raw = null;
        switch (format)
        {
          case SettingsFormats.Binary:
          {
            raw = Hydralizer.Hydralize(this, lite, true);
          }
          break;

          case SettingsFormats.Xml:
          {
            string xmlText = Hydralizer.ToXml(this, lite, true);
            raw = System.Text.Encoding.ASCII.GetBytes(xmlText);
          }
          break;
        }

        if (m_encoder != null)
        {
          using (CryptoStream cryptoStream = new CryptoStream(fout, m_encoder, CryptoStreamMode.Write))
          {
            cryptoStream.Write(raw, 0, raw.Length);
          }

          // CryptoStream will dispose underlying stream (fout)
          fout = null;
          success = true;
        }
        else
        {
          fout.Write(raw, 0, raw.Length);
          success = true;
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine("Exception while saving {0} object: {1}", GetType(), ex.Message);
      }
      finally
      {
        fout?.Dispose();
      }

      return success;
    }

    /// <summary>
    /// Attempts to load data into this object from the file specified
    /// </summary>
    /// <param name="filename">The file to load this object's data from</param>
    /// <param name="format">The data format to use when loading</param>
    /// <param name="context">The context to use for indexed objects</param>
    /// <returns>True if the object's data was loaded successfully, false otherwise</returns>
    public virtual bool Load(string filename, SettingsFormats format = SettingsFormats.Binary, IndexContext context = null)
    {
      FileStream fin = null;

      try
      {
        if (File.Exists(filename))
        {
          fin = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
          byte[] raw = new byte[fin.Length];
          int decodedLength = raw.Length;

          if (m_decoder != null)
          {
            using (CryptoStream cryptoStream = new CryptoStream(fin, m_decoder, CryptoStreamMode.Read))
            {
              // likely will read less than encrypted length (from padding), but offsets and lengths can compensate
              decodedLength = cryptoStream.Read(raw, 0, raw.Length);
            }

            // CryptoStream disposes underlying stream, set to null
            fin = null;
          }
          else
          {
            fin.Read(raw, 0, raw.Length);
          }

          HydralizeInfo info = new HydralizeInfo(context);
          switch (format)
          {
            case SettingsFormats.Binary:
            {
              int offset = 0;
              info.Dehydralize(raw, ref offset);
            }
            break;

            case SettingsFormats.Xml:
            {
              string xmlText = System.Text.Encoding.ASCII.GetString(raw, 0, decodedLength);
              info.FromXml(xmlText);
            }
            break;
          }

          if (!CheckVersion(info))
          {
            return false;
          }

          LoadObjectData(info);

          return true;
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine("Exception while loading {0} object: {1}", GetType(), ex.Message);
      }
      finally
      {
        fin?.Dispose();
      }

      return false;
    }

    /// <summary>
    /// A virtual upgrade method that derived classes should implement to handle upgrading older
    /// save files to newer versions
    /// </summary>
    /// <param name="info">The object containing the object info loaded from file.</param>
    /// <returns>True if the upgrade was successful, false otherwise</returns>
    public virtual bool Upgrade(HydralizeInfo info)
    {
      return true;
    }

    /// <summary>
    /// A virtual downgrade method that derived classes should implement to handle downgrading newer
    /// save files to an older version (this version) of code.
    /// </summary>
    /// <param name="info">The object containing the object info loaded from file.</param>
    /// <returns>True if the downgrade was successful, false otherwise</returns>
    public virtual bool Downgrade(HydralizeInfo info)
    {
      return true;
    }

    /// <summary>
    /// Checks the code version against the loaded file and returns, upgrades, or downgrades in response.
    /// </summary>
    /// <param name="info">The object info that was loaded from file.</param>
    /// <returns>True if the version was successfully processed, false otherwise.</returns>
    private bool CheckVersion(HydralizeInfo info)
    {
      int version = info.Get<int>(nameof(m_fileVersion), 0);
      if (m_saveVersion == version)
      {
        return true;
      }
      else if (m_saveVersion > version)
      {
        return Upgrade(info);
      }
      else if (m_saveVersion < version)
      {
        return Downgrade(info);
      }

      return false;
    }
  }
}
