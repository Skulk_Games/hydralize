#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Reflection;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Describes a field in a <see cref="HydraTypeInfo"/>
  /// </summary>
  public class HydraFieldInfo
  {
    /// <summary>
    /// The name of the field
    /// </summary>
    private string m_fieldName;

    /// <summary>
    /// The type of the field
    /// </summary>
    private HydraTypeInfo m_fieldType;

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraFieldInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this <see cref="HydraFieldInfo"/> with values describing the indicated field information
    /// </summary>
    /// <param name="fieldInfo">The field info to describe</param>
    public HydraFieldInfo(FieldInfo fieldInfo)
    {
      m_fieldName = fieldInfo.Name;
      m_fieldType = HydraTypeInfo.Build(fieldInfo.FieldType);
    }

    /// <summary>
    /// Initializes this <see cref="HydraFieldInfo"/> with the specified name and type
    /// </summary>
    /// <param name="name">The name of the field</param>
    /// <param name="fieldType">The type of the field</param>
    public HydraFieldInfo(string name, Type fieldType)
    {
      m_fieldName = name;
      m_fieldType = HydraTypeInfo.Build(fieldType);
    }

    /// <summary>
    /// The name of this field
    /// </summary>
    public string Name {get => m_fieldName;}

    /// <summary>
    /// The information describing the type of this field
    /// </summary>
    public HydraTypeInfo FieldType {get => m_fieldType;}

    /// <summary>
    /// Determines if another <see cref="HydraFieldInfo"/> instance describes the same field
    /// </summary>
    /// <param name="obj">The object to compare</param>
    /// <returns>True if the object is a <see cref="HydraFieldInfo"/> that describes the same field, otherwise false</returns>
    public override bool Equals(object obj)
    {
      HydraFieldInfo other = obj as HydraFieldInfo;
      if (other == null)
      {
        return false;
      }
        
      if (string.Equals(m_fieldName, other.m_fieldName))
      {
        if ((m_fieldType == null && other.m_fieldType != null) || (m_fieldType != null && other.m_fieldType == null))
        {
          return false;
        }
        else if ((m_fieldType == null && other.m_fieldType == null) ||
                 (m_fieldType.Equals(other.m_fieldType)))
        {
          return true;
        }
      }

      return false;
    }
    
    /// <summary>
    /// Gets a hashcode for this field
    /// </summary>
    /// <returns>A hash code computed from the name and type of this field</returns>
    public override int GetHashCode()
    {
      int code = 0;
      if (m_fieldName != null)
      {
        code = m_fieldName.GetHashCode();
      }

      if (m_fieldType != null)
      {
        code ^= m_fieldType.GetHashCode();
      }

      return code;
    }
  }
}