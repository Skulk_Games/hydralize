#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydralize
{
  /// <summary>
  /// A <see cref="HydraTypeInfo"/> derived class that describes a generic object
  /// </summary>
  public class GenericTypeInfo : ObjectTypeInfo
  {
    /// <summary>
    /// The type information for generic arguments
    /// </summary>
    protected HydraTypeInfo[] m_genericArguments;

    /// <summary>
    /// Default constructor
    /// </summary>
    public GenericTypeInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this <see cref="GenericTypeInfo"/> with values describing the specified generic type
    /// </summary>
    /// <param name="type">The generic object type to describe</param>
    public GenericTypeInfo(Type type) : base(type)
    {
      Type[] genericTypes = type.GetGenericArguments();
      if (genericTypes != null)
      {
        m_genericArguments = new HydraTypeInfo[genericTypes.Length];
        for (int i = 0; i < genericTypes.Length; ++i)
        {
          m_genericArguments[i] = HydraTypeInfo.Build(genericTypes[i]);
        }
      }
    }

    /// <inheritdoc/>
    public override bool IsGeneric { get => true; }

    /// <inheritdoc/>
    public override HydraTypeInfo[] GetGenericArguments()
    {
      return m_genericArguments;
    }
  }
}