#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Describes a System.Type with basic reflection values in a format that can be serialized by the <see cref="Hydralizer"/> class
  /// </summary>
  public class HydraTypeInfo
  {
    /// <summary>
    /// The name of the type
    /// </summary>
    protected string m_name;

    /// <summary>
    /// The Hydra type code for the type
    /// </summary>
    protected HydraTypeCodes m_typeCode;

    /// <summary>
    /// Default constructor
    /// </summary>
    public HydraTypeInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this type with values describing the indicated type
    /// </summary>
    /// <param name="type">The type to describe</param>
    public HydraTypeInfo(Type type)
    {
      m_name = type.Name;
      m_typeCode = HydraType.DetermineType(type);
    }

    /// <summary>
    /// Explicit cast from <see cref="Type"/> to a <see cref="HydraTypeInfo"/>
    /// </summary>
    /// <param name="type">The type to cast to a <see cref="HydraTypeInfo"/></param>
    public static explicit operator HydraTypeInfo(Type type)
    {
      return HydraTypeInfo.Build(type);
    }

    /// <summary>
    /// The name of the type
    /// </summary>
    public string Name {get => m_name;}

    /// <summary>
    /// The Hydra type code for the type
    /// </summary>
    public HydraTypeCodes TypeCode {get => m_typeCode;}

    /// <summary>
    /// Indicates whether this type is an enum
    /// </summary>
    public virtual bool IsEnum { get => false; }

    /// <summary>
    /// Indicates whether this type is a generic object
    /// </summary>
    public virtual bool IsGeneric { get => false;}

    /// <summary>
    /// Indicates whether this type is an array
    /// </summary>
    public virtual bool IsArray { get => false; }

    /// <summary>
    /// Builds a <see cref="HydraTypeInfo"/> from the specified <see cref="Type"/>
    /// </summary>
    /// <param name="type">The <see cref="Type"/> to describe as a <see cref="HydraTypeInfo"/></param>
    /// <returns>A new <see cref="HydraTypeInfo"/> instance describing the specified type</returns>
    public static HydraTypeInfo Build(Type type)
    {
      HydraTypeCodes typeCode = HydraType.DetermineType(type);
      switch (typeCode)
      {
        case HydraTypeCodes.INT8:
        case HydraTypeCodes.INT16:
        case HydraTypeCodes.INT32:
        case HydraTypeCodes.INT64:
        case HydraTypeCodes.UINT8:
        case HydraTypeCodes.UINT16:
        case HydraTypeCodes.UINT32:
        case HydraTypeCodes.UINT64:
        case HydraTypeCodes.FLOAT:
        case HydraTypeCodes.DOUBLE:
        case HydraTypeCodes.BOOL:
        case HydraTypeCodes.CHAR:
        case HydraTypeCodes.DATETIME:
        case HydraTypeCodes.STRING:
        {
          // Primitives
          if (type.IsEnum)
          {
            return new EnumTypeInfo(type);
          }
          else
          {
            return new HydraTypeInfo(type);
          }
        }

        case HydraTypeCodes.OBJECT:
        case HydraTypeCodes.HYDRA_OBJECT:
        {
          if (type.IsGenericType)
          {
            return new GenericTypeInfo(type);
          }
          else
          {
            return new ObjectTypeInfo(type);
          }
        }

        case HydraTypeCodes.ARRAY_BOOL:
        case HydraTypeCodes.ARRAY_CHAR:
        case HydraTypeCodes.ARRAY_DATETIME:
        case HydraTypeCodes.ARRAY_DOUBLE:
        case HydraTypeCodes.ARRAY_FLOAT:
        case HydraTypeCodes.ARRAY_HYDRA_OBJECT:
        case HydraTypeCodes.ARRAY_INT16:
        case HydraTypeCodes.ARRAY_INT32:
        case HydraTypeCodes.ARRAY_INT64:
        case HydraTypeCodes.ARRAY_INT8:
        case HydraTypeCodes.ARRAY_OBJECT:
        case HydraTypeCodes.ARRAY_STRING:
        case HydraTypeCodes.ARRAY_UINT16:
        case HydraTypeCodes.ARRAY_UINT32:
        case HydraTypeCodes.ARRAY_UINT64:
        case HydraTypeCodes.ARRAY_UINT8:
        {
          return new ArrayTypeInfo(type);
        }

        default:
          throw new Exception(string.Format("Unhandled map type: {0}", typeCode));
      }
    }

    /// <summary>
    /// Gets the generic arguments for this type
    /// </summary>
    /// <returns>The generic arguments for this type or null if it doesn't have any</returns>
    public virtual HydraTypeInfo[] GetGenericArguments()
    {
      return null;
    }

    /// <summary>
    /// Gets the fields on this type
    /// </summary>
    /// <returns>An array of fields on this type or null if there are none</returns>
    public virtual HydraFieldInfo[] GetFields()
    {
      return null;
    }

    /// <summary>
    /// Gets the specified field on this type
    /// </summary>
    /// <param name="path">A full '.' delimited path to the field info to get</param>
    /// <returns>The <see cref="HydraFieldInfo"/> object describing the field with the matching path or null if not found</returns>
    public virtual HydraFieldInfo GetField(string path)
    {
      return null;
    }

    /// <summary>
    /// Gets an array of the names of each enumeration item for enum types
    /// </summary>
    /// <returns>The array of the names for the enumeration items or null if there are none</returns>
    public virtual string[] GetEnumNames()
    {
      return null;
    }

    /// <summary>
    /// Gets an array of the values for this enum type
    /// </summary>
    /// <returns>The array of enum values on this type or null if there are none</returns>
    public virtual Array GetEnumValues()
    {
      return null;
    }

    /// <summary>
    /// Gets the type information for the element of array types
    /// </summary>
    /// <returns>The element type information if this type is an array, otherwise null</returns>
    public virtual HydraTypeInfo GetElementType()
    {
      return null;
    }

    /// <summary>
    /// Gets an array of paths to fields and subfields on this type
    /// </summary>
    /// <returns>An array of paths to fields and subfields on this type or null if there are none</returns>
    public virtual string[] GetPaths()
    {
      return null;
    }

    /// <summary>
    /// Determines whether another <see cref="HydraTypeInfo"/> describes the same type.
    /// </summary>
    /// <param name="obj">The object to compare against this instance</param>
    /// <returns>True if the objects describe the same type, otherwise false</returns>
    public override bool Equals(object obj)
    {
      HydraTypeInfo other = obj as HydraTypeInfo;
      if (other == null)
      {
        return false;
      }
        
      if (TypeCode == other.TypeCode && Name.Equals(other.Name) && IsEnum == other.IsEnum && IsGeneric == other.IsGeneric)
      {
        if (IsGeneric)
        {
          var args = GetGenericArguments();
          var otherArgs = other.GetGenericArguments();
          if ((args == null && otherArgs != null) || (otherArgs == null && args != null))
          {
            return false;
          }

          if (args.Length != otherArgs.Length)
          {
            return false;
          }

          for (int i = 0; i < args.Length; ++i)
          {
            if (!args[i].Equals(otherArgs[i]))
            {
              return false;
            }
          }
        }

        if (IsArray)
        {
          if (!GetElementType().Equals(other.GetElementType()))
          {
            return false;
          }
        }

        // Check fields
        var fields = GetFields();
        var otherFields = other.GetFields();
        if ((fields != null && otherFields == null) || (fields == null && otherFields != null))
        {
          return false;
        }

        if (fields != null)
        {
          for (int i = 0; i < fields.Length; ++i)
          {
            if (!fields[i].Equals(otherFields[i]))
            {
              return false;
            }
          }
        }

        if (IsEnum)
        {
          var names = GetEnumNames();
          var values = GetEnumValues();
          var otherNames = other.GetEnumNames();
          var otherValues = other.GetEnumValues();
          if (names.Length != otherNames.Length)
          {
            return false;
          }

          for (int i = 0; i < names.Length; ++i)
          {
            if (!string.Equals(names[i], otherNames[i]))
            {
              return false;
            }

            if (!values.GetValue(i).Equals(otherValues.GetValue(i)))
            {
              return false;
            }
          }
        }

        return true;
      }

      return false;
    }
    
    /// <summary>
    /// Gets a hash code value for this <see cref="HydraTypeInfo"/> instance.
    /// </summary>
    /// <returns>A hash code calculated from the value of this <see cref="HydraTypeInfo"/> instance</returns>
    public override int GetHashCode()
    {
      int hash = m_name?.GetHashCode() ?? 0 ^ (byte)m_typeCode ^ (IsEnum ? 1 : 0) ^ (IsGeneric ? 2 : 0);
      if (IsGeneric)
      {
        var args = GetGenericArguments();
        foreach (var arg in args)
        {
          hash ^= arg.GetHashCode();
        }
      }

      if (IsArray)
      {
        hash ^= GetElementType().GetHashCode();
      }

      var fields = GetFields();
      if (fields != null)
      {
        foreach (var field in fields)
        {
          hash ^= field.GetHashCode();
        }
      }

      if (IsEnum)
      {
        foreach (var name in GetEnumNames())
        {
          hash ^= name.GetHashCode();
        }

        foreach (var value in GetEnumValues())
        {
          hash ^= value.GetHashCode();
        }
      }

      return hash;
    }
  }
}