#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Contains information that describes an object
  /// </summary>
  public class ObjectMap
  {
    /// <summary>
    /// The name of this map
    /// </summary>
    private string m_name;

    /// <summary>
    /// The type this map describes
    /// </summary>
    private HydraTypeInfo m_typeInfo;

    /// <summary>
    /// Some attribute information specified on the type
    /// </summary>
    private List<ObjectEntryAttribute> m_attributes = new List<ObjectEntryAttribute>();

    /// <summary>
    /// The name of this object
    /// </summary>
    public string Name { get => m_name; set => m_name = value; }

    /// <summary>
    /// The type information for this object
    /// </summary>
    public HydraTypeInfo TypeInfo { get => m_typeInfo; set => m_typeInfo = value; }

    /// <summary>
    /// The list of custom attributes providing additional information about this object
    /// </summary>
    public List<ObjectEntryAttribute> Attributes { get => m_attributes; set => m_attributes = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public ObjectMap()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this map describing the specified type
    /// </summary>
    /// <param name="type">The type to describe in this map</param>
    public ObjectMap(Type type)
    {
      m_name = type.Name;
      m_typeInfo = HydraTypeInfo.Build(type);
      AddAttributes(type, this);
    }

    /// <summary>
    /// Gets all the path names to fields in this map
    /// </summary>
    /// <returns>The list of path names to fields in this map</returns>
    public List<string> GetPaths()
    {
      List<string> paths = new List<string>();

      if (m_typeInfo != null)
      {
        string[] typePaths = m_typeInfo.GetPaths();
        if (typePaths != null)
        {
          paths.AddRange(typePaths);
        }
      }

      return paths;
    }

    /// <summary>
    /// Searches for the field with the full path name specified. The path should be a '.' delimited sequence of field names.
    /// </summary>
    /// <param name="fullName">The period delimited field name path</param>
    /// <returns>The matching field info or null if no field matched</returns>
    public HydraFieldInfo GetField(string fullName)
    {
      return m_typeInfo.GetField(fullName);
    }

    /// <summary>
    /// Uses information in this map to add the value to the <see cref="HydralizeInfo"/> object
    /// </summary>
    /// <param name="fullName">The full path name for the value to set</param>
    /// <param name="value">The value to set for the item</param>
    /// <param name="info">The object where the values will be added</param>
    /// <returns>True if the object was added to the info object, false otherwise</returns>
    public bool AddObjectInfo(string fullName, object value, ref HydralizeInfo info)
    {
      HydraFieldInfo field = GetField(fullName);
      if (field != null)
      {
        return info.AddInfo(fullName, value, field.FieldType);
      }

      return false;
    }

    /// <summary>
    /// Adds the attributes from a type to the map
    /// </summary>
    /// <param name="type">The type to get attributes from</param>
    /// <param name="map">The map to add the attributes on</param>
    public static void AddAttributes(Type type, ObjectMap map)
    {
      object[] attribs = type.GetCustomAttributes(true);
      for (int i = 0; i < attribs.Length; ++i)
      {
        ObjectEntryAttribute entryAttribute = attribs[i] as ObjectEntryAttribute;
        if (entryAttribute != null)
        {
          map.Attributes.Add(entryAttribute);
        }
      }
    }

    /// <summary>
    /// Adds the attributes found on the specified field to the map entry
    /// </summary>
    /// <param name="fieldInfo">The field to get the attributes from</param>
    /// <param name="map">The map to add the attributes on</param>
    public static void AddAttributes(FieldInfo fieldInfo, ObjectMap map)
    {
      object[] attribs = fieldInfo.GetCustomAttributes(true);
      for (int i = 0; i < attribs.Length; ++i)
      {
        ObjectEntryAttribute entryAttribute = attribs[i] as ObjectEntryAttribute;
        if (entryAttribute != null)
        {
          map.Attributes.Add(entryAttribute);
        }
      }
    }
  }
}
