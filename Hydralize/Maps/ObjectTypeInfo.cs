#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Reflection;

namespace Skulk.Hydralize
{
  /// <summary>
  /// A <see cref="HydraTypeInfo"/> derived class that describes an object type
  /// </summary>
  public class ObjectTypeInfo : HydraTypeInfo
  {
    /// <summary>
    /// An array of the fields on the type
    /// </summary>
    protected HydraFieldInfo[] m_fields;

    /// <summary>
    /// Default constructor
    /// </summary>
    public ObjectTypeInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this <see cref="ObjectTypeInfo"/> with values describing the specified object type
    /// </summary>
    /// <param name="type">The object type to describe</param>
    public ObjectTypeInfo(Type type) : base(type)
    {
      if (m_typeCode == HydraTypeCodes.OBJECT || m_typeCode == HydraTypeCodes.HYDRA_OBJECT)
      {
        IHydralizeConverter converter = Hydralizer.LookupConverter(type);
        if (converter != null)
        {
          m_fields = converter.GetFields(type);
        }
        else
        {
          var fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
          if (fields != null)
          {
            m_fields = new HydraFieldInfo[fields.Length];
            for (int i = 0; i < fields.Length; ++i)
            {
              m_fields[i] = new HydraFieldInfo(fields[i]);
            }
          }
        }
      }
    }

    /// <inheritdoc/>
    public override HydraFieldInfo[] GetFields()
    {
      return m_fields;
    }

    /// <inheritdoc/>
    public override HydraFieldInfo GetField(string path)
    {
      if (m_fields != null)
      {
        int pos = path.IndexOf('.');
        string member = null;
        string subPath = null;
        if (pos > 0)
        {
          member = path.Substring(0, pos);
          subPath = path.Substring(pos + 1);
        }
        else
        {
          member = path;
        }

        for (int i = 0; i < m_fields.Length; ++i)
        {
          if (m_fields[i].Name == member)
          {
            if (subPath != null)
            {
              return m_fields[i].FieldType.GetField(subPath);
            }
            else
            {
              return m_fields[i];
            }
          }
        }
      }

      return null;
    }

    /// <inheritdoc/>
    public override string[] GetPaths()
    {
      if (m_fields != null)
      {
        System.Collections.Generic.List<string> paths = new System.Collections.Generic.List<string>();
        for (int i = 0; i < m_fields.Length; ++i)
        {
          paths.Add(m_fields[i].Name);
          string[] subPaths = m_fields[i].FieldType.GetPaths();
          if (subPaths != null)
          {
            for (int j = 0; j < subPaths.Length; ++j)
            {
              paths.Add(string.Format("{0}.{1}", m_fields[i].Name, subPaths[j]));
            }
          }
        }

        return paths.ToArray();
      }

      return null;
    }
  }
}