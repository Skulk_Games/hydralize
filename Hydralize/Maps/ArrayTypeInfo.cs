#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydralize
{
  /// <summary>
  /// A <see cref="HydraTypeInfo"/> derived class that describes array types.
  /// </summary>
  public class ArrayTypeInfo : HydraTypeInfo
  {
    /// <summary>
    /// The type information for the array element type
    /// </summary>
    protected HydraTypeInfo m_elementType;

    /// <summary>
    /// Default constructor
    /// </summary>
    public ArrayTypeInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this <see cref="ArrayTypeInfo"/> with values describing the specified type
    /// </summary>
    /// <param name="type">The array type to describe</param>
    public ArrayTypeInfo(Type type) : base(type)
    {
      m_elementType = HydraTypeInfo.Build(type.GetElementType());
    }

    /// <inheritdoc/>
    public override bool IsArray { get => true; }

    /// <inheritdoc/>
    public override HydraTypeInfo GetElementType()
    {
      return m_elementType;
    }

    /// <inheritdoc/>
    public override HydraFieldInfo GetField(string path)
    {
      if (m_elementType != null)
      {
        return m_elementType.GetField(path);
      }
      else
      {
        return null;
      }
    }
  }
}