#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Holds data that describes an entry in an <see cref="ObjectMap"/>
  /// </summary>
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
  public class ObjectEntryAttribute : Attribute
  {
    private string m_name;
    private object m_data;

    /// <summary>
    /// The name of this attribute
    /// </summary>
    public string Name { get => m_name; set => m_name = value; }

    /// <summary>
    /// The data associated with this attribute
    /// </summary>
    public object Data { get => m_data; set => m_data = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public ObjectEntryAttribute()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this entry attribute with the data specified
    /// </summary>
    /// <param name="name">The name of this attribute</param>
    /// <param name="data">The data for this attribute</param>
    public ObjectEntryAttribute(string name, object data)
    {
      m_name = name;
      m_data = data;
    }
  }
}
