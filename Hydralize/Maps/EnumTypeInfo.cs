#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;

namespace Skulk.Hydralize
{
  /// <summary>
  /// A <see cref="HydraTypeInfo"/> derived class that describes Enum types
  /// </summary>
  public class EnumTypeInfo : HydraTypeInfo
  {
    /// <summary>
    /// The array of enum names
    /// </summary>
    protected string[] m_enumNames;

    /// <summary>
    /// The array of enum values
    /// </summary>
    protected object[] m_enumValues;

    /// <summary>
    /// Default constructor
    /// </summary>
    public EnumTypeInfo()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this <see cref="EnumTypeInfo"/> with values describing the specified enum type
    /// </summary>
    /// <param name="type">The enum type to describe</param>
    public EnumTypeInfo(Type type) : base(type)
    {
      m_enumNames = type.GetEnumNames();
      Array values = type.GetEnumValues();
      m_enumValues = new object[values.Length];
      for (int i = 0; i < m_enumValues.Length; ++i)
      {
        m_enumValues[i] = Convert.ChangeType(values.GetValue(i), type.GetEnumUnderlyingType());
      }
    }

    /// <inheritdoc/>
    public override bool IsEnum { get => true; }

    /// <inheritdoc/>
    public override string[] GetEnumNames()
    {
      return m_enumNames;
    }

    /// <inheritdoc/>
    public override Array GetEnumValues()
    {
      return m_enumValues;
    }
  }
}