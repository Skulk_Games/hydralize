#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Skulk.Hydralize
{
  /// <summary>
  /// Describes a generic type argument in a <see cref="HydralizeInfo"/> that is indexed
  /// </summary>
  /// <remarks>
  /// Allows generic argument types to be included with indexed object info so that indexed objects can
  /// support arbitrary generic types
  /// </remarks>
  public class IndexedGenericType
  {
    /// <summary>
    /// The minimum number of bytes it takes to serialize indexed generic type information
    /// </summary>
    /// <remarks>
    /// 1 byte for the assembly ID, 4 for the type index, and another 1 to specify the sub args length
    /// </remarks>
    private const int BASE_SIZE = 6;

    /// <summary>
    /// The index of the assembly that contains the type
    /// </summary>
    private byte m_assembly;

    /// <summary>
    /// The index of the type in the assembly
    /// </summary>
    private int m_typeIndex;

    /// <summary>
    /// An array of indexed sub types if this level describes a generic type
    /// </summary>
    private IndexedGenericType[] m_subArgs;

    /// <summary>
    /// The index of the assembly that contains the type
    /// </summary>
    public byte Assembly { get => m_assembly; set => m_assembly = value; }

    /// <summary>
    /// The index of the type in the assembly or group
    /// </summary>
    public int TypeIndex { get => m_typeIndex; set => m_typeIndex = value; }

    /// <summary>
    /// An array of indexed sub types if this level describes a generic type
    /// </summary>
    public IndexedGenericType[] SubArgs { get => m_subArgs; set => m_subArgs = value; }

    /// <summary>
    /// Default constructor
    /// </summary>
    public IndexedGenericType()
    {
      // Empty
    }

    /// <summary>
    /// Initializes this instance to describe the specified type
    /// </summary>
    /// <param name="type">The type to describe</param>
    /// <param name="context">The index context to use</param>
    public IndexedGenericType(Type type, IndexContext context)
    {
      Init(type, context);
    }

    /// <summary>
    /// Initializes this instance to describe the specified type using the indicated index context
    /// </summary>
    /// <param name="type">The type to describe</param>
    /// <param name="context">The index context to use</param>
    public void Init(Type type, IndexContext context)
    {
      if (!type.IsEnum)
      {
        HydraTypeCodes hydraTypeCode = HydraType.DetermineType(type);
        if (HydraType.IsPrimitiveType(hydraTypeCode))
        {
          // Flip the sign and store the typecode for primitive types
          m_typeIndex = -1 * (byte)hydraTypeCode;
          return;
        }
      }

      if (context.Index(type, out m_assembly, out m_typeIndex))
      {
        if (type.IsGenericType)
        {
          Type[] args = type.GetGenericArguments();
          m_subArgs = new IndexedGenericType[args.Length];
          for (int i = 0; i < args.Length; ++i)
          {
            m_subArgs[i] = new IndexedGenericType(args[i], context);
          }
        }
      }
    }

    /// <summary>
    /// Gets the type this instance describes using the specified index context
    /// </summary>
    /// <param name="context">The index context to use when resolving type indices</param>
    /// <returns>The generic type described by this instance</returns>
    public Type GetGenericType(IndexContext context)
    {
      if (m_typeIndex < 0)
      {
        return HydraType.DetermineType((HydraTypeCodes)(-1 * m_typeIndex));
      }
      else
      {
        Type baseType = null;
        if (context == null)
        {
          baseType = Hydralizer.DefaultContext.Lookup(m_assembly, m_typeIndex);
        }
        else
        {
          baseType = context.Lookup(m_assembly, m_typeIndex);
        }

        if (baseType != null && m_subArgs != null && baseType.IsGenericType)
        {
          Type[] args = new Type[m_subArgs.Length];
          for (int i = 0; i < args.Length; ++i)
          {
            args[i] = m_subArgs[i].GetGenericType(context);
          }

          return baseType.MakeGenericType(args);
        }
        else
        {
          return baseType;
        }
      }
    }

    /// <summary>
    /// Calculates how many bytes will be used to store this type information when serializing
    /// </summary>
    /// <returns>The number of bytes this type information will use when serialized</returns>
    public int CalculateByteSize()
    {
      // Takes 5 for the assembly ID and type index
      // Takes another 1 to specify sub args array length
      int size = BASE_SIZE;
      if (m_subArgs != null)
      {
        for (int i = 0; i < m_subArgs.Length; ++i)
        {
          size += m_subArgs[i].CalculateByteSize();
        }
      }

      return size;
    }

    /// <summary>
    /// Puts this type information into the specified array of bytes
    /// </summary>
    /// <param name="array">The array of bytes to serialize this type information into</param>
    /// <param name="offset">The index of the first byte to use in the array</param>
    /// <returns>True if the type information fits and was placed in the array, otherwise false</returns>
    public bool Put(byte[] array, ref int offset)
    {
      // Calculate the byte size of the data
      int size = CalculateByteSize();

      if (array.Length >= offset + size)
      {
        array[offset++] = m_assembly;
        Hydralizer.Put(array, m_typeIndex, ref offset, false);
        if (m_subArgs != null)
        {
          array[offset++] = (byte)m_subArgs.Length;
          for (int i = 0; i < m_subArgs.Length; ++i)
          {
            m_subArgs[i].Put(array, ref offset);
          }
        }
        else
        {
          array[offset++] = 0;
        }

        return true;
      }

      return false;
    }

    /// <summary>
    /// Gets type information from an array of serialized bytes
    /// </summary>
    /// <param name="array">The array of serialized bytes</param>
    /// <param name="offset">The index of the first byte to read</param>
    /// <returns>True if type information was successfully read, otherwise false</returns>
    public bool Get(byte[] array, ref int offset)
    {
      if (offset + (BASE_SIZE) <= array.Length)
      {
        // Read assembly ID
        m_assembly = Hydralizer.GetByte(array, ref offset);
        m_typeIndex = Hydralizer.GetInt32(array, ref offset);
        byte subLength = Hydralizer.GetByte(array, ref offset);
        if (subLength > 0)
        {
          m_subArgs = new IndexedGenericType[subLength];
          for (int i = 0; i < subLength; ++i)
          {
            m_subArgs[i] = new IndexedGenericType();
            if (!m_subArgs[i].Get(array, ref offset))
            {
              return false;
            }
          }
        }

        return true;
      }

      return false;
    }

    /// <summary>
    /// Writes the indexed generic type information to an XML tag
    /// </summary>
    /// <param name="level">The indent level of this tag</param>
    /// <returns>The string for the XML tag that represents this data</returns>
    public string ToXml(int level = 0)
    {
      StringBuilder indent = new StringBuilder();
      for (int i = 0; i < level; ++i)
      {
        indent.Append("  ");
      }

      StringBuilder tag = new StringBuilder();
      if (m_subArgs != null && m_subArgs.Length > 0)
      {
        tag.AppendFormat("{0}<IndexedArg assembly='{1}' type='{2}'>\n", indent.ToString(), m_assembly, m_typeIndex);
        for (int i = 0; i < m_subArgs.Length; ++i)
        {
          tag.Append(m_subArgs[i].ToXml(level + 1));
        }
        tag.AppendFormat("{0}</IndexedArg>", indent.ToString());
      }
      else
      {
        tag.AppendFormat("{0}<IndexedArg assembly='{1}' type='{2}'/>\n", indent.ToString(), m_assembly, m_typeIndex);
      }

      return tag.ToString();
    }

    /// <summary>
    /// Loads the data for this object from an XML node
    /// </summary>
    /// <param name="node">The XML node containing the data for this object</param>
    public void FromXml(XmlNode node)
    {
      if (node == null)
      {
        return;
      }

      byte.TryParse(node.Attributes["assembly"].Value, out m_assembly);
      int.TryParse(node.Attributes["type"].Value, out m_typeIndex);
      if (node.HasChildNodes)
      {
        m_subArgs = new IndexedGenericType[node.ChildNodes.Count];
        for (int i = 0; i < m_subArgs.Length; ++i)
        {
          m_subArgs[i] = new IndexedGenericType();
          m_subArgs[i].FromXml(node.ChildNodes[i]);
        }
      }
    }

    /// <summary>
    /// Stores the data on this instance into a JSON object
    /// </summary>
    /// <returns>The newly created JSON object that holds the data for this instance</returns>
    public JSON.JsonObject ToJson()
    {
      JSON.JsonObject obj = new JSON.JsonObject();
      obj.Elements.Add("assembly", new JSON.JsonElement() { Key = "assembly", TokenValue = new JSON.JsonValue() { Value = m_assembly } });
      obj.Elements.Add("type", new JSON.JsonElement() { Key = "type", TokenValue = new JSON.JsonValue() { Value = m_typeIndex } });
      if (m_subArgs != null && m_subArgs.Length > 0)
      {
        JSON.JsonArray subArgs = new JSON.JsonArray();
        for (int i = 0; i < m_subArgs.Length; ++i)
        {
          subArgs.Values.Add(m_subArgs[i].ToJson());
        }

        obj.Elements.Add("args", new JSON.JsonElement() { Key = "args", TokenValue = subArgs });
      }

      return obj;
    }

    /// <summary>
    /// Loads the values for this instance from the specified JSON object
    /// </summary>
    /// <param name="jsonObject">The JSON object containing the values to load on this instance</param>
    public void FromJson(JSON.JsonObject jsonObject)
    {
      if (jsonObject.Elements.TryGetValue("assembly", out JSON.JsonElement assemblyElement))
      {
        m_assembly = (byte)(ulong)(assemblyElement.TokenValue as JSON.JsonValue).Value;
      }
      if (jsonObject.Elements.TryGetValue("type", out JSON.JsonElement typeElement))
      {
        m_typeIndex = (int)Convert.ChangeType((typeElement.TokenValue as JSON.JsonValue).Value, typeof(int));
      }
      if (jsonObject.Elements.TryGetValue("args", out JSON.JsonElement args))
      {
        List<JSON.JsonToken> tokens = (args.TokenValue as JSON.JsonArray).Values;
        m_subArgs = new IndexedGenericType[tokens.Count];
        for (int i = 0; i < tokens.Count; ++i)
        {
          m_subArgs[i] = new IndexedGenericType();
          m_subArgs[i].FromJson(tokens[i] as JSON.JsonObject);
        }
      }
    }
  }
}
