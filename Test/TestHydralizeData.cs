#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydralize;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Skulk.Test
{
  /// <summary>
  /// Contains test for the <see cref="HydralizeData"/> class.
  /// </summary>
  public class TestHydralizeData
  {
    [Test]
    public void ListToArray()
    {
      Array types = Enum.GetValues(typeof(HydraTypeCodes));
      for (int i = 0; i < types.Length; ++i)
      {
        HydraTypeCodes typeCode = (HydraTypeCodes)types.GetValue(i);

        // Test simple primitives
        if (typeCode != HydraTypeCodes.INVALID && (HydraTypeCodes.NULL_FLAG & typeCode) == 0 &&
            typeCode != HydraTypeCodes.ENUM && !HydraType.IsArrayType(typeCode) &&
            HydraType.IsPrimitiveType(typeCode))
        {
          Type elementType = HydraType.DetermineType(typeCode);
          Type listType = typeof(List<>).MakeGenericType(elementType);
          IList list = Activator.CreateInstance(listType) as IList;
          Array emptyArray = HydralizeData.ListToArray(list);
          Assert.AreEqual(0, emptyArray.Length);
          Assert.AreEqual(HydraType.DetermineType(HydraType.ToArrayType(typeCode)), emptyArray.GetType());

          // Now add a value
          object addedValue = null;
          if (typeCode != HydraTypeCodes.STRING)
          {
            addedValue = Activator.CreateInstance(elementType);
          }
          else
          {
            addedValue = string.Empty;
          }
          
          list.Add(addedValue);
          Array singleArray = HydralizeData.ListToArray(list);
          Assert.AreEqual(1, singleArray.Length);
          Assert.AreEqual(HydraType.DetermineType(HydraType.ToArrayType(typeCode)), singleArray.GetType());
          Assert.AreEqual(addedValue, singleArray.GetValue(0));
        }
      }
    }

    /// <summary>
    ///  Test serializing values to and from XML for some primitive types.
    /// </summary>
    [Test]
    public void ObjectFromXml()
    {
      PrimitivesTestClass testClass = new PrimitivesTestClass()
      {
        UData8 = 1,
        UData16 = 2,
        UData32 = 3,
        UData64 = 4,
        SData8 = 5,
        SData16 = 6,
        SData32 = 7,
        SData64 = 8,
        BoolData = true,
        CharData = 'a' 
      };

      HydralizeInfo info = new HydralizeInfo();
      testClass.GetObjectData(info);
      string xml = info.ToXml();
      HydralizeInfo result = new HydralizeInfo();
      result.FromXml(xml);
      PrimitivesTestClass resultPrimitives = new PrimitivesTestClass();
      resultPrimitives.LoadObjectData(info);
      Assert.AreEqual(testClass, resultPrimitives);

      // Test DateTime
      DateTime now = DateTime.Now;
      HydralizeData data = new HydralizeData(now);
      xml = data.ToXml("datetime");
      System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
      xmlDoc.LoadXml(xml);
      HydralizeData dateTimeData = new HydralizeData(xmlDoc.FirstChild);
      Assert.AreEqual(now, dateTimeData.Data);

      // Test String
      string greeting = "Hello, World";
      data = new HydralizeData(greeting);
      xml = data.ToXml("greeting");
      xmlDoc.LoadXml(xml);
      HydralizeData greetingResult = new HydralizeData(xmlDoc.FirstChild);
      Assert.AreEqual(greeting, greetingResult.Data);

      // null string
      greeting = null;
      data = new HydralizeData(greeting);
      xml = data.ToXml("greeting");
      xmlDoc.LoadXml(xml);
      greetingResult = new HydralizeData(xmlDoc.FirstChild);
      // null becomes empty string with XML parsing
      Assert.AreEqual(string.Empty, greetingResult.Data);

      // Test Double
      double testDouble = 555.123456;
      data = new HydralizeData(testDouble);
      xml = data.ToXml("double");
      xmlDoc.LoadXml(xml);
      HydralizeData doubleResult = new HydralizeData(xmlDoc.FirstChild);
      Assert.AreEqual(testDouble, doubleResult.Data);

      // Test Float
      float testFloat = 123.45678f;
      data = new HydralizeData(testFloat);
      xml = data.ToXml("float");
      xmlDoc.LoadXml(xml);
      HydralizeData floatResult = new HydralizeData(xmlDoc.FirstChild);
      Assert.AreEqual(testFloat, floatResult.Data);
    }
  }
}