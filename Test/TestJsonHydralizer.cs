#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydralize;
using Skulk.Hydralize.JSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skulk.Test
{
  public class TestJsonHydralizer
  {
    [Test]
    public void JsonWrite()
    {
      JsonObject jsonObj = new JsonObject();
      jsonObj.Elements.Add("name", new JsonElement() { Key = "name", TokenValue = new JsonValue() { Value = "bob" } });
      jsonObj.Elements.Add("id", new JsonElement() { Key = "id", TokenValue = new JsonValue() { Value = 1 } });

      JsonArray arr = new JsonArray();
      arr.Values.Add(new JsonValue() { Value = "hi" });
      arr.Values.Add(new JsonValue() { Value = 13 });
      arr.Values.Add(new JsonValue() { Value = true });
      arr.Values.Add(new JsonValue() { Value = "Hi my name is \"Bob\"" });

      JsonObject subObj = new JsonObject();
      subObj.Elements.Add("path", new JsonElement() { Key = "path", TokenValue = new JsonValue() { Value = "/home/something.txt" } });
      arr.Values.Add(subObj);
      jsonObj.Elements.Add("array", new JsonElement() { Key = "array", TokenValue = arr });

      string json = jsonObj.Write();
      Console.WriteLine(json);

      JsonObject resultObj = new JsonObject();
      int offset = 0;
      Assert.IsTrue(resultObj.Read(json, ref offset));
      Assert.AreEqual(3, resultObj.Elements.Count);
      Assert.IsTrue(resultObj.Elements.ContainsKey("name"));
      Assert.IsTrue(resultObj.Elements.ContainsKey("id"));
      Assert.AreEqual(JsonTokenType.Value, resultObj.Elements["name"].TokenValue.TokenType);
      JsonValue jsonValue = resultObj.Elements["name"].TokenValue as JsonValue;
      Assert.IsNotNull(jsonValue);
      Assert.AreEqual("bob", jsonValue.Value);
      JsonArray jsonArray = resultObj.Elements["array"].TokenValue as JsonArray;
      Assert.IsNotNull(jsonArray);
      Assert.AreEqual(arr.Values.Count, jsonArray.Values.Count);
      for (int i = 0; i < arr.Values.Count; ++i)
      {
        switch(arr.Values[i].TokenType)
        {
          case JsonTokenType.Value:
          {
            Assert.AreEqual(((JsonValue)arr.Values[i]).Value, ((JsonValue)jsonArray.Values[i]).Value);
            break;
          }
        }
      }
    }

    [Test]
    public void SerializeEnums()
    {
      EnumsTestClass enums = new EnumsTestClass()
      {
        EnumVal = TestEnum.THIRD,
        EnumVals = new TestEnum[] { TestEnum.THIRD, TestEnum.SECOND, TestEnum.FIRST }
      };

      HydralizeInfo info = Hydralizer.SaveObjectInfo(enums, false);
      string json = info.ToJson(0);
      Console.WriteLine(json);

      HydralizeInfo resultInfo = new HydralizeInfo();
      JsonObject resultObj = new JsonObject();
      int offset = 0;
      Assert.IsTrue(resultObj.Read(json, ref offset));
      resultInfo.FromJson(resultObj);

      EnumsTestClass resultEnums = new EnumsTestClass();
      resultEnums.LoadObjectData(resultInfo);

      Assert.AreEqual(enums.EnumVal, resultEnums.EnumVal);
      Assert.AreEqual(enums.EnumVals, resultEnums.EnumVals);
    }

    [Test]
    public void SerializeObjectDictionary()
    {
      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(1, typeof(IdTestClass).Assembly));
      Assert.IsTrue(Hydralizer.DefaultContext.AddGroup(2, Hydralizer.CoreTypes));
      Hydralizer.AddConverter(new DictionaryConverter());
      Dictionary<string, object> dict = new Dictionary<string, object>();
      dict.Add("id", 3);
      dict.Add("name", "fred");
      dict.Add("obj", new IdTestClass() { Id = 13 });

      HydralizeInfo info = Hydralizer.SaveObjectInfo(dict, false, false);
      string json = info.ToJson(0);
      Console.WriteLine(json);

      info = new HydralizeInfo();
      JsonObject jsonObject = new JsonObject();
      int offset = 0;
      jsonObject.Read(json, ref offset);
      info.FromJson(jsonObject);

      Dictionary<string, object> resultDict = new Dictionary<string, object>();
      object resultObj = resultDict;
      Hydralizer.LoadObjectInfo(ref resultObj, info);
      Assert.AreEqual(dict.Count, resultDict.Count);
      Assert.AreEqual(dict["id"], resultDict["id"]);
      Assert.AreEqual(dict["name"], resultDict["name"]);
      var resultIdClass = resultDict["obj"] as IdTestClass;
      // An arbitrary non-primitive object will fail without indexing to support polymorphism
      Assert.IsNull(resultIdClass);

      // Now try again while indexing
      info = Hydralizer.SaveObjectInfo(dict, false, true);
      json = info.ToJson(0);
      Console.WriteLine(json);

      info = new HydralizeInfo();
      jsonObject = new JsonObject();
      offset = 0;
      jsonObject.Read(json, ref offset);
      info.FromJson(jsonObject);
      Dictionary<string, object> idxDict = Hydralizer.Dehydralize(info) as Dictionary<string, object>;
      Assert.AreEqual(dict.Count, idxDict.Count);
      Assert.AreEqual(dict["id"], idxDict["id"]);
      Assert.AreEqual(dict["name"], idxDict["name"]);
      resultIdClass = idxDict["obj"] as IdTestClass;
      Assert.IsNotNull(resultIdClass);
      Assert.AreEqual((dict["obj"] as IdTestClass).Id, resultIdClass.Id);
    }

    [Test]
    public void Generics()
    {
      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.DefaultContext.AddAssembly(1, typeof(BaseHydralizeExample).Assembly));
      Assert.IsTrue(Hydralizer.DefaultContext.AddGroup(2, Hydralizer.CoreTypes));
      Assert.IsTrue(Hydralizer.DefaultContext.AddGroup(5, new Type[] { typeof(Queue<>) }));

      HydralizeListObject listObject = new HydralizeListObject()
      {
        BaseNum = 3,
        Ids = new List<int>() { 2, 4, 6, 8, 10 }
      };

      var info = Hydralizer.SaveObjectInfo(listObject, false, true);
      string json = info.ToJson(0);
      Console.WriteLine(json);

      info = new HydralizeInfo();
      JsonObject jsonObject = new JsonObject();
      int offset = 0;
      jsonObject.Read(json, ref offset);
      info.FromJson(jsonObject);
      var resultObj = Hydralizer.Dehydralize(info) as HydralizeListObject;
      Assert.IsNotNull(resultObj);
      Assert.AreEqual(listObject.BaseNum, resultObj.BaseNum);
      Assert.AreEqual(listObject.Ids, resultObj.Ids);

      // Dictionary
      DictionaryTestClass dictTest = new DictionaryTestClass()
      {
        Names = new Dictionary<int, string>()
        {
          { 1, "fred" },
          { 2, "bob" }
        },
        Actives = new Dictionary<int, bool>()
        {
          { 0, false },
          { 1, true }
        }
      };

      json = Hydralizer.ToJson(dictTest, false, true);
      Console.WriteLine(json);
      info = new HydralizeInfo();
      jsonObject = new JsonObject();
      offset = 0;
      jsonObject.Read(json, ref offset);
      info.FromJson(jsonObject);
      var resultDict = Hydralizer.Dehydralize(info) as DictionaryTestClass;
      Assert.IsNotNull(resultDict);
      Assert.AreEqual(dictTest.Names, resultDict.Names);
      Assert.AreEqual(dictTest.Actives, resultDict.Actives);
    }
  }
}
