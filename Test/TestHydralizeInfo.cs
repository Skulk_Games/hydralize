#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydralize;
using System;
using System.Collections.Generic;

namespace Skulk.Test
{
  /// <summary>
  /// Contains test for the <see cref="HydralizeInfo"/> class.
  /// </summary>
  public class TestHydralizeInfo
  {
    /// <summary>
    /// Tests the ability to set and get the mode flags.
    /// </summary>
    [Test]
    public void SetMode()
    {
      HydralizeInfo info = new HydralizeInfo();
      Assert.AreEqual(false, info.Lite);
      Assert.AreEqual(false, info.Indexed);
      Assert.AreEqual(false, info.Direct);
      info.Lite = true;
      Assert.AreEqual(true, info.Lite);
      info.Indexed = true;
      Assert.AreEqual(true, info.Indexed);
      info.Direct = true;
      Assert.AreEqual(true, info.Direct);
      info.Lite = false;
      Assert.AreEqual(false, info.Lite);
      info.Indexed = false;
      Assert.AreEqual(false, info.Indexed);
      info.Direct = false;
      Assert.AreEqual(false, info.Direct);
    }

    /// <summary>
    /// Test the behavior when adding an invalid type.
    /// </summary>
    [Test]
    public void AddInvalidType()
    {
      // The DBNull class has its own typecode and is not handled by this library so it's useful
      // for exercising the exceptions with unhandled/invalid types
      HydralizeInfo info = new HydralizeInfo();
      Assert.Throws(typeof(Exception), () => info.AddInfo("one", null, typeof(DBNull)));
      Assert.Throws(typeof(Exception), () => info.AddInfo("two", (object)null));
      Assert.Throws(typeof(Exception), () => info.AddInfo("three", DBNull.Value));
    }

    /// <summary>
    /// Tests adding primitive types.
    /// </summary>
    [Test]
    public void AddPrimitiveInfo()
    {
      HydralizeInfo info = new HydralizeInfo();

      // enum
      info.AddInfo("enum", TestEnum.FIRST);
      info.AddInfo("enum2", (object)TestEnum.SECOND);
      Assert.AreEqual(TestEnum.FIRST, info.Get<TestEnum>("enum"));
      Assert.AreEqual(TestEnum.SECOND, info.Get<TestEnum>("enum2"));

      // enum[]
      info.ClearInfo();
      info.AddInfo("enums", new TestEnum[] { TestEnum.THIRD, TestEnum.SECOND });
      TestEnum[] enums = info.GetArray<TestEnum>("enums");
      Assert.IsNotNull(enums);
      Assert.AreEqual(2, enums.Length);
      Assert.AreEqual(TestEnum.THIRD, enums[0]);
      Assert.AreEqual(TestEnum.SECOND, enums[1]);

      // sbyte
      info.ClearInfo();
      sbyte sbyteVal = 13;
      info.AddInfo("sbyte", (sbyte)13);
      info.AddInfo("sbyte2", (object)sbyteVal);
      Assert.AreEqual(13, info.Get<sbyte>("sbyte"));
      Assert.AreEqual(13, info.Get<sbyte>("sbyte2"));

      // ulong
      info.ClearInfo();
      UInt64 u64Val = 500000;
      info.AddInfo("u64", (UInt64)100000);
      info.AddInfo("u64-2", (object)u64Val);
      Assert.AreEqual(100000, info.Get<UInt64>("u64"));
      Assert.AreEqual(500000, info.Get<UInt64>("u64-2"));

      // short
      info.ClearInfo();
      Int16 i16Val = 30000;
      info.AddInfo("i16", (short)-30000);
      info.AddInfo("i16-2", (object)i16Val);
      Assert.AreEqual(-30000, info.Get<short>("i16"));
      Assert.AreEqual(30000, info.Get<short>("i16-2"));

      // long
      info.ClearInfo();
      long i64Val = 500000;
      info.AddInfo("long", (long)-123456789);
      info.AddInfo("long-2", (object)i64Val);
      Assert.AreEqual(-123456789, info.Get<long>("long"));
      Assert.AreEqual(500000, info.Get<long>("long-2"));

      //float
      info.ClearInfo();
      float fVal = 500000.12f;
      info.AddInfo("val", 100.5f);
      info.AddInfo("val-2", (object)fVal);
      Assert.AreEqual(100.5f, info.Get<float>("val"));
      Assert.AreEqual(fVal, info.Get<float>("val-2"));

      //double
      info.ClearInfo();
      double dVal = 999999999.9999999;
      info.AddInfo("val", 111111111.1111111);
      info.AddInfo("val-2", (object)dVal);
      Assert.AreEqual(111111111.1111111, info.Get<double>("val"));
      Assert.AreEqual(dVal, info.Get<double>("val-2"));

      // byte[]
      info.ClearInfo();
      byte[] bytes = new byte[] { 1, 2, 3 };
      info.AddInfo("bytes", bytes);
      info.AddInfo("bytes-2", (object)bytes);
      byte[] resultBytes = info.GetArray<byte>("bytes");
      Assert.IsTrue(ArrayEquals(bytes, resultBytes));
      resultBytes = info.GetArray<byte>("bytes-2");
      Assert.IsTrue(ArrayEquals(bytes, resultBytes));

      // ushort[]
      info.ClearInfo();
      ushort[] ushorts = new ushort[] { 1, 2, 3 };
      info.AddInfo("ushorts", ushorts);
      info.AddInfo("ushorts-2", (object)ushorts);
      ushort[] resultUshorts = info.GetArray<ushort>("ushorts");
      Assert.IsTrue(ArrayEquals(ushorts, resultUshorts));
      resultUshorts = info.GetArray<ushort>("ushorts-2");
      Assert.IsTrue(ArrayEquals(ushorts, resultUshorts));

      // char[]
      info.ClearInfo();
      char[] chars = new char[] { 'a', 'b', 'c' };
      info.AddInfo("val", chars);
      info.AddInfo("val-2", (object)chars);
      char[] resultChars = info.GetArray<char>("val");
      Assert.IsTrue(ArrayEquals(chars, resultChars));
      resultChars = info.GetArray<char>("val-2");
      Assert.IsTrue(ArrayEquals(chars, resultChars));

      // uint[]
      info.ClearInfo();
      uint[] uints = new uint[] { 123, 345, 567 };
      info.AddInfo("val", uints);
      info.AddInfo("val-2", (object)uints);
      uint[] resultUints = info.GetArray<uint>("val");
      Assert.IsTrue(ArrayEquals(uints, resultUints));
      resultUints = info.GetArray<uint>("val-2");
      Assert.IsTrue(ArrayEquals(uints, resultUints));

      // ulong[]
      info.ClearInfo();
      ulong[] ulongs = new ulong[] { 1234, 3456, 5678 };
      info.AddInfo("val", ulongs);
      info.AddInfo("val-2", (object)ulongs);
      ulong[] resultUlongs = info.GetArray<ulong>("val");
      Assert.IsTrue(ArrayEquals(ulongs, resultUlongs));
      resultUlongs = info.GetArray<ulong>("val-2");
      Assert.IsTrue(ArrayEquals(ulongs, resultUlongs));

      // short[]
      info.ClearInfo();
      short[] shorts = new short[] { -100, 200, -300 };
      info.AddInfo("val", shorts);
      info.AddInfo("val-2", (object)shorts);
      short[] resultShorts = info.GetArray<short>("val");
      Assert.IsTrue(ArrayEquals(shorts, resultShorts));
      resultShorts = info.GetArray<short>("val-2");
      Assert.IsTrue(ArrayEquals(shorts, resultShorts));

      // int[]
      info.ClearInfo();
      int[] ints = new int[] { -1000, 2000, -3000 };
      info.AddInfo("val", ints);
      info.AddInfo("val-2", (object)ints);
      int[] resultInts = info.GetArray<int>("val");
      Assert.IsTrue(ArrayEquals(ints, resultInts));
      resultInts = info.GetArray<int>("val-2");
      Assert.IsTrue(ArrayEquals(ints, resultInts));

      // long[]
      info.ClearInfo();
      long[] longs = new long[] { 10000, 200000, -300000 };
      info.AddInfo("val", longs);
      info.AddInfo("val-2", (object)longs);
      long[] resultLongs = info.GetArray<long>("val");
      Assert.IsTrue(ArrayEquals(longs, resultLongs));
      resultLongs = info.GetArray<long>("val-2");
      Assert.IsTrue(ArrayEquals(longs, resultLongs));

      // string[]
      info.ClearInfo();
      string[] strings = new string[] { "hi", "hello", "bye" };
      info.AddInfo("val", strings);
      info.AddInfo("val-2", (object)strings);
      string[] resultStrings = info.GetArray<string>("val");
      Assert.IsTrue(ArrayEquals(strings, resultStrings));
      resultStrings = info.GetArray<string>("val-2");
      Assert.IsTrue(ArrayEquals(strings, resultStrings));

      // float[]
      info.ClearInfo();
      float[] floats = new float[] { 100.123f, 200.456f, -58.67f };
      info.AddInfo("val", floats);
      info.AddInfo("val-2", (object)floats);
      float[] resultFloats = info.GetArray<float>("val");
      Assert.IsTrue(ArrayEquals(floats, resultFloats));
      resultFloats = info.GetArray<float>("val-2");
      Assert.IsTrue(ArrayEquals(floats, resultFloats));

      // double[]
      info.ClearInfo();
      double[] doubles = new double[] { 10000.98, 2000.5, -1000000.1 };
      info.AddInfo("val", doubles);
      info.AddInfo("val-2", (object)doubles);
      double[] resultDoubles = info.GetArray<double>("val");
      Assert.IsTrue(ArrayEquals(doubles, resultDoubles));
      resultDoubles = info.GetArray<double>("val-2");
      Assert.IsTrue(ArrayEquals(doubles, resultDoubles));
    }

    /// <summary>
    /// Determines whether two arrays contain equal elements
    /// </summary>
    /// <param name="one">The first array to compare</param>
    /// <param name="two">The second array to compare</param>
    /// <returns>True if the arrays are the same length and contain equal elements, otherwise false.</returns>
    private bool ArrayEquals(Array one, Array two)
    {
      if (one.Length == two.Length)
      {
        for (int i = 0; i < one.Length; ++i)
        {
          if (!one.GetValue(i).Equals(two.GetValue(i)))
          {
            return false;
          }
        }

        return true;
      }

      return false;
    }

    /// <summary>
    /// Tests adding nested <see cref="HydralizeInfo"/> objects.
    /// </summary>
    [Test]
    public void AddNestedInfo()
    {
      HydralizeInfo mini = new HydralizeInfo();
      mini.AddInfo("id", 1);
      HydralizeInfo sub = new HydralizeInfo();
      sub.AddInfo("name", "bob");
      // Add this way to test the code path
      sub.AddInfo("child", mini, typeof(HydralizeInfo));
      HydralizeInfo main = new HydralizeInfo();
      main.AddInfo("count", 32);
      // Add the direct way to test the other code path
      main.AddInfo("child", sub);

      // Check that we have nested objects now
      Assert.AreEqual(32, (int)main.GetRaw("count"));
      HydralizeInfo resultSub = main.GetRaw("child") as HydralizeInfo;
      Assert.IsNotNull(resultSub);
      Assert.AreEqual("bob", resultSub.GetRaw("name") as string);
      HydralizeInfo resultMini = resultSub.GetRaw("child") as HydralizeInfo;
      Assert.IsNotNull(resultMini);
      Assert.AreEqual(1, resultMini.Get<int>("id"));

      // add HydralizeInfo[] nested array
      HydralizeInfo[] infos = new HydralizeInfo[] { mini, sub };
      main.ClearInfo();
      main.AddInfo("infos", infos);

      // Check that we can get the data as an array
      HydralizeInfo[] resultInfos = main.GetArray<HydralizeInfo>("infos");
      Assert.AreEqual(2, resultInfos.Length);
      Assert.AreEqual(1, resultInfos[0].Get<int>("id"));
      Assert.AreEqual("bob", resultInfos[1].Get<string>("name"));
    }

    [Test]
    public void AddList()
    {
      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(1, typeof(List<>).Assembly));

      HydralizeInfo info = new HydralizeInfo();
      List<int> ints = new List<int>();
      ints.AddRange(new int[] { 1, 2, 3, 4, 5 });
      info.AddInfo("ints", ints);
      List<int> results = new List<int>();
      object obj = results;
      Hydralizer.LoadObjectInfo(ref obj, (HydralizeInfo)info.GetRaw("ints"));
      Assert.AreEqual(ints.Count, results.Count);
      for (int i = 0; i < ints.Count; ++i)
      {
        Assert.AreEqual(ints[i], results[i]);
      }

      info = Hydralizer.CreateIndexedInfo(ints, true, false);
      IndexedGenericType[] args = info.GenericArgs;
      Assert.AreEqual(1, args.Length);
      Assert.AreEqual(typeof(int), args[0].GetGenericType(null));
    }
  }
}
