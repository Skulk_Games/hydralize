#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Skulk.Hydralize;

namespace Skulk.Test
{
  [AttributeUsage(AttributeTargets.Field | AttributeTargets.Class)]
  class HydraCategoryAttribute : Attribute
  {
    public string Category { get; set; }
  }

  class CustomGeneric<T, U>
  {
    [HydraCategory(Category = "Identifier")]
    private T m_id;

    [HydraCategory(Category = "Telemetry")]
    private U m_value;

    public T Id { get => m_id; set => m_id = value; }
    public U Value { get => m_value; set => m_value = value; }
  }

  public class TestHydraTypeInfo
  {
    [Test]
    public void Primitives()
    {
      HydraTypeInfo typeInfo = HydraTypeInfo.Build(typeof(PrimitivesTestClass));
      HydraFieldInfo[] fields = typeInfo.GetFields();
      Assert.IsNotNull(fields);
      FieldInfo[] fieldInfos = typeof(PrimitivesTestClass).GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
      Assert.AreEqual(fieldInfos.Length, fields.Length);
      for (int i = 0; i < fieldInfos.Length; ++i)
      {
        Assert.AreEqual(fieldInfos[i].Name, fields[i].Name);
        Assert.AreEqual(fieldInfos[i].FieldType.Name, fields[i].FieldType.Name);
        Assert.AreEqual(HydraType.DetermineType(fieldInfos[i].FieldType), fields[i].FieldType.TypeCode);

        // Check that primitive types return null on N/A values
        Assert.IsNull(fields[i].FieldType.GetGenericArguments());
        Assert.IsNull(fields[i].FieldType.GetFields());
        Assert.IsNull(fields[i].FieldType.GetField("path"));
        Assert.IsNull(fields[i].FieldType.GetEnumNames());
        Assert.IsNull(fields[i].FieldType.GetEnumValues());
        Assert.IsNull(fields[i].FieldType.GetElementType());
        Assert.IsNull(fields[i].FieldType.GetPaths());
      }


      // Serialize/Deserialize
      byte[] raw = Hydralizer.Hydralize(typeInfo, true, true);
      int offset = 0;
      HydraTypeInfo result = Hydralizer.Dehydralize(raw, ref offset) as HydraTypeInfo;
      Assert.AreEqual(typeInfo, result);
    }

    [Test]
    public void Arrays()
    {
      HydraTypeInfo typeInfo = HydraTypeInfo.Build(typeof(ArraysTestClass));
      HydraFieldInfo[] fields = typeInfo.GetFields();
      Assert.IsNotNull(fields);
      FieldInfo[] fieldInfos = typeof(ArraysTestClass).GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
      Assert.AreEqual(fieldInfos.Length, fields.Length);
      for (int i = 0; i < fieldInfos.Length; ++i)
      {
        Assert.AreEqual(fieldInfos[i].Name, fields[i].Name);
        Assert.AreEqual(fieldInfos[i].FieldType.Name, fields[i].FieldType.Name);
        Assert.AreEqual(HydraType.DetermineType(fieldInfos[i].FieldType), fields[i].FieldType.TypeCode);
        Assert.IsTrue(fields[i].FieldType.IsArray);
        Assert.AreEqual(HydraType.DetermineType(fieldInfos[i].FieldType.GetElementType()), fields[i].FieldType.GetElementType().TypeCode);
      }

      // Serialize/Deserialize
      byte[] raw = Hydralizer.Hydralize(typeInfo, true, true);
      int offset = 0;
      HydraTypeInfo result = Hydralizer.Dehydralize(raw, ref offset) as HydraTypeInfo;
      Assert.AreEqual(typeInfo, result);
    }

    [Test]
    public void Generics()
    {
      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(1, typeof(List<>).Assembly));
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(2, typeof(HydraCategoryAttribute).Assembly));

      HydraTypeInfo typeInfo = HydraTypeInfo.Build(typeof(List<int>));
      Assert.IsNotNull(typeInfo.GetFields());
      Assert.AreEqual(1, typeInfo.GetFields().Length);
      Assert.IsTrue(typeInfo.IsGeneric);
      Assert.IsNotNull(typeInfo.GetGenericArguments());
      Assert.AreEqual(1, typeInfo.GetGenericArguments().Length);
      Assert.AreEqual(HydraTypeCodes.INT32, typeInfo.GetGenericArguments()[0].TypeCode);

      // Serialize/Deserialize
      byte[] raw = Hydralizer.Hydralize(typeInfo, true, true);
      int offset = 0;
      HydraTypeInfo result = Hydralizer.Dehydralize(raw, ref offset) as HydraTypeInfo;
      Assert.AreEqual(typeInfo, result);

      typeInfo = HydraTypeInfo.Build(typeof(Dictionary<string, string>));
      HydraTypeInfo[] genericArgs = typeInfo.GetGenericArguments();
      Assert.IsNotNull(typeInfo.GetFields());
      Assert.AreEqual(2, typeInfo.GetFields().Length);
      Assert.IsTrue(typeInfo.IsGeneric);
      Assert.IsNotNull(genericArgs);
      Assert.AreEqual(2, genericArgs.Length);
      Assert.AreEqual(HydraTypeCodes.STRING, genericArgs[0].TypeCode);
      Assert.AreEqual(HydraTypeCodes.STRING, genericArgs[1].TypeCode);

      // Serialize/Deserialize
      raw = Hydralizer.Hydralize(typeInfo, true, true);
      offset = 0;
      result = Hydralizer.Dehydralize(raw, ref offset) as HydraTypeInfo;
      Assert.AreEqual(typeInfo, result);

      typeInfo = HydraTypeInfo.Build(typeof(Dictionary<int, List<double>>));
      genericArgs = typeInfo.GetGenericArguments();
      Assert.IsNotNull(typeInfo.GetFields());
      Assert.AreEqual(2, typeInfo.GetFields().Length);
      Assert.IsTrue(typeInfo.GetFields()[1].FieldType.IsArray);
      Assert.AreEqual(1, typeInfo.GetFields()[1].FieldType.GetElementType().GetFields().Length);
      Assert.IsTrue(typeInfo.IsGeneric);
      Assert.IsNotNull(genericArgs);
      Assert.AreEqual(2, genericArgs.Length);
      Assert.AreEqual(HydraTypeCodes.INT32, genericArgs[0].TypeCode);
      Assert.AreEqual(HydraTypeCodes.OBJECT, genericArgs[1].TypeCode);
      Assert.AreEqual(HydraTypeCodes.DOUBLE, genericArgs[1].GetGenericArguments()[0].TypeCode);

      // Serialize/Deserialize
      raw = Hydralizer.Hydralize(typeInfo, true, true);
      offset = 0;
      result = Hydralizer.Dehydralize(raw, ref offset) as HydraTypeInfo;
      Assert.AreEqual(typeInfo, result);

      // Custom generic
      typeInfo = HydraTypeInfo.Build(typeof(CustomGeneric<int, string>));
      genericArgs = typeInfo.GetGenericArguments();
      HydraFieldInfo[] fields = typeInfo.GetFields();
      Assert.IsNotNull(fields);
      Assert.AreEqual(2, fields.Length);
      Assert.AreEqual(HydraTypeCodes.INT32, fields[0].FieldType.TypeCode);
      Assert.AreEqual(HydraTypeCodes.STRING, fields[1].FieldType.TypeCode);
      Assert.IsTrue(typeInfo.IsGeneric);
      Assert.IsNotNull(genericArgs);
      Assert.AreEqual(2, genericArgs.Length);
      Assert.AreEqual(HydraTypeCodes.INT32, genericArgs[0].TypeCode);
      Assert.AreEqual(HydraTypeCodes.STRING, genericArgs[1].TypeCode);

      // Serialize/Deserialize
      raw = Hydralizer.Hydralize(typeInfo, true, true);
      offset = 0;
      result = Hydralizer.Dehydralize(raw, ref offset) as HydraTypeInfo;
      Assert.AreEqual(typeInfo, result);
    }

    [Test]
    public void Enums()
    {
      HydraTypeInfo typeInfo = (HydraTypeInfo)typeof(EnumsTestClass);
      HydraFieldInfo[] fields = typeInfo.GetFields();
      Assert.AreEqual(2, fields.Length);

      // Enum type
      Assert.IsTrue(fields[0].FieldType.IsEnum);
      Assert.IsFalse(fields[0].FieldType.IsArray);
      Assert.AreEqual(HydraType.DetermineType(typeof(TestEnum).GetEnumUnderlyingType()), fields[0].FieldType.TypeCode);
      string[] names = Enum.GetNames(typeof(TestEnum));
      Array values = Enum.GetValues(typeof(TestEnum));
      Assert.AreEqual(names.Length, fields[0].FieldType.GetEnumNames().Length);
      Assert.AreEqual(values.Length, fields[0].FieldType.GetEnumValues().Length);
      for (int i = 0; i < names.Length; ++i)
      {
        Assert.AreEqual(names[i], fields[0].FieldType.GetEnumNames()[i]);
        Assert.AreEqual(values.GetValue(i), (TestEnum)fields[0].FieldType.GetEnumValues().GetValue(i));
      }

      // Enum array
      Assert.IsFalse(fields[1].FieldType.IsEnum);
      Assert.IsTrue(fields[1].FieldType.IsArray);
      Assert.IsTrue(fields[1].FieldType.GetElementType().IsEnum);
      HydraTypeCodes expectedElementTypeCode = HydraType.DetermineType(typeof(TestEnum).GetEnumUnderlyingType());
      HydraTypeCodes expectedArrayType = HydraType.ToArrayType(expectedElementTypeCode);
      Assert.AreEqual(expectedArrayType, fields[1].FieldType.TypeCode);
      Assert.AreEqual(expectedElementTypeCode, fields[1].FieldType.GetElementType().TypeCode);
      Assert.AreEqual(names.Length, fields[1].FieldType.GetElementType().GetEnumNames().Length);
      Assert.AreEqual(values.Length, fields[1].FieldType.GetElementType().GetEnumValues().Length);
      for (int i = 0; i < names.Length; ++i)
      {
        Assert.AreEqual(names[i], fields[1].FieldType.GetElementType().GetEnumNames()[i]);
        Assert.AreEqual(values.GetValue(i), (TestEnum)fields[1].FieldType.GetElementType().GetEnumValues().GetValue(i));
      }

      // Serialize/Deserialize the info
      byte[] raw = Hydralizer.Hydralize(typeInfo, true, true);
      int offset = 0;
      HydraTypeInfo result = Hydralizer.Dehydralize(raw, ref offset) as HydraTypeInfo;
      Assert.AreEqual(typeInfo.TypeCode, result.TypeCode);
      Assert.AreEqual(typeInfo.Name, result.Name);
      int expectedLength = typeInfo.GetFields().Length;
      Assert.AreEqual(expectedLength, result.GetFields().Length);
      for (int i = 0; i < expectedLength; ++i)
      {
        Assert.AreEqual(typeInfo.GetFields()[i], result.GetFields()[i]);
      }

      Assert.AreEqual(typeInfo, result);
    }

    /// <summary>
    /// Test the <see cref="HydraTypeInfo.Equals(object)"/> method
    /// </summary>
    [Test]
    public void Equality()
    {
      HydraTypeInfo prim1 = HydraTypeInfo.Build(typeof(PrimitivesTestClass));
      HydraTypeInfo prim2 = HydraTypeInfo.Build(typeof(PrimitivesTestClass));
      HydraTypeInfo enum1 = HydraTypeInfo.Build(typeof(EnumsTestClass));
      HydraTypeInfo enum2 = HydraTypeInfo.Build(typeof(EnumsTestClass));
      HydraTypeInfo arrays1 = HydraTypeInfo.Build(typeof(ArraysTestClass));
      HydraTypeInfo arrays2 = HydraTypeInfo.Build(typeof(ArraysTestClass));
      HydraTypeInfo lists1 = HydraTypeInfo.Build(typeof(ListTestClass));
      HydraTypeInfo lists2 = HydraTypeInfo.Build(typeof(ListTestClass));
      HydraTypeInfo dict1 = HydraTypeInfo.Build(typeof(DictionaryTestClass));
      HydraTypeInfo dict2 = HydraTypeInfo.Build(typeof(DictionaryTestClass));
      HydraTypeInfo dict3 = HydraTypeInfo.Build(typeof(Dictionary<string, string>));
      HydraTypeInfo dict4 = HydraTypeInfo.Build(typeof(Dictionary<int, string>));
      HydraTypeInfo tuple1 = HydraTypeInfo.Build(typeof(Tuple<int>));
      HydraTypeInfo tuple2 = HydraTypeInfo.Build(typeof(Tuple<int, int>));

      Assert.IsFalse(prim1.Equals(null));
      Assert.IsTrue(prim1.Equals(prim2));
      Assert.IsFalse(prim1.Equals(enum1));
      Assert.IsFalse(prim1.Equals(typeof(PrimitivesTestClass)));
      Assert.IsTrue(prim2.Equals(prim1));
      Assert.IsTrue(enum1.Equals(enum2));
      Assert.IsTrue(enum2.Equals(enum1));
      Assert.IsFalse(enum1.Equals(prim1));
      Assert.IsTrue(arrays1.Equals(arrays2));
      Assert.IsTrue(arrays2.Equals(arrays1));
      Assert.IsFalse(arrays1.Equals(lists1));
      Assert.IsFalse(arrays1.Equals(prim1));
      Assert.IsFalse(arrays1.Equals(enum1));
      Assert.IsTrue(lists1.Equals(lists2));
      Assert.IsTrue(lists2.Equals(lists1));
      Assert.IsFalse(lists1.Equals(prim1));
      Assert.IsFalse(lists2.Equals(enum1));
      Assert.IsFalse(lists2.Equals(arrays1));
      Assert.IsTrue(dict1.Equals(dict2));
      Assert.IsTrue(dict2.Equals(dict1));
      Assert.IsFalse(dict1.Equals(prim1));
      Assert.IsFalse(dict1.Equals(enum1));
      Assert.IsFalse(dict1.Equals(arrays1));
      Assert.IsFalse(dict1.Equals(lists1));
      Assert.IsFalse(dict3.Equals(dict4));
      Assert.IsFalse(tuple1.Equals(tuple2));
    }

    /// <summary>
    /// Tests the <see cref="HydraTypeInfo.GetHashCode"/> method
    /// </summary>
    [Test]
    public void HashCode()
    {
      HydraTypeInfo prim1 = HydraTypeInfo.Build(typeof(PrimitivesTestClass));
      HydraTypeInfo prim2 = HydraTypeInfo.Build(typeof(PrimitivesTestClass));
      HydraTypeInfo enum1 = HydraTypeInfo.Build(typeof(EnumsTestClass));
      HydraTypeInfo enum2 = HydraTypeInfo.Build(typeof(EnumsTestClass));
      HydraTypeInfo arrays1 = HydraTypeInfo.Build(typeof(ArraysTestClass));
      HydraTypeInfo arrays2 = HydraTypeInfo.Build(typeof(ArraysTestClass));
      HydraTypeInfo lists1 = HydraTypeInfo.Build(typeof(ListTestClass));
      HydraTypeInfo lists2 = HydraTypeInfo.Build(typeof(ListTestClass));

      Assert.AreEqual(prim1.GetHashCode(), prim2.GetHashCode());
      Assert.AreEqual(enum1.GetHashCode(), enum2.GetHashCode());
      Assert.AreEqual(arrays1.GetHashCode(), arrays2.GetHashCode());
      Assert.AreEqual(lists1.GetHashCode(), lists2.GetHashCode());
    }
  }
}