#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Skulk.Hydralize;

namespace Skulk.Test
{
  /// <summary>
  /// Class holding the tests that verify behavior of the Hydralizer with XML files
  /// </summary>
  public class TestXmlHydralizer
  {
    private class DictionaryObject
    {
      public Dictionary<string, string> vals = new Dictionary<string, string>();
    }

    private class ObjectDictionary
    {
      public Dictionary<string, object> vals = new Dictionary<string, object>();
    }

    [Test]
    public void TestXmlBool()
    {
      HydralizeInfo info = new HydralizeInfo();
      bool aBool = true;
      info.AddInfo("bool", aBool);
      string xml = info.ToXml();

      Console.WriteLine(xml);

      HydralizeInfo result = new HydralizeInfo();
      result.FromXml(xml);
      Assert.IsTrue(result.Get<bool>("bool"));
    }

    [Test]
    public void TestXmlBoolArray()
    {
      bool[] bools = new bool[] { true, true, false, true, false };
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("bool_array", bools);
      string xml = info.ToXml();
      Console.WriteLine(xml);
      HydralizeInfo result = new HydralizeInfo();
      result.FromXml(xml);
      bool[] resultBools = result.GetArray<bool>("bool_array");
      Assert.AreEqual(bools.Length, resultBools.Length);
      Assert.IsTrue(bools.SequenceEqual(resultBools));
    }

    [Test]
    public void TestXmlObject()
    {
      PrimitivesTestClass primitives = new PrimitivesTestClass()
      {
        BoolData = true,
        SData16 = -52,
        SData32 = 159,
        SData64 = 555000,
        SData8 = -120,
        UData16 = 62015,
        UData32 = 100000,
        UData64 = 1,
        UData8 = 0,
        CharData = 'c'
      };
      HydralizeInfo info = new HydralizeInfo();
      Hydralizer.SaveObjectInfo(primitives, info);
      string xml = info.ToXml();
      Console.WriteLine(xml);

      HydralizeInfo resultInfo = new HydralizeInfo();
      resultInfo.FromXml(xml);
      PrimitivesTestClass result = new PrimitivesTestClass();
      result.LoadObjectData(resultInfo);
      Assert.IsTrue(result.Equals(primitives));
    }

    [Test]
    public void TestXmlNestedObjects()
    {
      PrimitivesTestClass primitives = new PrimitivesTestClass()
      {
        BoolData = true,
        SData16 = -52,
        SData32 = 159,
        SData64 = 555000,
        SData8 = -120,
        UData16 = 62015,
        UData32 = 100000,
        UData64 = 1,
        UData8 = 0,
        CharData = 'a'
      };

      PrimitivesTestClass primitives2 = new PrimitivesTestClass()
      {
        BoolData = false,
        SData16 = -53,
        SData32 = 300,
        SData64 = 666000,
        SData8 = -7,
        UData16 = 55000,
        UData32 = 500000,
        UData64 = 1000000,
        UData8 = 1,
        CharData = 'b'
      };

      ArraysTestClass arrays = new ArraysTestClass()
      {
        BoolData = new bool[] { true, true, false },
        SData16 = new short[] { 1, 2, -3 },
        SData32 = new int[] { 100, 200, -300 },
        SData64 = new long[] { 1000, 2000, -3000 },
        SData8 = new sbyte[] { 1, 2, 3, -4 },
        UData16 = new ushort[] { 10, 20, 30 },
        UData32 = new uint[] { 100, 200, 300 },
        UData64 = new ulong[] { 1000, 2000, 3000 },
        UData8 = new byte[] { 1, 2, 255 }
      };

      ObjectsTestClass objects = new ObjectsTestClass()
      {
        Primitive = primitives,
        Array = primitives2
      };

      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("objects", objects);
      string xml = info.ToXml();
      Console.WriteLine(xml);

      HydralizeInfo resultInfo = new HydralizeInfo();
      resultInfo.FromXml(xml);
      ObjectsTestClass result = resultInfo.GetObj<ObjectsTestClass>("objects");

      Assert.IsTrue(objects.Primitive.Equals(result.Primitive));
      Assert.IsTrue(objects.Array.Equals(result.Array));

      // Test lite Hydralizer
      xml = Hydralizer.ToXml(objects, true, false);
      Console.WriteLine(xml);
      result = new ObjectsTestClass();
      Hydralizer.FromXml(result, xml);

      Assert.IsTrue(objects.Primitive.Equals(result.Primitive));
      Assert.IsTrue(objects.Array.Equals(result.Array));
    }

    [Test]
    public void TestHydralizeIComparable()
    {
      IComparable raw1 = "Hello";
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("raw", raw1);
      string xml = info.ToXml();
      Console.WriteLine(xml);
      HydralizeInfo resultInfo = new HydralizeInfo();
      resultInfo.FromXml(xml);
      IComparable result1 = resultInfo.Get<IComparable>("raw");
      Assert.AreEqual(raw1, result1);

      ComparableTestClass comparable = new ComparableTestClass()
      {
        Value1 = "Hello",
        Value2 = 52
      };

      xml = Hydralizer.ToXml(comparable, true);
      Console.WriteLine(xml);
      ComparableTestClass comparableResult = new ComparableTestClass();
      Hydralizer.FromXml(comparableResult, xml);

      Assert.AreEqual(comparable.Value1, comparableResult.Value1);
      Assert.AreEqual(comparable.Value2, comparableResult.Value2);
    }

    [Test]
    public void TestLiteMode()
    {
      PrimitivesTestClass primitives = new PrimitivesTestClass()
      {
        BoolData = true,
        SData16 = -52,
        SData32 = 159,
        SData64 = 555000,
        SData8 = -120,
        UData16 = 62015,
        UData32 = 100000,
        UData64 = 1,
        UData8 = 0,
        CharData = 'a'
      };

      PrimitivesTestClass primitives2 = new PrimitivesTestClass()
      {
        BoolData = false,
        SData16 = -53,
        SData32 = 300,
        SData64 = 666000,
        SData8 = -7,
        UData16 = 55000,
        UData32 = 500000,
        UData64 = 1000000,
        UData8 = 1,
        CharData = 'b'
      };

      ObjectsTestClass objects = new ObjectsTestClass()
      {
        Primitive = primitives,
        Array = primitives2
      };

      HydralizeInfo info = new HydralizeInfo(true);
      info.AddInfo("objects", objects, false);
      string xml = info.ToXml();
      Console.WriteLine(xml);

      HydralizeInfo resultInfo = new HydralizeInfo();
      resultInfo.FromXml(xml);
      ObjectsTestClass result = resultInfo.GetObj<ObjectsTestClass>("objects", 0);

      Assert.IsTrue(objects.Primitive.Equals(result.Primitive));
      Assert.IsTrue(objects.Array.Equals(result.Array));
    }

    [Test]
    public void TestXmlDictionary()
    {
      DictionaryObject vals = new DictionaryObject();
      vals.vals.Add("Name1", "Bob");
      vals.vals.Add("Name2", "Fred");
      vals.vals.Add("Name3", "Joe");
      string xml = Hydralizer.ToXml(vals);
      DictionaryObject result = new DictionaryObject();
      Hydralizer.FromXml(result, xml);
      Assert.IsTrue(result.vals.ContainsKey("Name1"));
      Assert.IsTrue(result.vals.ContainsKey("Name2"));
      Assert.IsTrue(result.vals.ContainsKey("Name3"));
      Assert.AreEqual("Bob", result.vals["Name1"]);
      Assert.AreEqual("Fred", result.vals["Name2"]);
      Assert.AreEqual("Joe", result.vals["Name3"]);

      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(1, typeof(ObjectDictionary).Assembly));
      Assert.IsTrue(Hydralizer.DefaultContext.AddGroup(2, Hydralizer.CoreTypes));

      ObjectDictionary objVals = new ObjectDictionary();
      objVals.vals.Add("Name", "Bob");
      objVals.vals.Add("Count", 12);
      objVals.vals.Add("Hungry", true);
      objVals.vals.Add("Obj", new IdTestClass() { Id = 1 });
      xml = Hydralizer.ToXml(objVals, true, false);
      System.IO.File.WriteAllText("objectDictionary.xml", xml);
      ObjectDictionary objResult = new ObjectDictionary();
      Hydralizer.FromXml(objResult, xml);
      Assert.IsTrue(objResult.vals.ContainsKey("Name"));
      Assert.IsTrue(objResult.vals.ContainsKey("Count"));
      Assert.IsTrue(objResult.vals.ContainsKey("Hungry"));
      Assert.IsTrue(objResult.vals.ContainsKey("Obj"));
      Assert.AreEqual("Bob", objResult.vals["Name"]);
      Assert.AreEqual(12, objResult.vals["Count"]);
      Assert.AreEqual(true, objResult.vals["Hungry"]);
      var resultIdClass = objResult.vals["Obj"] as IdTestClass;
      Assert.IsNull(resultIdClass);

      // Now check with indexing
      xml = Hydralizer.ToXml(objVals, true, true);
      System.IO.File.WriteAllText("objectDictionary.xml", xml);
      objResult = new ObjectDictionary();
      Hydralizer.FromXml(objResult, xml);
      Assert.IsTrue(objResult.vals.ContainsKey("Name"));
      Assert.IsTrue(objResult.vals.ContainsKey("Count"));
      Assert.IsTrue(objResult.vals.ContainsKey("Hungry"));
      Assert.IsTrue(objResult.vals.ContainsKey("Obj"));
      Assert.AreEqual("Bob", objResult.vals["Name"]);
      Assert.AreEqual(12, objResult.vals["Count"]);
      Assert.AreEqual(true, objResult.vals["Hungry"]);
      resultIdClass = objResult.vals["Obj"] as IdTestClass;
      Assert.IsNotNull(resultIdClass);
      Assert.AreEqual(1, resultIdClass.Id);
    }
  }
}
