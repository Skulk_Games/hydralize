#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using Skulk.Hydralize;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using NUnit.Framework;
using System.Linq;
using Skulk.Hydralize.Encoding;
using System.IO;

namespace Skulk.Test
{
  public class TestLoadableSettings
  {
    private class SaveData : LoadableSettings
    {
      private int m_count;
      private bool m_useNames;
      private string m_name;
      private byte[] m_key;

      public SaveData(int version, ICryptoTransform encryptor = null, ICryptoTransform decryptor = null) : base(version, encryptor, decryptor)
      {
        // Empty
      }

      public SaveData(ICryptoTransform encryptor = null, ICryptoTransform decryptor = null) : base(1, encryptor, decryptor)
      {
        // Empty
      }

      public int Count { get => m_count; set => m_count = value; }
      public bool UseNames { get => m_useNames; set => m_useNames = value; }
      public string Name { get => m_name; set => m_name = value; }
      public byte[] Key { get => m_key; set => m_key = value; }

      public override bool Equals(object obj)
      {
        SaveData otherSave = obj as SaveData;
        if (otherSave != null)
        {
          return m_count == otherSave.m_count &&
            m_useNames == otherSave.m_useNames &&
            m_name == null ? otherSave.m_name == null : m_name.Equals(otherSave.m_name) &&
            m_key == null ? otherSave.m_key == null : Enumerable.SequenceEqual(m_key, otherSave.m_key);
        }

        return false;
      }

      public override int GetHashCode()
      {
        return m_count ^ m_useNames.GetHashCode() ^ (m_name?.GetHashCode() ?? 0) ^ (m_key?.GetHashCode() ?? 0);
      }
    }

    private class SaveDataVersioned : SaveData
    {
      public SaveDataVersioned(int version) : base(version)
      {
        // Empty
      }

      public override bool Upgrade(HydralizeInfo info)
      {
        // Change the value from 0 base to 1 base (add 1 to count)
        int version = (int)info.GetRaw("m_fileVersion", 0);

        switch (version)
        {
          case 1:
          {
            var pairs = info.GetFields();
            bool success = false;
            foreach (var pair in pairs)
            {
              // Just increment the count
              if (pair.Key == "1" || pair.Key == "m_count")
              {
                pair.Value.Data = (int)pair.Value.Data + 1;
                success = true;
              }
            }

            return success;
          }
        }

        return true;
      }

      public override bool Downgrade(HydralizeInfo info)
      {
        // Change the value from 0 base to 1 base (add 1 to count)
        int version = (int)info.GetRaw("m_fileVersion", 0);

        switch (version)
        {
          case 2:
          {
            var pairs = info.GetFields();
            bool success = false;
            foreach (var pair in pairs)
            {
              // Just increment the count
              if (pair.Key == "1" || pair.Key == "m_count")
              {
                pair.Value.Data = (int)pair.Value.Data - 1;
                success = true;
              }
            }

            return success;
          }
        }

        return true;
      }
    }

    [Test]
    public void TestSameVersion()
    {
      SaveData saveData = new SaveData()
      {
        Count = 3,
        Name = "Save",
        UseNames = true,
        Key = new byte[] { 1, 2, 3 }
      };

      string file = "settings.bin";
      Assert.IsTrue(saveData.Save(file, SettingsFormats.Binary, false));
      SaveData result = new SaveData();
      Assert.IsTrue(result.Load(file, SettingsFormats.Binary));
      Assert.IsTrue(saveData.Equals(result));
      File.Delete(file);

      // Save lite
      Assert.IsTrue(saveData.Save(file, SettingsFormats.Binary, true));
      result = new SaveData();
      Assert.IsTrue(result.Load(file, SettingsFormats.Binary));
      Assert.IsTrue(saveData.Equals(result));
      File.Delete(file);

      // XML
      Assert.IsTrue(saveData.Save(file, SettingsFormats.Xml, false));
      result = new SaveData();
      Assert.IsTrue(result.Load(file, SettingsFormats.Xml));
      Assert.IsTrue(saveData.Equals(result));
      File.Delete(file);

      // Save lite
      Assert.IsTrue(saveData.Save(file, SettingsFormats.Xml, true));
      result = new SaveData();
      Assert.IsTrue(result.Load(file, SettingsFormats.Xml));
      Assert.IsTrue(saveData.Equals(result));
      File.Delete(file);

      // Save encrypted
      TinyEncryption tea = new TinyEncryption(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
      saveData = new SaveData(tea.GetEncryptor(), tea.GetDecryptor())
      {
        Count = 3,
        Name = "Save",
        UseNames = true,
        Key = new byte[] { 1, 2, 3 }
      };

      Assert.IsTrue(saveData.Save(file, SettingsFormats.Binary, false));
      result = new SaveData(tea.GetEncryptor(), tea.GetDecryptor());
      Assert.IsTrue(result.Load(file, SettingsFormats.Binary));
      Assert.IsTrue(saveData.Equals(result));
      File.Delete(file);

      Assert.IsTrue(saveData.Save(file, SettingsFormats.Binary, true));
      result = new SaveData(tea.GetEncryptor(), tea.GetDecryptor());
      Assert.IsTrue(result.Load(file, SettingsFormats.Binary));
      Assert.IsTrue(saveData.Equals(result));
      File.Delete(file);

      Assert.IsTrue(saveData.Save(file, SettingsFormats.Xml, false));
      result = new SaveData(tea.GetEncryptor(), tea.GetDecryptor());
      Assert.IsTrue(result.Load(file, SettingsFormats.Xml));
      Assert.IsTrue(saveData.Equals(result));
      File.Delete(file);

      Assert.IsTrue(saveData.Save(file, SettingsFormats.Xml, true));
      result = new SaveData(tea.GetEncryptor(), tea.GetDecryptor());
      Assert.IsTrue(result.Load(file, SettingsFormats.Xml));
      Assert.IsTrue(saveData.Equals(result));
      File.Delete(file);
    }

    /// <summary>
    /// Test the upgrade functionality of <see cref="LoadableSettings"/>
    /// </summary>
    [Test]
    public void Upgrade()
    {
      string saveFile = "settings.bin";
      SaveData save = new SaveData()
      {
        Count = 10,
        Name = "Fred",
        UseNames = true,
        Key = new byte[] { 1, 2, 3 }
      };

      Assert.IsTrue(save.Save(saveFile));
      SaveDataVersioned load = new SaveDataVersioned(2);
      Assert.IsTrue(load.Load(saveFile));
      Assert.AreEqual(save.Count + 1, load.Count);
      Assert.AreEqual(save.Name, load.Name);
      Assert.AreEqual(save.UseNames, load.UseNames);
      Assert.AreEqual(save.Key.Length, load.Key.Length);
      Assert.IsTrue(save.Key.SequenceEqual(load.Key));
    }

    /// <summary>
    /// Test the downgrade functionality of <see cref="LoadableSettings"/>
    /// </summary>
    [Test]
    public void Downgrade()
    {
      string saveFile = "settings.bin";
      SaveDataVersioned save = new SaveDataVersioned(2)
      {
        Count = 10,
        Name = "Fred",
        UseNames = true,
        Key = new byte[] { 1, 2, 3 }
      };

      Assert.IsTrue(save.Save(saveFile));
      SaveDataVersioned load = new SaveDataVersioned(1);
      Assert.IsTrue(load.Load(saveFile));
      Assert.AreEqual(save.Count - 1, load.Count);
      Assert.AreEqual(save.Name, load.Name);
      Assert.AreEqual(save.UseNames, load.UseNames);
      Assert.AreEqual(save.Key.Length, load.Key.Length);
      Assert.IsTrue(save.Key.SequenceEqual(load.Key));
    }
  }
}
