#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydralize;
using System;
using System.Collections.Generic;
using System.Text;

namespace Skulk.Test
{
  /// <summary>
  /// Test implementations of the <see cref="IHydralizeConverter"/> interface
  /// </summary>
  public class TestHydralizeConverters
  {
    /// <summary>
    /// Test clearing and adding converters
    /// </summary>
    [Test]
    public void ClearAndAdd()
    {
      Hydralizer.ClearConverters();

      // Serializing a list will now pull in 4 fields
      List<int> ids = new List<int>();
      ids.Add(1);
      ids.Add(2);
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("ids", ids);
      HydralizeInfo listInfo = info.GetRaw("ids") as HydralizeInfo;
      Assert.AreEqual(4, listInfo.GetFields().Length);

      // Add the list converter, now only a single array is used
      Hydralizer.AddConverter(new ListConverter());
      info = new HydralizeInfo();
      info.AddInfo("ids", ids);
      listInfo = info.GetRaw("ids") as HydralizeInfo;
      Assert.AreEqual(1, listInfo.GetFields().Length);

      // Without the converter dictionaries throw an exception
      info = new HydralizeInfo();
      Dictionary<int, string> names = new Dictionary<int, string>();
      names.Add(0, "bob");
      names.Add(1, "fred");
      Assert.Throws(typeof(InvalidCastException), () => info.AddInfo("names", names));

      // Load all converters in the Hydralizer assembly, (the dictionary converter) and now it should work
      Hydralizer.LoadConverters(typeof(Hydralizer).Assembly);
      info = new HydralizeInfo();
      info.AddInfo("names", names);
      HydralizeInfo dictInfo = info.GetRaw("names") as HydralizeInfo;
      Assert.AreEqual(2, dictInfo.GetFields().Length);
    }

    /// <summary>
    /// Test the generic dictionary converter
    /// </summary>
    [Test]
    public void TestDictionaryConverter()
    {
      Hydralizer.DefaultContext.Clear();
      Hydralizer.AddSupportedAssembly(1, typeof(Dictionary<,>).Assembly);
      DictionaryConverter converter = new DictionaryConverter();
      Dictionary<int, string> nameLookup = new Dictionary<int, string>();
      nameLookup.Add(0, "bob");
      nameLookup.Add(1, "fred");

      HydralizeInfo info = new HydralizeInfo();
      converter.Hydralize((object)nameLookup, ref info);
      Dictionary<int, string> resultNames = new Dictionary<int, string>();
      converter.Dehydralize((object)resultNames, ref info);
      Assert.AreEqual(nameLookup.Count, resultNames.Count);
      for (int i = 0; i < nameLookup.Count; ++i)
      {
        Assert.AreEqual(nameLookup[i], resultNames[i]);
      }

      // Test with nested
      Dictionary<int, Dictionary<int, string>> nameLib = new Dictionary<int, Dictionary<int, string>>();
      nameLib.Add(0, new Dictionary<int, string>());
      nameLib[0].Add(0, "bob");
      nameLib[0].Add(1, "fred");
      nameLib.Add(1, new Dictionary<int, string>());
      nameLib[1].Add(0, "joe");

      info = new HydralizeInfo();
      converter.Hydralize((object)nameLib, ref info);
      Dictionary<int, Dictionary<int, string>> resultLib = new Dictionary<int, Dictionary<int, string>>();
      converter.Dehydralize((object)resultLib, ref info);
      Assert.AreEqual(nameLib.Count, resultLib.Count);
      for (int i = 0; i < nameLib.Count; ++i)
      {
        Assert.AreEqual(nameLib[i].Count, resultLib[i].Count);
        for (int j = 0; j < nameLib[i].Count; ++j)
        {
          Assert.AreEqual(nameLib[i][j], resultLib[i][j]);
        }
      }

      // Nested and indexed
      info = Hydralizer.CreateIndexedInfo(nameLib, true, false);
      byte[] raw = Hydralizer.Hydralize(nameLib, true, true);
      int offset = 0;
      resultLib = Hydralizer.Dehydralize(raw, ref offset) as Dictionary<int, Dictionary<int, string>>;
      Assert.IsNotNull(resultLib);
      Assert.AreEqual(nameLib.Count, resultLib.Count);
      for (int i = 0; i < nameLib.Count; ++i)
      {
        Assert.AreEqual(nameLib[i].Count, resultLib[i].Count);
        for (int j = 0; j < nameLib[i].Count; ++j)
        {
          Assert.AreEqual(nameLib[i][j], resultLib[i][j]);
        }
      }
    }
  }
}
