#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Skulk.Hydralize.Encoding;
using System.Security.Cryptography;
using System.IO;

namespace Skulk.Test
{
  public class TestEncoding
  {
    private byte[] m_key1 = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
    private byte[] m_key2 = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 };

    private byte[] m_iv1 = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

    private byte[] data10 = new byte[] { 1, 3, 2, 1, 2, 3, 4, 5, 6, 10 };
    private byte[] data16 = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

    [Test]
    public void TestTinyEncoder()
    {
      uint[] key = new uint[] { 0x01020304, 0x05060708, 0x090a0b0c, 0x0d0e0f10 };
      TinyEncryption tea = new TinyEncryption(key);

      ICryptoTransform encryptor = tea.GetEncryptor();
      ICryptoTransform decryptor = tea.GetDecryptor();
      MemoryStream encodedStream = new MemoryStream();
      byte[] encoded = null;

      using (CryptoStream cryptoStream = new CryptoStream(encodedStream, encryptor, CryptoStreamMode.Write, true))
      {
        cryptoStream.Write(data10);
      }
      encoded = new byte[encodedStream.Length];
      Array.Copy(encodedStream.GetBuffer(), encoded, encoded.Length);

      // CryptoStream seems a little backwards when it comes to padding. But basically does not work well unless you're reading the
      // whole written chunk at once in order for padding to work correctly. Which seems to agree with common practice of length in plaintext
      // and data in crypto data.
      using (MemoryStream memoryStream = new MemoryStream(encoded))
      {
        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
        {
          byte[] result2 = new byte[encoded.Length];
          Assert.AreEqual(data10.Length, cryptoStream.Read(result2, 0, result2.Length));
          byte[] remainder = new byte[8];
          Assert.AreEqual(0, cryptoStream.Read(remainder));
        }
      }
    }

    /// <summary>
    /// Tests some manual encryption and decryption calls directly to the <see cref="TinyEncoder"/> and <see cref="TinyDecoder"/> classes.
    /// </summary>
    /// <remarks>
    /// This test is primarily to exercise lines that aren't covered by the <see cref="TinyEncryption"/> class
    /// and <see cref="CryptoStream"/>.
    /// </remarks>
    [Test]
    public void TestTinyEncoderDecoder()
    {
      TinyEncoder encoder = new TinyEncoder(data16);
      TinyDecoder decoder = new TinyDecoder(data16);
      
      // Test simple encode/decode process to prove the key worked
      if (encoder.CanTransformMultipleBlocks && decoder.CanTransformMultipleBlocks)
      {
        byte[] crypto = new byte[data16.Length];
        int count = encoder.TransformBlock(data16, 0, data16.Length, crypto, 0);
        Assert.AreEqual(data16.Length, count);

        byte[] result = new byte[crypto.Length];
        count = decoder.TransformBlock(crypto, 0, crypto.Length, result, 0);
        Assert.AreEqual(data16.Length, count);

        for (int i = 0; i < data16.Length; ++i)
        {
          Assert.AreEqual(data16[i], result[i]);
        }
      }

      // Test partial block encoding/decoding with manual calls
      {
        byte[] crypto = new byte[data16.Length];
        int count = encoder.TransformBlock(data10, 0, encoder.InputBlockSize, crypto, 0);
        Assert.AreEqual(count, encoder.OutputBlockSize);
        byte[] remainder = encoder.TransformFinalBlock(data10, count, data10.Length - count);
        Assert.AreEqual(encoder.OutputBlockSize, remainder.Length);
        Array.Copy(remainder, 0, crypto, count, remainder.Length);

        byte[] result = new byte[data10.Length];
        count = decoder.TransformBlock(crypto, 0, decoder.InputBlockSize, result, 0);
        Assert.AreEqual(decoder.OutputBlockSize, count);
        remainder = decoder.TransformFinalBlock(crypto, count, crypto.Length - count);
        Assert.AreEqual(data10.Length - count, remainder.Length);
        Array.Copy(remainder, 0, result, count, remainder.Length);

        for (int i = 0; i < data10.Length; ++i)
        {
          Assert.AreEqual(data10[i], result[i]);
        }
      }
    }

    /// <summary>
    /// Tests that the <see cref="TinyEncoder"/> and <see cref="TinyDecoder"/> classes throw exceptions for invalid keys.
    /// </summary>
    [Test]
    public void InvalidKeyExceptions()
    {
      for (int i = 0; i < 32; ++i)
      {
        if (i == 16)
        {
          continue;
        }

        Assert.Throws<Exception>(() => { new TinyEncoder(new byte[i]); });
      }

      for (int i = 0; i < 8; ++i)
      {
        if (i == 4)
        {
          continue;
        }

        Assert.Throws<Exception>(() => { new TinyEncoder(new uint[i]); });
      }

      for (int i = 0; i < 32; ++i)
      {
        if (i == 16)
        {
          continue;
        }

        Assert.Throws<Exception>(() => { new TinyDecoder(new byte[i]); });
      }

      for (int i = 0; i < 8; ++i)
      {
        if (i == 4)
        {
          continue;
        }

        Assert.Throws<Exception>(() => { new TinyDecoder(new uint[i]); });
      }
    }
  }
}
