#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using NUnit.Framework;
using Skulk.Hydralize.Encoding;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Skulk.Test
{
  public class TestTranscoder
  {
    /// <summary>
    /// Tests using a transcoder pair with the Hydralize implementation of the Tiny Encryption Algorithm
    /// </summary>
    [Test]
    public void TestTranscoderTea()
    {
      Skulk.Hydralize.Encoding.TinyEncryption tinyEncryption = new Skulk.Hydralize.Encoding.TinyEncryption(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
      TranscoderPair transcoder = new TranscoderPair(tinyEncryption.GetEncryptor(), tinyEncryption.GetDecryptor());

      byte[] sequence10 = new byte[] { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };
      byte[] junk5 = new byte[] { 1, 8, 3, 19, 45 };

      // Test and expect padding to be removed automatically
      byte[] encoded = transcoder.Encode(sequence10);
      byte[] decoded = transcoder.Decode(encoded, 0, encoded.Length);
      Assert.IsTrue(IsArrayEqual(decoded, sequence10));

      encoded = transcoder.Encode(junk5);
      decoded = transcoder.Decode(encoded, 0, encoded.Length);
      Assert.IsTrue(IsArrayEqual(decoded, junk5));

      byte[] junk50 = new byte[50];
      Random rand = new Random(DateTime.UtcNow.Millisecond);
      for (int i = 0; i < junk50.Length; ++i)
      {
        junk50[i] = (byte)rand.Next(256);
      }
      encoded = transcoder.Encode(junk50);
      decoded = transcoder.Decode(encoded, 0, encoded.Length);
      Assert.IsTrue(IsArrayEqual(decoded, junk50));

      // Encode only a segment
      encoded = transcoder.Encode(junk50, 10, 12);
      decoded = transcoder.Decode(encoded, 0, encoded.Length);
      byte[] segment = new byte[12];
      Array.Copy(junk50, 10, segment, 0, 12);
      Assert.IsTrue(IsArrayEqual(decoded, segment));

      // Decode only a segment (needs to align with blocks)
      encoded = transcoder.Encode(junk50);
      decoded = transcoder.Decode(encoded, transcoder.Decoder.InputBlockSize * 2, 5);
      segment = new byte[5];
      Array.Copy(junk50, transcoder.Decoder.InputBlockSize * 2, segment, 0, 5);
      Assert.IsTrue(IsArrayEqual(decoded, segment));

      // Test that padding is removed
      encoded = transcoder.Encode(junk50);
      decoded = transcoder.Decode(encoded);
      Assert.IsTrue(IsArrayEqual(decoded, junk50));
    }

    /// <summary>
    /// Tests using a transcoder pair with AesManaged with CBC and padding
    /// </summary>
    [Test]
    public void TestTranscoderAes()
    {
      Aes aes = Aes.Create();
      aes.Key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
      aes.IV = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
      aes.Padding = PaddingMode.PKCS7;
      aes.Mode = CipherMode.CBC;

      TranscoderPair transcoder = new TranscoderPair(aes.CreateEncryptor(), aes.CreateDecryptor());

      byte[] sequence10 = new byte[] { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };
      byte[] junk5 = new byte[] { 1, 8, 3, 19, 45 };

      byte[] encoded = transcoder.Encode(sequence10);
      byte[] decoded = transcoder.Decode(encoded, 0, encoded.Length);
      Assert.IsTrue(IsArrayEqual(decoded, sequence10));

      encoded = transcoder.Encode(junk5);
      decoded = transcoder.Decode(encoded, 0, junk5.Length);
      Assert.IsTrue(IsArrayEqual(decoded, junk5));

      byte[] junk50 = new byte[50];
      Random rand = new Random(DateTime.UtcNow.Millisecond);
      for (int i = 0; i < junk50.Length; ++i)
      {
        junk50[i] = (byte)rand.Next(256);
      }
      encoded = transcoder.Encode(junk50);
      decoded = transcoder.Decode(encoded, 0, encoded.Length);
      Assert.IsTrue(IsArrayEqual(decoded, junk50));

      // Encode only a segment
      encoded = transcoder.Encode(junk50, 10, 12);
      decoded = transcoder.Decode(encoded, 0, 12);
      byte[] segment = new byte[12];
      Array.Copy(junk50, 10, segment, 0, 12);
      Assert.IsTrue(IsArrayEqual(decoded, segment));

      // Decode only a segment (needs to align with blocks and has to still follow CBC)
      encoded = transcoder.Encode(junk50);
      List<byte> extra = new List<byte>(encoded);
      extra.InsertRange(0, new byte[] { 0, 0, 1, 1, 2, 2 });
      extra.AddRange(new byte[] { 0, 0, 1, 1, 2, 2 });
      decoded = transcoder.Decode(extra.ToArray(), 6, 5);
      segment = new byte[5];
      Array.Copy(junk50, 0, segment, 0, 5);
      Assert.IsTrue(IsArrayEqual(decoded, segment));

      // Re-initialize because only partially decoding messes up CBC
      transcoder = new TranscoderPair(aes.CreateEncryptor(), aes.CreateDecryptor());
      
      // Test that padding is removed
      encoded = transcoder.Encode(junk50);
      decoded = transcoder.Decode(encoded);
      Assert.IsTrue(IsArrayEqual(decoded, junk50));
    }

    private bool IsArrayEqual(byte[] array1, byte[] array2)
    {
      if (array1 == null || array2 == null)
      {
        return array1 == array2;
      }

      if (array1.Length == array2.Length)
      {
        for (int i = 0; i < array1.Length; ++i)
        {
          if (array1[i] != array2[i])
          {
            return false;
          }
        }

        return true;
      }

      return false;
    }
  }
}
