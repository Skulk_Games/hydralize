#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Skulk.Hydralize;

namespace Skulk.Test
{
  enum Digits
  {
    Zero,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine
  }

  class IdTestClass : HydralizeObject
  {
    private int m_id;

    public int Id { get => m_id; set => m_id = value; }
  }

  class PrimitivesTestClass : HydralizeObject
  {
    private byte m_uData8;
    private UInt16 m_uData16;
    private UInt32 m_uData32;
    private UInt64 m_uData64;
    private char m_charData;

    private sbyte m_sData8;
    private Int16 m_sData16;
    private Int32 m_sData32;
    private Int64 m_sData64;

    private bool m_boolData;

    public byte UData8 { get => m_uData8; set => m_uData8 = value; }
    public ushort UData16 { get => m_uData16; set => m_uData16 = value; }
    public uint UData32 { get => m_uData32; set => m_uData32 = value; }
    public ulong UData64 { get => m_uData64; set => m_uData64 = value; }
    public sbyte SData8 { get => m_sData8; set => m_sData8 = value; }
    public short SData16 { get => m_sData16; set => m_sData16 = value; }
    public int SData32 { get => m_sData32; set => m_sData32 = value; }
    public long SData64 { get => m_sData64; set => m_sData64 = value; }
    public bool BoolData { get => m_boolData; set => m_boolData = value; }
    public char CharData { get => m_charData; set => m_charData = value; }

    public override bool Equals(object obj)
    {
      PrimitivesTestClass prim = obj as PrimitivesTestClass;
      if (prim != null)
      {
        return m_uData8 == prim.m_uData8 &&
          m_uData16 == prim.m_uData16 &&
          m_uData32 == prim.m_uData32 &&
          m_uData64 == prim.m_uData64 &&
          m_sData8 == prim.m_sData8 &&
          m_sData16 == prim.m_sData16 &&
          m_sData32 == prim.m_sData32 &&
          m_sData64 == prim.m_sData64 &&
          m_boolData == prim.m_boolData &&
          m_charData == prim.m_charData;
      }
      return false;
    }

    public override int GetHashCode()
    {
      return (int)((int)(m_uData64 ^ m_uData16 ^ m_uData32 ^ m_charData) ^ m_sData8 ^ m_sData16 ^ m_sData32 ^ m_sData64) + (m_boolData ? 1 : 0);
    }
  }

  class EnumsTestClass : HydralizeObject
  {
    TestEnum m_enumVal;
    TestEnum[] m_enumVals;

    internal TestEnum EnumVal { get => m_enumVal; set => m_enumVal = value; }
    internal TestEnum[] EnumVals { get => m_enumVals; set => m_enumVals = value; }
  }

  class ArraysTestClass : HydralizeObject
  {
    private byte[] m_uData8;
    private UInt16[] m_uData16;
    private UInt32[] m_uData32;
    private UInt64[] m_uData64;

    private sbyte[] m_sData8;
    private Int16[] m_sData16;
    private Int32[] m_sData32;
    private Int64[] m_sData64;

    private bool[] m_boolData;

    public byte[] UData8 { get => m_uData8; set => m_uData8 = value; }
    public ushort[] UData16 { get => m_uData16; set => m_uData16 = value; }
    public uint[] UData32 { get => m_uData32; set => m_uData32 = value; }
    public ulong[] UData64 { get => m_uData64; set => m_uData64 = value; }
    public sbyte[] SData8 { get => m_sData8; set => m_sData8 = value; }
    public short[] SData16 { get => m_sData16; set => m_sData16 = value; }
    public int[] SData32 { get => m_sData32; set => m_sData32 = value; }
    public long[] SData64 { get => m_sData64; set => m_sData64 = value; }
    public bool[] BoolData { get => m_boolData; set => m_boolData = value; }
  }

  class ObjectsTestClass : HydralizeObject
  {
    private PrimitivesTestClass m_primitive;
    private PrimitivesTestClass m_array;

    internal PrimitivesTestClass Primitive { get => m_primitive; set => m_primitive = value; }
    internal PrimitivesTestClass Array { get => m_array; set => m_array = value; }
  }

  class ObjectsArrayTestClass : HydralizeObject
  {
    private PrimitivesTestClass[] m_primitives;

    internal PrimitivesTestClass[] Primitives { get => m_primitives; set => m_primitives = value; }
  }

  class ListTestClass : HydralizeObject
  {
    private List<Int32> m_intList;
    private List<bool> m_boolList;
    private List<PrimitivesTestClass> m_primitivesList;
    private List<ObjectsTestClass> m_objectsList;

    public List<int> IntList { get => m_intList; set => m_intList = value; }
    public List<bool> BoolList { get => m_boolList; set => m_boolList = value; }
    internal List<PrimitivesTestClass> PrimitivesList { get => m_primitivesList; set => m_primitivesList = value; }
    internal List<ObjectsTestClass> ObjectsList { get => m_objectsList; set => m_objectsList = value; }
  }

  class DictionaryTestClass
  {
    private Dictionary<int, string> m_names;
    private Dictionary<int, bool> m_actives;

    public Dictionary<int, string> Names { get => m_names; set => m_names = value; }
    public Dictionary<int, bool> Actives { get => m_actives; set => m_actives = value; }

    public override bool Equals(object obj)
    {
      DictionaryTestClass testClass = obj as DictionaryTestClass;
      if (testClass != null)
      {
        if (m_names == null)
        {
          if (testClass.m_names != null)
          {
            return false;
          }
        }
        else if (testClass.m_names == null)
        {
          return false;
        }
        else
        {
          if (m_names.Count == testClass.m_names.Count)
          {
            try
            {
              foreach (int key in m_names.Keys)
              {
                if (m_names[key] != testClass.m_names[key])
                {
                  return false;
                }
              }
            }
            catch
            {
              return false;
            }
          }
          else
          {
            return false;
          }
        }

        if (m_actives == null)
        {
          if (testClass.m_actives != null)
          {
            return false;
          }
        }
        else if (testClass.m_actives == null)
        {
          return false;
        }
        else
        {
          if (m_actives.Count == testClass.m_actives.Count)
          {
            try
            {
              foreach (int key in m_actives.Keys)
              {
                if (m_actives[key] != testClass.m_actives[key])
                {
                  return false;
                }
              }
            }
            catch
            {
              return false;
            }
          }
          else
          {
            return false;
          }
        }

        return true;
      }
      return false;
    }

    public override int GetHashCode()
    {
      return (m_names?.GetHashCode() ?? 0) + (m_actives?.GetHashCode() ?? 0);
    }
  }

  class ComparableTestClass
  {
    IComparable m_value1;
    IComparable m_value2;

    public IComparable Value1 { get => m_value1; set => m_value1 = value; }
    public IComparable Value2 { get => m_value2; set => m_value2 = value; }

    public override bool Equals(object obj)
    {
      var other = obj as ComparableTestClass;
      if (other != null)
      {
        // value 1
        if (m_value1 != null)
        {
          if (!m_value1.Equals(other.m_value1))
          {
            return false;
          }
        }
        else if (other.m_value1 != null)
        {
          return false;
        }

        // value 2
        if (m_value2 != null)
        {
          if (!m_value2.Equals(other.m_value2))
          {
            return false;
          }
        }
        else if (other.m_value2 != null)
        {
          return false;
        }

        return true;
      }

      return false;
    }

    public override int GetHashCode()
    {
      return (m_value1?.GetHashCode() ?? 0) ^ (m_value2?.GetHashCode() ?? 0);
    }
  }

  [ObjectEntry("Description", "A class that contains test description data")]
  class DescriptiveTestClass
  {
    [ObjectEntry("Description", "The ID of this object")]
    private byte m_id;

    [ObjectEntry("Description", "The name of this object")]
    private string m_name;

    [ObjectEntry("Max", 3)]
    [ObjectEntry("Description", "The client ID of this object")]
    private int m_client;

    public byte Id { get => m_id; set => m_id = value; }
    public string Name { get => m_name; set => m_name = value; }
    public int Client { get => m_client; set => m_client = value; }
  }

  class ParameterDictionaryTestClass
  {
    private Dictionary<string, object> m_parameters = new Dictionary<string, object>();

    public Dictionary<string, object> Parameters { get => m_parameters; set => m_parameters = value; }

    public override bool Equals(object obj)
    {
      var other = obj as ParameterDictionaryTestClass;
      if (other != null)
      {
        if (m_parameters == null && other.m_parameters == null)
        {
          return true;
        }
        else if (m_parameters != null && other.m_parameters != null)
        {
          if (m_parameters.Count == other.m_parameters.Count)
          {
            foreach (string key in m_parameters.Keys)
            {
              if (other.m_parameters.ContainsKey(key))
              {
                if (!m_parameters[key].Equals(other.m_parameters[key]))
                {
                  return false;
                }
              }
              else
              {
                return false;
              }
            }

            return true;
          }
        }
      }

      return false;
    }

    public override int GetHashCode()
    {
      int hash = 0;
      if (m_parameters != null)
      {
        hash ^= m_parameters.Count;
        foreach (KeyValuePair<string, object> pair in m_parameters)
        {
          hash ^= (pair.Key?.GetHashCode() ?? 0);
          hash ^= (pair.Value?.GetHashCode() ?? 0);
        }
      }

      return hash;
    }
  }

  class ParameterListTestClass
  {
    private List<object> m_parameters = new List<object>();

    public List<object> Parameters { get => m_parameters; set => m_parameters = value; }

    public override int GetHashCode()
    {
      return m_parameters == null ? 0 : m_parameters.Count;
    }

    public override bool Equals(object obj)
    {
      var other = obj as ParameterListTestClass;
      if (other != null)
      {
        if (m_parameters == null && other.m_parameters == null)
        {
          return true;
        }
        else if (m_parameters != null && other.m_parameters != null)
        {
          if (m_parameters.Count == other.m_parameters.Count)
          {
            for (int i = 0; i < m_parameters.Count; ++i)
            {
              if (!m_parameters[i].Equals(other.m_parameters[i]))
              {
                return false;
              }
            }

            return true;
          }
        }
      }

      return false;
    }
  }

  class DataExample
  {
    private int m_id;
    private string m_name;
    private bool m_valid;

    public int Id { get => m_id; set => m_id = value; }
    public string Name { get => m_name; set => m_name = value; }
    public bool Valid { get => m_valid; set => m_valid = value; }

    public override bool Equals(object obj)
    {
      DataExample other = obj as DataExample;
      if (other != null)
      {
        if (m_id == other.m_id && m_valid == other.m_valid)
        {
          if (m_name == null)
          {
            return other.m_name == null;
          }
          else
          {
            return m_name.Equals(other.m_name);
          }
        }
      }
      return false;
    }

    public override int GetHashCode()
    {
      return m_id ^ (m_name?.GetHashCode() ?? 0) ^ m_valid.GetHashCode();
    }
  }

  class ObjDataExample
  {
    private string m_name;
    private DataExample m_data;
    private DataExample[] m_datas;

    public string Name { get => m_name; set => m_name = value; }
    public DataExample Data { get => m_data; set => m_data = value; }
    public DataExample[] Datas { get => m_datas; set => m_datas = value; }

    public override bool Equals(object obj)
    {
      ObjDataExample other = obj as ObjDataExample;
      if (other != null)
      {
        if (m_name == null)
        {
          if (other.m_name != null)
          {
            return false;
          }
        }
        else if (m_name.Equals(other.m_name))
        {
          if (m_data == null)
          {
            if (other.m_data != null)
            {
              return false;
            }
          }
          else if (m_data.Equals(other.m_data))
          {
            if (m_datas == null)
            {
              if (other.m_datas != null)
              {
                return false;
              }
            }
            else if (other.m_datas != null)
            {
              if (m_datas.Length == other.m_datas.Length)
              {
                for (int i = 0; i < m_datas.Length; ++i)
                {
                  if (!m_datas[i].Equals(other.m_datas[i]))
                  {
                    return false;
                  }
                }

                return true;
              }
            }
          }
        }
      }
      return false;
    }

    public override int GetHashCode()
    {
      int hash = (m_name?.GetHashCode() ?? 0) ^ (m_data?.GetHashCode() ?? 0);
      if (m_datas != null)
      {
        hash ^= m_datas.Length;
        for (int i = 0; i < m_datas.Length; ++i)
        {
          hash ^= (m_datas[i]?.GetHashCode() ?? 0);
        }
      }

      return hash;
    }
  }

  class DateTimeObjectExample
  {
    private string m_name;
    private DateTime m_time;
    private DateTime[] m_times;

    public string Name { get => m_name; set => m_name = value; }
    public DateTime Time { get => m_time; set => m_time = value; }
    public DateTime[] Times { get => m_times; set => m_times = value; }

    public override bool Equals(object obj)
    {
      DateTimeObjectExample other = obj as DateTimeObjectExample;
      if (other != null)
      {
        if (m_name == null)
        {
          if (other.m_name != null)
          {
            return false;
          }
        }
        else
        {
          if (!m_name.Equals(other.m_name))
          {
            return false;
          }
        }

        if (!m_time.Equals(other.m_time))
        {
          return false;
        }

        if (m_times == null)
        {
          if (other.m_times != null)
          {
            return false;
          }
        }
        else if (other.m_times == null)
        {
          return false;
        }
        else
        {
          if (m_times.Length == other.m_times.Length)
          {
            for (int i = 0; i < m_times.Length; ++i)
            {
              if (!m_times[i].Equals(other.m_times[i]))
              {
                return false;
              }
            }
            return true;
          }
        }
        return true;
      }
      return false;
    }

    public override int GetHashCode()
    {
      int hash = (m_name?.GetHashCode() ?? 0) ^ m_time.GetHashCode();
      if (m_times != null)
      {
        hash ^= m_times.Length;
        for (int i = 0; i < m_times.Length; ++i)
        {
          hash ^= m_times[i].GetHashCode();
        }
      }

      return hash;
    }
  }

  class HydralizeExample : IHydralize
  {
    public byte m_id;
    public UInt16 m_value;
    public UInt32 m_uuid;
    public int m_count;
    public byte[] m_aFrames;
    public char m_char;
    public char[] m_letters;

    public void GetObjectData(HydralizeInfo info)
    {
      info.AddInfo("id", m_id);
      info.AddInfo("value", m_value);
      info.AddInfo("uuid", m_uuid);
      info.AddInfo("count", m_count);
      info.AddInfo("frames", m_aFrames, typeof(byte[]));
      info.AddInfo("char", m_char);
      info.AddInfo("letters", m_letters, typeof(char[]));
    }

    public void LoadObjectData(HydralizeInfo info)
    {
      if (!info.Lite)
      {
        m_id = info.Get<byte>("id");
        m_value = info.Get<UInt16>("value");
        m_uuid = info.Get<UInt32>("uuid");
        m_count = info.Get<int>("count");
        m_aFrames = info.Get<byte[]>("frames");
        m_char = info.Get<char>("char");
        m_letters = info.Get<char[]>("letters");
      }
      else
      {
        m_id = info.Get<byte>(0);
        m_value = info.Get<UInt16>(1);
        m_uuid = info.Get<UInt32>(2);
        m_count = info.Get<int>(3);
        m_aFrames = info.Get<byte[]>(4);
        m_char = info.Get<char>(5);
        m_letters = info.Get<char[]>(6);
      }
    }
  }

  class HydralizeContextExample : IHydralize
  {
    public byte m_id;
    public object m_object;

    public void GetObjectData(HydralizeInfo info)
    {
      info.AddInfo("id", m_id);
      info.AddInfo("object", m_object, typeof(object));
    }

    public void LoadObjectData(HydralizeInfo info)
    {
      if (!info.Lite)
      {
        m_id = info.Get<byte>("id");
        m_object = info.Get<object>("object");
      }
      else
      {
        m_id = info.Get<byte>(0);
      }
    }
  }

  class NullHydralizeExample : HydralizeObject
  {
    private List<int> m_listInts;
    private BaseHydralizeExample m_obj;
    private BaseHydralizeExample[] m_objs;
    private bool[] m_bools;
    private byte[] m_bytes;
    private UInt16[] m_ushorts;
    private UInt32[] m_uints;
    private UInt64[] m_ulongs;
    private Int16[] m_shorts;
    private Int32[] m_ints;
    private Int64[] m_longs;

    public List<int> ListInts { get => m_listInts; set => m_listInts = value; }
    public bool[] Bools { get => m_bools; set => m_bools = value; }
    public byte[] Bytes { get => m_bytes; set => m_bytes = value; }
    public ushort[] Ushorts { get => m_ushorts; set => m_ushorts = value; }
    public uint[] Uints { get => m_uints; set => m_uints = value; }
    public ulong[] Ulongs { get => m_ulongs; set => m_ulongs = value; }
    public short[] Shorts { get => m_shorts; set => m_shorts = value; }
    public int[] Ints { get => m_ints; set => m_ints = value; }
    public long[] Longs { get => m_longs; set => m_longs = value; }
    public BaseHydralizeExample Obj { get => m_obj; set => m_obj = value; }
    public BaseHydralizeExample[] Objs { get => m_objs; set => m_objs = value; }

    public bool Equals(NullHydralizeExample y)
    {
      return m_listInts == y.m_listInts &&
        m_bools == y.m_bools &&
        m_bytes == y.m_bytes &&
        m_ushorts == y.m_ushorts &&
        m_uints == y.m_uints &&
        m_ulongs == y.m_ulongs &&
        m_shorts == y.m_shorts &&
        m_ints == y.m_ints &&
        m_longs == y.m_longs &&
        m_obj == y.m_obj &&
        m_objs == y.m_objs;
    }

    public int GetHashCode(NullHydralizeExample obj)
    {
      return base.GetHashCode();
    }
  }

  /// <summary>
  /// A HydralizeObject derived class with a couple of strings
  /// Used for testing string Hydralizing
  /// </summary>
  class StringHydralizeExample : HydralizeObject
  {
    private string m_name;
    private string m_data;

    public string Name { get => m_name; set => m_name = value; }
    public string Data { get => m_data; set => m_data = value; }

    public StringHydralizeExample()
    {
      // Empty
    }

    public StringHydralizeExample(string name, string data)
    {
      m_name = name;
      m_data = data;
    }

    public override bool Equals(object other)
    {
      StringHydralizeExample stringOther = other as StringHydralizeExample;
      if (stringOther != null)
      {
        bool result = m_name?.Equals(stringOther.m_name) == stringOther.m_name?.Equals(m_name);
        result &= m_data?.Equals(stringOther.m_data) == stringOther.m_data?.Equals(m_name);
        return result;
      }
      return false;
    }

    public override int GetHashCode()
    {
      int hash = m_name?.GetHashCode() ?? 0;
      hash ^= m_data?.GetHashCode() ?? 0;
      return hash;
    }
  }

  class BaseHydralizeExample : HydralizeObject, IEqualityComparer<BaseHydralizeExample>
  {
    private UInt64 m_baseNum;

    public ulong BaseNum { get => m_baseNum; set => m_baseNum = value; }

    public bool Equals(BaseHydralizeExample x, BaseHydralizeExample y)
    {
      return x.BaseNum == y.BaseNum;
    }

    public override bool Equals(object other)
    {
      BaseHydralizeExample baseOther = other as BaseHydralizeExample;
      if (baseOther != null)
      {
        return m_baseNum == baseOther.m_baseNum;
      }

      return false;
    }

    public override int GetHashCode()
    {
      return (int)m_baseNum;
    }

    public int GetHashCode(BaseHydralizeExample obj)
    {
      return base.GetHashCode();
    }

    public BaseHydralizeExample()
    {
      // Empty
    }
  }

  class HydralizeExampleObject : BaseHydralizeExample
  {
    private int m_count;
    private bool m_happy;
    private UInt32 m_time;
    private string m_name;
    private int[] m_aIds;
    private char m_letter;
    private char[] m_letters;

    [NonHydralizeableField]
    private object m_dontSetMe;

    public HydralizeExampleObject()
    {
      // Empty
    }

    public int Count { get => m_count; set => m_count = value; }
    public bool Happy { get => m_happy; set => m_happy = value; }
    public uint Time { get => m_time; set => m_time = value; }
    public string Name { get => m_name; set => m_name = value; }
    public int[] Ids { get => m_aIds; set => m_aIds = value; }
    public object DontSetMe { get => m_dontSetMe; set => m_dontSetMe = value; }
    public char Letter { get => m_letter; set => m_letter = value; }
    public char[] Letters { get => m_letters; set => m_letters = value; }
  }

  class HydralizeListObject : BaseHydralizeExample
  {
    private List<int> m_ids = new List<int>();

    public List<int> Ids { get => m_ids; set => m_ids = value; }

    public HydralizeListObject()
    {
      // Empty
    }
  }

  class NestedExampleObject : BaseHydralizeExample
  {
    string m_name;
    HydralizeExample m_example;
    HydralizeListObject m_list;

    public string Name { get => m_name; set => m_name = value; }
    public HydralizeExample Example { get => m_example; set => m_example = value; }
    public HydralizeListObject ListObject { get => m_list; set => m_list = value; }

    public NestedExampleObject()
    {
      // Empty
    }

    public NestedExampleObject(string name, HydralizeExample example, HydralizeListObject list)
    {
      m_name = name;
      m_example = example;
      m_list = list;
    }
  }

  class PolyExampleObject : BaseHydralizeExample
  {
    string m_name;
    BaseHydralizeExample m_example;

    public PolyExampleObject()
    {
      // Empty
    }

    public PolyExampleObject(string name, BaseHydralizeExample example)
    {
      m_name = name;
      m_example = example;
    }

    public string Name { get => m_name; set => m_name = value; }
    public BaseHydralizeExample Example { get => m_example; set => m_example = value; }
  }

  class PolyArrayExample : BaseHydralizeExample
  {
    BaseHydralizeExample[] m_objs;

    public BaseHydralizeExample[] Objs { get => m_objs; set => m_objs = value; }

    public PolyArrayExample()
    {
      // Empty
    }

    public PolyArrayExample(BaseHydralizeExample[] objs)
    {
      m_objs = objs;
    }
  }

  class PolyListObject : BaseHydralizeExample
  {
    List<BaseHydralizeExample> m_objs;

    public List<BaseHydralizeExample> Objs { get => m_objs; set => m_objs = value; }

    public PolyListObject()
    {
      // Empty
    }

    public PolyListObject(List<BaseHydralizeExample> example)
    {
      m_objs = example;
    }
  }

  class ObjectArrayObject
  {
    object[] m_values;

    public object[] Values { get => m_values; set => m_values = value; }
  }

  class CustomInterfaceObject : IHydralize
  {
    private string name;
    private bool set;
    private int length;
    private string location;

    public string Name { get => name; set => name = value; }
    public bool Set { get => set; set => set = value; }
    public int Length { get => length; set => length = value; }
    public string Location { get => location; set => location = value; }

    public void GetObjectData(HydralizeInfo info)
    {
      info.AddInfo("name", name);
      info.AddInfo("location", location);
    }

    public void LoadObjectData(HydralizeInfo info)
    {
      name = info.Get<string>("name", 0);
      set = !string.IsNullOrEmpty(name);
      length = name?.Length ?? 0;
      location = info.Get<string>("location", 1);
    }
  }

  class InterfaceHolderObject
  {
    private int id;
    private CustomInterfaceObject data;

    public int Id { get => id; set => id = value; }
    public CustomInterfaceObject Data { get => data; set => data = value; }
  }

  class GeneralInterfaceHolderObject
  {
    private int id;
    private object data;

    public int Id { get => id; set => id = value; }
    public object Data { get => data; set => data = value; }
  }

  class GenericTestObject<T>
  {
    private T m_value;

    public T Value { get => m_value; set => m_value = value; }
  }

  class GenericDoubleObject<T1,T2>
  {
    private T1 m_id;
    private T2 m_value;

    public T1 Id { get => m_id; set => m_id = value; }
    public T2 Value { get => m_value; set => m_value = value; }
  }

  class NestedGenerics
  {
    private GenericDoubleObject<int, GenericTestObject<string>> m_set;

    public GenericDoubleObject<int, GenericTestObject<string>> Set { get => m_set; set => m_set = value; }
  }

  class NestedGenericsEnum
  {
    private GenericDoubleObject<Digits, GenericTestObject<TestEnum>> m_set;

    public GenericDoubleObject<Digits, GenericTestObject<TestEnum>> Set { get => m_set; set => m_set = value; }
  }

  struct IntVector
  {
    public int X;
    public int Y;
    public int Z;
  }
}
