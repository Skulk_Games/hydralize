#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using System.Reflection;
using Skulk.Hydralize;

namespace Skulk.Test
{
  public enum TestEnum
  {
    FIRST,
    SECOND,
    THIRD
  }

  [TestFixture]
  public class TestHydralizer
  {
    private static Random ms_rand = new Random(DateTime.Now.Millisecond);

    /// <summary>
    /// Test some of the Hydralizer functionality for adding and clearing indexed assemblies.
    /// </summary>
    [Test]
    public void TestAssemblyIndexing()
    {
      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(1, typeof(HydralizeExample).Assembly));
      Assert.IsTrue(Hydralizer.DefaultContext.GetGroup(1, out Type[] types));
      Assert.AreEqual(typeof(HydralizeExample).Assembly.GetTypes(), types);

      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.DefaultContext.GetGroup(0, out types));
      Assert.AreEqual(typeof(Hydralizer).Assembly.GetTypes(), types);
      Assert.IsFalse(Hydralizer.DefaultContext.GetGroup(1, out types));
    }

    /// <summary>
    /// Test calling the Put method with an invalid Enum typecode.
    /// </summary>
    [Test]
    public void TestInvalidEnumPut()
    {
      byte[] array = new byte[255];
      int offset = 0;
      TestEnum testEnum = TestEnum.FIRST;
      TestEnum[] testEnums = new TestEnum[] { TestEnum.THIRD, TestEnum.SECOND, TestEnum.FIRST };

      Assert.Throws(typeof(ArgumentException), () => Hydralizer.Put(array, testEnum, HydraTypeCodes.ENUM, ref offset, true));
      Assert.Throws(typeof(ArgumentException), () => Hydralizer.Put(array, testEnums, HydraTypeCodes.ARRAY_ENUM, ref offset, true));
    }

    /// <summary>
    /// Test the Hydralizer.Put method that allows a HydralizeData object to be put directly.
    /// </summary>
    [Test]
    public void TestPutHydralizeData()
    {
      byte[] raw = new byte[16];
      HydralizeData data = new HydralizeData(true);
      int offset = 0;
      Hydralizer.Put(raw, data, ref offset, true);
      data = new HydralizeData(52);
      Hydralizer.Put(raw, data, ref offset, true);

      offset = 0;
      data = Hydralizer.Get(raw, ref offset);
      Assert.AreEqual(HydraTypeCodes.BOOL, data.HydraTypeCode);
      Assert.IsTrue((bool)data.Data);
      data = Hydralizer.Get(raw, ref offset);
      Assert.AreEqual(HydraTypeCodes.INT32, data.HydraTypeCode);
      Assert.AreEqual(52, data.Data);
    }

    [Test]
    public void TestObjectHydralizing()
    {
      HydralizeExample ex = new HydralizeExample();
      ex.m_id = 52;
      ex.m_value = 1;
      ex.m_uuid = 123456;
      ex.m_count = 101;
      ex.m_aFrames = new byte[] { 1, 2, 3, 9, 10, 11, 12 };
      ex.m_char = 'a';
      ex.m_letters = new char[] { 'a', 'b', 'c' };
      byte[] aBytes = Hydralizer.Hydralize(ex);
      Console.WriteLine("Hydralized length: {0}", aBytes.Length);
      HydralizeExample ex2 = new HydralizeExample();
      int index = 0;
      Hydralizer.Dehydralize(ex2, aBytes, ref index);

      Assert.AreEqual(ex.m_id, ex2.m_id);
      Assert.AreEqual(ex.m_value, ex2.m_value);
      Assert.AreEqual(ex.m_uuid, ex2.m_uuid);
      Assert.AreEqual(ex.m_count, ex2.m_count);
      Assert.AreEqual(ex.m_aFrames.Length, ex2.m_aFrames.Length);
      Assert.IsTrue(ex2.m_aFrames.SequenceEqual(ex.m_aFrames));
      Assert.AreEqual(ex.m_char, ex2.m_char);
      Assert.AreEqual(ex.m_letters.Length, ex2.m_letters.Length);
      Assert.IsTrue(ex2.m_letters.SequenceEqual(ex.m_letters));
    }

    [Test]
    public void TestObjectLiteHydralizing()
    {
      HydralizeExample ex = new HydralizeExample();
      ex.m_id = 52;
      ex.m_value = 1;
      ex.m_uuid = 123456;
      ex.m_count = 101;
      ex.m_aFrames = new byte[] { 1, 2, 3, 9, 10, 11, 12 };
      ex.m_char = 'a';
      ex.m_letters = new char[] { 'a', 'b', 'c' };
      byte[] aBytes = Hydralizer.Hydralize(ex, true);
      Console.WriteLine("Hydralized length: {0}", aBytes.Length);
      HydralizeExample ex2 = new HydralizeExample();
      int index = 0;
      Hydralizer.Dehydralize(ex2, aBytes, ref index);

      Assert.AreEqual(ex.m_id, ex2.m_id);
      Assert.AreEqual(ex.m_value, ex2.m_value);
      Assert.AreEqual(ex.m_uuid, ex2.m_uuid);
      Assert.AreEqual(ex.m_count, ex2.m_count);
      Assert.AreEqual(ex.m_aFrames.Length, ex2.m_aFrames.Length);
      Assert.IsTrue(ex2.m_aFrames.SequenceEqual(ex.m_aFrames));
      Assert.AreEqual(ex.m_char, ex2.m_char);
      Assert.AreEqual(ex.m_letters.Length, ex2.m_letters.Length);
      Assert.IsTrue(ex2.m_letters.SequenceEqual(ex.m_letters));
    }

    [Test]
    public void TestHydralizeByte()
    {
      HydralizeType<byte>(0);
      HydralizeType<byte>((byte)ms_rand.Next(0, 256));
      HydralizeType<byte>(255);

      // Test direct hydralizing
      HydralizeTypeDirect<byte>(0);
      HydralizeTypeDirect<byte>((byte)ms_rand.Next(0, 256));
      HydralizeTypeDirect<byte>(255);
    }

    [Test]
    public void TestHydralizeUInt16()
    {
      HydralizeType<UInt16>(UInt16.MinValue);
      HydralizeType<UInt16>((UInt16)ms_rand.Next(UInt16.MinValue, UInt16.MaxValue));
      HydralizeType<UInt16>(UInt16.MaxValue);
    }

    [Test]
    public void TestHydralizeChar()
    {
      HydralizeType<char>(char.MinValue);
      HydralizeType<char>((char)ms_rand.Next(char.MinValue, char.MaxValue));
      HydralizeType<char>(char.MaxValue);
    }

    [Test]
    public void TestHydralizeUInt32()
    {
      HydralizeType<UInt32>(UInt32.MinValue);
      HydralizeType<UInt32>((UInt32)ms_rand.Next(int.MinValue, int.MaxValue));
      HydralizeType<UInt32>(UInt32.MaxValue);
    }

    [Test]
    public void TestHydralizeUInt64()
    {
      HydralizeType<UInt64>(UInt64.MinValue);
      HydralizeType<UInt64>((UInt64)ms_rand.Next(int.MinValue, int.MaxValue));
      HydralizeType<UInt64>(UInt64.MaxValue);
    }

    [Test]
    public void TestHydralizeInt8()
    {
      HydralizeType<sbyte>(sbyte.MinValue);
      HydralizeType<sbyte>((sbyte)ms_rand.Next(sbyte.MinValue, sbyte.MaxValue));
      HydralizeType<sbyte>(sbyte.MaxValue);
    }

    [Test]
    public void TestHydralizeInt16()
    {
      HydralizeType<Int16>(Int16.MinValue);
      HydralizeType<Int16>((Int16)ms_rand.Next(Int16.MinValue, Int16.MaxValue));
      HydralizeType<Int16>(Int16.MaxValue);
    }

    [Test]
    public void TestHydralizeInt32()
    {
      HydralizeType<Int32>(Int32.MinValue);
      HydralizeType<Int32>((Int32)ms_rand.Next(Int32.MinValue, Int32.MaxValue));
      HydralizeType<Int32>(Int32.MaxValue);
    }

    [Test]
    public void TestHydralizeInt64()
    {
      HydralizeType<Int64>(Int64.MinValue);
      HydralizeType<Int64>((Int64)ms_rand.Next(Int32.MinValue, Int32.MaxValue));
      HydralizeType<Int64>(Int64.MaxValue);
    }

    [Test]
    public void TestHydralizeString()
    {
      HydralizeType<string>("");
      HydralizeType<string>("abcdef");
      HydralizeType<string>("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab");
      HydralizeType<string>(null);
    }

    [Test]
    public void TestHydralizeBool()
    {
      HydralizeType<bool>(false);
      HydralizeType<bool>(true);
    }

    [Test]
    public void TestHydralizeFloat()
    {
      HydralizeType<float>(float.MinValue);
      HydralizeType<float>((float)ms_rand.Next(Int32.MinValue, Int32.MaxValue));
      HydralizeType<float>(float.MaxValue);
    }

    [Test]
    public void TestHydralizeDouble()
    {
      HydralizeType<double>(double.MinValue);
      HydralizeType<double>((double)ms_rand.Next(Int32.MinValue, Int32.MaxValue));
      HydralizeType<double>(double.MaxValue);
    }

    [Test]
    public void TestHydralizeEnum()
    {
      HydralizeType<TestEnum>(TestEnum.FIRST);
      HydralizeType<TestEnum>(TestEnum.THIRD);
    }

    [Test]
    public void TestHydralizeEnumObject()
    {
      EnumsTestClass objEnum = new EnumsTestClass()
      {
        EnumVal = TestEnum.THIRD,
        EnumVals = new TestEnum[] { TestEnum.THIRD, TestEnum.SECOND, TestEnum.FIRST }
      };

      byte[] bytes = Hydralizer.Hydralize(objEnum);
      EnumsTestClass objEnumLoad = new EnumsTestClass();
      int offset = 0;
      Hydralizer.Dehydralize(objEnumLoad, bytes, ref offset);

      Assert.AreEqual(objEnum.EnumVal, objEnumLoad.EnumVal);
      Assert.AreEqual(objEnum.EnumVals.Length, objEnumLoad.EnumVals.Length);
      for (int i = 0; i < objEnum.EnumVals.Length; ++i)
      {
        Assert.AreEqual(objEnum.EnumVals[i], objEnumLoad.EnumVals[i]);
      }
    }

    [Test]
    public void TestHydralizeableObject()
    {
      HydralizeExampleObject exObj = new HydralizeExampleObject();
      exObj.BaseNum = 52;
      exObj.Count = 1;
      exObj.Happy = true;
      exObj.Ids = new int[] { 1, 2, 3 };
      exObj.Name = "joe";
      exObj.Time = 12345;
      exObj.Letter = 'a';
      exObj.Letters = new char[] { 'a', 'b', 'c' };
      HydralizeInfo info = new HydralizeInfo();
      HydralizeInfo resultInfo = new HydralizeInfo();
      HydralizeExampleObject result = new HydralizeExampleObject();

      info.AddInfo("obj", exObj, false);
      byte[] aHydralized = info.Hydralize();
      int offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result = resultInfo.GetObj<HydralizeExampleObject>("obj");

      Assert.AreEqual(exObj.Count, result.Count);
      Assert.AreEqual(exObj.DontSetMe, result.DontSetMe);
      Assert.AreEqual(exObj.Happy, result.Happy);
      Assert.AreEqual(exObj.Ids.Length, result.Ids.Length);
      Assert.IsTrue(result.Ids.SequenceEqual(exObj.Ids));
      Assert.IsTrue(exObj.Name.Equals(result.Name));
      Assert.AreEqual(exObj.Time, result.Time);
      Assert.AreEqual(exObj.BaseNum, result.BaseNum);
      Assert.AreEqual(exObj.Letter, result.Letter);
      Assert.AreEqual(exObj.Letters.Length, result.Letters.Length);
      Assert.IsTrue(result.Letters.SequenceEqual(exObj.Letters));

      // Test lite mode
      info = new HydralizeInfo(true);
      info.AddInfo("obj", exObj, true);
      aHydralized = info.Hydralize();
      offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result = resultInfo.GetObj<HydralizeExampleObject>("obj", 0);

      Assert.AreEqual(exObj.Count, result.Count);
      Assert.AreEqual(exObj.DontSetMe, result.DontSetMe);
      Assert.AreEqual(exObj.Happy, result.Happy);
      Assert.AreEqual(exObj.Ids.Length, result.Ids.Length);
      Assert.IsTrue(result.Ids.SequenceEqual(exObj.Ids));
      Assert.IsTrue(exObj.Name.Equals(result.Name));
      Assert.AreEqual(exObj.Time, result.Time);
      Assert.AreEqual(exObj.BaseNum, result.BaseNum);
      Assert.AreEqual(exObj.Letter, result.Letter);
      Assert.AreEqual(exObj.Letters.Length, result.Letters.Length);
      Assert.IsTrue(result.Letters.SequenceEqual(exObj.Letters));
    }

    [Test]
    public void TestHydralizeableInterfaceObject()
    {
      HydralizeExampleObject exObj = new HydralizeExampleObject();
      exObj.BaseNum = 52;
      exObj.Count = 1;
      exObj.Happy = true;
      exObj.Ids = new int[] { 1, 2, 3 };
      exObj.Name = "joe";
      exObj.Time = 12345;
      HydralizeInfo info = new HydralizeInfo();
      HydralizeInfo resultInfo = new HydralizeInfo();
      HydralizeExampleObject result = new HydralizeExampleObject();

      info.AddInfo("obj", exObj, false);
      byte[] aHydralized = info.Hydralize();
      int offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result = resultInfo.GetObj<HydralizeExampleObject>("obj");

      Assert.AreEqual(exObj.Count, result.Count);
      Assert.AreEqual(exObj.DontSetMe, result.DontSetMe);
      Assert.AreEqual(exObj.Happy, result.Happy);
      Assert.AreEqual(exObj.Ids.Length, result.Ids.Length);
      Assert.IsTrue(result.Ids.SequenceEqual(exObj.Ids));
      Assert.IsTrue(exObj.Name.Equals(result.Name));
      Assert.AreEqual(exObj.Time, result.Time);
      Assert.AreEqual(exObj.BaseNum, result.BaseNum);

      // Test lite mode
      info = new HydralizeInfo(true);
      info.AddInfo("obj", exObj, true);
      aHydralized = info.Hydralize();
      offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result = resultInfo.GetObj<HydralizeExampleObject>("obj", 0);

      Assert.AreEqual(exObj.Count, result.Count);
      Assert.AreEqual(exObj.DontSetMe, result.DontSetMe);
      Assert.AreEqual(exObj.Happy, result.Happy);
      Assert.AreEqual(exObj.Ids.Length, result.Ids.Length);
      Assert.IsTrue(result.Ids.SequenceEqual(exObj.Ids));
      Assert.IsTrue(exObj.Name.Equals(result.Name));
      Assert.AreEqual(exObj.Time, result.Time);
      Assert.AreEqual(exObj.BaseNum, result.BaseNum);
    }

    [Test]
    public void TestHydralizeObject()
    {
      HydralizeInfo info = new HydralizeInfo();
      HydralizeInfo resultInfo = new HydralizeInfo();
      HydralizeExample ex = new HydralizeExample();
      ex.m_count = 1;
      ex.m_id = 2;
      ex.m_uuid = 3;
      ex.m_value = 108;
      ex.m_aFrames = new byte[] { 1, 2, 3 };
      ex.m_char = 'a';
      ex.m_letters = new char[] { 'a', 'b', 'c' };
      HydralizeExample result = new HydralizeExample();

      info.AddInfo("value", ex, false);

      byte[] aHydralized = info.Hydralize();
      int offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result = resultInfo.GetObj<HydralizeExample>("value");

      Assert.AreEqual(ex.m_id, result.m_id);
      Assert.AreEqual(ex.m_value, result.m_value);
      Assert.AreEqual(ex.m_uuid, result.m_uuid);
      Assert.AreEqual(ex.m_count, result.m_count);
      Assert.AreEqual(ex.m_aFrames.Length, result.m_aFrames.Length);
      Assert.IsTrue(result.m_aFrames.SequenceEqual(ex.m_aFrames));
      Assert.AreEqual(ex.m_char, result.m_char);
      Assert.AreEqual(ex.m_letters.Length, result.m_letters.Length);
      Assert.IsTrue(result.m_letters.SequenceEqual(ex.m_letters));

      // Test lite mode
      info = new HydralizeInfo(true);
      info.AddInfo("value", ex, true);
      aHydralized = info.Hydralize();
      offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result = resultInfo.GetObj<HydralizeExample>(0);

      Assert.AreEqual(ex.m_id, result.m_id);
      Assert.AreEqual(ex.m_value, result.m_value);
      Assert.AreEqual(ex.m_uuid, result.m_uuid);
      Assert.AreEqual(ex.m_count, result.m_count);
      Assert.AreEqual(ex.m_aFrames.Length, result.m_aFrames.Length);
      Assert.IsTrue(result.m_aFrames.SequenceEqual(ex.m_aFrames));
      Assert.AreEqual(ex.m_char, result.m_char);
      Assert.AreEqual(ex.m_letters.Length, result.m_letters.Length);
      Assert.IsTrue(result.m_letters.SequenceEqual(ex.m_letters));
    }

    [Test]
    public void TestHydralizePrimitiveObjectArray()
    {
      object[] vals = new object[]
      {
        "Hello",
        3,
        true,
        1000,
        "chickens",
        4.7,
        1.3f
      };

      byte[] raw = Hydralizer.Hydralize(vals);
      int offset = 0;
      object resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      Assert.AreEqual(typeof(object[]), resultObj.GetType());
      object[] results = resultObj as object[];
      Assert.AreEqual(vals.Length, results.Length);
      for (int i = 0; i < results.Length; ++i)
      {
        Assert.AreEqual(vals[i], results[i]);
      }
    }

    /// <summary>
    /// Test hydralizing an object that contains an object array with an assortment of direct values.
    /// </summary>
    [Test]
    public void TestHydralizeObjectArrayObject()
    {
      Hydralizer.AddAllReferencedAssemblies(typeof(TestHydralizer).Assembly);
      ObjectArrayObject obj = new ObjectArrayObject()
      {
        Values = new object[]
        {
          1,
          "Hi",
          13.5,
          true
        }
      };

      byte[] raw = Hydralizer.Hydralize(obj, true, false);
      ObjectArrayObject result = new ObjectArrayObject();
      int offset = 0;
      Hydralizer.Dehydralize(result, raw, ref offset);

      Assert.AreEqual(obj.Values.Length, result.Values.Length);
      for (int i = 0; i < obj.Values.Length; ++i)
      {
        Assert.AreEqual(obj.Values[i], result.Values[i]);
      }

      // Now test with an actual object mixed in
      obj = new ObjectArrayObject()
      {
        Values = new object[]
        {
          1,
          "Hi",
          new DataExample()
          {
            Id = 0,
            Name = "Example",
            Valid = true
          },
          13.5,
          true
        }
      };

      raw = Hydralizer.Hydralize(obj, true, true);
      offset = 0;
      result = new ObjectArrayObject();
      Hydralizer.Dehydralize(result, raw, ref offset);

      Assert.AreEqual(obj.Values.Length, result.Values.Length);
      for (int i = 0; i < obj.Values.Length; ++i)
      {
        Assert.AreEqual(obj.Values[i], result.Values[i]);
      }
    }

    [Test]
    public void TestHydralizeAssortedObjectArray()
    {
      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(0, typeof(Hydralizer).Assembly));
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(1, typeof(ComparableTestClass).Assembly));

      object[] vals = new object[]
      {
        "Hello",
        3,
        true,
        new ComparableTestClass() { Value1 = 1, Value2 = 2 }
      };

      byte[] raw = Hydralizer.Hydralize(vals, true, true);
      int offset = 0;
      object resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      Assert.AreEqual(typeof(object[]), resultObj.GetType());
      object[] results = resultObj as object[];
      Assert.AreEqual(vals.Length, results.Length);
      for (int i = 0; i < results.Length; ++i)
      {
        if (i == 3)
        {
          var obj = results[i] as ComparableTestClass;
          Assert.IsNotNull(obj);
          Assert.AreEqual(1, obj.Value1);
          Assert.AreEqual(2, obj.Value2);
        }
        else
        {
          Assert.AreEqual(vals[i], results[i]);
        }
      }
    }

    [Test]
    public void TestHydralizeObjectArray()
    {
      HydralizeInfo info = new HydralizeInfo();
      HydralizeInfo resultInfo = new HydralizeInfo();
      HydralizeExample[] exs = new HydralizeExample[5];
      HydralizeExample[] result = new HydralizeExample[5];

      for (int i = 0; i < exs.Length; i++)
      {
        result[i] = new HydralizeExample();
        exs[i] = new HydralizeExample();
        exs[i].m_count = 1;
        exs[i].m_id = 2;
        exs[i].m_uuid = 3;
        exs[i].m_value = 108;
        exs[i].m_aFrames = new byte[] { 1, 2, 3 };
        exs[i].m_char = 'a';
        exs[i].m_letters = new char[] { 'a', 'b', 'c' };
      }

      info.AddInfo("value", exs, false);

      byte[] aHydralized = info.Hydralize();
      int offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result = resultInfo.GetObjArray<HydralizeExample>("value");

      for (int i = 0; i < result.Length; i++)
      {
        Assert.AreEqual(exs[i].m_id, result[i].m_id);
        Assert.AreEqual(exs[i].m_value, result[i].m_value);
        Assert.AreEqual(exs[i].m_uuid, result[i].m_uuid);
        Assert.AreEqual(exs[i].m_count, result[i].m_count);
        Assert.AreEqual(exs[i].m_aFrames.Length, result[i].m_aFrames.Length);
        Assert.IsTrue(result[i].m_aFrames.SequenceEqual(exs[i].m_aFrames));
        Assert.AreEqual(exs[i].m_char, result[i].m_char);
        Assert.AreEqual(exs[i].m_letters.Length, result[i].m_letters.Length);
        Assert.IsTrue(result[i].m_letters.SequenceEqual(exs[i].m_letters));
      }

      // Test lite mode
      info = new HydralizeInfo(true);
      info.AddInfo("value", exs, true);
      aHydralized = info.Hydralize();
      offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result = resultInfo.GetObjArray<HydralizeExample>(0);

      for (int i = 0; i < result.Length; i++)
      {
        Assert.AreEqual(exs[i].m_id, result[i].m_id);
        Assert.AreEqual(exs[i].m_value, result[i].m_value);
        Assert.AreEqual(exs[i].m_uuid, result[i].m_uuid);
        Assert.AreEqual(exs[i].m_count, result[i].m_count);
        Assert.AreEqual(exs[i].m_aFrames.Length, result[i].m_aFrames.Length);
        Assert.IsTrue(result[i].m_aFrames.SequenceEqual(exs[i].m_aFrames));
        Assert.AreEqual(exs[i].m_char, result[i].m_char);
        Assert.AreEqual(exs[i].m_letters.Length, result[i].m_letters.Length);
        Assert.IsTrue(result[i].m_letters.SequenceEqual(exs[i].m_letters));
      }
    }

    [Test]
    public void TestHydralizeUInt8Array()
    {
      byte[] buff = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 3, 6, 8, 4, 15, 28, 43 };
      List<byte[]> result = HydralizeArray<byte[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeUInt16Array()
    {
      UInt16[] buff = new UInt16[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 3, 6, 8, 4, 15, 28, 43, 15000, 22000, 65000 };
      List<UInt16[]> result = HydralizeArray<UInt16[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));

      // What if the array is null?
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("value", null, typeof(ushort[]));
      byte[] raw = info.Hydralize();
      HydralizeInfo resultInfo = new HydralizeInfo();
      int offset = 0;
      resultInfo.Dehydralize(raw, ref offset);
      ushort[] resultArr = resultInfo.Get<ushort[]>("value", 0);
      Assert.IsNull(resultArr);
    }

    [Test]
    public void TestHydralizeCharArray()
    {
      char[] buff = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
      List<char[]> result = HydralizeArray<char[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeUInt32Array()
    {
      UInt32[] buff = new UInt32[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 3, 6, 8, 4, 15, 28, 43, 15000, 22000, 65000, 1600000, UInt32.MaxValue };
      List<UInt32[]> result = HydralizeArray<UInt32[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeUInt64Array()
    {
      UInt64[] buff = new UInt64[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 3, 6, 8, 4, 15, 28, 43, 15000, 22000, 65000, 1600000, UInt32.MaxValue, UInt64.MaxValue };
      List<UInt64[]> result = HydralizeArray<UInt64[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeInt8Array()
    {
      sbyte[] buff = new sbyte[] { 1, 2, -3, 4, 5, 6, 7, 8, 9, -10, 3, 6, 8, 4, 15, 28, 43 };
      List<sbyte[]> result = HydralizeArray<sbyte[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeInt16Array()
    {
      Int16[] buff = new Int16[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 3, 6, 8, 4, 15, 28, 43, Int16.MaxValue, Int16.MinValue };
      List<Int16[]> result = HydralizeArray<Int16[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeInt32Array()
    {
      Int32[] buff = new Int32[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 3, 6, 8, 4, 15, 28, 43, Int16.MaxValue, Int16.MinValue, Int32.MinValue, Int32.MaxValue };
      List<Int32[]> result = HydralizeArray<Int32[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeInt64Array()
    {
      Int64[] buff = new Int64[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 3, 6, 8, 4, 15, 28, 43, Int16.MaxValue, Int16.MinValue, Int32.MinValue, Int32.MaxValue, Int64.MinValue, Int64.MaxValue };
      List<Int64[]> result = HydralizeArray<Int64[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeStringArray()
    {
      string[] buff = new string[] { "Test", "More", "stuff", "loooooooooooooooooooooooooooooongs123", null, "last" };
      List<string[]> result = HydralizeArray<string[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeBoolArray()
    {
      bool[] buff = new bool[] { true, false, true, true, true, false };
      List<bool[]> result = HydralizeArray<bool[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeFloatArray()
    {
      float[] buff = new float[] { float.MinValue, float.MaxValue, 52, 3.45f, 2.01f };
      List<float[]> result = HydralizeArray<float[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeDoubleArray()
    {
      double[] buff = new double[] { float.MinValue, float.MaxValue, 52, 3.45f, 2.01f, double.MinValue, double.MaxValue };
      List<double[]> result = HydralizeArray<double[]>(buff);
      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(buff));
      Assert.IsTrue(result[1].SequenceEqual(buff));
    }

    [Test]
    public void TestHydralizeEnumArray()
    {
      TestEnum[] enums = { TestEnum.FIRST, TestEnum.SECOND, TestEnum.THIRD, TestEnum.SECOND };
      List<TestEnum[]> result = HydralizeArray(enums);

      Assert.AreEqual(2, result.Count);
      Assert.IsTrue(result[0].SequenceEqual(enums));
      Assert.IsTrue(result[1].SequenceEqual(enums));

      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("enums", enums);
      string xml = info.ToXml();
      Console.WriteLine(xml);

      HydralizeInfo resultInfo = new HydralizeInfo();
      resultInfo.FromXml(xml);
      TestEnum[] resultEnums = resultInfo.Get<TestEnum[]>("enums");
      Assert.AreEqual(enums.Length, resultEnums.Length);
      Assert.IsTrue(enums.SequenceEqual(resultEnums));
    }

    [Test]
    public void TestHydralizeList()
    {
      List<byte> bytes = new List<byte>();
      bytes.AddRange(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 });
      HydralizeList(bytes);

      List<int> ints = new List<int>();
      ints.AddRange(new int[] { 100, 200, 300, 400, 500, 600 });
      HydralizeList(ints);

      List<BaseHydralizeExample> objs = new List<BaseHydralizeExample>();
      objs.Add(new BaseHydralizeExample());
      objs.Add(new BaseHydralizeExample());
      objs[0].BaseNum = 52;
      objs[1].BaseNum = 3;
      HydralizeObjectList(objs);

      HydralizeListObject listObj = new HydralizeListObject();
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("value", listObj, false);
      byte[] aHydralized = info.Hydralize();
      HydralizeInfo result = new HydralizeInfo();
      int offset = 0;
      result.Dehydralize(aHydralized, ref offset);
      HydralizeListObject listObjResult = result.GetObj<HydralizeListObject>("value");

      Assert.IsNotNull(listObjResult);
      Assert.IsNotNull(listObjResult.Ids);
      Assert.AreEqual(listObj.Ids.Count, listObjResult.Ids.Count);

      listObj.Ids.AddRange(new int[] { 1, 345, 100000, 87 });
      info.ClearInfo();
      offset = 0;
      info.AddInfo("value", listObj);
      aHydralized = info.Hydralize();
      result.Dehydralize(aHydralized, ref offset);
      listObjResult = result.GetObj<HydralizeListObject>("value");

      Assert.IsNotNull(listObjResult);
      Assert.IsNotNull(listObjResult.Ids);
      Assert.AreEqual(listObj.Ids.Count, listObjResult.Ids.Count);
      for (int i = 0; i < listObj.Ids.Count; ++i)
      {
        Assert.AreEqual(listObj.Ids[i], listObjResult.Ids[i]);
      }
    }

    [Test]
    public void TestHydralizeStringList()
    {
      List<string> stringList = new List<string>() { "hello", "goodbye" };
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("strList", stringList);

      byte[] bytes = info.Hydralize();
      HydralizeInfo result = new HydralizeInfo();
      int offset = 0;
      result.Dehydralize(bytes, ref offset);
      List<string> strResult = result.GetObj<List<string>>("strList", 0);

      Assert.AreEqual(stringList.Count, strResult.Count);
      for (int i = 0; i < strResult.Count; ++i)
      {
        Assert.AreEqual(stringList[i], strResult[i]);
      }

      info = new HydralizeInfo(true);
      info.AddInfo("strList", stringList);
      bytes = info.Hydralize();
      result = new HydralizeInfo();
      offset = 0;
      result.Dehydralize(bytes, ref offset);

      strResult = result.GetObj<List<string>>("strList", 0);

      Assert.AreEqual(stringList.Count, strResult.Count);
      for (int i = 0; i < strResult.Count; ++i)
      {
        Assert.AreEqual(stringList[i], strResult[i]);
      }
    }

    [Test]
    public void TestHydralizeDictionary()
    {
      Hydralizer.DefaultContext.Clear();
      Hydralizer.AddSupportedAssembly(1, typeof(Dictionary<,>).Assembly);
      Hydralizer.AddAllReferencedAssemblies(typeof(TestHydralizer).Assembly);
      Hydralizer.AddAllReferencedAssemblies(typeof(Hydralizer).Assembly);

      Dictionary<int, string> nameDict = new Dictionary<int, string>();
      for (int i = 0; i < 10; ++i)
      {
        nameDict.Add(i, "value_" + i.ToString());
      }
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("dictionary", nameDict);
      byte[] raw = info.Hydralize();

      HydralizeInfo result = new HydralizeInfo();
      int offset = 0;
      result.Dehydralize(raw, ref offset);
      Dictionary<int, string> resultDict = new Dictionary<int, string>();
      object obj = resultDict;
      Hydralizer.LoadObjectInfo(ref obj, (HydralizeInfo)result.GetRaw("dictionary"));

      Assert.AreEqual(nameDict.Count, resultDict.Count);
      for (int i = 0; i < nameDict.Count; ++i)
      {
        Assert.AreEqual(nameDict[i], resultDict[i]);
      }

      DictionaryTestClass dictObj = new DictionaryTestClass();
      dictObj.Names = new Dictionary<int, string>();
      dictObj.Actives = new Dictionary<int, bool>();
      for (int i = 0; i < 10; ++i)
      {
        dictObj.Names.Add(i, "name_" + i.ToString());
        dictObj.Actives.Add(i, i % 2 == 0);
      }

      raw = Hydralizer.Hydralize(dictObj, false, false);
      DictionaryTestClass resultObj = new DictionaryTestClass();
      offset = 0;
      Hydralizer.Dehydralize(resultObj, raw, ref offset);
      Assert.AreEqual(dictObj, resultObj);

      raw = Hydralizer.Hydralize(dictObj, true, true);
      offset = 0;
      resultObj = (DictionaryTestClass)Hydralizer.Dehydralize(raw, ref offset);
      Assert.AreEqual(dictObj, resultObj);

      // Nested dictionaries
      Dictionary<int, Dictionary<int, string>> msgLookup = new Dictionary<int, Dictionary<int, string>>();
      msgLookup.Add(0, new Dictionary<int, string>());
      msgLookup[0].Add(0, "0.0");
      msgLookup[0].Add(1, "0.1");
      msgLookup.Add(1, new Dictionary<int, string>());
      msgLookup[1].Add(0, "1.0");
      msgLookup[1].Add(1, "1.1");

      raw = Hydralizer.Hydralize(msgLookup, true, true);
      offset = 0;
      obj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(obj);
      Dictionary<int, Dictionary<int, string>> resultLookup = obj as Dictionary<int, Dictionary<int, string>>;
      Assert.IsNotNull(resultLookup);
      Assert.AreEqual(msgLookup.Count, resultLookup.Count);
    }

    [Test]
    public void TestIndexedHydralization()
    {
      Hydralizer.AddAllReferencedAssemblies(typeof(TestHydralizer).Assembly);

      HydralizeExample example = new HydralizeExample();
      example.m_id = 3;
      example.m_count = 1;
      example.m_aFrames = new byte[] { 2, 4, 5 };
      example.m_uuid = 333;
      example.m_value = 3000;
      HydralizeListObject listObject = new HydralizeListObject();
      listObject.BaseNum = 3;
      listObject.Ids.AddRange(new int[] { 1, 2, 3, 4, 5 });
      NestedExampleObject nested = new NestedExampleObject("test", example, listObject);

      byte[] aRaw = Hydralizer.Hydralize(nested, true, true);
      int offset = 0;
      object hydraObj = Hydralizer.Dehydralize(aRaw, ref offset);
      Assert.IsNotNull(hydraObj);
      NestedExampleObject result = hydraObj as NestedExampleObject;
      Assert.IsNotNull(result);

      Assert.AreEqual("test", result.Name);
      Assert.IsNotNull(result.Example);
      Assert.IsNotNull(result.ListObject);
    }

    [Test]
    public void TestIndexedHydralizationPolymorphism()
    {
      Hydralizer.AddAllReferencedAssemblies(typeof(TestHydralizer).Assembly);

      HydralizeExampleObject example = new HydralizeExampleObject();
      example.Count = 3;
      example.BaseNum = 1;
      example.DontSetMe = new byte[] { 2, 4, 5 };
      example.Happy = true;
      example.Ids = new int[] { 3000 };
      example.Name = "fred";
      example.Time = 32;
      HydralizeListObject listObject = new HydralizeListObject();
      listObject.BaseNum = 3;
      listObject.Ids.AddRange(new int[] { 1, 2, 3, 4, 5 });
      PolyExampleObject poly = new PolyExampleObject("test", example);

      byte[] aRaw = Hydralizer.Hydralize(poly, true, true);
      int offset = 0;
      object hydraObj = Hydralizer.Dehydralize(aRaw, ref offset);
      Assert.IsNotNull(hydraObj);
      PolyExampleObject result = hydraObj as PolyExampleObject;
      Assert.IsNotNull(result);

      Assert.AreEqual(poly.BaseNum, result.BaseNum);
      Assert.AreEqual(poly.Name, result.Name);
      Assert.IsNotNull(result.Example);
      HydralizeExampleObject exObj = result.Example as HydralizeExampleObject;
      Assert.IsNotNull(exObj);
      Assert.AreEqual(example.BaseNum, exObj.BaseNum);
      Assert.AreEqual(example.Count, exObj.Count);
      Assert.AreEqual(example.Happy, exObj.Happy);
      Assert.IsNull(exObj.DontSetMe);
      Assert.AreEqual(example.Ids.Length, exObj.Ids.Length);
      Assert.AreEqual(example.Name, exObj.Name);
      Assert.AreEqual(example.Time, exObj.Time);
    }

    [Test]
    public void TestIndexedHydraArray()
    {
      Hydralizer.AddAllReferencedAssemblies(typeof(TestHydralizer).Assembly);

      BaseHydralizeExample[] objs = new BaseHydralizeExample[3];
      HydralizeExampleObject exObj = new HydralizeExampleObject();
      exObj.BaseNum = 1;
      exObj.Count = 2;
      exObj.Happy = true;
      exObj.Ids = new int[] { 1, 2, 9 };
      exObj.Name = "exObj";
      exObj.Time = 3;

      HydralizeListObject listObj = new HydralizeListObject();
      listObj.BaseNum = 2;
      listObj.Ids.AddRange(new int[] { 2, 4, 6, 4, 2 });

      PolyExampleObject poly = new PolyExampleObject("joe", new BaseHydralizeExample());
      poly.Example.BaseNum = 3;

      objs[0] = exObj;
      objs[1] = listObj;
      objs[2] = poly;

      PolyArrayExample arrayExample = new PolyArrayExample(objs);
      arrayExample.BaseNum = 52;

      byte[] aRaw = Hydralizer.Hydralize(arrayExample, true, true);
      int offset = 0;
      object hydraObj = Hydralizer.Dehydralize(aRaw, ref offset);
      PolyArrayExample result = hydraObj as PolyArrayExample;
      Assert.IsNotNull(result);
      Assert.AreEqual(arrayExample.BaseNum, result.BaseNum);
      Assert.IsNotNull(result.Objs);
      Assert.AreEqual(objs.Length, result.Objs.Length);
      HydralizeExampleObject resultObj = result.Objs[0] as HydralizeExampleObject;
      Assert.IsNotNull(resultObj);
      HydralizeListObject resultList = result.Objs[1] as HydralizeListObject;
      Assert.IsNotNull(resultList);
      PolyExampleObject resultPoly = result.Objs[2] as PolyExampleObject;
      Assert.IsNotNull(resultPoly);
    }

    [Test]
    public void TestIndexedHydraList()
    {
      Hydralizer.AddAllReferencedAssemblies(typeof(TestHydralizer).Assembly);

      List<BaseHydralizeExample> objs = new List<BaseHydralizeExample>();
      HydralizeExampleObject exObj = new HydralizeExampleObject();
      exObj.BaseNum = 1;
      exObj.Count = 2;
      exObj.Happy = true;
      exObj.Ids = new int[] { 1, 2, 9 };
      exObj.Name = "exObj";
      exObj.Time = 3;

      HydralizeListObject listObj = new HydralizeListObject();
      listObj.BaseNum = 2;
      listObj.Ids.AddRange(new int[] { 2, 4, 6, 4, 2 });

      PolyExampleObject poly = new PolyExampleObject("joe", new BaseHydralizeExample());
      poly.Example.BaseNum = 3;

      objs.Add(exObj);
      objs.Add(listObj);
      objs.Add(poly);

      PolyListObject listExample = new PolyListObject(objs);
      listExample.BaseNum = 52;

      byte[] aRaw = Hydralizer.Hydralize(listExample, true, true);
      int offset = 0;
      object hydraObj = Hydralizer.Dehydralize(aRaw, ref offset);
      PolyListObject result = hydraObj as PolyListObject;
      Assert.IsNotNull(result);
      Assert.AreEqual(listExample.BaseNum, result.BaseNum);
      Assert.IsNotNull(result.Objs);
      Assert.AreEqual(objs.Count, result.Objs.Count);
      HydralizeExampleObject resultObj = result.Objs[0] as HydralizeExampleObject;
      Assert.IsNotNull(resultObj);
      HydralizeListObject resultList = result.Objs[1] as HydralizeListObject;
      Assert.IsNotNull(resultList);
      PolyExampleObject resultPoly = result.Objs[2] as PolyExampleObject;
      Assert.IsNotNull(resultPoly);
    }

    [Test]
    public void TestHydralizingNullValues()
    {
      Hydralizer.AddAllReferencedAssemblies(typeof(TestHydralizer).Assembly);

      // Test simple manual null value
      HydralizeExample ex = new HydralizeExample();
      ex.m_aFrames = null;

      HydralizeInfo hInfo = new HydralizeInfo(false);
      ex.GetObjectData(hInfo);
      byte[] bytes = hInfo.Hydralize();

      HydralizeInfo resultInfo = new HydralizeInfo();
      int offset = 0;
      resultInfo.Dehydralize(bytes, ref offset);
      HydralizeExample ex2 = new HydralizeExample();
      ex2.m_aFrames = new byte[] { 1, 2, 3 };
      ex2.LoadObjectData(resultInfo);

      Assert.IsNull(ex.m_aFrames);
      Assert.AreEqual(ex.m_aFrames, ex2.m_aFrames);

      // Test null values in a class deriving from HydralizeObject
      NullHydralizeExample nullEx = new NullHydralizeExample();
      hInfo = new HydralizeInfo();
      nullEx.GetObjectData(hInfo);
      bytes = hInfo.Hydralize();
      NullHydralizeExample nullEx2 = new NullHydralizeExample()
      {
        Bools = new bool[] { true },
        Bytes = new byte[] { 1 },
        Ints = new int[] { 1 },
        ListInts = new List<int>(),
        Longs = new long[] { 1 },
        Shorts = new short[] { 1 },
        Uints = new uint[] { 1 },
        Ulongs = new ulong[] { 1 },
        Ushorts = new ushort[] { 1 },
        Obj = new BaseHydralizeExample(),
        Objs = new BaseHydralizeExample[] { new BaseHydralizeExample() }
      };
      resultInfo = new HydralizeInfo();
      offset = 0;
      resultInfo.Dehydralize(bytes, ref offset);
      nullEx2.LoadObjectData(resultInfo);

      Assert.IsTrue(nullEx.Equals(nullEx2));

      // Test lite
      hInfo = new HydralizeInfo(true);
      nullEx.GetObjectData(hInfo);
      bytes = hInfo.Hydralize();
      resultInfo = new HydralizeInfo();
      offset = 0;
      resultInfo.Dehydralize(bytes, ref offset);
      nullEx2 = new NullHydralizeExample()
      {
        Bools = new bool[] { true },
        Bytes = new byte[] { 1 },
        Ints = new int[] { 1 },
        ListInts = new List<int>(),
        Longs = new long[] { 1 },
        Shorts = new short[] { 1 },
        Uints = new uint[] { 1 },
        Ulongs = new ulong[] { 1 },
        Ushorts = new ushort[] { 1 },
        Obj = new BaseHydralizeExample(),
        Objs = new BaseHydralizeExample[] { new BaseHydralizeExample() }
      };
      nullEx2.LoadObjectData(resultInfo);

      Assert.IsTrue(nullEx.Equals(nullEx2));

      // Test Indexed
      bytes = Hydralizer.Hydralize(nullEx, true, true);
      offset = 0;
      object hydralizeObj = Hydralizer.Dehydralize(bytes, ref offset);
      Assert.IsNotNull(hydralizeObj);
      NullHydralizeExample resultObj = hydralizeObj as NullHydralizeExample;
      Assert.IsNotNull(resultObj);
      Assert.IsTrue(nullEx.Equals(resultObj));

      // Test strings
      StringHydralizeExample strEx = new StringHydralizeExample("Fred", null);
      bytes = Hydralizer.Hydralize(strEx, true, false);
      StringHydralizeExample strResult = new StringHydralizeExample("name", "data");
      offset = 0;
      Hydralizer.Dehydralize(strResult, bytes, ref offset);
      Assert.IsTrue(strEx.Equals(strResult));
    }

    [Test]
    public void TestNonInterfaceHydralization()
    {
      DataExample data = new DataExample()
      {
        Id = 52,
        Name = "Bob",
        Valid = true
      };

      byte[] aRaw = Hydralizer.Hydralize(data);
      int offset = 0;
      DataExample result = new DataExample();
      Hydralizer.Dehydralize(result, aRaw, ref offset);

      Assert.IsNotNull(result);
      Assert.AreEqual(data.Id, result.Id);
      Assert.AreEqual(data.Name, result.Name);
      Assert.AreEqual(data.Valid, result.Valid);

      ObjDataExample objData = new ObjDataExample()
      {
        Name = "Joe",
        Data = new DataExample() { Name = "Bob", Id = 1, Valid = true },
        Datas = new DataExample[]
        {
          new DataExample() {Name = "Fred", Id = 2, Valid = true },
          new DataExample() {Name = "Bill", Id = 3, Valid = true }
        }
      };

      aRaw = Hydralizer.Hydralize(objData);
      ObjDataExample objResult = new ObjDataExample();
      offset = 0;
      Hydralizer.Dehydralize(objResult, aRaw, ref offset);
      Assert.IsNotNull(objResult);
      Assert.IsTrue(objData.Equals(objResult));
    }

    [Test]
    public void TestHydralizingDateTime()
    {
      HydralizeType<DateTime>(new DateTime(1980, 1, 6));

      DateTimeObjectExample ex = new DateTimeObjectExample()
      {
        Name = "Test",
        Time = new DateTime(2000, 1, 1),
        Times = new DateTime[]
        {
          new DateTime(2001,1,1),
          new DateTime(2002,1,1),
          new DateTime(3000,1,1)
        }
      };

      byte[] aRaw = Hydralizer.Hydralize(ex);
      DateTimeObjectExample result = new DateTimeObjectExample();
      int offset = 0;
      Hydralizer.Dehydralize(result, aRaw, ref offset);
      Assert.IsTrue(ex.Equals(result));

      aRaw = Hydralizer.Hydralize(ex, true);
      result = new DateTimeObjectExample();
      offset = 0;
      Hydralizer.Dehydralize(result, aRaw, ref offset);
      Assert.IsTrue(ex.Equals(result));

      aRaw = Hydralizer.Hydralize(ex, true, true);
      result = new DateTimeObjectExample();
      offset = 0;
      Hydralizer.Dehydralize(result, aRaw, ref offset);
      Assert.IsTrue(ex.Equals(result));
    }

    [Test]
    public void TestHydralizeIComparable()
    {
      IComparable raw1 = "Hello";
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("raw", raw1);
      byte[] raw = info.Hydralize();
      HydralizeInfo resultInfo = new HydralizeInfo();
      int offset = 0;
      resultInfo.Dehydralize(raw, ref offset);
      IComparable result1 = resultInfo.Get<IComparable>("raw");
      Assert.AreEqual(raw1, result1);

      ComparableTestClass comparable = new ComparableTestClass()
      {
        Value1 = "Hello",
        Value2 = 52
      };

      raw = Hydralizer.Hydralize(comparable);
      ComparableTestClass comparableResult = new ComparableTestClass();
      offset = 0;
      Hydralizer.Dehydralize(comparableResult, raw, ref offset);

      Assert.AreEqual(comparable.Value1, comparableResult.Value1);
      Assert.AreEqual(comparable.Value2, comparableResult.Value2);
    }

    [Test]
    public void TestHydralizeParameterDictionary()
    {
      Hydralizer.AddSupportedAssembly(0, typeof(Hydralizer).Assembly);
      Hydralizer.AddSupportedAssembly(1, typeof(ParameterDictionaryTestClass).Assembly);
      Hydralizer.DefaultContext.AddGroup(2, Hydralizer.CoreTypes);

      ParameterDictionaryTestClass paramDictionary = new ParameterDictionaryTestClass();
      paramDictionary.Parameters.Add("Name", "TestParameterDictionary");
      paramDictionary.Parameters.Add("Time", DateTime.UtcNow);
      paramDictionary.Parameters.Add("Count", 5);
      paramDictionary.Parameters.Add("Array", new int[] { 3, 2, 1, 0 });
      paramDictionary.Parameters.Add("Null", null);

      byte[] raw = Hydralizer.Hydralize(paramDictionary);
      int offset = 0;
      ParameterDictionaryTestClass resultDictionary = new ParameterDictionaryTestClass();
      Hydralizer.Dehydralize(resultDictionary, raw, ref offset);

      Assert.AreEqual(paramDictionary.Parameters.Count, resultDictionary.Parameters.Count);
      foreach (KeyValuePair<string, object> pair in paramDictionary.Parameters)
      {
        Assert.IsTrue(resultDictionary.Parameters.ContainsKey(pair.Key));
        Assert.AreEqual(pair.Value, resultDictionary.Parameters[pair.Key]);
      }

      paramDictionary.Parameters.Add("Object", new ComparableTestClass() { Value1 = 1, Value2 = "hello" });
      raw = Hydralizer.Hydralize(paramDictionary, true, true);
      offset = 0;
      resultDictionary = Hydralizer.Dehydralize(raw, ref offset) as ParameterDictionaryTestClass;
      Assert.IsNotNull(resultDictionary);
      Assert.AreEqual(paramDictionary.Parameters.Count, resultDictionary.Parameters.Count);
      foreach (KeyValuePair<string, object> pair in paramDictionary.Parameters)
      {
        Assert.IsTrue(resultDictionary.Parameters.ContainsKey(pair.Key));
        if (pair.Key.Equals("Object"))
        {
          ComparableTestClass resultComparable = resultDictionary.Parameters[pair.Key] as ComparableTestClass;
          Assert.IsNotNull(resultComparable);
          Assert.AreEqual(1, resultComparable.Value1);
          Assert.AreEqual("hello", resultComparable.Value2);
        }
        else
        {
          Assert.AreEqual(pair.Value, resultDictionary.Parameters[pair.Key]);
        }
      }
    }

    /// <summary>
    /// Tests that a mixture of primitive and object types can be in an object dictionary.
    /// </summary>
    /// <remarks>It does need to be indexed otherwise deserializing objects within the dictionary will fail.</remarks>
    [Test]
    public void TestHydralizeMixedParameterDictionary()
    {
      Hydralizer.DefaultContext.Clear();
      Hydralizer.AddSupportedAssembly(0, typeof(Hydralizer).Assembly);
      Hydralizer.AddSupportedAssembly(1, typeof(ParameterDictionaryTestClass).Assembly);
      Hydralizer.DefaultContext.AddGroup(2, Hydralizer.CoreTypes);

      ParameterDictionaryTestClass paramDictionary = new ParameterDictionaryTestClass();
      paramDictionary.Parameters.Add("Name", "TestParameterDictionary");
      paramDictionary.Parameters.Add("Time", DateTime.UtcNow);
      paramDictionary.Parameters.Add("Count", 5);
      paramDictionary.Parameters.Add("Object", new ComparableTestClass() { Value1 = 1, Value2 = "hello" });
      paramDictionary.Parameters.Add("Array", new float[] { 1.1f, 2.2f, 3.3f });

      byte[] raw = Hydralizer.Hydralize(paramDictionary, true, true);
      int offset = 0;
      ParameterDictionaryTestClass resultDictionary = new ParameterDictionaryTestClass();
      Hydralizer.Dehydralize(resultDictionary, raw, ref offset);

      Assert.AreEqual(paramDictionary.Parameters.Count, resultDictionary.Parameters.Count);
      foreach (KeyValuePair<string, object> pair in paramDictionary.Parameters)
      {
        Assert.IsTrue(resultDictionary.Parameters.ContainsKey(pair.Key));
        Assert.AreEqual(pair.Value, resultDictionary.Parameters[pair.Key]);
      }
    }

    /// <summary>
    /// Tests that a mixture of primitive and object types can be in an object list.
    /// </summary>
    /// <remarks>It does need to be indexed otherwise deserializing objects within the list will fail.</remarks>
    [Test]
    public void TestHydralizeMixedParameterList()
    {
      Hydralizer.AddSupportedAssembly(0, typeof(Hydralizer).Assembly);
      Hydralizer.AddSupportedAssembly(1, typeof(ParameterListTestClass).Assembly);
      Hydralizer.DefaultContext.AddGroup(2, Hydralizer.CoreTypes);

      ParameterListTestClass paramList = new ParameterListTestClass();
      paramList.Parameters.Add("TestParameterDictionary");
      paramList.Parameters.Add(DateTime.UtcNow);
      paramList.Parameters.Add(5);
      paramList.Parameters.Add(new ComparableTestClass() { Value1 = 1, Value2 = "hello" });

      byte[] raw = Hydralizer.Hydralize(paramList, true, true);
      int offset = 0;
      ParameterListTestClass resultList = new ParameterListTestClass();
      Hydralizer.Dehydralize(resultList, raw, ref offset);

      Assert.AreEqual(paramList.Parameters.Count, resultList.Parameters.Count);
      for (int i = 0; i < paramList.Parameters.Count; ++i)
      {
        Assert.AreEqual(paramList.Parameters[i], resultList.Parameters[i]);
      }
    }

    /// <summary>
    /// Test that using a custom index context works correctly
    /// </summary>
    [Test]
    public void TestIndexContext()
    {
      IndexContext context = new IndexContext();
      Assert.IsTrue(context.AddAssembly(100, typeof(HydralizeExample).Assembly));

      // Create and add a manual group
      Type[] manualTypes = new Type[]
      {
        typeof(List<>),
        typeof(Dictionary<,>),
        null
      };
      Assert.IsTrue(context.AddGroup(101, manualTypes));

      var pairs = context.GetPairs();

      // Expect Hydralizer assembly with ID 0 and custom added ones at 100 and 101
      Assert.AreEqual(3, pairs.Count);
      for (int i = 0; i < pairs.Count; ++i)
      {
        switch (pairs[i].Key)
        {
          case 0:
          {
            Type[] types = typeof(Hydralizer).Assembly.GetTypes();
            Assert.AreEqual(types.Length, pairs[i].Value.Length);
            for (int j = 0; j < pairs[i].Value.Length; ++j)
            {
              Assert.AreEqual(types[j], pairs[i].Value[j], "Group: {1} type mismatch at index: {0}", j, pairs[i].Key);
            }
            break;
          }

          case 100:
          {
            Type[] types = typeof(HydralizeExample).Assembly.GetTypes();
            Assert.AreEqual(types.Length, pairs[i].Value.Length);
            for (int j = 0; j < pairs[i].Value.Length; ++j)
            {
              Assert.AreEqual(types[j], pairs[i].Value[j], "Group: {1} type mismatch at index: {0}", j, pairs[i].Key);
            }
            break;
          }

          case 101:
          {
            Assert.AreEqual(manualTypes.Length, pairs[i].Value.Length);
            for (int j = 0; j < pairs[i].Value.Length; ++j)
            {
              Assert.AreEqual(manualTypes[j], pairs[i].Value[j], "Group: {1} type mismatch at index: {0}", j, pairs[i].Key);
            }
            break;
          }

          default:
          {
            Assert.Fail("Unexpected group ID: {0} in index context", pairs[i].Key);
            break;
          }
        }
      }

      HydralizeExample example = new HydralizeExample()
      {
        m_id = 1,
        m_aFrames = new byte[] { 2, 4, 6 },
        m_char = 'a',
        m_count = 5,
        m_letters = new char[] { 'a', 'b', 'c', 'd' },
        m_uuid = 52,
        m_value = 12
      };

      // Check that indexed hydralizing and dehydralizing works
      byte[] buffer = Hydralizer.Hydralize(example, true, true, context);
      int offset = 0;

      // Check that the custom defined context is used
      HydralizeInfo info = new HydralizeInfo(context);
      info.Dehydralize(buffer, ref offset);
      Assert.IsTrue(info.Indexed);
      Assert.AreEqual(100, info.AssemblyId);

      // Check that the object gets dehydralized correctly
      object result = Hydralizer.Dehydralize(info);
      Assert.IsNotNull(result);
      HydralizeExample exampleResult = result as HydralizeExample;
      Assert.IsNotNull(exampleResult);
      Assert.AreEqual(example.m_id, exampleResult.m_id);
      Assert.AreEqual(example.m_char, exampleResult.m_char);
      Assert.AreEqual(example.m_count, exampleResult.m_count);
      Assert.AreEqual(example.m_uuid, exampleResult.m_uuid);
      Assert.AreEqual(example.m_value, exampleResult.m_value);
      Assert.AreEqual(example.m_aFrames.Length, exampleResult.m_aFrames.Length);
      for (int i = 0; i < example.m_aFrames.Length; ++i)
      {
        Assert.AreEqual(example.m_aFrames[i], exampleResult.m_aFrames[i]);
      }
      Assert.AreEqual(example.m_letters.Length, exampleResult.m_letters.Length);
      for (int i = 0; i < example.m_letters.Length; ++i)
      {
        Assert.AreEqual(example.m_letters[i], exampleResult.m_letters[i]);
      }

      // Check that indexing works with the manual group
      List<int> ints = new List<int>();
      ints.Add(1);
      Assert.IsTrue(context.Index(ints, out byte groupId, out int typeIndex));
      Assert.AreEqual(101, groupId);
      Assert.AreEqual(0, typeIndex);

      Dictionary<int, string> dict = new Dictionary<int, string>();
      Assert.IsTrue(context.Index(dict, out groupId, out typeIndex));
      Assert.AreEqual(101, groupId);
      Assert.AreEqual(1, typeIndex);
    }

    /// <summary>
    /// Tests the Hydralization/Dehydralization of nested objects implementing the IHydralize interface.
    /// </summary>
    [Test]
    public void TestNestedInterfaceObject()
    {
      Hydralizer.AddSupportedAssembly(1, typeof(CustomInterfaceObject).Assembly);

      CustomInterfaceObject customObj = new CustomInterfaceObject()
      {
        Name = "bob",
        Set = false,
        Length = 0,
        Location = "home"
      };

      InterfaceHolderObject holderObject = new InterfaceHolderObject()
      {
        Id = 1,
        Data = customObj
      };

      byte[] raw = Hydralizer.Hydralize(holderObject, true, false);

      InterfaceHolderObject result = new InterfaceHolderObject();
      int offset = 0;
      Hydralizer.Dehydralize(result, raw, ref offset);

      // Check the values to confirm the IHydralize interface was used
      Assert.AreEqual(holderObject.Id, result.Id);
      Assert.AreEqual(customObj.Name, result.Data.Name);
      Assert.IsTrue(result.Data.Set);
      Assert.AreEqual(result.Data.Name.Length, result.Data.Length);
      Assert.AreEqual(customObj.Location, result.Data.Location);

      // Now use indexing on a general case
      GeneralInterfaceHolderObject generalHolder = new GeneralInterfaceHolderObject()
      {
        Id = 2,
        Data = customObj
      };

      raw = Hydralizer.Hydralize(generalHolder, true, true);
      offset = 0;
      GeneralInterfaceHolderObject generalResult = Hydralizer.Dehydralize(raw, ref offset) as GeneralInterfaceHolderObject;

      // Check the result and make sure the IHydralize interface was used
      Assert.IsNotNull(generalResult);
      Assert.AreEqual(generalHolder.Id, generalResult.Id);
      CustomInterfaceObject resultData = generalResult.Data as CustomInterfaceObject;
      Assert.IsNotNull(resultData);
      Assert.AreEqual(customObj.Name, resultData.Name);
      Assert.IsTrue(resultData.Set);
      Assert.AreEqual(customObj.Name.Length, resultData.Length);
      Assert.AreEqual(customObj.Location, resultData.Location);
    }

    /// <summary>
    /// Tests the ability to hydralize generic classes
    /// </summary>
    [Test]
    public void HydralizeGenericObjects()
    {
      GenericTestObject<int> genericInt = new GenericTestObject<int>()
      {
        Value = 123
      };

      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(1, typeof(GenericTestObject<>).Assembly));

      byte[] raw = Hydralizer.Hydralize(genericInt, true, true);
      int offset = 0;
      object resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      GenericTestObject<int> resultInt = resultObj as GenericTestObject<int>;
      Assert.IsNotNull(resultInt);
      Assert.AreEqual(genericInt.Value, resultInt.Value);

      GenericDoubleObject<string, bool> genericDouble = new GenericDoubleObject<string, bool>()
      {
        Id = "IsHungry",
        Value = true
      };

      raw = Hydralizer.Hydralize(genericDouble, true, true);
      offset = 0;
      resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      GenericDoubleObject<string, bool> resultDouble = resultObj as GenericDoubleObject<string, bool>;
      Assert.IsNotNull(resultDouble);
      Assert.AreEqual(genericDouble.Id, resultDouble.Id);
      Assert.AreEqual(genericDouble.Value, resultDouble.Value);

      // Try nested generics
      NestedGenerics nested = new NestedGenerics()
      {
        Set = new GenericDoubleObject<int, GenericTestObject<string>>()
        {
          Id = 52,
          Value = new GenericTestObject<string>()
          {
            Value = "Bob"
          }
        }
      };

      raw = Hydralizer.Hydralize(nested, true, true);
      offset = 0;
      resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      NestedGenerics resultNest = resultObj as NestedGenerics;
      Assert.IsNotNull(resultNest);
      Assert.IsNotNull(resultNest.Set);
      Assert.AreEqual(resultNest.Set.Id, nested.Set.Id);
      Assert.AreEqual(resultNest.Set.Value.Value, nested.Set.Value.Value);
    }

    /// <summary>
    /// Tests the ability to hydralize generic classes that use enum or struct type arguments.
    /// </summary>
    [Test]
    public void HydralizeGenericWithEnumsOrStruct()
    {
      GenericTestObject<TestEnum> genericEnum = new GenericTestObject<TestEnum>()
      {
        Value = TestEnum.SECOND
      };

      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(1, typeof(GenericTestObject<>).Assembly));

      byte[] raw = Hydralizer.Hydralize(genericEnum, true, true);
      int offset = 0;
      object resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      GenericTestObject<TestEnum> resultInt = resultObj as GenericTestObject<TestEnum>;
      Assert.IsNotNull(resultInt);
      Assert.AreEqual(genericEnum.Value, resultInt.Value);

      GenericDoubleObject<Digits, TestEnum> genericDouble = new GenericDoubleObject<Digits, TestEnum>()
      {
        Id = Digits.Eight,
        Value = TestEnum.THIRD
      };

      raw = Hydralizer.Hydralize(genericDouble, true, true);
      offset = 0;
      resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      GenericDoubleObject<Digits, TestEnum> resultDouble = resultObj as GenericDoubleObject<Digits, TestEnum>;
      Assert.IsNotNull(resultDouble);
      Assert.AreEqual(genericDouble.Id, resultDouble.Id);
      Assert.AreEqual(genericDouble.Value, resultDouble.Value);

      // Try nested generics
      NestedGenericsEnum nested = new NestedGenericsEnum()
      {
        Set = new GenericDoubleObject<Digits, GenericTestObject<TestEnum>>()
        {
          Id = Digits.Five,
          Value = new GenericTestObject<TestEnum>()
          {
            Value = TestEnum.FIRST
          }
        }
      };

      raw = Hydralizer.Hydralize(nested, true, true);
      offset = 0;
      resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      NestedGenericsEnum resultNest = resultObj as NestedGenericsEnum;
      Assert.IsNotNull(resultNest);
      Assert.IsNotNull(resultNest.Set);
      Assert.AreEqual(resultNest.Set.Id, nested.Set.Id);
      Assert.AreEqual(resultNest.Set.Value.Value, nested.Set.Value.Value);

      // Test with a struct
      GenericTestObject<IntVector> genericStruct = new GenericTestObject<IntVector>()
      {
        Value = new IntVector() { X = 1, Y = 2, Z = 3 }
      };

      raw = Hydralizer.Hydralize(genericStruct, true, true);
      offset = 0;
      resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      GenericTestObject<IntVector> resultStruct = resultObj as GenericTestObject<IntVector>;
      Assert.IsNotNull(resultStruct);
      Assert.AreEqual(genericStruct.Value, resultStruct.Value);
    }

    /// <summary>
    /// Tests the <see cref="Hydralizer.GetHydralizeableFields(Type)"/> method
    /// </summary>
    [Test]
    public void GetHydralizeableFields()
    {
      StringHydralizeExample example = new StringHydralizeExample();
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("obj", example);

      FieldInfo[] fields = Hydralizer.GetHydralizeableFields(example.GetType());

      HydralizeInfo objInfo = info.GetRaw("obj") as HydralizeInfo;
      Assert.AreEqual(fields.Length, objInfo.GetFields().Length);
      for (int i = 0; i < fields.Length; ++i)
      {
        Assert.AreEqual(fields[i].Name, objInfo.GetFields()[i].Key);
      }
    }

    /// <summary>
    /// Tests hydralizing a struct
    /// </summary>
    [Test]
    public void HydralizeStruct()
    {
      IntVector vec = new IntVector();
      vec.X = 1;
      vec.Y = 2;
      vec.Z = 3;

      byte[] buffer = Hydralizer.Hydralize(vec);
      int offset = 0;
      object obj = new IntVector();

      // The only trick is to pass by reference
      Hydralizer.Dehydralize(ref obj, buffer, ref offset);
      IntVector result = (IntVector)obj;

      Assert.AreEqual(vec.X, result.X);
      Assert.AreEqual(vec.Y, result.Y);
      Assert.AreEqual(vec.Z, result.Z);

      Hydralizer.DefaultContext.Clear();
      Assert.IsTrue(Hydralizer.AddSupportedAssembly(1, typeof(IntVector).Assembly));
      buffer = Hydralizer.Hydralize(vec, true, true);
      offset = 0;
      result = (IntVector)Hydralizer.Dehydralize(buffer, ref offset);

      Assert.AreEqual(vec.X, result.X);
      Assert.AreEqual(vec.Y, result.Y);
      Assert.AreEqual(vec.Z, result.Z);
    }

    /// <summary>
    /// Test hydralizing a value indirectly into another object.
    /// </summary>
    /// <typeparam name="T">The type of value to hydralize indirectly.</typeparam>
    /// <param name="value">The value to hydralize indirectly.</param>
    private void HydralizeType<T>(T value)
    {
      HydralizeInfo info = new HydralizeInfo();
      HydralizeInfo resultInfo = new HydralizeInfo();
      if (value == null)
      {
        info.AddInfo("value", value, typeof(T));
      }
      else
      {
        info.AddInfo("value", value);
      }

      byte[] aHydralized = info.Hydralize();
      int offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      Assert.AreEqual(value, resultInfo.Get<T>("value"));

      // Test lite mode
      info.Lite = true;
      aHydralized = info.Hydralize();
      offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      Assert.AreEqual(value, resultInfo.Get<T>(0));
    }

    /// <summary>
    /// Tests hydralizing a value directly as an object.
    /// </summary>
    /// <typeparam name="T">The type of the value to hydralize as an object.</typeparam>
    /// <param name="value">The value to hydralize as an object.</param>
    private void HydralizeTypeDirect<T>(T value)
    {
      int offset = 0;
      byte[] raw = Hydralizer.Hydralize(value);
      object resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      Assert.AreEqual(typeof(T), resultObj.GetType());
      Assert.AreEqual(value, (T)resultObj);

      offset = 0;
      raw = Hydralizer.Hydralize(value, true, true);
      resultObj = Hydralizer.Dehydralize(raw, ref offset);
      Assert.IsNotNull(resultObj);
      Assert.AreEqual(typeof(T), resultObj.GetType());
      Assert.AreEqual(value, (T)resultObj);
    }

    private List<T> HydralizeArray<T>(T value)
    {
      List<T> result = new List<T>();
      HydralizeInfo info = new HydralizeInfo();
      HydralizeInfo resultInfo = new HydralizeInfo();
      info.AddInfo("value", value);

      byte[] aHydralized = info.Hydralize();
      int offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result.Add(resultInfo.Get<T>("value"));

      // Test lite mode
      info.Lite = true;
      aHydralized = info.Hydralize();
      offset = 0;
      resultInfo.Dehydralize(aHydralized, ref offset);
      result.Add(resultInfo.Get<T>(0));

      return result;
    }

    private void HydralizeList<T>(List<T> list) where T : new()
    {
      List<List<T>> results = new List<List<T>>();
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("value", list);
      byte[] aHydralized = info.Hydralize();
      HydralizeInfo result = new HydralizeInfo();
      int offset = 0;
      result.Dehydralize(aHydralized, ref offset);
      results.Add(result.GetObj<List<T>>("value"));

      info.Lite = true;
      aHydralized = info.Hydralize();
      offset = 0;
      result.Dehydralize(aHydralized, ref offset);
      results.Add(result.GetObj<List<T>>(0));

      Assert.AreEqual(2, results.Count);
      Assert.IsTrue(results[0].SequenceEqual(list));
      Assert.IsTrue(results[1].SequenceEqual(list));
    }

    private void HydralizeObjectList<T>(List<T> list) where T : IHydralize, new()
    {
      List<List<T>> results = new List<List<T>>();
      HydralizeInfo info = new HydralizeInfo();
      info.AddInfo("value", list);
      byte[] aHydralized = info.Hydralize();
      HydralizeInfo result = new HydralizeInfo();
      int offset = 0;
      result.Dehydralize(aHydralized, ref offset);
      results.Add(result.GetObj<List<T>>("value"));

      info.Lite = true;
      aHydralized = info.Hydralize();
      offset = 0;
      result.Dehydralize(aHydralized, ref offset);
      results.Add(result.GetObj<List<T>>(0));

      Assert.AreEqual(2, results.Count);
      for (int i = 0; i < 2; i++)
      {
        for (int j = 0; j < results[i].Count; j++)
        {
          Assert.IsTrue(results[i][j].Equals(list[j]));
        }
      }
    }
  }
}
