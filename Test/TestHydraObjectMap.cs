#region License
// The MIT License (MIT)
// 
// Copyright © 2022 Ryan Steven Jaynes
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
// 
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Skulk.Hydralize;

namespace Skulk.Test
{
  [TestFixture]
  public class TestHydraObjectMap
  {
    [Test]
    public void TestGetMap()
    {
      PrimitivesTestClass primitivesTest = new PrimitivesTestClass();
      ObjectMap map = primitivesTest.GetObjectMap();
      Console.WriteLine("Created map with {0} fields.", map.TypeInfo.GetFields()?.Length ?? 0);
      Assert.AreEqual(10, map.TypeInfo.GetFields()?.Length);

      ArraysTestClass arraysTest = new ArraysTestClass();
      map = arraysTest.GetObjectMap();
      Assert.AreEqual(9, map.TypeInfo.GetFields()?.Length);

      ObjectsTestClass objectsTest = new ObjectsTestClass();
      map = objectsTest.GetObjectMap();
      Assert.AreEqual(2, map.TypeInfo.GetFields()?.Length);

      ObjectsArrayTestClass objectsArrayTest = new ObjectsArrayTestClass();
      map = objectsArrayTest.GetObjectMap();
      Assert.AreEqual(1, map.TypeInfo.GetFields()?.Length);

      ListTestClass listTest = new ListTestClass();
      map = listTest.GetObjectMap();
      Assert.AreEqual(4, map.TypeInfo.GetFields()?.Length);

      map = new ObjectMap(typeof(DictionaryTestClass));
      Assert.AreEqual(2, map.TypeInfo.GetFields()?.Length);

      EnumsTestClass enumsTest = new EnumsTestClass();
      map = enumsTest.GetObjectMap();
      Assert.AreEqual(2, map.TypeInfo.GetFields()?.Length);
      foreach (HydraFieldInfo fieldInfo in map.TypeInfo.GetFields())
      {
        if (!fieldInfo.FieldType.IsArray)
        {
          Assert.IsTrue(fieldInfo.FieldType.IsEnum);
        }
        else
        {
          Assert.IsTrue(fieldInfo.FieldType.GetElementType().IsEnum);
        }
      }
    }

    [Test]
    public void TestBaseMap()
    {
      Hydralizer.AddSupportedAssembly(0, typeof(ObjectMap).Assembly);

      PrimitivesTestClass primitivesTest = new PrimitivesTestClass();
      ObjectMap map = new ObjectMap(primitivesTest.GetType());
      Console.WriteLine("Created map with {0} fields.", map.TypeInfo.GetFields()?.Length ?? 0);
      Assert.AreEqual(10, map.TypeInfo.GetFields()?.Length);
      byte[] raw = Hydralizer.Hydralize(map, true, true);
      string xml = Hydralizer.ToXml(map, true, true);

      using (FileStream fout = File.Open("primObjMap.bin", FileMode.Create))
      {
        fout.Write(raw, 0, raw.Length);
      }
      using (StreamWriter sw = new StreamWriter(File.Open("primObjMap.xml", FileMode.Create)))
      {
        sw.Write(xml);
      }
      using (StreamReader sr = new StreamReader(File.Open("primObjMap.xml", FileMode.Open)))
      {
        ObjectMap primMap = new ObjectMap();
        Hydralizer.FromXml(primMap, sr.ReadToEnd());
      }
      {
        ObjectMap primMap = new ObjectMap();
        int offset = 0;
        primMap = Hydralizer.Dehydralize(raw, ref offset) as ObjectMap;
      }

      ArraysTestClass arraysTest = new ArraysTestClass();
      map = new ObjectMap(arraysTest.GetType());
      Assert.AreEqual(9, map.TypeInfo.GetFields()?.Length);

      ObjectsTestClass objectsTest = new ObjectsTestClass();
      map = new ObjectMap(objectsTest.GetType());
      Assert.AreEqual(2, map.TypeInfo.GetFields()?.Length);
      HydraFieldInfo fieldInfo = map.GetField("m_primitive.m_uData8");
      Assert.IsNotNull(fieldInfo);

      ObjectsArrayTestClass objectsArrayTest = new ObjectsArrayTestClass();
      map = new ObjectMap(objectsArrayTest.GetType());
      Assert.AreEqual(1, map.TypeInfo.GetFields()?.Length);
      fieldInfo = map.GetField("m_primitives.m_uData8");
      Assert.IsNotNull(fieldInfo);

      ListTestClass listTest = new ListTestClass();
      map = new ObjectMap(listTest.GetType());
      Assert.AreEqual(4, map.TypeInfo.GetFields()?.Length);
    }

    [Test]
    public void TestMapPaths()
    {
      ObjectMap map = new ObjectMap(typeof(PrimitivesTestClass));
      Console.WriteLine(map.Name);
      List<string> paths = map.GetPaths();
      Assert.IsTrue(paths.Count > 0);
      for (int i = 0; i < paths.Count; ++i)
      {
        HydraFieldInfo fieldInfo = map.GetField(paths[i]);
        Assert.IsNotNull(fieldInfo);
        Console.WriteLine("Entry - Name:{0} Type:{1} Element:{2}", fieldInfo.Name, fieldInfo.FieldType, fieldInfo.FieldType.GetElementType()?.TypeCode ?? HydraTypeCodes.INVALID);
      }

      map = new ObjectMap(typeof(ArraysTestClass));
      Console.WriteLine(map.Name);
      paths = map.GetPaths();
      Assert.IsTrue(paths.Count > 0);
      for (int i = 0; i < paths.Count; ++i)
      {
        HydraFieldInfo fieldInfo = map.GetField(paths[i]);
        Assert.IsNotNull(fieldInfo);
        Console.WriteLine("Entry - Name:{0} Type:{1} Element:{2}", fieldInfo.Name, fieldInfo.FieldType, fieldInfo.FieldType.GetElementType()?.TypeCode ?? HydraTypeCodes.INVALID);
      }

      map = new ObjectMap(typeof(ObjectsTestClass));
      Console.WriteLine(map.Name);
      paths = map.GetPaths();
      Assert.IsTrue(paths.Count > 0);
      for (int i = 0; i < paths.Count; ++i)
      {
        HydraFieldInfo fieldInfo = map.GetField(paths[i]);
        Assert.IsNotNull(fieldInfo);
        Console.WriteLine("Entry - Name:{0} Type:{1} Element:{2}", fieldInfo.Name, fieldInfo.FieldType, fieldInfo.FieldType.GetElementType()?.TypeCode ?? HydraTypeCodes.INVALID);
      }
    }

    [Test]
    public void TestMapAttributes()
    {
      DescriptiveTestClass descriptiveTestClass = new DescriptiveTestClass()
      {
        Id = 3,
        Name = "Test class",
        Client = 14
      };

      ObjectMap map = new ObjectMap(typeof(DescriptiveTestClass));

      Assert.AreEqual(1, map.Attributes.Count);
      byte[] raw = Hydralizer.Hydralize(map, true, true);
      int offset = 0;
      ObjectMap resultMap = (ObjectMap)Hydralizer.Dehydralize(raw, ref offset);
      Assert.AreEqual(1, resultMap.Attributes.Count);
      Assert.IsNotNull(resultMap.Attributes.Find(x => x.Name == "Description"));
      Assert.AreEqual(3, resultMap.TypeInfo.GetFields().Length);
    }

    [Test]
    public void TestSettingMapValues()
    {
      ObjectMap map = new ObjectMap(typeof(PrimitivesTestClass));
      HydralizeInfo info = new HydralizeInfo();
      PrimitivesTestClass primOrig = new PrimitivesTestClass()
      {
        UData8 = 3,
        UData16 = 1000,
        UData32 = 100000,
        UData64 = 10000000000,
        BoolData = true,
        SData8 = -3,
        SData16 = -1000,
        SData32 = -100000,
        SData64 = -5000000000
      };

      ObjectMap objMap = new ObjectMap(typeof(PrimitivesTestClass));
      HydralizeInfo objInfo = new HydralizeInfo();
      Assert.IsTrue(objMap.AddObjectInfo("m_uData8",  primOrig.UData8, ref objInfo));
      Assert.IsTrue(objMap.AddObjectInfo("m_uData16", primOrig.UData16, ref objInfo));
      Assert.IsTrue(objMap.AddObjectInfo("m_uData32", primOrig.UData32, ref objInfo));
      Assert.IsTrue(objMap.AddObjectInfo("m_uData64", primOrig.UData64, ref objInfo));
      Assert.IsTrue(objMap.AddObjectInfo("m_sData8",  primOrig.SData8, ref objInfo));
      Assert.IsTrue(objMap.AddObjectInfo("m_sData16", primOrig.SData16, ref objInfo));
      Assert.IsTrue(objMap.AddObjectInfo("m_sData32", primOrig.SData32, ref objInfo));
      Assert.IsTrue(objMap.AddObjectInfo("m_sData64", primOrig.SData64, ref objInfo));
      Assert.IsTrue(objMap.AddObjectInfo("m_boolData", primOrig.BoolData, ref objInfo));

      PrimitivesTestClass objLoad = new PrimitivesTestClass();
      objLoad.LoadObjectData(objInfo);
      Assert.IsTrue(primOrig.Equals(objLoad));

      EnumsTestClass enumTestOrig = new EnumsTestClass()
      {
        EnumVal = TestEnum.THIRD,
        EnumVals = new TestEnum[] { TestEnum.SECOND, TestEnum.THIRD }
      };
      map = new ObjectMap(typeof(EnumsTestClass));
      info = new HydralizeInfo();

      Assert.IsTrue(map.AddObjectInfo("m_enumVal", 2, ref info));
      Assert.IsTrue(map.AddObjectInfo("m_enumVals", new int[] { 1, 2 }, ref info));

      EnumsTestClass enumTestLoad = new EnumsTestClass();
      enumTestLoad.LoadObjectData(info);

      Assert.AreEqual(enumTestOrig.EnumVal, enumTestLoad.EnumVal);
      Assert.AreEqual(enumTestOrig.EnumVals.Length, enumTestLoad.EnumVals.Length);
      for (int i = 0; i < enumTestOrig.EnumVals.Length; ++i)
      {
        Assert.AreEqual(enumTestOrig.EnumVals[i], enumTestLoad.EnumVals[i]);
      }

      map = new ObjectMap(typeof(ObjectsTestClass));
      info = new HydralizeInfo();
      Assert.IsTrue(map.AddObjectInfo("m_primitive", new HydralizeInfo(), ref info));
      Assert.IsTrue(map.AddObjectInfo("m_primitive.m_uData8", 52, ref info));
      Assert.IsTrue(map.AddObjectInfo("m_primitive.m_uData16", 3000, ref info));
      Assert.IsTrue(map.AddObjectInfo("m_primitive.m_sData8", -3, ref info));
      Assert.IsTrue(map.AddObjectInfo("m_primitive.m_sData16", -3000, ref info));

      ObjectsTestClass objectsLoad = new ObjectsTestClass();
      objectsLoad.LoadObjectData(info);
      Assert.IsNotNull(objectsLoad.Primitive);
      Assert.AreEqual(52, objectsLoad.Primitive.UData8);
      Assert.AreEqual(3000, objectsLoad.Primitive.UData16);
      Assert.AreEqual(-3, objectsLoad.Primitive.SData8);
      Assert.AreEqual(-3000, objectsLoad.Primitive.SData16);
      Assert.IsNull(objectsLoad.Array);
    }
  }
}
