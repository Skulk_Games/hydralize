# Hydralize

## Overview

Hydralize is a custom serialization library used as the serializer for my Skulk.Hydra networking framework.
It's designed to be more compact than default serialization but supporting many useful features such as object serialization
without the need for any attributes or inheritance restrictions as well as polymorphism in serialization data.

## Documentation

The documentation for this project is still a work in progress. I will start a sphinx project to generate a readthedocs style user's
guide eventually.

## Copyright and Attributions

Unless otherwise specified (C) 2019 Ryan Steven Jaynes

